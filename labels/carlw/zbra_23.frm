#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#
#
#SELECT=select deptno, ltrim(to_char(vc_retprc,'$999.99')) vc_retprc from cstprq where prtnum = '@prtnum'  and cstnum = '@cstnum'
#
#DATA=@deptno~@vc_retprc~
#
^XA^DF23^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LL304;
^FO20,80^AB20,20^FDANN & HOPE^FS;
^FO20,110^AB20,20^FDDEPT.#^FS;
^FO140,110^AB20,20^FN1^FS;
^FO40,140^AB20,20^FN2^FS;

^FO300,80^AB20,20^FDANN & HOPE^FS;
^FO300,110^AB20,20^FDDEPT.#^FS;
^FO420,110^AB20,20^FN1^FS;
^FO320,140^AB20,20^FN2^FS;

^FO570,80^AB20,20^FDANN & HOPE^FS;
^FO570,110^AB20,20^FDDEPT.#^FS;
^FO690,110^AB20,20^FN1^FS;
^FO590,140^AB20,20^FN2^FS;
^PQ1,0,0,N^XZ;
