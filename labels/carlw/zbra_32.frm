#SELECT=select ltrim(to_char(vc_retprc,'999.99')) vc_retprc, ltrim(cstprt) cstprt from cstprq where prtnum = '@prtnum' and cstnum = '@cstnum'
#
#DATA=@cstprt~@vc_retprc~
#
#
#
^XA^DF32^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,10;
^FO50,80^AB20,20^FN1^FS;
^FO10,120^AB30,30^FD$^FS;
^FO40,120^AB30,30^FN2^FS;

^FO330,80^AB20,20^FN1^FS;
^FO290,120^AB30,30^FD$^FS;
^FO320,120^AB30,30^FN2^FS;

^FO610,80^AB20,20^FN1^FS;
^FO570,120^AB30,30^FD$^FS;
^FO600,120^AB30,30^FN2^FS;
^XZ;
^PQ1,0,0,N^XZ;
