# $Id: zbra_rpslh1.frm,v 1.1.1.1 2001/09/18 23:05:47 lh51sh Exp $
#
# RPS LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' srvnum, ' ' shipper, ' ' packid, ' ' traknm, ' ' carcod, ' ' dstlvl from dual
#SELECT=select '('||substr(rtrim('@traknm'),1,7)||')' srvnum, substr(rtrim('@traknm'),8,7) shipper, substr(rtrim('@traknm'),15,8) packid from dual if (@traknm)
#SELECT=select carcod, dstlvl from carxrf where carcod = '@carcod' and srvlvl = '@srvlvl'
#SELECT=select rtrim('@traknm') traknm from dual if (@traknm)
#SELECT=select '>;>8'||'@traknm' traknm from dual
#DATA=@shipper~@packid~@shipper~@packid~@carcod~@carcod~@srvnum~@traknm~@shipper~@packid~
^XA^DFrpslh1^FS;
^LH0,0;
^CF0,20^FO80,50^A0N^FDRPS Tracking ID^FS;
^CF0,20^FO420,50^A0N^FDRPS Tracking ID^FS;
^CF0,25^FO30,80^A0N^FN1^FS;
^CF0,25^FO180,80^A0N^FN2^FS;
^CF0,25^FO350,80^A0N^FN3^FS;
^CF0,25^FO500,80^A0N^FN4^FS;
^CF0,20^FO70,110^A0N^FN5^FS;
^CF0,20^FO450,110^A0N^FN6^FS;
^CF0,20^FO100,150^A0N^FDFor RPS Information Call: 1-800-782-3725^FS;
^CF0,35^FO80,560^A0N^FN7^FS;
^CF0,75^FO70,600^A0N^FDGND^FS;
^CF0,45^FO380,640^A0N^FD021^FS;
^CF0,35^FO570,220^A0N^FD08810^FS;
^CF0,25^FO300,220^A0N^FDHOAN^FS;
^BY4,2.4^FO60,250^BCN,300,N,N,N^FN8^FS;
^CF0,35^FO320,560^A0N^FN9^FS;
^CF0,35^FO520,560^A0N^FN10^FS;
^CF0,75^FO570,625^A0N^FDFedEx^FS;
^CF0,55^FO570,685^A0N^FDGround^FS;
~DGTRUCK.GRF,4352,32,01hJF80,:018hI060,:::018hI018,:018hJ06,:018hJ0180,::::018hK060,:018gX0MF98,:018gX0MF86,:::018gX0MF8180,:018gX0MF8060,:018gX0MF8018,:::018003IF03IF801FFEgG0MF8007,:018I0F03C0F01E0781EgG0MF8I0C0,:018I0F03C0F01E0781EgG0MF8I030,:018I0F03C0F01E07EgI0MF8I030,::018I0FFC00F01E01FF8gG0MF8J0C,:018I0F0F00IF8I07EgG0MF8J03,:018I0F03C0FJ0781EgG0MF8K0C0,:::018003FF0JFE007FF8gG0MF8K030,:018gX0MF8L0C,:018gX0MF8L03,:01gRFEL0MF8L03,:018gX0MF8M0F0,:018gX0MF8N0F,:018hU0F0,::018hV0F80,:018hW078,:018hX078,:018hY078:018i06:::018gX0LF8U06::::::::::::::::::::::::::::::018N0JF8gQ01JFQ06:018L03LFEgO07LFCO06::018K03NFEgM07NFCN06:018K0PF8gK01PFN06:::07iGFE:M03PFEgK07PFC,:::::N0PF8gK01PF0,:::N03NFEgM07NFC0,:O03LFEgO07LFC,::Q0JF8gQ01JF,:;
^FO100,200^XGTRUCK.GRF^FS^LRN;
^CF0,55^FO70,690^A0N^FDCOLLECT^FS;
^XZ;
