#
#SELECT=select '@lodnum' lblload, 'Device: '||'@devcod' devcod, 'Batch: '||'@lblbat' lblbathdr from dual
#
#SELECT=select 'Picks: '||count(subucc) pick_count from pckwrk where lblbat='@lblbat' and subucc is not null
#
#SELECT=select 'User ID: '||min(vc_prt_usr_id) usr_id from pckwrk where lblbat='@lblbat'
#
#DATA=@lblload~@devcod~@lblbathdr~@pick_count~@usr_id~
^XA^DFloadlbl^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^BY2,2.3,255^FO60,200^BCN,,N,N,N,N^FN1^FS;
^FO15,500^ADN,74,25^FN1^FS;
^FO15,600^ADN,74,25^FN2^FS;
^FO15,700^ADN,74,25^FN3^FS;
^FO16,800^ADN,74,25^FN4^FS;
^FO15,900^ADN,74,25^FN5^FS;
^PQ1,0,0,N^XZ;
