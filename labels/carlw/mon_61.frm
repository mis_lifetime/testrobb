#SELECT=select vc_prenum, ltrim(to_char(vc_retprc,'999.99')) vc_retprc, ltrim(vc_lblfd3) vc_lblfd3, ltrim(vc_lblfd4)
vc_lblfd4, substr(cstprt, 1, 10) cstprt from cstprq where prtnum = '@prtnum' and cstnum = '@cstnum'
#
#DATA={B,@vc_prenum,N,1|\n1,"@vc_retprc"|\n2,"@cstprt"|\n3,"@vc_lblfd3"|\n4,"@vc_lblfd4"|}\n
#
{F,61,A,N,E,110,120,"CARSON'S"|
C,64,40,0,1001,2,1,B,L,0,0,"DEPT#78",1|
C,41,40,0,1001,2,1,B,L,0,0,"$",1|
T,1,6,V,41,55,0,1001,2,1,B,L,0,0,1|
C,30,30,0,1000,1,1,B,L,0,0,"SKU",1|
T,2,10,V,30,55,0,1000,1,1,B,L,0,0,1|
T,3,15,V,20,30,0,1000,1,1,B,L,0,0,1|
T,4,15,V,10,30,0,1000,1,1,B,L,0,0,1|
}
