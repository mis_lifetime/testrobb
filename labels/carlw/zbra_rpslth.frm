# $Id: zbra_rpslth.frm,v 1.1.1.1 2001/09/18 23:05:47 lh51sh Exp $
#
# RPS LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' srvnum, ' ' shipper, ' ' packid, ' ' traknm, ' ' carcod, ' ' dstlvl from dual
#SELECT=select '('||substr(rtrim('@traknm'),1,7)||')' srvnum, substr(rtrim('@traknm'),8,7) shipper, substr(rtrim('@traknm'),15,8) packid from dual if (@traknm)
#SELECT=select carcod, dstlvl from carxrf where carcod = '@carcod' and srvlvl = '@srvlvl'
#SELECT=select rtrim('@traknm') traknm from dual if (@traknm)
#DATA=@shipper~@packid~@shipper~@packid~@carcod~@carcod~@srvnum~@traknm~@shipper~@packid~
^XA^DFrpslth^FS;
^LH0,0;
^CF0,20^FO80,50^A0N^FDRPS Tracking ID^FS;
^CF0,20^FO420,50^A0N^FDRPS Tracking ID^FS;
^CF0,25^FO30,80^A0N^FN1^FS;
^CF0,25^FO180,80^A0N^FN2^FS;
^CF0,25^FO350,80^A0N^FN3^FS;
^CF0,25^FO500,80^A0N^FN4^FS;
^CF0,20^FO70,110^A0N^FN5^FS;
^CF0,20^FO450,110^A0N^FN6^FS;
^CF0,20^FO100,150^A0N^FDFor RPS Information Call: 1-800-782-3725^FS;
^CF0,25^FO50,180^A0N^FN7^FS;
^CF0,25^FO500,180^A0N^FD08810^FS;
^CF0,25^FO250,220^A0N^FDHOAN^FS;
^BY2.0^FO50,260^BCN,250,N,N,N^FN8^FS;
^CF0,35^FO210,520^A0N^FN9^FS;
^CF0,35^FO410,520^A0N^FN10^FS;
^CF0,35^FO70,600^A0N^FDCOLLECT^FS;
^XZ;
