#
#Standard Pallet label with Part, lodnum, fifo date, number of cases, lotnum
#
# See if exact contents known or not and number of parts
#
#SELECT=select unkflg, count(distinct(prtnum)) prtcnt from invlod,invsub, invdtl where invlod.lodnum = '@lodnum' and invlod.lodnum = invsub.lodnum and invdtl.subnum = invsub.subnum group by unkflg
#
# If the contents are not known blank out values
#
#SELECT=select ' ' prtnum, to_char(fifdte,'MM/DD/YY') fifdte, sum(untqty) unsqty, lotnum from invlod,invsub,invdtl where invlod.lodnum = '@lodnum' and invlod.lodnum = invsub.lodnum and invsub.subnum = invdtl.subnum and invsub.phyflg = 'N' group by prtnum, fifdte, lotnum if (@unkflg = 'Y')
#
#SELECT=select count(*) serqty from invlod,invsub,invdtl where invlod.lodnum = '@lodnum' and invlod.lodnum = invsub.lodnum and invsub.subnum = invdtl.subnum and invsub.phyflg = 'Y' if (@unkflg = 'Y')
#
#SELECT=select 0 prtcnt, @unsqty + @serqty casqty from dual if (@unkflg = 'Y')
#
#SELECT=select prtnum, to_char(fifdte,'MM/DD/YY') fifdte, to_char(sum(untqty), '999999') casqty, lotnum from invlod,invsub,invdtl where invlod.lodnum = '@lodnum' and invlod.lodnum = invsub.lodnum and invsub.subnum = invdtl.subnum group by prtnum, fifdte, lotnum if (@prtcnt = 1 or @unkflg = 'Y')
#
#SELECT=select  ' ' casqty, ' ' lotnum, ' ' prtnum, ' ' fifdte from dual if (@prtcnt > 1)
#
#SELECT=select substr('@lodnum',1,6) lodnum, substr('@prtnum',1,10) prtnum from dual
#
#DATA=@prtnum~@lodnum~@fifdte~@casqty~@lotnum~
^XA^DFpallbl^FS;
^PRE;
^LH30,30;
^BY5,1,800;
^FWR;
^FO140,600^AD,180,40^FN1^FS;
^FO570,600^AD,160,60^FN2^FS;
^FO50,50^BCR,,N,N,N^FN2^FS;
^FO480,600^AD,72,40^FN3^FS;
^FO320,630^AD,126,50^FN4^FS;
^FO10,600^AD,126,40^FN5^FS;
^FO360,600^AD,72,40^FDQTY:^FS;
^XZ;
