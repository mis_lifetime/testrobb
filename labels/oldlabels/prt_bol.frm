#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select substr(stgdte,1,11) stgdte,cponum,ship_id,ffcust,ffname,ffadd1,ffadd2,ffcity,ffstcd,ffposc,ordnum,cardsc,carcod,srvlvl,usr_vc_totctn,to_char(usr_totwgt,'99999') usr_totwgt from shphdr where ship_id = '@ship_id' and shpseq = '@shpseq' and subsid = '@subsid' and ordnum = '@ordnum'
#
#The next selects check to see if there are multiple cponum's on the shipment
#if there is more then 1 it prints See Manifest in the purchace order box on the
#pre-printed form otherwise it prints the cponum.
#
#SELECT=select count(distinct cponum) cpocnt from shphdr where waybil = '@waybil'
#SELECT=select DECODE('@cpocnt', '1','@cponum','SEE MANIFEST') tmpcpo  from dual
#
#The next select grabs the trailer seal fields from the shpmst for user text on
#the bol
#
#SELECT=select trlsl1,trlsl2,trlsl3,trlsl4 from shpmst where shpnum = '@shpnum'
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select '@stgdte' lblstgdte,'@ship_id' lblship_id, substr('@ffcust', 1, instr('@ffcust','-',1,1)-1) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd'||'  '||substr('@ffposc', 1, 5) lblffcstz, substr('@cardsc', 1, 20) lblcarrier, substr('@tmpcpo', 1, 14) lblcustpo, '@trlsl1' lbltrlsl1, '@trlsl2' lbltrlsl2, '@trlsl3' lbltrlsl3, '@trlsl4' lbltrlsl4 from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select 'LIFETIME HOAN CORP.' lbllif, 'ROBBINSVILLE, NJ 08691' lblday, 'MASTER A/C #' lblmast, '' lbldesc1, 'PACKING SLIP WITH SHIPMENT' lbldesc2, 'FAK70' lblfak70, '@carcod' lblcarcod from dual
#
#SELECT=select decode('@srvlvl','CC','COLLECT','PP','PREPAID') pplbl from dual
#
#DATA=\n\n\n\n\n\n\p50 @lblstgdte\p70 @lblship_id\n\n\p5 @lbllif\n\p5 @lblday\n\n\n\p5 @lblffname\n\p5 @lblffadr1\n\p5 @lblffadr2\n\p5 @lblffcstz\p35 @lblmast\n\p37 @lblffcust\n\n\p10 @lblcustpo\p26 @lblcarrier\p45 @usr_vc_totctn\n\n\n\n\p1 @usr_vc_totctn\p54 @usr_totwgt\n\p62 @lblfak70\n\p26 @lbldesc1\n\p26 @lbldesc2\n\p26 @lbltrlsl1\n\p26 @lbltrlsl2\n\p26 @lbltrlsl3\n\p26 @lbltrlsl4\n\n\n\n\n\n\p1 @usr_vc_totctn\p54 @usr_totwgt\n\p62 @lblfak70\n\n\n\n\n\n\n\p22 @pplbl\f
