#
#SELECT=select '@lodnum' lblload, 'Device: '||'@devcod' devcod, 'Batch: '||'@lblbat' lblbathdr,'@print_dte' print_dte from dual
#
#SELECT=select 'Picks: '||count(subucc) pick_count from pckwrk where lblbat='@lblbat' and subucc is not null
#
#SELECT=select 'User ID: '||min(vc_prt_usr_id) usr_id from pckwrk where lblbat='@lblbat'
#
#DATA=@dummy~@devcod~@lblbathdr~@pick_count~@usr_id~@print_dte~
^XA^DFloadlbl^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,100^ADN,74,25^FN2^FS;
^FO15,200^ADN,74,25^FN3^FS;
^FO16,300^ADN,74,25^FN4^FS;
^FO15,400^ADN,74,25^FN5^FS;
^FO15,500^ADN,74,25^FDDate/Time Printed:^FS;
^FO15,600^ADN,74,25^FN6^FS;
^FO15,700^ADN,74,25^FDDate/Time Out:^FS;
^FO15,890^GB785,0,2^FS;
^FO15,900^ADN,74,25^FDDate/Time Back:^FS;
^FO15,1100^GB785,0,2^FS;
^PQ1,0,0,N^XZ;
