#SELECT=select ltrim(to_char(vc_retprc,'$999.99')) vc_retprc, ltrim(cstprt) cstprt from cstprq where prtnum = '@prtnum' and cstnum = '@cstnum'
#
#DATA=@cstprt~@vc_retprc~
#
^XA^DF29^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,10;
^FO30,70^AB20,20^FN1^FS;
^FO10,130^AB30,30^FN2^FS;

^FO300,70^AB20,20^FN1^FS;
^FO280,130^AB30,30^FN2^FS;

^FO570,70^AB20,20^FN1^FS;
^FO550,130^AB30,30^FN2^FS;
^XZ;
^PQ1,0,0,N^XZ;
