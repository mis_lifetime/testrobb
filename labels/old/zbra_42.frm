#SELECT=select ltrim(to_char(vc_retprc,'999.99')) vc_retprc, ltrim(vc_lblfd2) vc_lblfd3, ltrim(vc_cstpr1) vc_cstpr1 from cstprq where prtnum = '@prtnum' and cstnum = '@cstnum'
#SELECT=select (prtmst.upccod)upccod , substr(prtdsc.lngdsc,1,15) lngdsc from prtmst, prtdsc  where prtnum = '@prtnum' and prtmst.prtnum||'|----' = prtdsc.colval and prtdsc.locale_id ='US_ENGLISH' and prtdsc.colnam='prtnum|prt_client_id'
#
#DATA=@vc_lblfd3~@vc_retprc~@vc_cstpr1~@upccod~@lngdsc~
#
#
^XA^DF42^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO10,65^ADN,54,15^FN2^FS;
^FO1,125^ADN,18,10^FN5^FS;
^BY1,2,60^FO1,160^BCN,,N,N,N^FN4^FS;
^FO10,225^CFD20,20^FN4^FS;

^FO240,65^ADN,54,15^FN2^FS;
^FO230,125^ADN,18,10^FN5^FS; 
^BY1,2,60^FO226,160^BCN,,N,N,N^FN4^FS;
^FO240,225^CFD20,20^FN4^FS;

^FO460,65^ADN,54,15^FN2^FS;
^FO450,125^ADN,18,10^FN5^FS; 
^BY1,2,60^FO450,160^BCN,,N,N,N^FN4^FS;
^FO470,225^CFD20,20^FN4^FS;
^XZ;
^PQ1,0,0,N^XZ;
