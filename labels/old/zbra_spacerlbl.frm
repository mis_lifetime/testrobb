#
#SELECT=select '@stoloc' loc, '@prtnum' prt, '@numlbls' numlbls from dual
#
#DATA=@loc~@prt~@numlbls~
^XA^DFspacerlbl^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,100^ADN,74,25^FDLocation:^FS;
^FO15,200^ADN,160,50^FN1^FS;
^FO16,400^ADN,74,25^FDItem:^FS;
^FO15,500^ADN,160,50^FN2^FS;
^FO15,700^ADN,74,25^FDLabel Qty:^FS;
^FO15,800^ADN,160,50^FN3^FS;
^PQ1,0,0,N^XZ;
