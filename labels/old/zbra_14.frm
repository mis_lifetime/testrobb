# $Header: /mnt/dc01/mchugh/prod/cvsroot/prod/les/labels/zbra_14.frm,v 1.2 2002/03/11 16:39:33 devsh Exp $
#
# $Log: zbra_14.frm,v $
# Revision 1.2  2002/03/11 16:39:33  devsh
# mr 000
# Snapshot commit of development code to preserve uncommited changes
#
# Revision 1.1.1.1  2001/09/18 23:05:46  lh51sh
# Initial Import
#
# Revision 1.3  2000/04/28  09:34:20  09:34:20  dcsmgr (DCS Application Mgr)
# 03/23/00 Revision
# 
# Revision 1.2  2000/04/26  16:24:47  16:24:47  dcsmgr (DCS Application Mgr)
# 09/02/99 Revision
# 
# Revision 1.1  2000/04/26  15:47:52  15:47:52  dcsmgr (DCS Application Mgr)
# Initial revision
# 
#
#SELECT=select min(cstprt)cstprt  from cstprq where prtnum = '@prtnum' and cstnum = '@cstnum'
#
#DATA=@cstprt~
#
#
^XA^DF14^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH0,10;
^FO75,60^CFD20,20^FN1^FS;
^FO80,160^AB20,20^FDPLN^FS;
^BY1,3.0,55^FO45,90^B3N,N,N,N,N^FN1^FS;
^FO345,60^CFD20,20^FN1^FS;
^FO355,160^AB20,20^FDPLN^FS;
^BY1,3.0,55^FO320,90^B3N,N,N,N,N^FN1^FS;
^FO625,60^CFD20,20^FN1^FS;
^FO625,160^AB20,20^FDPLN^FS;
^BY1,3.0,55^FO590,90^B3N,N,N,N,N^FN1^FS;
^XZ;
^PQ1,0,0,N^XZ;
