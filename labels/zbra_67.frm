#SELECT=select cstprt, substr(ltrim(vc_lblfd2),1,12) vc_lblfd2, ltrim(to_char(vc_retprc,'$999.99')) vc_retprc, substr(ltrim(vc_lblfd3),1,12) vc_lblfd3 from cstprq where prtnum = '@prtnum' and cstnum = '@cstnum'
#
#
#DATA=@cstprt~@vc_lblfd2~@vc_retprc~
#
#
^XA^DF47^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH6,10;
^FO30,30^AB20,20^FDWORLD MARKET ^FS;
^FO70,55^AB20,20^FDTAIWAN ^FS;
^BY1,1,50^FO50,80^B3N,N,N,N,N^FN1^FS;
^FO60,130^AB20,20^FN1^FS;
^FO0,125^AB20,20^FN2^FS;
^FO60,160^AB30,30^FN3^FS;

^FO300,30^AB20,20^FDWORLD MARKET^FS;
^FO340,55^AB20,20^FDTAIWAN ^FS;
^BY1,1,50^FO320,80^B3N,N,N,N,N^FN1^FS;
^FO330,130^AB20,20^FN1^FS;
^FO270,125^AB20,20^FN2^FS;
^FO330,160^AB30,30^FN3^FS;

^FO570,30^AB20,20^FDWORLD MARKET^FS;
^FO610,55^AB20,20^FDTAIWAN^FS;
^BY1,1,50^FO590,80^B3N,N,N,N,N^FN1^FS;
^FO600,130^AB20,20^FN1^FS;
^FO540,125^AB20,20^FN2^FS;
^FO600,160^AB30,30^FN3^FS;
^XZ;
^PQ1,0,0,N^XZ;
