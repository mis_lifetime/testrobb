/* Include standard RF definitions */

#include "rf_format.h"

/* Text & Message definitions */

#define FORM_HELPTEXT           hlpUndirIdentify,               "enter truck number"

#define DLG_ADD_TRUCK           "dlgCreateNewTruck",            "create a new truck?"

#define DLG_CHECK_IN_TRLR       "dlgTrailerNotCheckedIn",       "trailer not checked in - check it in?"
#define DLG_OK_TO_IDENTIFY      "dlgOkToIdentify",              "continue to identify?"
#define DLG_TRLR_EX_OK_TO_ID    "dlgTrlrExpOkToIdentify",        "trailer not checked in - continue?"
#define DLG_TRLR_HAS_BEEN_CLOSED "dlgTrailerHasBeenClosed",     "trailer has been closed"
#define ERR_CRE_RCVTRK          "errCreatingReceiveTruck",      "error creating receive truck"
#define ERR_EXP_EXISTS          "errExpectedReceiptExists",     "expected receipt already exists"
#define ERR_TRK_UNAVAIL         "errTruckUnavailable",          "truck not available"
#define ERR_ARV_TRUCK           "errArriveTruck",               "error arriving truck"
#define ERR_RCV_TRK_TURN_FOR_SHIP "errRcvTrkTurnForShip",       "receiving truck turned for shipping"

#define ERR_INV_CLIENT		"errInvalidClientId",		"invalid client id"
#define ERR_INV_INVNUM          "errInvalidInvnum",             "invalid invoice number"
#define ERR_INV_RCVDCK          "errInvalidRcvdck",             "invalid receiving dock location"
#define ERR_INV_RCVTRK          "errInvalidRcvtrk",             "invalid receiving truck number"
#define ERR_INV_SUPNUM		"errInvalidSupnum",		"invalid supplier number"

/* Field & Label positioning definitions */

#define LABEL1_CAPTION          ttlUndirIdentify, "identify product"
#define LABEL1_TAG              ttlUndirIdentify
#define LABEL1_LEFT             RF_TITLE_LEFT
#define LABEL1_TOP              RF_TITLE_TOP
#define LABEL1_WIDTH            RF_TITLE_WIDTH

#ifdef RF_FORMAT_40X8
    #define LABEL2_CAPTION      rcv_id, "receiving id:"
    #define LABEL2_TAG          rcv_id
    #define LABEL2_LEFT         1
    #define LABEL2_TOP          1
    #define LABEL2_WIDTH        18
    #define LABEL2_JUSTIFY      Right
    #define LABEL3_CAPTION      trkref, "truck reference:"
    #define LABEL3_TAG          trkref
    #define LABEL3_LEFT         1
    #define LABEL3_TOP          2
    #define LABEL3_WIDTH        18
    #define LABEL3_JUSTIFY      Right
    #define LABEL4_CAPTION      numpal, "pallets:"
    #define LABEL4_TAG          numpal
    #define LABEL4_LEFT         1
    #define LABEL4_TOP          3
    #define LABEL4_WIDTH        8 
    #define LABEL4_JUSTIFY      Right
    #define LABEL5_CAPTION      numcas, "cases:"
    #define LABEL5_TAG          numcas
    #define LABEL5_LEFT         15
    #define LABEL5_TOP          3
    #define LABEL5_WIDTH        8
    #define LABEL5_JUSTIFY      Right
    #define LABEL6_CAPTION      rcvdck, "dock:"
    #define LABEL6_TAG          rcvdck
    #define LABEL6_LEFT         1
    #define LABEL6_TOP          4
    #define LABEL6_WIDTH        5
    #define LABEL6_JUSTIFY      Right
    #define LABEL7_CAPTION      lblflg, "lbl:"
    #define LABEL7_TAG          lblflg
    #define LABEL7_LEFT         29
    #define LABEL7_TOP          3
    #define LABEL7_WIDTH        8
    #define LABEL7_JUSTIFY      Right
    #define LABEL8_CAPTION      invnum, "inv:"
    #define LABEL8_TAG          invnum
    #define LABEL8_LEFT         1
    #define LABEL8_TOP          5
    #define LABEL8_WIDTH        5
    #define LABEL8_JUSTIFY      Right
    #define LABEL9_CAPTION      client_id, "cli:"
    #define LABEL9_TAG          client_id
    #define LABEL9_LEFT         22
    #define LABEL9_TOP          5
    #define LABEL9_WIDTH        7
    #define LABEL9_JUSTIFY      Right
    #define LABEL10_CAPTION     supnum, "supl:"
    #define LABEL10_TAG         supnum
    #define LABEL10_LEFT        1
    #define LABEL10_TOP         6
    #define LABEL10_WIDTH       5
    #define LABEL10_JUSTIFY     Right
    #define LABEL11_CAPTION     waybil, "waybill:"
    #define LABEL11_TAG         waybil
    #define LABEL11_LEFT        17
    #define LABEL11_TOP         6
    #define LABEL11_WIDTH       7
    #define LABEL11_JUSTIFY     Right
    #define RCV_ID_TAG          rcv_id
    #define RCV_ID_LEFT         20
    #define RCV_ID_TOP          1
    #define RCV_ID_DSP_WIDTH    20
    #define TRKREF_TAG          trkref
    #define TRKREF_LEFT         20
    #define TRKREF_TOP          2
    #define TRKREF_DSP_WIDTH    20
    #define NUMPAL_TAG          numpal
    #define NUMPAL_LEFT         10
    #define NUMPAL_TOP          3
    #define NUMCAS_TAG          numcas
    #define NUMCAS_LEFT         24
    #define NUMCAS_TOP          3
    #define RCVDCK_TAG          rcvdck
    #define RCVDCK_LEFT         7
    #define RCVDCK_TOP          4
    #define RCVDCK_DSP_WIDTH    15
    #define LBLFLG_TAG          lblflg
    #define LBLFLG_LEFT         38
    #define LBLFLG_TOP          3
    #define INVNUM_TAG          invnum
    #define INVNUM_LEFT         7
    #define INVNUM_TOP          5
    #define INVNUM_DSP_WIDTH    15
    #define CLIENT_ID_TAG       client_id
    #define CLIENT_ID_LEFT      30
    #define CLIENT_ID_TOP       5
    #define CLIENT_ID_DSP_WIDTH 10
    #define SUPNUM_TAG          supnum
    #define SUPNUM_LEFT         7
    #define SUPNUM_TOP          6
    #define SUPNUM_DSP_WIDTH    10
    #define WAYBIL_TAG          waybil
    #define WAYBIL_LEFT         25
    #define WAYBIL_TOP          6
    #define WAYBIL_DSP_WIDTH    15
#endif

#ifdef RF_FORMAT_20X16
    #define LABEL2_CAPTION      rcv_id, "rcv:"
    #define LABEL2_TAG          rcv_id
    #define LABEL2_LEFT         1
    #define LABEL2_TOP          1
    #define LABEL2_WIDTH        4
    #define LABEL2_JUSTIFY      Right
    #define LABEL3_CAPTION      trkref, "ref:"
    #define LABEL3_TAG          trkref
    #define LABEL3_LEFT         1
    #define LABEL3_TOP          2
    #define LABEL3_WIDTH        4
    #define LABEL3_JUSTIFY      Right
    #define LABEL4_CAPTION      numpal, "pal:"
    #define LABEL4_TAG          numpal
    #define LABEL4_LEFT         1
    #define LABEL4_TOP          3
    #define LABEL4_WIDTH        4
    #define LABEL4_JUSTIFY      Right
    #define LABEL5_CAPTION      numcas, "cas:"
    #define LABEL5_TAG          numcas
    #define LABEL5_LEFT         1
    #define LABEL5_TOP          4
    #define LABEL5_WIDTH        4
    #define LABEL5_JUSTIFY      Right
    #define LABEL6_CAPTION      rcvdck, "dck:"
    #define LABEL6_TAG          rcvdck
    #define LABEL6_LEFT         1
    #define LABEL6_TOP          6
    #define LABEL6_WIDTH        4
    #define LABEL6_JUSTIFY      Right
    #define LABEL7_CAPTION      lblflg, "lbl:"
    #define LABEL7_TAG          lblflg
    #define LABEL7_LEFT         1
    #define LABEL7_TOP          5
    #define LABEL7_WIDTH        4
    #define LABEL7_JUSTIFY      Right
    #define LABEL8_CAPTION      invnum, "inv:"
    #define LABEL8_TAG          invnum
    #define LABEL8_LEFT         1
    #define LABEL8_TOP          7
    #define LABEL8_WIDTH        4
    #define LABEL8_JUSTIFY      Right
    #define LABEL9_CAPTION      client_id, "cli:"
    #define LABEL9_TAG          client_id
    #define LABEL9_LEFT         1
    #define LABEL9_TOP          8
    #define LABEL9_WIDTH        4
    #define LABEL9_JUSTIFY      Right
    #define LABEL10_CAPTION     supnum, "sup:"
    #define LABEL10_TAG         supnum
    #define LABEL10_LEFT        1
    #define LABEL10_TOP         9
    #define LABEL10_WIDTH       4
    #define LABEL10_JUSTIFY     Right
    #define LABEL11_CAPTION     waybil, "way:"
    #define LABEL11_TAG         waybil
    #define LABEL11_LEFT        1
    #define LABEL11_TOP         10
    #define LABEL11_WIDTH       4
    #define LABEL11_JUSTIFY     Right
    #define RCV_ID_TAG          rcv_id
    #define RCV_ID_LEFT         6
    #define RCV_ID_TOP          1
    #define RCV_ID_DSP_WIDTH    15
    #define TRKREF_TAG          trkref
    #define TRKREF_LEFT         6
    #define TRKREF_TOP          2
    #define TRKREF_DSP_WIDTH    15
    #define NUMPAL_TAG          numpal
    #define NUMPAL_LEFT         6
    #define NUMPAL_TOP          3
    #define NUMCAS_TAG          numcas
    #define NUMCAS_LEFT         6
    #define NUMCAS_TOP          4
    #define RCVDCK_TAG          rcvdck
    #define RCVDCK_LEFT         6
    #define RCVDCK_TOP          6
    #define RCVDCK_DSP_WIDTH    15
    #define LBLFLG_TAG          lblflg
    #define LBLFLG_LEFT         6
    #define LBLFLG_TOP          5
    #define INVNUM_TAG          invnum
    #define INVNUM_LEFT         6
    #define INVNUM_TOP          7
    #define INVNUM_DSP_WIDTH    15
    #define CLIENT_ID_TAG       client_id
    #define CLIENT_ID_LEFT      6
    #define CLIENT_ID_TOP       8
    #define CLIENT_ID_DSP_WIDTH 10
    #define SUPNUM_TAG          supnum
    #define SUPNUM_LEFT         6
    #define SUPNUM_TOP          9
    #define SUPNUM_DSP_WIDTH    15
    #define WAYBIL_TAG          waybil
    #define WAYBIL_LEFT         6
    #define WAYBIL_TOP          10
    #define WAYBIL_DSP_WIDTH    15
#endif

/* Form definition */

Begin Form UNDIR_IDENTIFY
    Left      RF_TERMINAL_LEFT
    Top       RF_TERMINAL_TOP
    Width     RF_TERMINAL_WIDTH
    Height    RF_TERMINAL_HEIGHT
    Caption   RF_FORM_CAPTION
    HelpText  FORM_HELPTEXT

    /* Function key definitions */
    
    Begin Fkey FKEY_BACK
        Caption FKEY_BACK_CAPTION
        Begin Action 
            Back ()
        End
    End
    
    Begin Fkey FKEY_TOOL
        Caption FKEY_TOOL_CAPTION
        Begin Action
            ExecuteForm ("TOOLS_MENU")
        End 
    End 

    Begin Fkey FKEY_HELP
        Caption FKEY_HELP_CAPTION
        Begin Action 
            DisplayHelp ()
        End
    End

    /* Timer definitions */

    /* Label definitions */

    Begin Label LABEL1
        Caption LABEL1_CAPTION
        Tag     LABEL1_TAG
        Left    LABEL1_LEFT
        Top     LABEL1_TOP
        Width   LABEL1_WIDTH
        Height  RF_FIELD_HEIGHT
        Justify Center
    End 

    Begin Label LABEL2
        Caption LABEL2_CAPTION
        Tag     LABEL2_TAG 
        Left    LABEL2_LEFT
        Top     LABEL2_TOP
        Width   LABEL2_WIDTH
        Height  RF_FIELD_HEIGHT
        Justify LABEL2_JUSTIFY 
    End 

    Begin Label LABEL3
        Caption LABEL3_CAPTION
        Tag     LABEL3_TAG 
        Left    LABEL3_LEFT
        Top     LABEL3_TOP
        Width   LABEL3_WIDTH
        Height  RF_FIELD_HEIGHT
        Justify LABEL3_JUSTIFY 
    End 

    Begin Label LABEL4
        Caption LABEL4_CAPTION
        Tag     LABEL4_TAG 
        Left    LABEL4_LEFT
        Top     LABEL4_TOP
        Width   LABEL4_WIDTH
        Height  RF_FIELD_HEIGHT
        Justify LABEL4_JUSTIFY 
    End 

    Begin Label LABEL5
        Caption LABEL5_CAPTION
        Tag     LABEL5_TAG 
        Left    LABEL5_LEFT
        Top     LABEL5_TOP
        Width   LABEL5_WIDTH
        Height  RF_FIELD_HEIGHT
        Justify LABEL5_JUSTIFY 
    End 

    Begin Label LABEL6
        Caption LABEL6_CAPTION
        Tag     LABEL6_TAG 
        Left    LABEL6_LEFT
        Top     LABEL6_TOP
        Width   LABEL6_WIDTH
        Height  RF_FIELD_HEIGHT
        Justify LABEL6_JUSTIFY
    End 

    Begin Label LABEL7
        Caption LABEL7_CAPTION
        Tag     LABEL7_TAG 
        Left    LABEL7_LEFT
        Top     LABEL7_TOP
        Width   LABEL7_WIDTH
        Height  RF_FIELD_HEIGHT
        Justify LABEL7_JUSTIFY 
    End 

    Begin Label LABEL8
        Caption LABEL8_CAPTION
        Tag     LABEL8_TAG 
        Left    LABEL8_LEFT
        Top     LABEL8_TOP
        Width   LABEL8_WIDTH
        Height  RF_FIELD_HEIGHT
        Justify LABEL8_JUSTIFY 
    End 

    Begin Label LABEL9
        Caption LABEL9_CAPTION
        Tag     LABEL9_TAG 
        Left    LABEL9_LEFT
        Top     LABEL9_TOP
        Width   LABEL9_WIDTH
        Height  RF_FIELD_HEIGHT
        Justify LABEL9_JUSTIFY 
    End 

    Begin Label LABEL10
        Caption LABEL10_CAPTION
        Tag     LABEL10_TAG 
        Left    LABEL10_LEFT
        Top     LABEL10_TOP
        Width   LABEL10_WIDTH
        Height  RF_FIELD_HEIGHT
        Justify LABEL10_JUSTIFY
    End 

    Begin Label LABEL11
        Caption LABEL11_CAPTION
        Tag     LABEL11_TAG 
        Left    LABEL11_LEFT
        Top     LABEL11_TOP
        Width   LABEL11_WIDTH
        Height  RF_FIELD_HEIGHT
        Justify LABEL11_JUSTIFY
    End 

    /* Local field definitions */

    Begin Field yard_loc
        #include "local_string.h"
        Width    YARD_LOC_LEN
    End

    Begin Field trlr_stat
        #include "local_string.h"
        Width    TRLR_STAT_LEN
    End

    Begin Field trlr_id
        #include "local_string.h"
        Width    TRLR_ID_LEN
    End

    Begin Field arecod  /* Constant */
        #include "local_string.h"
        Width    ARECOD_LEN
    End

    Begin Field invflg
        #include "local_integer.h"
        Width    RF_FLAG_LEN
    End

    Begin Field unused
        #include "local_string.h"
        Width    RF_FLAG_LEN
    End

    Begin Field need_to_checkin
        #include "local_integer.h"
        Width    RF_FLAG_LEN
    End

    Begin Field allow_trlr_chkin
        #include "local_integer.h"
        Width    RF_FLAG_LEN
    End

    Begin Field sql_buffer
        #include "local_string.h"
        Width    1000
    End

    Begin Field trknum
        #include "local_string.h"
        Width    TRKNUM_LEN
    End

    Begin Field trlr_cod
        #include "local_string.h"
        Width    TRLR_COD_LEN
    End

    Begin Field clsdte
        #include "local_string.h"
        Width    DB_STD_DATE_LEN
    End

    /* Field definitions */
    
    Begin Field rcv_id
        Tag             RCV_ID_TAG
        Left            RCV_ID_LEFT
        Top             RCV_ID_TOP
        Width           TRKNUM_LEN
        DisplayWidth    RCV_ID_DSP_WIDTH
        Height          RF_FIELD_HEIGHT
        Datatype        String
        TabOrder        1
        DisplayMask     0
        DefaultData     0
        EntryMask       ENTRY_MASK_INPUT_REQUIRED

	Begin Action EntryAction

/*
	     * Ensure invoice information is cleared 
 * (if backspacing to rcv_id field)
 */

	    Length (rcv_id)
	    Begin Result 1
		ClearField ("invnum")
		ClearField ("supnum")
		ClearField ("waybil")

                Set (client_id, INIT_POLICIES.client_id)

		DisplayField ("invnum")
		DisplayField ("client_id")
		DisplayField ("supnum")
		DisplayField ("waybil")
	    End

        End /* End Field rcv_id EntryAction */
        
        Begin Action ExitAction

            /* Determine if receive truck is defined and available */

            ExecuteDSQL ("get rf receiving trailer information where rcv_id = '@rcv_id' into :rcv_id, :trknum, :clsdte, :trkref, :numcas, :numpal, :lblflg, :trlr_stat, :trlr_id, :rcvdck, :trlr_cod")
            GetResults()

            /* If receive truck is not defined or available, then do following */

            Begin Result !eOK

                /*
                 * Determine if creating receive trucks is allowed.  If not 
                 * then output warning message & exit
                 */

                Verify (INIT_POLICIES.addtrk, RF_FLAG_YES)
                Begin Result 0
                    Beep (3)
                    GetResponse (ERR_INV_RCVTRK)
                    Reenter ()
                End

                /* Prompt user for confirmation to add new receive truck */

                PromptYN (DLG_ADD_TRUCK)
                Begin Result 0
                    Reenter ()
                End

                /* Retrieve some default new receive truck information */

                Set (trknum, rcv_id)
                Set (trlr_stat, INIT_POLICIES.addsts)

                /* Enabled receive truck fields for data entry */

                SetFieldAttr ("trkref", ENTRY_MASK_ENABLED)
                SetFieldAttr ("numpal", ENTRY_MASK_ENABLED)
                SetFieldAttr ("numcas", ENTRY_MASK_ENABLED)
                SetFieldAttr ("rcvdck", ENTRY_MASK_ENABLED)
                SetFieldAttr ("lblflg", ENTRY_MASK_ENABLED)

                /* Clear receive truck field values */

                ClearField ("trkref")
                ClearField ("numpal")
                ClearField ("numcas")
                ClearField ("rcvdck")
                ClearField ("lblflg")

                /* Display receive truck field values */

                DisplayField ("trkref")
                DisplayField ("numpal")
                DisplayField ("numcas")
                DisplayField ("rcvdck")
                DisplayField ("lblflg")
            End

            /* If receive truck is defined and available, then do the following */

            Begin Result eOK
                
                /* verify the has been turned around for shipping */
                Verify (trlr_cod, TRLR_COD_RCV)

                Begin Result 0
                    Beep (3)
                    GetResponse (ERR_RCV_TRK_TURN_FOR_SHIP)
                    ClearField("rcv_id")
                    ClearField("trknum")
                    ClearField("clsdte")
                    ClearField("trlr_cod")
                    ClearField("trlr_id")
                    ClearField("trlr_stat")
                    ClearField("trkref")
                    ClearField("numpal")
                    ClearField("numcas")
                    ClearField("rcvdck")
                    ClearField("lblflg")
                    Reenter()
                End
                
                /* ensure the trailer hasn't been closed */
                Length(clsdte)
                
                Begin Result 1
                    Beep (3)
                    GetResponse (DLG_TRLR_HAS_BEEN_CLOSED)
                    ClearField("rcv_id")
                    ClearField("trknum")
                    ClearField("clsdte")
                    ClearField("trlr_cod")
                    ClearField("trlr_id")
                    ClearField("trlr_stat")
                    ClearField("trkref")
                    ClearField("numpal")
                    ClearField("numcas")
                    ClearField("rcvdck")
                    ClearField("lblflg")
                    Reenter()
                End

                /* display the fields */

                DisplayField ("trkref")
                DisplayField ("numpal")
                DisplayField ("numcas")
                DisplayField ("rcvdck")
                DisplayField ("lblflg")
                DisplayField ("rcv_id")
                
                /*
                 * See if receive truck has been checked in.
                 * If it hasn't, and the policy says that can check it in,
                 * Prompt them to determine if they want to check it in.
                 * If they say no, or if the policy says they can't check
                 * the trailer in, we'll let them identify anyway.
                 */

                Set (need_to_checkin, RF_FLAG_FALSE)

                Verify (trlr_stat, TRLSTS_EXPECTED)
                Begin Result 1
                    Verify (allow_trlr_chkin, RF_FLAG_TRUE)
                    Begin Result 1
                        /*
                         * The policy allows trlr check in
                         * Prompt them to check it in
                         */
                        PromptYN (DLG_CHECK_IN_TRLR)
                        Begin Result 0
                            /*
                             * They said no to checking it in.
                             * See if they want to continue identifying
                             * anyway.
                             */
                            PromptYN (DLG_OK_TO_IDENTIFY)
                            Begin Result 0
                                Reenter ()
                            End
                        End 
                        Begin Result 1
                            /*
                             * They said yes to checking it in.
                             * Now we just need the rcvdck.
                             */
                            Set (need_to_checkin, RF_FLAG_TRUE)
                        End
                    End
                    Begin Result 0
                        /*
                         * The policy doesn't allow trailer check in,
                         * so we'll just notify them that the trailer
                         * has not been checked in and see if they
                         * want to continue identifying.
                         */
                        PromptYN (DLG_TRLR_EX_OK_TO_ID)
                        Begin Result 0
                            Reenter ()
                        End
                    End
                End

                /*
                 * Determine if invoice receiving is enabled.  If not and the 
                 * receive truck already has at least one invoice assigned 
                 * to it, then transfer control to the next form in 
                 * sequence (identify_load)  I.e. skip invoice information 
                 * entry.
                 */

               Verify (need_to_checkin, RF_FLAG_TRUE)
	       Begin Result 0

                  Verify (INIT_POLICIES.invrcv, RF_FLAG_NO)
                  Begin Result 1

                    Set (sql_buffer, "[select trknum from rcvinv where trknum='@trknum'")
                    
                    Length (client_id)
                    Begin Result 1
                        Concatenate (sql_buffer, "and client_id = '@client_id']");
                    End
                    Begin Result 0
                        Concatenate (sql_buffer,"]")
                    End

                    ExecuteDSQL (sql_buffer)
                    Begin Result eOK
                        ExecuteForm ("IDENTIFY_LOAD")
                    End
                  End
                End

                /* Disable receive truck fields for data entry (i.e. readonly) */

                SetFieldAttr ("trkref", ENTRY_MASK_PROTECTED)
                SetFieldAttr ("numpal", ENTRY_MASK_PROTECTED)
                SetFieldAttr ("numcas", ENTRY_MASK_PROTECTED)
                SetFieldAttr ("rcvdck", ENTRY_MASK_PROTECTED)
                SetFieldAttr ("lblflg", ENTRY_MASK_PROTECTED)
		Verify (need_to_checkin, RF_FLAG_TRUE)
		Begin Result 1
                    SetFieldAttr ("rcvdck", ENTRY_MASK_ENABLED)
		End 

                /* Display receive truck field values */

                DisplayField ("trkref")
                DisplayField ("numpal")
                DisplayField ("numcas")
                DisplayField ("rcvdck")
                DisplayField ("lblflg")
            End

        End /* End Field rcv_id ExitAction */

    End /* End Field rcv_id */

    Begin Field trkref
        Tag             TRKREF_TAG
        Left            TRKREF_LEFT
        Top             TRKREF_TOP
        Width           TRKREF_LEN
        DisplayWidth    TRKREF_DSP_WIDTH
        Height          RF_FIELD_HEIGHT
        Datatype        String
        TabOrder        1
        DisplayMask     0
        DefaultData     0
        EntryMask       ENTRY_MASK_INPUT_REQUIRED
    End 
    
    Begin Field numpal
        Tag             NUMPAL_TAG
        Left            NUMPAL_LEFT
        Top             NUMPAL_TOP
        Width           RF_QUANTITY_LEN
        Height          RF_FIELD_HEIGHT
        Datatype        Long
        TabOrder        1
        DisplayMask     0
        DefaultData     0
        EntryMask       ENTRY_MASK_INPUT_REQUIRED
    End 
    
    Begin Field numcas
        Tag             NUMCAS_TAG
        Left            NUMCAS_LEFT
        Top             NUMCAS_TOP
        Width           RF_QUANTITY_LEN
        Height          RF_FIELD_HEIGHT
        Datatype        Long
        TabOrder        1
        DisplayMask     0
        DefaultData     0
        EntryMask       ENTRY_MASK_INPUT_REQUIRED
    End 

    Begin Field lblflg
        Tag             LBLFLG_TAG
        Left            LBLFLG_LEFT
        Top             LBLFLG_TOP
        Width           FLAG_LEN
        Height          RF_FIELD_HEIGHT
        Datatype        Boolean
        TabOrder        1
        DisplayMask     0
        DefaultData     0
        EntryMask       ENTRY_MASK_INPUT_REQUIRED

        Begin Action ExitAction

            /* Create new receive truck with entered information */

            ExecuteDSQL ("create receive truck where trknum = '@trknum' and trkref = '@trkref' and numpal = '@numpal' and numcas = '@numcas' and yard_loc = '@rcvdck' and lblflg = @lblflg")
            Begin Result RF_STS_EXISTS
                Beep (3)
                GetResponse (ERR_EXP_EXISTS)
                Reenter ()
            End
            Begin Result !eOK
                Beep (3)
                GetResponse (ERR_CRE_RCVTRK)
                Reenter ()
            End

            /* Now get the updated trailer information, including the trlr_stat */

            ExecuteDSQL ("[select trlr.trlr_id, trlr.trlr_stat, rcvtrk.trkref, rcvtrk.numpal, rcvtrk.numcas, trlr.yard_loc, rcvtrk.lblflg into :trlr_id, :trlr_stat, :trkref, :numpal, :numcas, :rcvdck, :lblflg from rcvtrk, trlr where rcvtrk. trlr_id = trlr.trlr_id and rcvtrk.trknum = '@trknum']")
            GetResults ()
            Begin Result !eOK
                DisplayMessageText ()
                Reenter ()
            End

            Verify (trlr_stat, TRLSTS_EXPECTED)
            Begin Result 1
                Verify (allow_trlr_chkin, RF_FLAG_TRUE)
                Begin Result 1
                    /*
                     * The policy allows trlr check in
                     * Prompt them to check it in
                     */
                    PromptYN (DLG_CHECK_IN_TRLR)
                    Begin Result 0
                        /*
                         * They said no to checking it in.
                         * See if they want to continue identifying
                         * anyway.
                         */
                        PromptYN (DLG_OK_TO_IDENTIFY)
                        Begin Result 0
                            Reenter ()
                        End
                    End
                    Begin Result 1
                        /*
                         * They said yes to checking it in.
                         * Now we just need the rcvdck.
                         */
                        Set (need_to_checkin, RF_FLAG_TRUE)
                    End
                End
                Begin Result 0
                    /*
                     * The policy doesn't allow trailer check in,
                     * so we'll just notify them that the trailer
                     * has not been checked in and see if they
                     * want to continue identifying.
                     */
                    PromptYN (DLG_TRLR_EX_OK_TO_ID)
                    Begin Result 0
                        Reenter ()
                    End
                End
            End

        End /* End Field lblflg ExitAction */

    End /* End Field lblflg */

    Begin Field rcvdck
        Tag             RCVDCK_TAG
        Left            RCVDCK_LEFT
        Top             RCVDCK_TOP
        Width           STOLOC_LEN
        DisplayWidth    RCVDCK_DSP_WIDTH
        Height          RF_FIELD_HEIGHT
        Datatype        String
        TabOrder        1
        DisplayMask     0
        DefaultData     0
        EntryMask       ENTRY_MASK_INPUT_REQUIRED
        
        Begin Action ExitAction

	    Verify (need_to_checkin, RF_FLAG_TRUE)
	    Begin Result 1
                /*
                 * If we're going to check the trailer in, then
                 * we need a rcvdck value.
                 */
                Verify (rcvdck,"")
                Begin Result 1
                    GetResponse(ERR_INV_RCVDCK)
                    ReEnter()
                End

                /* Check in the trailer */
                
                ExecuteDSQL ("process rf trailer check in for identify where trlr_id = '@trlr_id' and yard_loc = '@rcvdck' and devcod = '@global.devcod' into :rcv_id, :trknum, :clsdte, :trkref, :numcas, :numpal, :lblflg, :trlr_stat, :trlr_id, :rcvdck, :trlr_cod")
                GetResults()

                Begin Result !eOK
                    Beep(3)
                    DisplayMessageText()
                    Reenter ()
                End

                /* Non Inventory Service Exit Point */
                Set(GET_SERVICES.non_invtid, trlr_id)
                Set(GET_SERVICES.non_invtyp, "TRLR")
                Set(GET_SERVICES.exitpnt_typ, "SERVICE-NONINVENTORY")
                Set(GET_SERVICES.exitpnt, "TRLRCHECKIN") 

                ShowForm("GET_SERVICES")

                Set(GET_SERVICES.non_invtid, trlr_id)
                Set(GET_SERVICES.non_invtyp, "TRLR")
                Set(GET_SERVICES.exitpnt_typ, "SERVICE-NONINVENTORY")
                Set(GET_SERVICES.exitpnt, "TRLRTODOCK") 

                ShowForm("GET_SERVICES")
	    End 
	    
        End /* End Field rcvdck ExitAction */

    End /* End Field rcvdck */
    
    Begin Field invnum
        Tag             INVNUM_TAG
        Left            INVNUM_LEFT
        Top             INVNUM_TOP
        Width           INVNUM_LEN
        DisplayWidth    INVNUM_DSP_WIDTH
        Height          RF_FIELD_HEIGHT
        Datatype        String
        TabOrder        1
        DisplayMask     0
        DefaultData     0
        EntryMask       ENTRY_MASK_INPUT_REQUIRED
        
        Begin Action EntryAction

            /*
             * If invoice number is not defined, then retrieve & display default
             * invoice number, client id,  & supplier number (if possible)
             */

            Length(invnum)
            Begin Result 0

                Set (sql_buffer, "get rf default invoice into :client_id, :supnum, :invnum, :waybil where trknum = '@trknum'")
                Length (client_id)
                Begin Result 1
                    Concatenate( sql_buffer, " and client_id = '@client_id'");
                End

                ExecuteDSQL (sql_buffer)
                GetResults()

/* If more than one default exists, then clear input fields */

		GetResults()
		Begin Result eOK
		    ClearField ("invnum")
		    ClearField ("client_id")
		    ClearField ("supnum")
		    ClearField ("waybil")
		End

/* Display default invoice information */

                DisplayField ("invnum")
                DisplayField ("client_id")
                DisplayField ("supnum")
                DisplayField ("waybil")

            End

/*
	     * If invoice number is already defined, then get & display related
	     * invoice information (if invoice number is unique).
 */

	    Begin Result 1
		Set (sql_buffer, "get rf receive invoice info into :invflg, :client_id, :supnum, :waybil where trknum = '@trknum' and invnum = '@invnum'")
                Length (client_id)
                Begin Result 1
                    Concatenate (sql_buffer, " and client_id = '@client_id'")
                End
		ExecuteDSQL (sql_buffer)
		GetResults ()
		Begin Result !eOK
		    Set (client_id, INIT_POLICIES.client_id)
		    ClearField ("supnum")
		    ClearField ("waybil")
		End
		DisplayField ("client_id")
		DisplayField ("supnum")
		DisplayField ("waybil")
		DisplayField ("waybil")
	    End

        End /* End Field invnum EntryAction */
        
        Begin Action ExitAction

/* Get related invoice information (if invoice number is unique) */

	    Set (sql_buffer, "get rf receive invoice info into :invflg, :client_id, :supnum, :waybil where trknum = '@trknum' and invnum = '@invnum'")
            Length (client_id)
            Begin Result 1
                Concatenate (sql_buffer, " and client_id = '@client_id'")
            End
	    ExecuteDSQL (sql_buffer)
	    GetResults ()
	    Begin Result eDB_NO_ROWS_AFFECTED
                Beep (3)
                GetResponse (ERR_INV_INVNUM)
                Reenter ()
            End
            Begin Result !eOK
                Beep (3)
                DisplayMessageText ()
                Reenter ()
            End

/*
	     * If matching receiving invoice line already exists for current
	     * truck number & invoice/client/supplier combination, then 
	     * transfer control to next form in sequence (identify_load)
 */

	    Verify (invflg, RF_FLAG_TRUE)
	    Begin Result 1
		ExecuteForm ("IDENTIFY_LOAD")
	    End

/*
	     * If all invoice information is present to create the matching
	     * receiving invoice line, then create the matching receiving
	     * invoice line.
 */

	    Length (client_id)
	    Begin Result 1
		Length (supnum)
		Begin Result 1
		    ExecuteDSQL ("create receive invoice from master where trknum = '@trknum' and invnum = '@invnum' and client_id = '@client_id' and supnum = '@supnum'")
		    Begin Result eDB_NO_ROWS_AFFECTED
			Beep (3)
			GetResponse (ERR_INV_INVNUM)
			Reenter ()
		    End
		    Begin Result !eOK
			Beep (3)
			DisplayMessageText ()
			Reenter ()
		    End
		End
	    End

/* Determine if supnum field should be enabled (for user input) */

	    Length (supnum)
	    Begin Result 1
		SetFieldAttr ("supnum", ENTRY_MASK_PROTECTED_AND_NO_DEFAULT_CHAR)
	    End
	    Begin Result 0
		SetFieldAttr ("supnum", ENTRY_MASK_INPUT_REQUIRED)
	    End

/* Display related invoice information */

	    DisplayField ("client_id")
	    DisplayField ("supnum")

        End /* End Field invnum ExitAction */
        
    End /* End Field invnum */
   
    Begin Field client_id
        Tag             CLIENT_ID_TAG
        Left            CLIENT_ID_LEFT
        Top             CLIENT_ID_TOP
        Width           CLIENT_ID_LEN
        DisplayWidth    CLIENT_ID_DSP_WIDTH
        Height          RF_FIELD_HEIGHT
        Datatype        String
        TabOrder        1
        DisplayMask     0
        DefaultData     0
        EntryMask       ENTRY_MASK_INVISIBLE

        Begin Action EntryAction

/*
	     * If client id is not defined, then retrieve & display default
	     * client id & supplier number (if possible)
 */

            Length(client_id)
            Begin Result 0
                ExecuteDSQL ("get rf default invoice into :client_id, :supnum, :unused, :waybil where trknum = '@trknum' and invnum = '@invnum'")
                GetResults()

/* If more than one default exists, then clear input fields */

		GetResults()
		Begin Result eOK
		    Set (client_id, INIT_POLICIES.client_id)
		    ClearField ("supnum")
		End

/* Display default invoice information */

                DisplayField ("client_id")
                DisplayField ("supnum")
            End

/*
	     * If client id is already defined, then get & display other related
	     * invoice information (if invoice number/client id is unique).
 */

	    Begin Result 1
 
		ExecuteDSQL ("get rf receive invoice info into :invflg, :unused, :supnum, :waybil where trknum = '@trknum' and invnum = '@invnum' and client_id = '@client_id'")
		GetResults ()
		Begin Result !eOK
		    ClearField ("supnum")
		End
		DisplayField ("supnum")
	    End

        End /* End Field client_id EntryAction */

        Begin Action ExitAction

/* Get related invoice information (if invoice number/client id is unique) */

	    ExecuteDSQL ("get rf receive invoice info into :invflg, :unused, :supnum, :waybil where trknum = '@trknum' and invnum = '@invnum' and client_id = '@client_id'")
	    GetResults ()
	    Begin Result eDB_NO_ROWS_AFFECTED
                Beep (3)
                GetResponse (ERR_INV_CLIENT)
                Reenter ()
            End
            Begin Result !eOK
                Beep (3)
                DisplayMessageText ()
                Reenter ()
            End

/*
	     * If matching receiving invoice line already exists for current
	     * truck number & invoice/client/supplier combination, then 
	     * transfer control to next form in sequence (identify_load)
 */

	    Verify (invflg, RF_FLAG_TRUE)
	    Begin Result 1
		ExecuteForm ("IDENTIFY_LOAD")
	    End

/*
	     * If all invoice information is present to create the matching
	     * receiving invoice line, then create the matching receiving
	     * invoice line.
 */

	    Length (supnum)
	    Begin Result 1
		ExecuteDSQL ("create receive invoice from master where trknum = '@trknum' and invnum = '@invnum' and client_id = '@client_id' and supnum = '@supnum'")
		Begin Result eDB_NO_ROWS_AFFECTED
		    Beep (3)
		    GetResponse (ERR_INV_CLIENT)
		    Reenter ()
		End
		Begin Result !eOK
		    Beep (3)
		    DisplayMessageText ()
		    Reenter ()
		End
	    End

/* Determine if supnum field should be enabled (for user input) */

	    Length (supnum)
	    Begin Result 1
		SetFieldAttr ("supnum", ENTRY_MASK_PROTECTED_AND_NO_DEFAULT_CHAR)
	    End
	    Begin Result 0
		SetFieldAttr ("supnum", ENTRY_MASK_INPUT_REQUIRED)
	    End

/* Display related invoice information */

	    DisplayField ("supnum")

        End /* End Field client_id ExitAction */

    End /* End Field client_id */
 
    Begin Field supnum
        Tag             SUPNUM_TAG
        Left            SUPNUM_LEFT
        Top             SUPNUM_TOP
        Width           SUPNUM_LEN
        DisplayWidth    SUPNUM_DSP_WIDTH
        Height          RF_FIELD_HEIGHT
        Datatype        String
        TabOrder        1
        DisplayMask     0
        DefaultData     0
        EntryMask       ENTRY_MASK_ENABLED

        Begin Action EntryAction

/*
	     * If supplier number is not defined, then retrieve & display
	     * default supplier number (if possible)
 */

            Length(supnum)
            Begin Result 0
                ExecuteDSQL ("get rf default invoice into :unused, :supnum, :unused, :waybil where trknum = '@trknum' and invnum = '@invnum' and client_id = '@client_id' ")
                GetResults()

/* If more than one default exists, then clear input fields */

		GetResults()
		Begin Result eOK
		    ClearField ("supnum")
		End

/* Display default invoice information */

                DisplayField ("supnum")
            End

        End /* End Field supnum EntryAction */
        
        Begin Action ExitAction

/* Get related invoice information */

	    ExecuteDSQL ("get rf receive invoice info into :invflg, :unused, :unused, :waybil where trknum = '@trknum' and invnum = '@invnum' and client_id = '@client_id' and supnum = '@supnum'")
	    GetResults ()
	    Begin Result eDB_NO_ROWS_AFFECTED
                Beep (3)
                GetResponse (ERR_INV_SUPNUM)
                Reenter ()
            End
            Begin Result !eOK
                Beep (3)
                DisplayMessageText ()
                Reenter ()
            End

/*
	     * If matching receiving invoice line already exists for current
	     * truck number & invoice/client/supplier combination, then 
	     * transfer control to next form in sequence (identify_load)
 */

	    Verify (invflg, RF_FLAG_TRUE)
	    Begin Result 1
		ExecuteForm ("IDENTIFY_LOAD")
	    End

/* Create the matching receiving invoice line. */

	    ExecuteDSQL ("create receive invoice from master where trknum = '@trknum' and invnum = '@invnum' and client_id = '@client_id' and supnum = '@supnum'")
	    Begin Result eDB_NO_ROWS_AFFECTED
                Beep (3)
                GetResponse (ERR_INV_SUPNUM)
                Reenter ()
            End
            Begin Result !eOK
                Beep (3)
                DisplayMessageText ()
                Reenter ()
            End

        End /* End Field supnum ExitAction */

    End /* End Field supnum */
    
    Begin Field waybil
        Tag             WAYBIL_TAG
        Left            WAYBIL_LEFT
        Top             WAYBIL_TOP
        Width           WAYBIL_LEN
        DisplayWidth    WAYBIL_DSP_WIDTH
        Height          RF_FIELD_HEIGHT
        Datatype        String
        TabOrder        1
        DisplayMask     0
        DefaultData     0
        EntryMask       ENTRY_MASK_ENABLED
        
        Begin Action ExitAction

            /* If defined, update waybil number for receiving invoice line */

            Length (waybil)
            Begin Result 1
                ExecuteDSQL ("[update rcvinv set waybil = '@waybil' where trknum = '@trknum' and client_id='@client_id' and supnum = '@supnum' and invnum = '@invnum']")
            End

            /* Transfer control to next form in sequence (identify_load) */

            ExecuteForm ("IDENTIFY_LOAD")

        End /* End Field waybil ExitAction */

    End /* End Field waybil */

    /* Form action definitions */

    Begin Action EntryAction

        /* Clear form fields */

        ClearForm ()

        /*
         * Determine if 'Client Id' field should be enabled and displayed
         * If we are in a 3pl environment, and the user only has single
         * client access, protect the field so that the user cannot change
         * it, otherwise, allow it to be changed for multiple client access
         * users.
         */

        Set (client_id, INIT_POLICIES.client_id)
        Verify (INIT_POLICIES.3plinstalled, RF_FLAG_YES)
        Begin Result 0
            SetFieldAttr ("client_id", ENTRY_MASK_INVISIBLE)
        End
        Begin Result 1
            Length (client_id)
            Begin Result 1
                SetFieldAttr ("client_id", ENTRY_MASK_PROTECTED_AND_NO_DEFAULT_CHAR)
            End
            Begin Result 0
	        SetFieldAttr ("client_id", ENTRY_MASK_INPUT_REQUIRED)
            End
        End
	Set (dekit.savopr, RF_OPR_UNDIR_IDENTIFY)
        
        Set (allow_trlr_chkin, RF_FLAG_FALSE)
        /*
         * Policy is enabled if rtnum1 = 1 (allow at identify) 
         * or 3 (allow for both identify and receive).
         */

        Verify(INIT_POLICIES.allowrcvtrlrchkin, 1)
        Begin Result 1
            Set (allow_trlr_chkin, RF_FLAG_TRUE)
        End        

        Verify(INIT_POLICIES.allowrcvtrlrchkin, 3)
        Begin Result 1
            Set (allow_trlr_chkin, RF_FLAG_TRUE)
        End

    End /* End of form EntryAction */
    
End /* End of form UNDIR_IDENTIFY */

