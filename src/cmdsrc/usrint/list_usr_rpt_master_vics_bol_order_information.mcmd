<command>
<name>list usr rpt master vics bol order information</name>
<description>List RPT Master VICS Bill Of Lading Order Information</description>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[
[select o.client_id client_id,
                 o.cponum cponum,
                 count(distinct ivs.subnum) pkgcnt,
sum(ivd.untqty * (p.netwgt/p.untcas)) weight,
                 cm.doc_num doc_num
            from invlod ivl,
                 invsub ivs, 
                 invdtl ivd, 
                 prtmst p,  
                 ord o, 
                 shipment sh, 
                 shipment_line sl,
                 stop st,
                 car_move cm,
                 trlr t
           where p.prtnum = ivd.prtnum
             and p.prt_client_id = ivd.prt_client_id
             and o.ordnum = sl.ordnum
             and o.client_id = sl.client_id
             and sh.ship_id = sl.ship_id
             and st.stop_id = sh.stop_id
             and cm.car_move_id = st.car_move_id
             and t.trlr_id = cm.trlr_id
             and ivd.ship_line_id = sl.ship_line_id
             and ivs.subnum = ivd.subnum
             and ivl.lodnum = ivs.lodnum
             and @+cm.doc_num
        group by o.client_id,
                o.cponum,
                cm.doc_num
union all
         select null client_id, 
                null ordnum, 
                null cponum, 
                null cascnt, 
                null weight
           from shipment
          where rownum < (select 6 - count(distinct o.cponum || o.client_id)
                         from shipment sh,
	               shipment_line sl,
                              ord o,
                          car_move cm,
                           stop st
                        where o.ordnum = sl.ordnum
                          and o.client_id = sl.client_id
			  and sl.ship_id = sh.ship_id
                          and st.stop_id = sh.stop_id
                         and cm.car_move_id = st.car_move_id
        			  and @+cm.doc_num)]
]]>
</local-syntax>
<documentation>
<remarks>
<![CDATA[
    <p>
    This command is used to list bill of lading order information.  It can be 
    used to get the supplement customer order info in 
    Sub-VICS-BOL-SupplementCustomerOrderInfo report or to get the customer 
    order info in Sub-VICS-BOL-CustomerOrderInfo report or in the 
    Std-VICS-BillOfLading report to determine the number orders in a shipment.

    Currently, the report itself is laid out to print about 5 order rows in
    the main report and about 34 order rows in the addendum pages.  The
    query above does a subtraction from the above numbers to force out the
    publishing of null rows whether the report is called from the main page
    or the supplemental page.
    </p>
]]>
</remarks>
<retcol name="client_id" type="COMTYP_STRING">Client Id</retcol>
<retcol name="ordnum" type="COMTYP_STRING">Order number</retcol>
<retcol name="cponum" type="COMTYP_STRING">Customer order number</retcol>
<retcol name="cascnt" type="COMTYP_INT">Case count for purchase order</retcol>
<retcol name="weight" type="COMTYP_FLOAT">Total weight for purchase order</retcol>
<exception value="eOK">Normal successful completion</exception>
<exception value="eDB_NO_ROWS_AFFECTED">No rows found</exception>
</documentation>
</command>
