<command>
<name>create usr sids for backorders</name>
<description>create usr sids for backorders</description>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[
/* All at once */
/* fill anthing order rule orders */
{
publish data where btcust = '155438'
|
list available orders WHERE btcust = @btcust and vc_numbch >= 1 catch (-1403)
|
if (@? = 0 and @ordnum)
{
  /* get the order fill rule for the order */
  [select min(vc_order_proc_logic) fill_rule from ord_line where ordnum = @ordnum and client_id = @client_id]
  |
  if (@fill_rule = null)  /*process as fill anything order */
   {
     /* see if there is already an sid out there in process for the order */
     [select count (distinct sl.ship_id) existing_sid_count from shipment_line sl, shipment s
     where sl.ship_id = s.ship_id
     and sl.ordnum = @ordnum
     and s.shpsts in ('R', 'I', 'S', 'X')]
     |
     check usr order deal of the day only
     |
     if (@dotd_only_count != 0)
       write daily transaction where oprcod = 'DOTDO' and actcod = 'DOTDO'  
                             and ordnum = @ordnum >> my_tranres
     |
     if (@existing_sid_count = 0 and @dotd_only_count  = 0)
     {
       /*see if we have any order lines that we can fill before we even attempt to create the SID */
       [select count(ordlin) can_ship_order_count
        from
          (select 
          ol.ordnum, 
          ol.ordlin, 
          ol.ordsln, 
          ol.prtnum,
          ol.lotnum,
          ol.invsts_prg,
          ol.pckqty,
          ol.non_alc_flg
          from ord_line ol where ol.ordnum = @ordnum 
          and ol.client_id = @client_id
          and (ol.non_alc_flg = '1' or ol.prjnum is null)
          and ol.pckqty > 0)
          where (decode(non_alc_flg,'1', varGetShipableKitQuantity(prtnum,lotnum,invsts_prg,ordnum,ordlin), varGetOrdLineAvailQty(prtnum,lotnum,invsts_prg))) > 0]
       |
       if (@can_ship_order_count > 0) 
       { 
        /* Okay to shipment plan the order */
        generate next number where numcod = 'ship_id'
        |
        publish data where ship_id = @nxtnum
        |
        generate next number where numcod = 'cons_batch'
        |
        save session variable where name = 'cons_batch_to_use' and value = @nxtnum
        |
        publish data where cons_batch = @nxtnum
        |
        /*get the order lines that we can actually shipment plan */
        {[select ordnum, ordlin, ordsln
          from
          (select 
           ol.ordnum, 
           ol.ordlin, 
           ol.ordsln, 
           ol.prtnum,
           ol.lotnum,
           ol.invsts_prg,
           ol.pckqty,
           ol.non_alc_flg
           from ord_line ol where ol.ordnum = @ordnum 
           and ol.client_id = @client_id
           and (ol.non_alc_flg = '1' or ol.prjnum is null)
           and ol.pckqty > 0)
        where (decode(non_alc_flg,'1', varGetShipableKitQuantity(prtnum,lotnum,invsts_prg,ordnum,ordlin), varGetOrdLineAvailQty(prtnum,lotnum,invsts_prg))) > 0]
        |
        preconsolidate shipment lines where ship_id = @ship_id 
          and cons_batch = @cons_batch 
          and client_id = '----' 
          and ordnum = @ordnum 
          and ordlin = @ordlin 
          and ordsln = @ordsln}
        ;
        get session variable where name = 'cons_batch_to_use'
        |
        consolidate shipment lines mainline where cons_batch  = @value
        |
        postconsolidate shipment lines where cons_batch  = @value 
        |
        [update shipment set wave_set = 'PFZABO' where ship_id = @ship_id and  nvl(wave_set,'NONE') not in (select distinct rtstr2
               from poldat 
               where polcod = 'USR'
                 and polvar = 'CANADA-CONSOLIDATION'
                  and rtstr2 is not null)] catch (-1403)
        |   
        commit 
       }
     }
   }
   else   if (@fill_rule = '1')  /*process as whole order */
   {
     /* see if there is already an sid out there in process for the order */
     [select count (distinct sl.ship_id) existing_sid_count from shipment_line sl, shipment s
     where sl.ship_id = s.ship_id
     and sl.ordnum = @ordnum
     and s.shpsts in ('R', 'I', 'S', 'X')]
     |
     check usr order deal of the day only
     |
     if (@dotd_only_count != 0)
       write daily transaction where oprcod = 'DOTDO' and actcod = 'DOTDO'  
                             and ordnum = @ordnum >> my_tranres
     |
     if (@existing_sid_count = 0 and @dotd_only_count  = 0)
     {
       /* see if we have any short lines on the order */
       [select count(ordlin) short_ordlin_count
       from
         (select 
         ol.ordnum, 
         ol.ordlin, 
         ol.ordsln, 
         ol.prtnum,
         ol.lotnum,
         ol.invsts_prg,
         ol.pckqty,
         ol.ordqty
         from ord_line ol where ol.ordnum = @ordnum 
         and ol.client_id = @client_id
         and ol.non_alc_flg != 1
         and ol.pckqty > 0)
        where (varGetOrdLineAvailQty(prtnum,lotnum,invsts_prg) - pckqty) < 0]
       |
       if (@short_ordlin_count = 0)
        /*we can fill the whole order*/
       {
        /* Okay to shipment plan the order */
        generate next number where numcod = 'ship_id'
        |
        publish data where ship_id = @nxtnum
        |
        generate next number where numcod = 'cons_batch'
        |
        save session variable where name = 'cons_batch_to_use' and value = @nxtnum
        |
        publish data where cons_batch = @nxtnum
        |
        /*get the order lines that we can actually shipment plan */
        {[select 
         ol.ordnum, 
         ol.ordlin, 
         ol.ordsln,
         ol.client_id
         from ord_line ol where ol.ordnum = @ordnum 
         and ol.client_id = @client_id
         and ol.non_alc_flg != 1
         and ol.pckqty > 0]
        |
        preconsolidate shipment lines where ship_id = @ship_id 
          and cons_batch = @cons_batch 
          and client_id = '----' 
          and ordnum = @ordnum 
          and ordlin = @ordlin 
          and ordsln = @ordsln}
        ;
        get session variable where name = 'cons_batch_to_use'
        |
        consolidate shipment lines mainline where cons_batch  = @value
        |
        postconsolidate shipment lines where cons_batch  = @value 
        |
        [update shipment set wave_set = 'PFZABO' where ship_id = @ship_id and nvl(wave_set,'NONE')  not in (select distinct rtstr2
               from poldat 
               where polcod = 'USR'
                 and polvar = 'CANADA-CONSOLIDATION'
                  and rtstr2 is not null)]  catch (-1403)
        |
        [select count (ship_id) exp_count 
         from shipment where ship_id = @ship_id 
         and carcod in ('UPND', 'UPS3', 'UPS2', 'UPS05')]
        |
        if (@exp_count != 0)
          [update shipment set carcod = decode(carcod,'LANDMS', 'LANDMS', 'UPSC1','UPSC1','UPSS') where ship_id = @ship_id ]
        |
        commit 
       }
     }
   }
}
}
|
commit
;
[select count(ship_id) backorder_sid_count from shipment where shpsts = 'R' and wave_set = 'PFZABO']
|
if (@backorder_sid_count != 0)
{
{
  [select sysdate my_wave_date from dual]  
  |
  do loop where count = 10
  |
  process wave rules  WHERE vc_plan_wavdte = @my_wave_date 
    AND rule_nam = 'LIFETIME PLAN WAVES' 
    AND wave_set = 'PFZABO' catch (@?)
  |
  if (@? = 0)
     commit
}
|
commit
  ;
  [select to_char(sysdate,'DD-MON-YYYY') date_today from dual] 
  |
  [select '\\172.16.1.16\files\hostout\Auto Backord '||(select to_char(sysdate,'DD-MON-YYYY') from dual)||'.pdf' name from dual]
  |
  generate moca report
   where rpt_id     = 'Usr-BackorderWave'
     and gen_usr_id =nvl(@gen_usr_id,@@usr_id)
     and keep_days  = '0'
     and locale_id  = 'US_ENGLISH'
     and log_event  ='1'
     and dest_typ   = "file"
     and dest       = @name
     and format_typ = "pdf" 
     and date_today = @date_today
  |
  raise user ems event for usr auto backorder
  |
  create usr auto allocate and release request where wave_set = 'PFZABO' catch (@?)
}
]]>
</local-syntax>
</command>
