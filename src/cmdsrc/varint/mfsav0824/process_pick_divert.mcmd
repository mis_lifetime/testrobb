<command>
<name>process pick divert</name>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[
[select count(*) the_dst_cnt from locmst where stoloc = @dstloc] 
|
if(@the_dst_cnt > 0) { /* if passed dest is valid */
  { get translated inventory identifier where id = @subucc catch (@?) |
    if (@? != 10323) publish data where subucc = @subucc } 
  |
  [select count(*) the_sub_cnt from pckwrk where subucc = @subucc] 
  |
  if(@the_sub_cnt = 0) { /* if no pckwrk, check invsub for replens */
    [select count(*) the_sub_cnt from invsub where subucc = @subucc] 
    |	
    if(@the_sub_cnt > 0) { /*carton is rpln; vfy if it must move to correct loc*/
      [select distinct l.stoloc the_cur_loc, l.lodnum, s.subnum, 
	   am.stgflg, am.arecod the_cur_area 
         from invlod l, invsub s, invdtl d, locmst lm, aremst am
         where s.subucc = @subucc and s.subnum = d.subnum 
	   and l.lodnum = s.lodnum and l.stoloc = lm.stoloc 
	   and lm.arecod = am.arecod] 
      |
      if(@stgflg = 1) { /* do not move box if already in staging area */
        set return status where status = 80040
      } 
      |
      if(@the_cur_loc = @dstloc or @the_cur_area != 'CNVR') { 
	/* carton is in right spot, get out */
        set return status where status = 0
      } /* endif inv was picked and in right spot */
      else { 
	/* carton is NOT in right spot - we need to move it */
        move inventory where dstloc=@dstloc and srcloc=@the_cur_loc
	    and srcsub=@subnum and movref = 'PCKDVT'
            and ship_line_id='' and wrkref='' and suppress_dlytrn='YES'
	| 
	write daily transaction where oprcod = 'PCKDVT4' 
	  and movref = @subucc and tostol = @dstloc and suppress_dlytrn=''
      } /* endif inv was picked and NOT in right spot */
    } /* endif check for replen requiring movement */
    else { /* invalid subucc */
      set return status where status = 80036
    } 
  } /* endif validate the passed sub */
  else { /* dest and subucc are valid...now determine if movement is required */
    [select p.wrkref, p.wrktyp, p.srcloc, p.prtnum, p.cmbcod, p.pcksts, 
	p.pckqty-p.appqty remqty, p.ship_line_id
	from pckwrk p where p.subucc=@subucc]
    |
    if(@pcksts = 'R' and @remqty = 0) { 
	/* it was picked, see if it is in the right spot */
      if(@wrktyp = 'P')	{ /* work is a pick so find wrkref on invdtl */
        [select distinct l.stoloc the_cur_loc, l.lodnum, s.subnum, 
	   am.stgflg, am.arecod the_cur_area 
           from invlod l, invsub s, invdtl d, locmst lm, aremst am
           where d.wrkref = @wrkref
           	and s.subnum = d.subnum and l.lodnum = s.lodnum
            	and l.stoloc = lm.stoloc and lm.arecod = am.arecod] 
      }
      else { /*work is kit so we need to find wrkref on invsub */
        [select distinct l.stoloc the_cur_loc, l.lodnum, s.subnum, 
	   am.stgflg, am.arecod the_cur_area 
            from invlod l, invsub s, invdtl d, locmst lm, aremst am
            where s.wrkref = @wrkref
		and s.subnum = d.subnum and l.lodnum = s.lodnum
	        and l.stoloc = lm.stoloc and lm.arecod = am.arecod] 
      }
      |
      if(@stgflg = 1) { /* do not move box if already in staging area */
        set return status where status = 80040
      } 
      |
      if(@the_cur_loc = @dstloc or @the_cur_area != 'CNVR' ) { 
	/* carton is in right spot, get out */
        set return status where status = 0
      } /* endif inv was picked and in right spot */
      else { /* carton is NOT in right spot - we need to move it */
        move inventory where dstloc=@dstloc and srcloc=@the_cur_loc
	    and srcsub=@subnum and movref = 'PCKDVT'
            and ship_line_id='' and wrkref='' and suppress_dlytrn='YES'
	| 
	write daily transaction where oprcod = 'PCKDVT1' 
	    and movref = @subucc and tostol = @dstloc and suppress_dlytrn=''
      } /* endif inv was picked and NOT in right spot */
    } /* endif inv was picked */
    else { /* inv was NOT picked */
      if(@pcksts = 'R') { /* it was not picked, we need to do the pick */
        if(@prtnum = 'KITPART') { 
	  /* We dont do kitpicks.  These should be done by cluster picking */
          set return status where status = 80039
	}
	else { /* work is non-kit and released */
	  [select am.stgflg dstloc_stg_flg from aremst am, locmst l 
	        where l.stoloc = @dstloc and l.arecod = am.arecod] 
	  |
	  if(@dstloc_stg_flg = 1) { 
	    /* if passed dest is a staging area, must pick to cnv b4 staging*/
            publish data where final_dstloc=@dstloc
	    |
            generate next number where numcod='subnum'  
	    | 
	    move inventory where dstloc = 'CONVEYOR' and srcloc = @srcloc  
		and wrkref = @wrkref and dstlod = 'PERM-CNVR-LOD'	
		and dstsub = @nxtnum and newdst=1  
		and prtnum = @prtnum and srcqty = @remqty and ship_line_id='' 
		and suppress_dlytrn='YES'
	    | 
            move inventory  where dstloc=@final_dstloc and srcloc='CONVEYOR' 
		and srclod = @dstlod  and srcsub = @dstsub	
		and movref = 'PCKDVT' and ship_line_id='' and wrkref=''
		and dstlod='' and dstsub='' and srcdtl='' and actcod='' 
		and suppress_dlytrn='YES'
	    | 
	    write daily transaction where oprcod = 'PCKDVT2' 
		and movref = @subucc and tostol = @dstloc 
		and lodnum=@dstlod and subnum=@dstsub 
	    	and suppress_dlytrn=''
	  } /* endif passed dest was a staging area */
	  else { /* when passed dest is NOT a staging area */
	    if (@dstloc='CONVEYOR') {
	      generate next number where numcod='subnum' 
	      | 
	      move inventory where dstloc = @dstloc and srcloc = @srcloc  
		and wrkref = @wrkref 
		and dstlod = 'PERM-CNVR-LOD' and dstsub=@nxtnum and newdst=1 
		and prtnum = @prtnum and srcqty = @remqty and ship_line_id='' 
		and suppress_dlytrn='YES'
	    } /* endif check for destination of conveyor */	
	    else { 
	      generate next number where numcod='lodnum' 
	      | 
	      move inventory where dstloc = @dstloc and srcloc = @srcloc  
		and wrkref = @wrkref and dstlod = @nxtnum and newdst=1 
		and prtnum = @prtnum and srcqty = @remqty and ship_line_id='' 
		and suppress_dlytrn='YES'
	    } /* end else block for non-conveyor */
	    | 
	    write daily transaction where oprcod = 'PCKDVT3' 
		and movref = @subucc and tostol = @dstloc 
		and lodnum=@dstlod and subnum=@dstsub and suppress_dlytrn=''
	    | 
	    if (@wrktyp='R' and @remqty>0 and @ship_line_id is null) { 
	      /* when replen comes thru we must stamp subucc*/
	      [update invsub set subucc=@subucc 
		   where subnum=@dstsub and lodnum=@dstlod]
            } /* endif stamp subucc for replen */
	  } /* endif passed dest was NOT a staging area */
	} /* endif NOT kit work and it is released */
      } /* endif check for work to be released -- valid status */
      else { /* cannot do pick - it has an invalid status */
        set return status where status = 80038
      }
    } /* endif inventory was NOT picked */
  } /* endif scanned subucc and passed dest are both valid */
} /* endif passed dest is valid */
else {  /* invalid passed dest loc */
  set return status where status = 80037
}

]]>
</local-syntax>
</command>
