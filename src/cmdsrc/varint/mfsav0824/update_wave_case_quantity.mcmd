<command>
<name>update wave case quantity</name>
<description>Update Wave Case Quantity</description>
<type>C Function</type>
<function>varUpdateWaveCaseQuantity</function>
<argument name="ship_id" datatype="string">
</argument>
<documentation>
<param name="ship_id">Shipment Id created in the wave planning process.</param>
<remarks>
<![CDATA[
  <p>
  This command is used to update the estimated case quantity that will be
  required for a shipment.  The "get shipment pick estimate" command is used
  to get the basic cases that will be delivered and "process cartonization" 
  will be used to estimate the number of cartons that will be created.
  </p>
  <p>
  The command is generally triggered off the "postconsolidate shipment lines"
  command which is called at the end of planning a wave.  </p>
]]>
</remarks>
<retcol name="ship_line_id" type="COMTYP_CHAR">The waveable shipment line that has had wave case quantity assigned to it.</retcol>
<retcol name="vc_wavcas_qty" type="COMTYP_FLOAT">The estimated number of cases that will be delivered as part of this
    shipment line.</retcol>
<exception value="eOK">Normal successful completion</exception>
<seealso cref="postconsolidate shipment lines">
</seealso>
<seealso cref="list waveable shipment lines">
</seealso>
<seealso cref="get shipment pick estimate">
</seealso>
<seealso cref="process cartonization">
</seealso>
</documentation>
</command>
