<command>

<name>list staged assigned shipments for ship dock</name>

<description>List Staged Assigned Shipments For Ship Dock</description>

<type>Local Syntax</type>

<local-syntax>

<![CDATA[
    {
    list shipment staging areas
    |
    [select distinct invlod.stoloc stoloc, ssv.ship_id, 
            ord.stcust, adrmst.adrnam, ord.cponum, ord.rtcust, adrmst2.adrnam rtadrnam, ssv.doc_num, 
            ssv.carcod, ssv.srvlvl,ssv.track_num,  ssv.shpsts, 
            ord.carflg, ord.client_id, ord.ordnum, 
            ssv.stop_id, 
            ssv.stop_nam,
            ssv.stop_seq,
            ssv.car_move_id,
            ssv.trlr_num,
            ssv.trlr_id,
            ssv.yard_loc,
            ssv.early_dlvdte, 
            ssv.late_dlvdte, ssv.early_shpdte, 
            ssv.late_shpdte 
       from ord, 
            adrmst, adrmst adrmst2, locmst, invlod, invsub, invdtl, 
            shipment_line,
            ship_struct_view ssv,
            cardtl
      where ssv.ship_id        = shipment_line.ship_id 
        and shipment_line.client_id = ord.client_id 
        and shipment_line.ordnum    = ord.ordnum
        and ssv.carcod         = cardtl.carcod
        and ssv.srvlvl         = cardtl.srvlvl
        and cardtl.cartyp <> 'S' 
        and ssv.shpsts        in ('S','L', 'X') 
        and ssv.stgdte    is not null 
        and ssv.super_ship_flg = 0
        and ord.st_adr_id           = adrmst.adr_id 
        and ord.rt_adr_id           = adrmst2.adr_id 
        and invdtl.ship_line_id     = shipment_line.ship_line_id 
        and invdtl.subnum           = invsub.subnum 
        and invsub.lodnum           = invlod.lodnum
        and invlod.stoloc           = locmst.stoloc
        and ssv.stop_id         is not null
        and @+ssv.doc_num
        and @+ssv.track_num
        and @+ssv.trlr_id
        and @+locmst.arecod 
        and @+ssv.ship_id
        and @+ssv.stop_id
        and @+ssv.car_move_id
        and @+invlod.lodnm
        and @+invdtl.prtnum
        and @+invdtl.prt_client_id
        and @+ord.ordnum
        and @+ord.client_id
        and @+locmst.stoloc
        and @+ssv.carcod
        and @+ssv.srvlvl
        and @+ssv.late_dlvdte:date
        and @+ssv.early_dlvdte:date
        and @+ssv.late_shpdte:date
        and @+ssv.early_dlvdte:date
        and @* 
      order by 1,2] catch (-1403) 
    }  >> res   /* Save results to res (even if No Rows)     */
    |
   [select distinct '' stoloc, ssv.ship_id, 
           ord.stcust, adrmst.adrnam, ord.cponum, ord.rtcust, adrmst2.adrnam rtadrnam, 
		ssv.doc_num, ssv.carcod, 
           ssv.srvlvl, ssv.track_num, ssv.shpsts, 
           ord.carflg, ord.client_id, ord.ordnum, 
           ssv.stop_id,
           ssv.stop_nam,
           ssv.stop_seq,
           ssv.car_move_id,
           ssv.trlr_num,
           ssv.trlr_id,
           ssv.yard_loc,
           ssv.early_dlvdte, 
           ssv.late_dlvdte, ssv.early_shpdte, 
           ssv.late_shpdte 
      from ord_line, 
           ord, 
           adrmst adrmst2, 
           adrmst, 
           shipment_line, 
           ship_struct_view ssv,
           cardtl 
     where not exists (select 'x'  
                         from aremst, 
                              locmst, invlod, 
                              invsub, 
                              invdtl 
                        where invdtl.ship_line_id = 
                                shipment_line.ship_line_id 
                      and invsub.subnum = invdtl.subnum 
                      and invlod.lodnum = invsub.lodnum
                      and locmst.stoloc = invlod.stoloc 
                      and aremst.arecod = locmst.arecod 
                      and aremst.shpflg = 0 ) 
       and ssv.ship_id        = shipment_line.ship_id 
       and ssv.carcod         = cardtl.carcod
       and ssv.srvlvl         = cardtl.srvlvl
       and cardtl.cartyp <> 'S'
       and ssv.stgdte    is not null 
       and ssv.stop_id is not null
       and ssv.super_ship_flg = 0
       and shipment_line.client_id = ord.client_id 
       and shipment_line.ordnum    = ord.ordnum 
       and shipment_line.ordnum    = ord_line.ordnum 
       and shipment_line.client_id = ord_line.client_id 
       and shipment_line.ordlin    = ord_line.ordlin 
       and shipment_line.ordsln    = ord_line.ordsln 
       and ord_line.non_alc_flg    = 0                
       and ssv.shpsts        in ('S','L', 'X') 
       and ord.st_adr_id           = adrmst.adr_id 
       and ord.rt_adr_id           = adrmst2.adr_id 
       and @+ssv.trlr_id
       and @+ssv.ship_id
       and @+ssv.stop_id
       and @+ssv.car_move_id
       and @+invlod.lodnm
       and @+invdtl.prtnum
       and @+invdtl.prt_client_id
       and @+ord.client_id
       and @+ord.ordnum
       and @+ssv.carcod
       and @+ssv.srvlvl
       and @+ssv.late_dlvdte:date
       and @+ssv.early_dlvdte:date
       and @+ssv.late_shpdte:date
       and @+ssv.early_dlvdte:date
       and @+ssv.track_num
       and @+ssv.doc_num
       and @*
  order by 1, 2] catch(-1403) >> res2 /* Save results to res (even if No Rows)*/
    |
    /* Now we combine the results from the above two selects into one result. */
    publish data combination
    where res = @res
      and res2 = @res2 >> res3
    | 
    if (@res3) 
       /* Now publish the above results w/ deferred flag appended.
        * Status (@?) will be either eOK or NO_ROWS from above. We need this
        * because the framework will send a 1=2 in the where clause to
        * insure a NO ROWS but still needs to get the column headers.
        * We do the catch above because if we let it go, the framework would
        * not see (and then not show) the deferred flag.
        */
        append shipment change deferred flag 
        where res = @res3 
          and status = @? catch(-1403) /* This will be either eOK or NO_ROWS */

]]>
</local-syntax>

<documentation>
<remarks>
<![CDATA[
  <p>
  This command is used to list the shipments by carrier move. 
  It's primary purpose is for use by Ship Dock Operations, to allow users
  to see all shipments that area assigned to carrier moves, in the areas
  designated for use by Ship Dock Operations
  </p>
  <p>
  This command will show staged shipments in only those areas that are defined
  for use by Ship Dock Operations since this purpose of this command is
  for use by Ship Dock Operatios.
  llThis command assumes that the shipping strucutre has been defined for
  the shipment.
  </p>
  <p>
  Plese note the following:
     <li>Need to use numerics in the order by to support sql Server.</li>
     <li>We need to look at the stage date in addition to the status.  As of
         6.1, is it possible to have a shipment in a loading status whose 
         stage date is not set (due to fluid loading), so we still have
         to make sure the stage date is null. </li>

    <li>We need to exclude non-allocatable order lines.  Since they have
        a stgdte and stgqty, they "appear" like a standard order line for
        which the inventory does not currently exist in the staging area
        unless we exclude them.</li>

    <li>We need to exclude any shipments whose carrier has a carrier type
        of Small Package.  Manifest problems arise when a small package
        shipment gets closed from ship dock operations.</li>
  </p>

  <p>
  The second half of the union handles the situation where a shipment
  may have at one time been staged, but there is currently
  no inventory in the staging area.
  For example, we have have an order that doesn't allow partials,
  but only part of it was picked, then the other pick was cancelled,
  so it got staged and loaded (may have been fluid loaded).
  Without the union, we would have no visibility to that shipment
  since there is currently no inventory in the staging lane.
  </p>

  <p>
  After all of that has been done, this command publishes the results from
  the initial selects and then appends the results with the deferred flag.
  The framework will initially call this command with a 1=2 in the where
  clause to insure NO ROWS in the data, but still expects to get the column
  headers.  We do the catch above because if we let it go the command would
  terminiate there and the framework would not see (and then not show) the
  deferred flag.  To make this work, it will do the catch on a NO ROWS, but
  then publish the status (@?), which will be either eOK or NO_ROWS from above.
  </p>

]]>
</remarks>

<exception value="eOK">Normal successful completion</exception>
<exception value="eDB_NO_ROWS_AFFECTED">Specified shipment not found</exception>

<seealso cref="list shipment staging areas"> </seealso>
<seealso cref="list staged unassigned shipments for ship dock"> </seealso>
<seealso cref="append shipment change deferred flag"> </seealso>

</documentation>
</command>
