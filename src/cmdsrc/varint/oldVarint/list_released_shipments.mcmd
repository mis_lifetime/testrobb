<command>

<name>list released shipments</name>

<description>List Released Shipments</description>

<type>Local Syntax</type>

<local-syntax>

<![CDATA[

 if (!@ordnum and @client_id)
    hide stack variable where name  = 'client_id'
|
list var released shipments where @*
]]>
</local-syntax>

<documentation>
<remarks>
<![CDATA[
  <p>
  This command is used to list the shipments that are allocated and released,
  but not yet staged.
  It's primary purpose is for use by Ship Dock Operations, to allow users
  to see what quantity still needs to be picked/delivered to the staging area.
  </p>
  <p>
  This command will show staged shipments in only those areas that are defined
  for use by Ship Dock Operations since this purpose of this command is
  for use by Ship Dock Operatios.
  </p>
  <p>
  Plese note the following:
     <li>Need to use numerics in the order by to support sql Server.</li>
     <li>We need to look at the stage date in addition to the status.  As of
         6.1, is it possible to have a shipment in a loading status whose 
         stage date is not set (due to fluid loading), so we still have
         to make sure the stage date is null. </li>
  </p>

  <p>
  The second half of the union handles the situation where a shipment
  may have at one time been staged, but there is currently
  no inventory in the staging area.
  For example, we have have an order that doesn't allow partials,
  but only part of it was picked, then the other pick was cancelled,
  so it got staged and loaded (may have been fluid loaded).
  Without the union, we would have no visibility to that shipment
  since there is currently no inventory in the staging lane.
  </p>

  <p>
   We need to make sure we are looking at shipments whose stgdte
   is null, rather than the shipment status.  Previously, we were
   looking at shipments with an inprocess status.  However, with
   fluid loading, as of 6.1, it is possible for shipments to have
   a loading status when they haven't been staged yet.
  </p>

  <p>
  The second half of the union is dealing with cross-dock records.
  We want to reflect as remaining qty any unallocated cross-dock records.
  </p>

  <p>
  The quantities are calculated as follows:
     <li>Pick Qty - the actual pick qty of the shipment.  However, the
         pick qty reflects only those shipments that have actually been
         released.  That means, if a shipment is in a held status, it's
         pick qty will actually be reflected in the remqty (un-released
         qty).</li>
     <li>Applied Qty - the actual picked qty of the shipment</li>
     <li>Arrived Qty - the actualy picked qty of the shipment that has
         arrived in the staging lane.</li>
     <li>Unreleased Qty - the pick qty of any shipments that are in 
         a Held or Pending status.</li>
    </p>

]]>
</remarks>

<exception value="eOK">Normal successful completion</exception>
<exception value="eDB_NO_ROWS_AFFECTED">Specified shipment not found</exception>

<seealso cref="list shipment staging areas"> </seealso>
<seealso cref="list staged unassigned shipments for ship dock"> </seealso>
<seealso cref="append shipment change deferred flag"> </seealso>

</documentation>
</command>
