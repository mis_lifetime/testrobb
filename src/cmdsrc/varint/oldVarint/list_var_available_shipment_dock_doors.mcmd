<command>
<name>list var available shipment dock doors</name>
<description>List Available Shipment Dock Doors</description>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[
/*
     * If the trailer id is passed in and it's
     * already at a valid shipment dock door,
     * we'll go ahead and include it in the 
     * list so we can default this in the
     * "load trailer" form of Ship Dock Opr.  
     */
    if (@trlr_id)
    {
    	list trailers 
	    where trlr_id = @trlr_id catch (@?)
	|
	list shipment dock doors
	    where stoloc = @yard_loc 
              and locsts = 'E'
              and useflg = 1 catch (@?)
	|
	[select locmst.stoloc 
       	   from locmst, aremst
      	  where aremst.arecod = locmst.arecod 
            and aremst.shp_dck_flg = '1'
            and @+locmst.cntdte:date
            and @+locmst.lstdte:date
            and locmst.locsts = 'E'
            and locmst.useflg = 1
            and @* 
          union
      	 select @yard_loc 
	   from dual]
    }
    else
    {
	[select locmst.stoloc 
       	   from locmst, aremst
      	  where aremst.arecod = locmst.arecod 
            and aremst.shp_dck_flg = '1'
            and @+locmst.cntdte:date
            and @+locmst.lstdte:date
            and locmst.useflg = 1
            and @*
	    order by locmst.stoloc] 
    }
]]>
</local-syntax>
<documentation>
<remarks>
<![CDATA[
  <p>
  This command is used to list the dock doors that are available to assign
  a new stop (or trailer) for loading.  Essentially, it returns all
  shipping dock doors that do not have another trailer currently at that
  location.
  </p>

  <p>
  This command is designed for use by the lookups on the load trailer
  form, to give the user a list of available dock doors to assign
  the trailer to.
  </p>
]]>
</remarks>
<exception value="eOK">Normal successful completion</exception>
<seealso cref="list shipment dock doors">
</seealso>
<seealso cref="list receiving dock doors">
</seealso>
<seealso cref="list destination yard locations">
</seealso>
</documentation>
</command>
