<command>
<name>process var deassign cfpp pickface</name>
<description>process var deassign cfpp pickface</description>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[
/* Retrieve the original tower pick face information for logging. */
[select rplcfg.stoloc
   from rplcfg,
        locmst
  where rplcfg.arecod = locmst.arecod
    and rplcfg.stoloc = locmst.stoloc
    and locmst.vc_pipflg = 1
    and rplcfg.arecod = 'CFPP'
    and rplcfg.prt_client_id = @prt_client_id
    and rplcfg.prtnum = @prtnum] catch(-1403) >>res
|
if (@? = 0)
{
    convert column results to string
      where resultset = @res
        and colnam = 'stoloc'
    |
    publish data where frstol_dly = @result_string
}
else
{
    publish data where frstol_dly = 'None'
}
|
save session variable where name = 'frstol_dly' and value = @frstol_dly
|
[select ceil(r.maxunt/p.untcas)*p.untcas old_maxunt
   from rplcfg r
   join prtmst p
     on p.prt_client_id = r.prt_client_id
    and p.prtnum = r.prtnum
   join locmst l
     on l.arecod = r.arecod
    and l.stoloc = r.stoloc
  where l.vc_pipflg = 1
    and r.arecod = 'CFPP'
    and r.prt_client_id = @prt_client_id
    and r.prtnum = @prtnum] catch(-1403)
|
save session variable
    where name = 'old_maxunt'
      and value = nvl(@old_maxunt, 0)
;
/* Update the part family. */
[select nvl(@to_prtfam, prtfam) to_prtfam
   from prtmst
  where prt_client_id = @prt_client_id
    and prtnum = @prtnum]
|
[update prtmst
    set prtfam = @to_prtfam
  where prt_client_id = @prt_client_id
    and prtnum = @prtnum]
|
save session variable
    where name = 'to_prtfam'
      and value = @to_prtfam
;
/* Remove the replenishment configurations. */
[select r.prtnum,
        r.prt_client_id,
        r.arecod,
        r.stoloc,
        r.invsts
   from rplcfg r
   join locmst l
     on l.arecod = r.arecod
    and l.stoloc = r.stoloc
  where l.vc_pipflg = 1
    and r.arecod = 'CFPP'
    and r.prt_client_id = @prt_client_id
    and r.prtnum = @prtnum] catch (-1403)
|
if (@? = 0)
{
    remove assigned location policy
     where prtnum= @prtnum
       and prt_client_id = @prt_client_id
       and begloc = @stoloc
       and endloc = @stoloc
       and invsts = @invsts catch(@?)
    |
    [update locmst
        set repflg = 0, stoflg = 0, vc_pipflg = 0
      where arecod = @arecod
        and stoloc = @stoloc]
    |
    remove replenishment configuration
     where prtnum = @prtnum
       and prt_client_id = @prt_client_id
       and arecod = @arecod
       and stoloc = @stoloc
       and invsts = @invsts
}
;
/* Move the inventory out of CFPP pick face if needed. */
if (@movflg = 1)
{
    get session variable
        where name = 'to_prtfam'
    |
    publish data
      where prtfam = @value
    |
    [select i.stoloc srcloc,
            i.prtnum,
            i.prt_client_id,
            nvl(i.untqty-i.comqty, 0) untqty,
            a.bldg_id
       from invsum i
       join locmst l
         on l.arecod = i.arecod
        and l.stoloc = i.stoloc
       join aremst a
         on a.arecod = i.arecod
      where l.vc_pipflg = 0
        and i.arecod = 'CFPP'
        and i.prt_client_id = @prt_client_id
        and i.prtnum = @prtnum] catch (-1403)
    |
    if (@? = 0)
    {
        [select lodnum,
                invsts,
                orgcod,
                revlvl,
                lotnum,
                untcas,
                untpak,
                sum(untqty) untqty
           from inventory_view
          where stoloc = @srcloc
          group by lodnum,
                   invsts,
                   orgcod,
                   revlvl,
                   lotnum,
                   untcas,
                   untpak] catch(@?)
        |
        if (@? = 0)
        {
            allocate location
               where cur_bldg_id = @bldg_id catch (@?)
            |
            if(@? != 0)
            {
                set return status where status = @?
            }
            |
            create work
                where oprcod = 'STO'
                  and srcloc = @srcloc
                  and dstloc = @nxtloc
            |
            /* Log transaction to indicate erroring the rack locations */
            [insert into dlytrn(oprcod,
                                actcod,
                                lodnum,
                                prtnum,
                                frstol,
                                tostol,
                                adj_ref2,
                                usr_id,
                                devcod,
                                trndte)
                          values('ERR-RACK',
                                 'ERR-RACK',
                                 @lodnum,
                                 @prtnum,
                                 @srcloc,
                                 @nxtloc,
                                 'N',
                                 @@usr_id,
                                 @@devcod,
                                 sysdate)]
            |
            /* We need to use the following attributes of source location. */
            [select z.wrkare new_srcwrkare,
                    z.wrkzon new_srcwrkzon,
                    z.trvseq new_zontrvseq,
                    l.trvseq new_loctrvseq,
                    l.locacc new_locacc
               from locmst l,
                    zonmst z
              where z.wrkzon = l.wrkzon
                and l.stoloc = @srcloc]
            |
            [update wrkque
                set srcwrkare = @new_srcwrkare,
                    srcwrkzon = @new_srcwrkzon,
                    zontrvseq = @new_zontrvseq,
                    loctrvseq = @new_loctrvseq,
                    locacc = @new_locacc
              where reqnum = @reqnum]
        }
    }
}
;
/* log the daily transaction for de-assign decision. */
get session variable
    where name = 'frstol_dly'
|
publish data
  where frstol_dly = @value
|
get session variable
    where name = 'old_maxunt'
|
publish data
  where old_maxunt = @value
|
get session variable
    where name = 'to_prtfam'
|
publish data
  where to_prtfam = @value
|
[insert into dlytrn(oprcod, 
                    actcod, 
                    prtnum, 
                    prt_client_id, 
                    frstol, 
                    tostol, 
                    fr_arecod, 
                    to_arecod, 
                    lodnum,
                    to_lodnum,
                    adj_ref1,
                    adj_ref2,
                    usr_id,
                    devcod,
                    trndte)
             values('P-SLOT', 
                    'REMOVE', 
                    @prtnum, 
                    @prt_client_id, 
                    @frstol_dly, 
                    '', 
                    'CFPP', 
                    '', 
                    @old_maxunt,
                    0,
                    decode(@movflg, '1', '1', '0'),
                    @to_prtfam,
                    @@usr_id,
                    @@devcod,
                    sysdate)]
]]>
</local-syntax>
</command>