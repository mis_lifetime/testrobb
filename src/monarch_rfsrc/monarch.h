#include <rf_format.h>

/* RF Terminal Settings */
#define FORMAT_20X4				"Y"
#define	MONARCH_TERMINAL_LEFT			1
#define MONARCH_TERMINAL_TOP			1
#define MONARCH_TERMINAL_WIDTH			20
#define MONARCH_TERMINAL_HEIGHT			4
#define LBLSTK_LEN				20
#define VERSTK_LEN				1
#define RF_LANGUAGE_CODE			"EN"
#define eINT_INVALID_UCCLBL			10197

/* Function Keys */
#define FKEY_BACK				F1
#define FKEY_BACK_QUOTED			"F1"
#define FKEY_GEN				F3
#define FKEY_GEN_QUOTED			        "F3"
//LH Start MR1926 03/16/02 Ira Laksmono Changed the message from F6 to F4.
#define	FKEY_DONE				F4
#define FKEY_DONE_QUOTED			"F4"
//LH End  MR1926 03/16/02 Ira Laksmono 
#define FKEY_HELP				F10
#define FKEY_HELP_QUOTED			"F10"
#define FKEY_ADJ				F2
#define FKEY_ADJ_QUOTED				"F2"
#define FKEY_ADJ_CAPTION			"Adjust"
//LH Start MR1926 03/16/02 Ira Laksmono Commented out F4 mapping.
//#define FKEY_RPR				F4
//#define FKEY_RPR_QUOTED				"F4"
//#define FKEY_RPR_CAPTION			"Reprint"
//LH End  MR1926 03/16/02 Ira Laksmono 

#define ALL_FORMS_CAPTION			"DCS Ver 5.1"
#define ALL_FORMS_FIELD_HEIGHT			1
#define ALL_FORMS_LOCAL_DATA_LEFT		1
#define ALL_FORMS_LOCAL_DATA_TOP		1  

#define ALL_FORMS_MSG_PROCESSING		"monProcessing",        "Processing..." 
#define ALL_FORMS_MSG_LOGOUT			"monLogout",		"Logout"
#define ALL_FORMS_MSG_BADLABEL			"monBadLabel",		"Bad label format"
#define ALL_FORMS_MSG_LBL_PRINTED		"monLblPrinted",	"Label Printed"

#define LOGIN_FORM_HELPTEXT			"Enter EmpID, Passwd"
#define LOGIN_FORM_LABEL1_CAPTION		ver, "DCS Ver 5.1"
#define LOGIN_FORM_LABEL1_TAG			ver
#define LOGIN_FORM_LABEL1_LEFT			1
#define LOGIN_FORM_LABEL1_TOP			0
#define LOGIN_FORM_LABEL1_WIDTH			11
#define LOGIN_FORM_LABEL1_JUSTIFY		Left
#define LOGIN_FORM_LABEL2_CAPTION		usr_id, "user id:"
#define LOGIN_FORM_LABEL2_TAG                   usr_id
#define LOGIN_FORM_LABEL2_LEFT			1
#define LOGIN_FORM_LABEL2_TOP			1
#define LOGIN_FORM_LABEL2_WIDTH                 8
#define LOGIN_FORM_LABEL2_JUSTIFY               Left   
#define LOGIN_FORM_EMPNUM_TAG                   usr_id
#define LOGIN_FORM_EMPNUM_LEFT			9
#define LOGIN_FORM_EMPNUM_TOP			1
#define LOGIN_FORM_LABEL3_CAPTION		passwd, "Passwd:"
#define LOGIN_FORM_LABEL3_TAG                   passwd
#define LOGIN_FORM_LABEL3_LEFT			1
#define LOGIN_FORM_LABEL3_TOP			2
#define LOGIN_FORM_LABEL3_WIDTH                 7
#define LOGIN_FORM_LABEL3_JUSTIFY               Left
#define LOGIN_FORM_PASSWD_TAG                   passwd
#define LOGIN_FORM_PASSWD_LEFT			9
#define LOGIN_FORM_PASSWD_TOP			2
#define LOGIN_FORM_MSG_BAD_DATA			"monBadlogin", "Bad Login "

#define CTNLBL_FORM_HELPTEXT			"Enter UCC Number"
#define CTNLBL_FORM_LABEL1_CAPTION		ucc, "UCC:"
#define CTNLBL_FORM_LABEL1_TAG			ucc
#define CTNLBL_FORM_LABEL1_LEFT			1
#define CTNLBL_FORM_LABEL1_TOP			0
#define CTNLBL_FORM_LABEL1_WIDTH		4
#define CTNLBL_FORM_LABEL1_JUSTIFY		Left
#define CTNLBL_FORM_UCC_TAG			ucc
#define CTNLBL_FORM_UCC_LEFT			1
#define	CTNLBL_FORM_UCC_TOP			1
#define CTNLBL_FORM_INVALID_UCC			"monInvalidLabel",		"Invalid Label"
#define CTNLBL_FORM_NO_TICKET_RQRD		"monNoTicketRequired",		"No Ticketing Rqrd"
#define CTNLBL_FORM_INVALID_LOC			"monInvalidLoc",		"Ctn not in valid loc"

#define VERSTK_FORM_HELPTEXT			"Verify Label Stock"
#define VERSTK_FORM_VERSTK_TAG			lblstk
#define VERSTK_FORM_VERSTK_LEFT			1
#define VERSTK_FORM_VERSTK_TOP			1
#define VERSTK_FORM_VERIFY_CAPTION		ververify, "Verify Label Stock"
#define VERSTK_FORM_VERIFY_TAG			ververify
#define VERSTK_FORM_VERIFY_LEFT			1
#define VERSTK_FORM_VERIFY_TOP			0
#define VERSTK_FORM_VERIFY_WIDTH		18
#define VERSTK_FORM_VERIFY_JUSTIFY		Left
#define VERSTK_FORM_NO_MESSAGE			"monNoMessage",			""

#define UPCLBL_FORM_CAPTION			"UPC:"
#define UPCLBL_FORM_HELPTEXT			"Enter UPC"
#define UPCLBL_FORM_LABEL1_CAPTION		upc, "UPC:"
#define UPCLBL_FORM_LABEL1_TAG			upc
#define UPCLBL_FORM_LABEL1_LEFT			1
#define UPCLBL_FORM_LABEL1_TOP			0
#define UPCLBL_FORM_LABEL1_WIDTH		4
#define UPCLBL_FORM_LABEL1_JUSTIFY		Left
#define UPCLBL_FORM_UPC_TAG			upc
#define UPCLBL_FORM_UPC_LEFT			1
#define UPCLBL_FORM_UPC_TOP			1
#define UPCLBL_FORM_INVALID_UPC			"monInvalidUpc",		"Invalid UPC Number"
#define UPCLBL_FORM_NOT_VALID			"monNotValid",			"UPC Invalid for Lbl"
#define UPCLBL_FORM_MOVE_COMPLETE		"monMoveComplete",		"Carton Move Completed"

//kzainal - 01/15/02 - PF000178 - label, message and position definition for DEPTKT_FORM.
#define DEPTKT_FORM_HELPTEXT			"Enter Destination"
#define DEPTKT_FORM_LABEL1_CAPTION		klodnum, "Load:"
#define DEPTKT_FORM_LABEL1_TAG			klodnum
#define DEPTKT_FORM_LABEL1_LEFT			1
#define DEPTKT_FORM_LABEL1_TOP			1
#define DEPTKT_FORM_LABEL1_WIDTH		5
#define DEPTKT_FORM_LABEL1_JUSTIFY		Left
#define DEPTKT_FORM_KLODNUM_TAG			klodnum
#define DEPTKT_FORM_KLODNUM_LEFT		6
#define	DEPTKT_FORM_KLODNUM_TOP			1
#define DEPTKT_FORM_LABEL2_CAPTION		kdstloc, "Loc :"
#define DEPTKT_FORM_LABEL2_TAG			kdstloc
#define DEPTKT_FORM_LABEL2_LEFT			1
#define DEPTKT_FORM_LABEL2_TOP			2
#define DEPTKT_FORM_LABEL2_WIDTH		5
#define DEPTKT_FORM_LABEL2_JUSTIFY		Left
#define DEPTKT_FORM_KDSTLOC_TAG			kdstloc
#define DEPTKT_FORM_KDSTLOC_LEFT		6
#define	DEPTKT_FORM_KDSTLOC_TOP			2
#define DEPTKT_FORM_INCOMPLETE_TKT		"monTicketIncomplete",		"Ticketing Incomplete"
#define DEPTKT_FORM_INCOMPLETE_TKT2		"monTicketIncomplete2",		"Print more Tickets"
#define DEPTKT_FORM_INVALID_LOC			"monInvalidTktLoc",		"Not a ticketing Loc"
#define DEPTKT_FORM_INVALID_LOD			"monInvalidTktLod",		"Invalid Load"
//LH Start MR1926 03/16/02 Ira Laksmono Changed the message from F6 to F4.
#define DEPTKT_FORM_MOVE_COMPLETE		"monTktMoveComplete",		"Hit F4 to Dispatch"

//kzainal - 01/15/02 - PF000178 - label, message and position definition for DSPTCH_FORM.
#define DSPTCH_FORM_HELPTEXT			"Enter Destination"
#define DSPTCH_FORM_TITLBL_CAPTION		titlbl, "Dispatch Load:"
#define DSPTCH_FORM_TITLBL_TAG			titlbl
#define DSPTCH_FORM_TITLBL_LEFT			3
#define DSPTCH_FORM_TITLBL_TOP			0
#define DSPTCH_FORM_TITLBL_WIDTH		14
#define DSPTCH_FORM_TITLBL_JUSTIFY		Left
#define DSPTCH_FORM_LABEL1_CAPTION		dlodnum, "Load:"
#define DSPTCH_FORM_LABEL1_TAG			dlodnum
#define DSPTCH_FORM_LABEL1_LEFT			1
#define DSPTCH_FORM_LABEL1_TOP			1
#define DSPTCH_FORM_LABEL1_WIDTH		5
#define DSPTCH_FORM_LABEL1_JUSTIFY		Left
#define DSPTCH_FORM_DLODNUM_TAG			dlodnum
#define DSPTCH_FORM_DLODNUM_LEFT		6
#define	DSPTCH_FORM_DLODNUM_TOP			1
#define DSPTCH_FORM_DISPATCH 		        "monDispatchLoad",		""
#define DSPTCH_FORM_DISPATCH_COMPLETE	        "monDispatchLoad",		"Load Dispatched"
