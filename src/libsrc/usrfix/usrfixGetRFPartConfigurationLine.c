/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999 Software Architects, Inc.
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>  

#include "rflib.h"

typedef struct
{
    char pcfkey    [PCFKEY_LEN + 1];
    char client_id [CLIENT_ID_LEN + 1];
    char supnum	   [SUPNUM_LEN + 1];
    char prvpcflin [PCFLIN_LEN + 1];
    char wh_id [WH_ID_LEN + 1];
} ARG;

LIBEXPORT
RETURN_STRUCT *usrfixGetRFPartConfigurationLine (char *pcfkey_i, 
                                                 char *client_id_i,
                                                 char *supnum_i, 
                                                 char *prvpcflin_i, 
                                                 char *wh_id_i)
{
    RETURN_STRUCT *returnData;

    mocaDataRes   *resultSet;
    mocaDataRow   *resultRow;

    char          buffer [5000];

    long          index;
    long          status;

    char           pcfkey[PCFKEY_LEN + 1];
    char           supnum[SUPNUM_LEN + 1];
    char 	   prvpcflin[PCFLIN_LEN + 1];
    char           client_id[CLIENT_ID_LEN + 1];
    char	   wh_id[WH_ID_LEN + 1];


    /* Initialize internal structures */

    memset(pcfkey, 0, sizeof(pcfkey));
    memset(client_id, 0, sizeof(client_id));
    memset(supnum, 0, sizeof(supnum));
    memset(wh_id, 0, sizeof(wh_id));
    memset(prvpcflin, 0, sizeof(prvpcflin));


    /* Verify required arguments are present */

    if (pcfkey_i && misTrimLen(pcfkey_i, PCFKEY_LEN))
	misTrimcpy(pcfkey, pcfkey_i, PCFKEY_LEN);
    else	
        return (APPMissingArg("pcfkey"));

    if (client_id_i && misTrimLen(client_id_i, CLIENT_ID_LEN))
	misTrimcpy(client_id, client_id_i, CLIENT_ID_LEN);
    else	
        return (srvSetupReturn(eSRV_INSUFF_ARGUMENTS, ""));

    if (supnum_i && misTrimLen(supnum_i, SUPNUM_LEN))
	misTrimcpy(supnum, supnum_i, SUPNUM_LEN);
    else	
        return (APPMissingArg("supnum"));

    if (wh_id_i && misTrimLen(wh_id_i, WH_ID_LEN))
	misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);
    else	
        return (APPMissingArg("wh_id"));

    /* Verify if any optional arguments are present */

    if (prvpcflin_i && misTrimLen(prvpcflin_i, PCFLIN_LEN))
	misTrimcpy(prvpcflin, prvpcflin_i, PCFLIN_LEN);

    /* Format SQL query */

    index = sprintf (buffer,
        "select "
        "    ph.pcfkey, ph.client_id, ph.wh_id, ph.supnum,  pd.pcflin,  ph.pcftyp, "
        "    pd.prtnum, pd.prt_client_id, pd.lotnum,  pd.sup_lotnum, pd.orgcod,  pd.revlvl, "
        "    pd.invsts, pd.rcvqty, "
        "    pd3.untqty untpak, "
        "    nvl (pd2.untqty, 1) untcas, "
        "    pd1.untqty untpal, "
        "    pf.ftpcod ftpcod, "
        "    pd4.uomcod rcvuom, "
        "    pm.lodlvl, pm.stkuom, pm.lotflg, pm.sup_lot_flg, pm.orgflg, pm.revflg, "
        "    pm.rcvflg, pm.subcfg, pm.scfpos, pm.dtlcfg, pm.dcfpos "
        "from pcfhdr ph "
        "join pcfdtl pd "
        "  on pd.pcfkey = ph.pcfkey "
        " and pd.client_id = ph.client_id "
        " and pd.supnum = ph.supnum "
        " and pd.wh_id = ph.wh_id "
        "join prtmst_view pm "
        "  on pm.prtnum = pd.prtnum "
        " and pm.prt_client_id = pd.prt_client_id "
        " and pm.wh_id = pd.wh_id "
        "left join prtftp pf on "
        "      pf.wh_id         = pm.wh_id         and "
        "      pf.prtnum        = pm.prtnum        and "
        "      pf.prt_client_id = pm.prt_client_id and "
        "      pf.ftpcod        = pd.ftpcod            "
        "left join prtftp_dtl pd1 on "
        "      pd1.prtnum       = pf.prtnum        and "
        "      pd1.prt_client_id= pf.prt_client_id and "
        "      pd1.wh_id        = pf.wh_id         and "
        "      pd1.ftpcod       = pf.ftpcod        and "
        "      pd1.pal_flg      = 1                    "
        "left join prtftp_dtl pd2 on "
        "      pd2.prtnum       = pf.prtnum        and "
        "      pd2.prt_client_id= pf.prt_client_id and "
        "      pd2.wh_id        = pf.wh_id         and "
        "      pd2.ftpcod       = pf.ftpcod        and "
        "      pd2.cas_flg      = 1                    "
        "left join prtftp_dtl pd3 on "
        "      pd3.prtnum       = pf.prtnum        and "
        "      pd3.prt_client_id= pf.prt_client_id and "
        "      pd3.wh_id        = pf.wh_id         and "
        "      pd3.ftpcod       = pf.ftpcod        and "
        "      pd3.pak_flg      = 1                    "
        "left join prtftp_dtl pd4 on "
        "      pd4.prtnum       = pf.prtnum        and "
        "      pd4.prt_client_id= pf.prt_client_id and "
        "      pd4.wh_id        = pf.wh_id         and "
        "      pd4.ftpcod       = pf.ftpcod        and "
        "      pd4.rcv_flg      = 1                    "
        "where ph.pcfkey        = pd.pcfkey        and "
        "      ph.client_id     = pd.client_id     and "
        "      ph.supnum        = pd.supnum        and "
        "      pd.prtnum        = pm.prtnum        and "
        "      pd.prt_client_id = pm.prt_client_id and "
        "      ph.wh_id         = pd.wh_id         and "
        "      ph.wh_id         = pm.wh_id         and "
        "      ph.pcfkey        = '%s' and "
        "      ph.client_id     = '%s' and "
        "      ph.supnum        = '%s' and "
        "      ph.wh_id = '%s'",
        pcfkey, client_id, supnum,
        wh_id);

    if (strlen (prvpcflin))
    {
	index += sprintf (&buffer [index], " and pd.pcflin > '%s' ", prvpcflin);
    }

    index += sprintf (&buffer [index], " order by pd.pcflin ");

    /* Perform SQL query & retrieve pointer to 1st result row */

    status = sqlExecStr (buffer, &resultSet);
    if (status == eDB_NO_ROWS_AFFECTED)
    {
        sqlFreeResults (resultSet);
        misTrc (T_FLOW, "No next part configuration line found for %s/%s/%s/%s",
		pcfkey, client_id, supnum, prvpcflin);
        return (srvResults(status, NULL));
    }
    if (status != eOK)
    {
        sqlFreeResults (resultSet);
        misTrc (T_FLOW, "Error (%ld) performing SQL query [%s]", status, buffer);
        return (srvResults(status, NULL));
    }
    resultRow = sqlGetRow (resultSet);

    /* Setup return information using 1st result row information */

    returnData = srvResultsInit (eOK,
			"pcfkey",	COMTYP_CHAR,	PCFKEY_LEN,
			"client_id",	COMTYP_CHAR,	CLIENT_ID_LEN,
			"supnum",	COMTYP_CHAR,	SUPNUM_LEN,
			"pcflin",	COMTYP_CHAR,	PCFLIN_LEN,
			"pcftyp",	COMTYP_CHAR,	PCFTYP_LEN,
			"prtnum",	COMTYP_CHAR,	PRTNUM_LEN,
			"prt_client_id",COMTYP_CHAR,	CLIENT_ID_LEN,
			"lotnum",	COMTYP_CHAR,	LOTNUM_LEN,
                        "sup_lotnum",   COMTYP_CHAR,    LOTNUM_LEN,
			"orgcod",	COMTYP_CHAR,	ORGCOD_LEN,
			"revlvl",	COMTYP_CHAR,	REVLVL_LEN,
			"invsts",	COMTYP_CHAR,	INVSTS_LEN,
			"rcvqty",	COMTYP_INT,	sizeof (long),
			"untpak",	COMTYP_INT,	sizeof (long),
			"untcas",	COMTYP_INT,	sizeof (long),
			"untpal",	COMTYP_INT,	sizeof (long),
			"ftpcod",	COMTYP_CHAR,	FTPCOD_LEN,
			"rcvuom",	COMTYP_CHAR,	UOMCOD_LEN,
			"lodlvl",	COMTYP_CHAR,	LODLVL_LEN,
			"stkuom",	COMTYP_CHAR,	UOMCOD_LEN,
			"lotflg",	COMTYP_INT,	sizeof (long),
			"sup_lot_flg", COMTYP_INT, sizeof (long),
			"orgflg",	COMTYP_INT,	sizeof (long),
			"revflg",	COMTYP_INT,	sizeof (long),
			"rcvflg",	COMTYP_INT,	sizeof (long),
			"subcfg",	COMTYP_CHAR,	CFGCOD_LEN,
			"scfpos",	COMTYP_INT,	sizeof (long),
			"dtlcfg",	COMTYP_CHAR,	CFGCOD_LEN,
			"dcfpos",	COMTYP_INT,	sizeof (long),
			"wh_id",	COMTYP_CHAR,	WH_ID_LEN,
			NULL);

    status = srvResultsAdd (returnData,
			rfGetString (resultSet, resultRow, "pcfkey"),
			rfGetString (resultSet, resultRow, "client_id"),
			rfGetString (resultSet, resultRow, "supnum"),
			rfGetString (resultSet, resultRow, "pcflin"),
			rfGetString (resultSet, resultRow, "pcftyp"),
			rfGetString (resultSet, resultRow, "prtnum"),
			rfGetString (resultSet, resultRow, "prt_client_id"),
			rfGetString (resultSet, resultRow, "lotnum"),
                        rfGetString (resultSet, resultRow, "sup_lotnum"),
			rfGetString (resultSet, resultRow, "orgcod"),
			rfGetString (resultSet, resultRow, "revlvl"),
			rfGetString (resultSet, resultRow, "invsts"),
			rfGetLong   (resultSet, resultRow, "rcvqty"),
			rfGetLong   (resultSet, resultRow, "untpak"),
			rfGetLong   (resultSet, resultRow, "untcas"),
			rfGetLong   (resultSet, resultRow, "untpal"),
			rfGetString (resultSet, resultRow, "ftpcod"),
			rfGetString (resultSet, resultRow, "rcvuom"),
			rfGetString (resultSet, resultRow, "lodlvl"),
			rfGetString (resultSet, resultRow, "stkuom"),
			rfGetLong   (resultSet, resultRow, "lotflg"),
			rfGetLong   (resultSet, resultRow, "sup_lot_flg"),
			rfGetLong   (resultSet, resultRow, "orgflg"),
			rfGetLong   (resultSet, resultRow, "revflg"),
			rfGetLong   (resultSet, resultRow, "rcvflg"),
			rfGetString (resultSet, resultRow, "subcfg"),
			rfGetLong   (resultSet, resultRow, "scfpos"),
			rfGetString (resultSet, resultRow, "dtlcfg"),
			rfGetLong   (resultSet, resultRow, "dcfpos"),
			rfGetString (resultSet, resultRow, "wh_id"),
			NULL);

    sqlFreeResults (resultSet);

    return (returnData);
}
