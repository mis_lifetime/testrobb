/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999 Software Architects, Inc.
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  $URL: https://athena.redprairie.com/svn/prod/wmd/tags/2013.2.1/src/libsrc/dcsrf/rflib.c $
 *  $Revision: 88731 $
 *
 *  Application:
 *  Created:
 *  $Author: mlange $
 *
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>

#include "rflib.h"

/* Wrapper around sqlGetLong to return zero if desired column is null */

long rfGetLong (mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam)
{
    static long default_value = 0;

    /* Return column long value (or default value if data is undefined) */

    if (sqlIsNull (resultSet, resultRow, colnam))
    {
	return (default_value);
    }
    else
    {
        return (sqlGetLong (resultSet, resultRow, colnam));
    }
}

/* Wrapper around sqlGetString to return zero length string if desired column is null */

char *rfGetString (mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam)
{
    static char *default_value = "";

    /* Return column string value (or default value if data is undefined) */

    if (sqlIsNull (resultSet, resultRow, colnam))
    {
	return (default_value);
    }
    else
    {
        return (sqlGetString (resultSet, resultRow, colnam));
    }
}

/* Format and log trace message */

long rfLogMsg (char *header, char *format, ...)
{
    va_list args;

    char buffer [5000];
    char final  [6000];

    /* Format message text */

    va_start (args, format);

    vsprintf (buffer, format, args);

    va_end (args);

    /* Prefix message header to message text */

    strcpy (final, header);
    strcat (final, buffer);

    /* Log (T_FLOW) level trace message */

    misTrc (T_FLOW, final);

    return (eOK);
}
