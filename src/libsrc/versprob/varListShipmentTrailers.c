static char    *rcsid = "$Id: intListShipmentTrailers.c,v 1.26 2002/08/14 13:41:24 mzais Exp $";
/*#START***********************************************************************
 *
 *  $Source: /cvs/les/wm/src/libsrc/dcsint/intListShipmentTrailers.c,v $
 *
 *  Copyright (c) 2002 RedPrairie Corporation. All rights reserved.
 *
 *#END************************************************************************/
#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

#include "../varint/intlib.h"

static mocaDataRes	*trlrColumns = NULL;
static mocaDataRes	*car_moveColumns = NULL;
static mocaDataRes	*stopColumns = NULL;
static mocaDataRes	*shipmentColumns = NULL;
static mocaDataRes	*pckwrkColumns = NULL;
static mocaDataRes	*pckmovColumns = NULL;

static long sInitialize()
{
    long ret_status;
    char buffer[1000];

    if (!trlrColumns)
    {
	sprintf(buffer, 
		" select * "
		"   from trlr "
		"  where 1 = 2 ");

	ret_status = sqlExecStr(buffer, &trlrColumns);
	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(trlrColumns);
	    trlrColumns = NULL;
	    return (ret_status);
	}
        misFlagCachedMemory((OSFPTR) sqlFreeResults, trlrColumns);
    }
    
    if (!car_moveColumns)
    {
	sprintf(buffer, 
		" select * "
		"   from car_move "
		"  where 1 = 2 " );

	ret_status = sqlExecStr(buffer, &car_moveColumns);
	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(car_moveColumns);
	    car_moveColumns = NULL;
	    return (ret_status);
	}
        misFlagCachedMemory((OSFPTR) sqlFreeResults, car_moveColumns);
    }

    if (!stopColumns)
    {
	sprintf(buffer, 
		" select * "
		"   from stop "
		"  where 1 = 2 " );

	ret_status = sqlExecStr(buffer, &stopColumns);
	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(stopColumns);
	    stopColumns = NULL;
	    return (ret_status);
	}
        misFlagCachedMemory((OSFPTR) sqlFreeResults, stopColumns);
    }

    if (!shipmentColumns)
    {
	sprintf(buffer, 
		" select * "
		"   from shipment "
		"  where 1 = 2 " );

	ret_status = sqlExecStr(buffer, &shipmentColumns);
	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(shipmentColumns);
	    shipmentColumns = NULL;
	    return (ret_status);
	}
        misFlagCachedMemory((OSFPTR) sqlFreeResults, shipmentColumns);
    }

    if (!pckwrkColumns)
    {
	sprintf(buffer, 
		" select * "
		"   from pckwrk "
		"  where 1 = 2 " );

	ret_status = sqlExecStr(buffer, &pckwrkColumns);
	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(pckwrkColumns);
	    pckwrkColumns = NULL;
	    return (ret_status);
	}
        misFlagCachedMemory((OSFPTR) sqlFreeResults, pckwrkColumns);
    }

    if (!pckmovColumns)
    {
	sprintf(buffer, 
		" select * "
		"   from pckmov "
		"  where 1 = 2 " );

	ret_status = sqlExecStr(buffer, &pckmovColumns);
	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(pckmovColumns);
	    pckmovColumns = NULL;
	    return (ret_status);
	}
        misFlagCachedMemory((OSFPTR) sqlFreeResults, pckmovColumns);
    }

    return (eOK);
} /* End intialize */

static void sFormatWhere(char *buffer, 
			 char *table,
			 char *argname, 
			 int  oper, 
			 void *argdata, 
			 char argtype,
			 char dbType)
{
    char temp1[200];
    char temp2[200];

    memset (temp1, 0, sizeof(temp1));
    memset (temp2, 0, sizeof(temp2));

    if (appDataToString(argdata, argtype, temp1) != eOK)
	return;

    /* Convert to date if we need to */
    if (dbType == COMTYP_DATTIM)
    {
        sprintf(temp2, 
	        "to_date('%s')",
		temp1);
        strcpy(temp1, temp2);
	memset(temp2, 0, sizeof(temp2));
    }

    sprintf(buffer, " and %s.%s ", table, argname);
    switch (oper)
    {
        case OPR_NOTNULL:
	    strcpy(temp2, "is not null ");
	    break;
        case OPR_ISNULL:
	    strcpy(temp2, "is null ");
	    break;
        case OPR_EQ:
	    sprintf(temp2, argtype == COMTYP_STRING && 
		    dbType != COMTYP_DATTIM ? " = '%s' " : " = %s ", temp1);
	    break;
        case OPR_NE:
	    sprintf(temp2, argtype == COMTYP_STRING && 
		    dbType != COMTYP_DATTIM ? " != '%s' " : " != %s ", temp1);
	    break;
        case OPR_LT:
	    sprintf(temp2, argtype == COMTYP_STRING && 
		    dbType != COMTYP_DATTIM ? " < '%s' " : " < %s ", temp1);
	    break;
        case OPR_LE:
	    sprintf(temp2, argtype == COMTYP_STRING && 
		    dbType != COMTYP_DATTIM ? " <= '%s' " : " <= %s ", temp1);
	    break;
        case OPR_GT:
	    sprintf(temp2, argtype == COMTYP_STRING && 
		    dbType != COMTYP_DATTIM ? " > '%s' " : " > %s ", temp1);
	    break;
        case OPR_GE:
	    sprintf(temp2, argtype == COMTYP_STRING && 
		    dbType != COMTYP_DATTIM ? " >= '%s' " : " >= %s ", temp1);
	    break;
        case OPR_LIKE:
	    sprintf(temp2, " like '%s%%' ", temp1);
	    break;
    }
    strcat(buffer, temp2);
} /* Format Where */

LIBEXPORT
RETURN_STRUCT *intListShipmentTrailers (void)
{

    RETURN_STRUCT  *CurPtr=NULL, *ArePtr=NULL;
    mocaDataRes	   *res, *res2;
    mocaDataRow	   *row;
    
    char    sqlBuffer[4000];
    char    buffer[4000];
    int     ret_status;
    char    arecod_list[1000];
    char    trlr_stat[TRLR_STAT_LEN];
    char    where_list_trlr_stat[1000];
    char    where_list_trlr[10000];
    char    where_list_car_move[10000];
    char    where_list_stop[10000];
    char    where_list_shipment[10000];
    char    where_list_pckwrk[10000];
    char    where_list_pckmov[10000];

    /* For srvEnumerateArgs */
    char 	argname[ARGNAM_LEN + 1];
    int 	oper;
    void 	*argdata;
    char 	argtype;
    SRV_ARGSLIST *args;

    CurPtr = NULL;

    memset(sqlBuffer, 0, sizeof(sqlBuffer));
    memset(where_list_trlr, 0, sizeof(where_list_trlr));
    memset(where_list_car_move, 0, sizeof(where_list_car_move));
    memset(where_list_stop, 0, sizeof(where_list_stop));
    memset(where_list_shipment, 0, sizeof(where_list_shipment));
    memset(where_list_pckwrk, 0, sizeof(where_list_pckwrk));
    memset(where_list_pckmov, 0, sizeof(where_list_pckmov));
    memset(where_list_trlr_stat, 0, sizeof(where_list_trlr_stat));
    memset(argname, 0, sizeof(argname));
    memset(trlr_stat, 0, sizeof(trlr_stat));
    
    if (!trlrColumns || !car_moveColumns ||
	!stopColumns || !shipmentColumns ||
	!pckwrkColumns || !pckmovColumns)
    {
	ret_status = sInitialize();
	if (ret_status != eOK)
	    return (srvResults(ret_status, NULL));
    }
	     
    /* Now we spin through our where clause... */
    args = NULL;
    while(eOK == srvEnumerateAllArgs(&args, argname, &oper, &argdata, &argtype))
    {
        buffer[0] = '\0';

	if (sqlFindColumn(trlrColumns, argname) != -1)
	{
	    /* We've found our argument in the trlr table */
	    sFormatWhere(buffer, "trlr", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(trlrColumns, 
					     sqlFindColumn(trlrColumns, 
							   argname)));
            if (strlen(buffer))
                strcat(where_list_trlr, buffer);

	    /* 
	     * We'll just keep track of the trailer status if
	     * it was passed in so that we don't have to build
	     * the default trlr_stat in list.
	     */

	    if (!misTrimStrncmp(argname, "trlr_stat", COLNAM_LEN))
		misTrimcpy(trlr_stat, argdata, TRLR_STAT_LEN);
	}
	else if (sqlFindColumn(car_moveColumns, argname) != -1)
	{
	    /* We've found our argument in the car_move table */
	    sFormatWhere(buffer, "car_move", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(car_moveColumns, 
					     sqlFindColumn(car_moveColumns, 
							   argname)));
            if (strlen(buffer))
                strcat(where_list_car_move, buffer);
	}
	else if (sqlFindColumn(stopColumns, argname) != -1)
	{
	    /* We've found our argument in the stop table */
	    sFormatWhere(buffer, "stop", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(stopColumns, 
					     sqlFindColumn(stopColumns, 
							   argname)));
            if (strlen(buffer))
                strcat(where_list_stop, buffer);
	}
	else if (sqlFindColumn(shipmentColumns, argname) != -1)
	{
	    /* We've found our argument in the shipment table */
	    sFormatWhere(buffer, "shipment", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(shipmentColumns, 
					     sqlFindColumn(shipmentColumns, 
							   argname)));
            if (strlen(buffer))
                strcat(where_list_shipment, buffer);
	}
	else if (sqlFindColumn(pckwrkColumns, argname) != -1)
	{
	    /* We've found our argument in the pckwrk table */
	    sFormatWhere(buffer, "pckwrk", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(pckwrkColumns, 
					     sqlFindColumn(pckwrkColumns, 
							   argname)));
            if (strlen(buffer))
                strcat(where_list_pckwrk, buffer);
	}
	else if (sqlFindColumn(pckmovColumns, argname) != -1)
	{
	    /* We've found our argument in the pckmov table */
	    sFormatWhere(buffer, "pckmov", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(pckmovColumns, 
					     sqlFindColumn(pckmovColumns, 
							   argname)));
            if (strlen(buffer))
                strcat(where_list_pckmov, buffer);
	}
	else
	{
	}

    }
    srvFreeArgList(args);
    
    /* If we didn't get a specific trailer status to look at,
     * build a default in list consisting of TRLSTS_LOADING, TRLSTS_SUSPENDED,
     * TRLSTS_LOADED, TRLSTS_CLOSED and TRLSTS_PENDING_DISPATCH.
     */
    if (!strlen(trlr_stat)) 
    {
	sprintf(where_list_trlr_stat,
		" and trlr.trlr_stat in ('%s', '%s', '%s', '%s', '%s')",
		TRLSTS_LOADING,
		TRLSTS_LOADED,
		TRLSTS_CLOSED,
                TRLSTS_SUSPENDED,
		TRLSTS_PENDING_DISPATCH);
    }
    
    /* If we didn't get a specific arecod to look at, find out what areas
     * we're processing for and build an 'in' list.
     */
    ArePtr = NULL;
    memset (arecod_list, 0, sizeof(arecod_list));
	
    sprintf (sqlBuffer, "list shipment staging areas");
	
    ret_status = srvInitiateCommand (sqlBuffer, &ArePtr);
    if (eOK != ret_status) 
    {
        srvFreeMemory(SRVRET_STRUCT, ArePtr);
	return(srvResults(ret_status,NULL));
    }
    res2=ArePtr->ReturnedData;
    for (row = sqlGetRow(res2); row; row = sqlGetNextRow(row))
    {
	if (strlen(arecod_list))
	    strcat(arecod_list, ",'");
	else
	    strcpy(arecod_list, "'");
	strcat(arecod_list, sqlGetString (res2, row, "arecod"));
	strcat(arecod_list, "'");
    }
    srvFreeMemory(SRVRET_STRUCT, ArePtr);

    sprintf(sqlBuffer,
              " select distinct "
              "        shipment.carcod, shipment.srvlvl,"
	      "        shipment.ship_id, shipment.stgdte, "
	      "        shipment.loddte, shipment.early_dlvdte, "
	      "        shipment.late_dlvdte, shipment.early_shpdte, "
	      "        shipment.late_shpdte, stop.stop_id,"
	      "        car_move.car_move_id, trlr.trlr_num, trlr.trlr_stat "
              "   from pckmov, pckwrk, shipment, stop, "
	      "        car_move, trlr "
              "  where shipment.stop_id  = stop.stop_id "
	      "    and stop.car_move_id  = car_move.car_move_id "
              "    and car_move.trlr_id  = trlr.trlr_id "
	      "    and shipment.loddte is not null "
	      "    and shipment.ship_id  = pckwrk.ship_id "
	      "    and shipment.shpsts  in ('%s', '%s') "
	      "    and pckwrk.cmbcod     = pckmov.cmbcod "
	      "    and pckmov.arecod    in (%s) "
              "    and trlr.trlr_cod = '%s' "
	      "    %s "
	      "    %s "
	      "    %s "
	      "    %s "
	      "    %s "
	      "    %s "
	      "    %s "
              "   order by trlr.trlr_num, trlr.trlr_stat, shipment.ship_id ",
		 SHPSTS_LOADED, SHPSTS_LOAD_COMPLETE,
                 arecod_list,
                 TRLR_COD_SHIP,
		 where_list_trlr_stat ? where_list_trlr_stat : "",
             	 where_list_trlr ? where_list_trlr : "", 	     
             	 where_list_car_move ? where_list_car_move : "", 	     
             	 where_list_stop ? where_list_stop : "",
             	 where_list_shipment ? where_list_shipment : "", 	     
             	 where_list_pckwrk ? where_list_pckwrk : "",
             	 where_list_pckmov ? where_list_pckmov : "");
	    
    ret_status = sqlExecStr(sqlBuffer, &res);
    if ( (eOK != ret_status) && (eDB_NO_ROWS_AFFECTED != ret_status))   
    {
	srvFreeMemory(SRVRET_STRUCT, CurPtr);
	return(srvResults(ret_status,NULL));
    }
    
    CurPtr = NULL;

    CurPtr = srvResultsInit(eOK,
                            "stop_id",      COMTYP_CHAR, STOP_ID_LEN,
                            "ship_id",      COMTYP_CHAR, SHIP_ID_LEN,
                            "stgdte", 	    COMTYP_DATTIM, DB_STD_DATE_LEN,
                            "loddte", 	    COMTYP_DATTIM, DB_STD_DATE_LEN,
                            "early_dlvdte", COMTYP_DATTIM, DB_STD_DATE_LEN,
                            "late_dlvdte",  COMTYP_DATTIM, DB_STD_DATE_LEN,
                            "early_shpdte", COMTYP_DATTIM, DB_STD_DATE_LEN,
                            "late_shpdte",  COMTYP_DATTIM, DB_STD_DATE_LEN,
			    "trlr_num",	    COMTYP_CHAR, TRLR_NUM_LEN,
			    NULL);
    
    /* Populate command return structure. */
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
    	srvResultsAdd (CurPtr,
		       sqlGetString(res, row, "stop_id"),
		       sqlGetString(res, row, "ship_id"),
		       sqlGetString(res, row, "stgdte"),
		       sqlGetString(res, row, "loddte"),
		       sqlGetString(res, row, "early_dlvdte"),
		       sqlGetString(res, row, "late_dlvdte"),
		       sqlGetString(res, row, "early_shpdte"),
		       sqlGetString(res, row, "late_shpdte"),
		       sqlGetString(res, row, "trlr_num"),
	   	       NULL);
    }
    
    return (CurPtr);
}
