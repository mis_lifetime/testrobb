static const char *rcsid = "$Id: intResetShipmentStatus.c,v 1.3 2001/01/19 21:24:02 mzais Exp $";
/*#START**********************************************************************
 *  McHugh Software International
 *  Copyright 1999 
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *#END***********************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include <common.h>
#include "../varint/intlib.h"


LIBEXPORT 
RETURN_STRUCT *varResetShipmentStatus(char *ship_line_id_i)
{
    RETURN_STRUCT *CurPtr;
    mocaDataRes *res;
    mocaDataRow *row;
    long  ret_status;
    char  buffer[1000];
    char  ship_id[SHIP_ID_LEN + 1];
    char  ship_line_id[SHIP_LINE_ID_LEN + 1];
    long  inpqty;
    short ReplenishmentExist;
    short PickExist;
    short LineInprocess;

    memset(ship_id, 0, sizeof(ship_id));
    memset(ship_line_id, 0, sizeof(ship_line_id));

    misTrc(T_FLOW, "Checking if shipment needs to be reset...");

    if (!ship_line_id_i || misTrimLen(ship_line_id_i, SHIP_LINE_ID_LEN) == 0)
	return APPMissingArg("ship_line_id");
    else
	misTrimcpy(ship_line_id, ship_line_id_i, SHIP_LINE_ID_LEN);

    /* Get shipment for update */
    sprintf(buffer,
	    "select s.ship_id, s.shpsts, "
	    "	    sl.ship_line_id, sl.inpqty "
	    "  from shipment_line sl, shipment s "
	    " where sl.ship_id = s.ship_id "
	    "	and s.ship_id = (select ship_id "
	    "                      from shipment_line "
            "                     where ship_line_id = '%s') ",
	    ship_line_id);
    ret_status = sqlExecStr(buffer, &res);

    if (ret_status != eOK)
    {
    	sqlFreeResults(res);
    	return (srvSetupReturn(ret_status, ""));
    }

    /*
     * Loop through the recordset to check if there are 
     * replenishments or picks existing for each line of the shipment
     */
    ReplenishmentExist = PickExist = LineInprocess = FALSE;

    for (row = sqlGetRow(res); 
	 row && (!ReplenishmentExist && !PickExist && !LineInprocess); 
	 row = sqlGetNextRow(row))
    {
    	inpqty = sqlGetLong(res, row, "inpqty");
    	misTrimcpy(ship_line_id, 
	           sqlGetString(res, row, "ship_line_id"), 
	           SHIP_LINE_ID_LEN);
    	misTrimcpy(ship_id, 
	           sqlGetString(res, row, "ship_id"), 
	           SHIP_ID_LEN);
    	if (inpqty == 0)
    	{
            /* Verify that there aren't any other
	     * replenishments existing for the shipment
	     */
	    sprintf(buffer,
		    "select rplref "
		    "  from rplwrk "
		    " where ship_line_id = '%s' ",
		    ship_line_id);
	    ret_status = sqlExecStr(buffer, NULL);

	    if (ret_status == eOK)
	    {
		ReplenishmentExist = TRUE;
    		misTrc(T_FLOW, 
		       "Can't reset shipment, replenishment(s) exists...");
		break;
	    }

	    /* if the query fails, get out */
	    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
	    {
	    	sqlFreeResults(res);
	    	return (srvSetupReturn(ret_status, ""));
	    }

            /* Verify that there aren't any other
	     * pick works existing for the shipment
	     */
	    sprintf(buffer,
		    "select wrkref "
		    "  from pckwrk "
		    " where ship_line_id = '%s' ",
		    ship_line_id);
	    ret_status = sqlExecStr(buffer, NULL);

	    if (ret_status == eOK)
	    {
		PickExist = TRUE;
    		misTrc(T_FLOW, "Can't reset shipment, pick(s) exists...");
		break;
	    }

	    /* if the query fails, get out */
	    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
	    {
	    	sqlFreeResults(res);
	    	return (srvSetupReturn(ret_status, ""));
	    }
	}
	else
	{
	    LineInprocess = TRUE;
    	    misTrc(T_FLOW, "Can't reset shipment, line(s) inprocess...");
	    break;
	}
    }

    /* if there are no replenishments or picks pending 
     * nor lines in process
     * then set the shipment to READY
     */
    if (!ReplenishmentExist && !PickExist && !LineInprocess)
    {
    	sprintf(buffer,
            	"change shipment status "
	    	"where ship_id = '%s' "
	    	"  and shpsts = '%s' ",
	    	ship_id, SHPSTS_READY);
	CurPtr = NULL;
    	ret_status = srvInitiateCommand(buffer, &CurPtr);
	srvFreeMemory (SRVRET_STRUCT, CurPtr);

    	/* if the update fails, get out */
    	if (ret_status != eOK)
    	{
      	    sqlFreeResults(res);
    	    return (srvSetupReturn(ret_status, ""));
    	}
    }

    sqlFreeResults(res);
    return (srvSetupReturn(eOK, ""));
}
