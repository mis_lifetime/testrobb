/*#START***********************************************************************
 *
 *  Copyright (c) RedPrairie 2002. All rights reserved. 
 *
 *#END************************************************************************/

#include <moca_app.h>
#include <mislib.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include "../varint/intlib.h"


static RETURN_STRUCT *sSetupEmptyReturn(long ret_status)
{
    RETURN_STRUCT  *ptr;

    ptr = srvResultsInit(ret_status,
                         "trknum",     COMTYP_CHAR, TRKNUM_LEN,
                         "carcod",     COMTYP_CHAR, CARCOD_LEN,
                         "trkref",     COMTYP_CHAR, TRKREF_LEN,
                         "trlr_stat",  COMTYP_CHAR, TRLR_STAT_LEN,
                         "trnspt",     COMTYP_CHAR, TRNSPT_LEN,
                         "client_id",  COMTYP_CHAR, CLIENT_ID_LEN,
                         "supnum",     COMTYP_CHAR, SUPNUM_LEN,
                         "invnum",     COMTYP_CHAR, INVNUM_LEN,
                         "numpal",     COMTYP_INT, sizeof(long),
                         "numcas",     COMTYP_INT, sizeof(long),
                         "grswgt",     COMTYP_FLOAT, sizeof(double),
                         "frtcst",     COMTYP_FLOAT, sizeof(double),
                         "shpdte",     COMTYP_DATTIM, MOCA_STD_DATE_LEN, 
                         "expdte",     COMTYP_DATTIM, MOCA_STD_DATE_LEN,
                         "arrdte",     COMTYP_DATTIM, MOCA_STD_DATE_LEN,
                         "clsdte",     COMTYP_DATTIM, MOCA_STD_DATE_LEN,
                         "yard_loc",   COMTYP_CHAR, YARD_LOC_LEN,
                         "devcod",     COMTYP_CHAR, DEVCOD_LEN, 
                         "lblflg",     COMTYP_CHAR, LBLFLG_LEN,
                         "hot_flg",    COMTYP_INT, sizeof(long),
                         "trlr_num",   COMTYP_CHAR,  TRLR_NUM_LEN,
                         NULL);
    return (ptr);
}


LIBEXPORT 
RETURN_STRUCT *usrListReceiveInvoiceTrucks( char *client_id_i,
                                  char *trknum_i, char *trkref_i, 
                                  char *supnum_i, char *prtnum_i, 
                                  char *invnum_i, 
                                  char *lodnum_i,
                                  moca_bool_t *rvsrcv_flg_i)
{

    RETURN_STRUCT  *CurPtr;
    mocaDataRes    *res;
    int             ret_status;
    char client_id[CLIENT_ID_LEN+1];
    char trknum[TRKNUM_LEN+1];
    char trkref[TRKREF_LEN+1];
    char supnum[SUPNUM_LEN+1];
    char prtnum[PRTNUM_LEN+1];
    char invnum[INVNUM_LEN+1];
    char lodnum[LODNUM_LEN+1];
    char buffer[4000];
    char tmp_buffer[1400];
    char where_clause[4000];
    char union_clause[4000];
    char where_clause_for_union[4000];
    char order_by[1000];
    char *var_whereclause;
    moca_bool_t rvsrcv_flg = BOOLEAN_FALSE;


    /* This handles any arguments that are passed in */
    /* for either the rcvtrk or rcvinv table, but haven't*/
    /* been explicitely defined. The only values we must */
    /* specify here are those which are found on both tables.*/

    static TABLE_FIELD_LIST tbl_fld_list[] =
    {   {"rcvtrk","trknum"},
        {"invlod","lodnum"},
        {"rcvtrk","trlr_id"},
        {"",""}    };

    CurPtr = NULL;

    /* Format the incoming variables */
    
    memset(client_id,0,sizeof(client_id));
    memset(trknum,0,sizeof(trknum));
    memset(trkref,0,sizeof(trkref));
    memset(supnum,0,sizeof(supnum));
    memset(prtnum,0,sizeof(prtnum));
    memset(invnum,0,sizeof(invnum));
    memset(lodnum,0,sizeof(lodnum));
    memset(where_clause, 0, sizeof(where_clause));
    memset(union_clause, 0, sizeof(union_clause));
    memset(where_clause_for_union, 0, sizeof(where_clause_for_union));
    memset(order_by, 0, sizeof(order_by));
    memset(tmp_buffer, 0, sizeof(tmp_buffer));


    if (client_id_i && misTrimLen(client_id_i, CLIENT_ID_LEN))
        misTrimcpy(client_id, client_id_i, CLIENT_ID_LEN);

    if (trknum_i && misTrimLen(trknum_i, TRKNUM_LEN))
        misTrimcpy(trknum, trknum_i, TRKNUM_LEN);

    if (trkref_i && misTrimLen(trkref_i, TRKREF_LEN))
        misTrimcpy(trkref, trkref_i, TRKREF_LEN);

    if (supnum_i && misTrimLen(supnum_i, SUPNUM_LEN))
        misTrimcpy(supnum, supnum_i, SUPNUM_LEN);

    if (prtnum_i && misTrimLen(prtnum_i, PRTNUM_LEN))
        misTrimcpy(prtnum, prtnum_i, PRTNUM_LEN);

    if (invnum_i && misTrimLen(invnum_i, INVNUM_LEN))
        misTrimcpy(invnum, invnum_i, INVNUM_LEN); 

    if (lodnum_i && misTrimLen(lodnum_i, LODNUM_LEN))
        misTrimcpy(lodnum, lodnum_i, LODNUM_LEN);

    if (rvsrcv_flg_i)
        rvsrcv_flg = *rvsrcv_flg_i;
    
    /*Grab any columns from rcvinv, rcvtrk that weren't expected */
    ret_status=appBuildWhereList (tbl_fld_list,"trlr, rcvinv,rcvtrk",
                                  &var_whereclause); 

    /*Build the where_clause for any arguments passed into the function */              

    /*If looking for specific rcvinv values, only grab invoices that match, 
     *otherwise grab all invoices for displayed trucks.           
     */

    if ( (strlen(supnum))||(strlen(invnum))||
         (strlen(client_id)||strlen(prtnum)||strlen(lodnum)||
            misCiStrstr(var_whereclause,"prtnum")||
            misCiStrstr(var_whereclause,"lodnum")|| 
            misCiStrstr(var_whereclause,"invnum")||
            misCiStrstr(var_whereclause,"supnum")||
            misCiStrstr(var_whereclause,"client_id")||
            misCiStrstr(var_whereclause,"invtyp")||
            misCiStrstr(var_whereclause,"sadnum")||
            misCiStrstr(var_whereclause,"waybil")||
            misCiStrstr(var_whereclause,"invdte")||
            misCiStrstr(var_whereclause,"orgref") ) )
    {
        sprintf(where_clause, 
               " where rcvtrk.trknum = rcvinv.trknum");
        sprintf(order_by, 
                "order by trlr.trlr_stat, rcvtrk.trknum, rcvtrk.trkref");
    }
    else 
    {
        /* 
         * A specific value for the rcvinv table was not specified.
         * Therefore, we will need to add an additional union on,
         * to make sure we pick up all rcvtrk records that 
         * may not have a rcvinv value yet defined.
         */

        sprintf(where_clause,
               " where rcvtrk.trknum = rcvinv.trknum");

        /*
         * It would not make sense to grab trucks that don't have 
         * an invoice assigned when we're doing a reverse receipt
         */

        /* Note, it's safe to apply the var_whereclause here because
         * we're only applying this union statement if there
         * are no invoice fields passed in on the stack.
         */

        if (rvsrcv_flg == BOOLEAN_FALSE)
        {
            sprintf(union_clause,
                   " union "
                   "   select rcvtrk.trknum, trlr.carcod, "
                   "          rcvtrk.trkref, trlr.trlr_stat, "
                   "          rcvtrk.trnspt, null, null, "
                   "          null, rcvtrk.numpal, rcvtrk.numcas, "
                   "          rcvtrk.grswgt, rcvtrk.frtcst, rcvtrk.shpdte, "
                   "          rcvtrk.expdte, trlr.arrdte, rcvtrk.clsdte, "
                   "          trlr.yard_loc, rcvtrk.devcod, rcvtrk.lblflg, "
                   "          trlr.hot_flg, "
                   "          trlr.trlr_num "
                   "   from trlr, "
                   "        rcvtrk "
                   "  where trlr.trlr_id = rcvtrk.trlr_id "
                   "    %s "
                   "    and not exists (select 'x' "
                   "                      from rcvinv "
                   "                     where rcvinv.trknum = rcvtrk.trknum)",
                   var_whereclause);
        }
        sprintf(order_by,
               " order by 4,1,3 ");
    }


    if (strlen(trknum))
    {
        sprintf (tmp_buffer, " and rcvtrk.trknum like '%s'", trknum);
        strcat(where_clause, tmp_buffer);
        if (strlen(union_clause))
            strcat(where_clause_for_union, tmp_buffer);
    }

    if (strlen(trkref))
    {
        sprintf (tmp_buffer, " and rcvtrk.trkref like '%s'", trkref);
        strcat(where_clause, tmp_buffer);
        if (strlen(union_clause))
            strcat(where_clause_for_union, tmp_buffer);
    } 

    if (strlen(supnum))
    {
        sprintf (tmp_buffer, " and rcvinv.supnum like '%s'", supnum);
        strcat(where_clause, tmp_buffer);
    }

    if (strlen(invnum))
    {
        sprintf (tmp_buffer, " and rcvinv.invnum like '%s'", invnum);
        strcat(where_clause, tmp_buffer);
    }

    if (strlen(client_id))
    {
        sprintf (tmp_buffer, " and rcvinv.client_id like '%s'", client_id);
        strcat(where_clause, tmp_buffer);
    }

    /* 
     * If we're returning a list for reverse receiving,
     * we want to make sure that we only display trucks that
     * have actually received inventory.
     */

    if (rvsrcv_flg == BOOLEAN_TRUE)
    {

        if (strlen(where_clause))
        {
            strcat(where_clause, " and ");
        }
        else
        {
            strcat(where_clause, " where ");
        }

        sprintf (tmp_buffer, " trlr.trlr_stat in ('%s','%s','%s') "
                             "   and exists (select trknum from rcvlin "
                             " where rcvlin.trknum = rcvtrk.trknum "
                             "   and rcvlin.invnum = rcvinv.invnum "
                             "   and rcvlin.supnum = rcvinv.supnum "
                             "   and rcvlin.client_id = rcvinv.client_id "
                             "   and rcvlin.rcvqty > 0 )",
                             TRLSTS_RECEIVING,
                             TRLSTS_SUSPENDED,
                             TRLSTS_CLOSED);
        strcat(where_clause, tmp_buffer);

        sprintf(order_by,
                "order by rcvtrk.trknum, rcvtrk.trkref");
    }

    if (strlen(prtnum))
    {
        sprintf(tmp_buffer,
            "   and exists (select trknum from rcvlin "
            " where rcvlin.trknum = rcvtrk.trknum "
            "   and rcvlin.invnum = rcvinv.invnum "
            "   and rcvlin.supnum = rcvinv.supnum "
            "   and rcvlin.client_id = rcvinv.client_id "
            "   and prtnum like nvl('%s', prtnum))", prtnum);
        strcat(where_clause, tmp_buffer);
    }

    if (strlen(where_clause))
    {
        strcat(where_clause, " and ");
    }
    else
    {
        strcat(where_clause, " where ");
    }
      
    /*
     * Make sure we are not displaying work orders.
     */

    sprintf(tmp_buffer,
            "  not exists (select 'x' "
            "                   from poldat " 
            "                  where polcod = '%s' "
            "                    and polvar = '%s' "
            "                    and polval = '%s' "
            "                    and rtstr1 = 'invtyp' "
            "                    and rtstr2 = rcvinv.invtyp) ",
            POLCOD_WORK_ORDER_PROC,
            POLVAR_MISC,
            POLVAL_WRKORD_RCV_DEFAULTS);
    strcat(where_clause, tmp_buffer);

    if (strlen(lodnum))
    {
        sprintf(tmp_buffer,
            "   and exists (select trknum "
            "  from rcvlin, invlod, invsub, invdtl "
            " where rcvlin.trknum = rcvtrk.trknum "
            "   and rcvlin.invnum = rcvinv.invnum "
            "   and rcvlin.supnum = rcvinv.supnum "
            "   and rcvlin.client_id = rcvinv.client_id "
            "   and rcvlin.rcvkey = invdtl.rcvkey "
            "   and invdtl.subnum = invsub.subnum "
            "   and invsub.lodnum = invlod.lodnum "
            "   and invlod.lodnum like nvl('%s', invlod.lodnum))", lodnum);
        strcat(where_clause, tmp_buffer);
    }       
         
    sprintf(buffer,
        "   select rcvtrk.trknum, trlr.carcod, "
        "          rcvtrk.trkref, trlr.trlr_stat, "
        "          rcvtrk.trnspt, rcvinv.client_id, rcvinv.supnum, "
        "          rcvinv.invnum, rcvtrk.numpal, rcvtrk.numcas, "
        "          rcvtrk.grswgt, rcvtrk.frtcst, rcvtrk.shpdte, "
        "          rcvtrk.expdte, trlr.arrdte, rcvtrk.clsdte, "
        "          trlr.yard_loc, rcvtrk.devcod, rcvtrk.lblflg, "           
        "          trlr.hot_flg, "
        "          trlr.trlr_num "
        "     from rcvtrk, "
        "          trlr,   "
        "          rcvinv  "
        "          %s "
        "          %s "
        "      and rcvtrk.trlr_id = trlr.trlr_id "
        "      %s "
        "         %s "
        "      %s ",
        where_clause, var_whereclause,
        union_clause,
        where_clause_for_union,
        order_by);


    ret_status= sqlExecStr(buffer, &res);
    
    if (ret_status != eOK)
    {
        sqlFreeResults(res);
        free(var_whereclause);
        CurPtr = sSetupEmptyReturn(ret_status);
        return (CurPtr);
    }

    free(var_whereclause);

    CurPtr = srvAddSQLResults(res, eOK);

    return (CurPtr);
}
