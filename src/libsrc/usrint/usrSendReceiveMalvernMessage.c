#include <moca_app.h>
#include <mocaerr.h>
#include <srvlib.h>

#include <sys/socket.h>
#include <stdio.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <string.h> 
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>

#define READ_BUF 12800
#define RECV_BUF 256000

int numbytes;
char term_char = 0x03;
struct sockaddr_in serv_addr;
char buf[READ_BUF];
char retbuf[RECV_BUF];
int sock_fd;
struct addrinfo *result;
struct addrinfo *res;
int error;
int timeouts = 0;

int connect_s(char *malv_ip, char *malv_port) {

    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 5;

    error = getaddrinfo(malv_ip, malv_port, NULL, &result);
    if (error != 0) {
        misTrc(T_FLOW, "Failed getting socket info");
        return -1;
    }

    freeaddrinfo(result);

    sock_fd = socket(result->ai_family, result->ai_socktype, result->ai_protocol);

    if (sock_fd < 0) {
        misTrc(T_FLOW, "Failed Initializing the Socket");
        return -1;
    }

    if (setsockopt(sock_fd, SOL_SOCKET, SO_RCVTIMEO, (char *) &tv, sizeof tv)) {
        misTrc(T_FLOW, "Failed setting socket options");
        return -1;
    }

    return connect(sock_fd, result->ai_addr, result->ai_addrlen);
}

int sDataOnSocket() {
    fd_set fds;
    struct timeval mytimeout;

    FD_ZERO(&fds);
    FD_SET(sock_fd, &fds);
    mytimeout.tv_sec = 0;
    mytimeout.tv_usec = 1;

    if (select(sock_fd + 1, &fds, NULL, NULL, &mytimeout) > 0)
        return 1;

    return 0;
}

int send_s(char* msg_s) {

    return send(sock_fd, msg_s, strlen(msg_s), 0);
}

int recv_s() {

    /*
    int i = 0;
    do {
 
        misTrc(T_FLOW, "recv_s: trying socket");
        if(sDataOnSocket()) break;
        
        misTrc(T_FLOW, "recv_s: socket not ready");
        usleep(1000);
        i++;
    } while (i < 901);
    */

    /*strcpy(retbuf, "RETURN: ");*/


    memset(retbuf, '\0', sizeof(retbuf));
    int totBytes = 0;
    do {
      memset(buf, '\0', sizeof(buf));
      numbytes = recv(sock_fd, buf, READ_BUF, 0);
      totBytes += numbytes;
      /*buf[numbytes] = '\0';*/
      if(numbytes > 0) {
        strcat(retbuf, buf);
      }
      misTrc(T_FLOW, "recv_s: received %i bytes", numbytes);
    } while ( sDataOnSocket() && numbytes > 0);

    misTrc(T_FLOW, "recv_s: tot received %i bytes", totBytes);
    misTrc(T_FLOW, "r_msg: %s", retbuf);
}

LIBEXPORT
RETURN_STRUCT *usrSendReceiveMalvernMessage(char *malv_ip, char *malv_port, char *s_msg)
{

    misTrc(T_FLOW, "s_msg: %s", s_msg);

    misTrc(T_FLOW, "Connection to %s:%s", malv_ip, malv_port);
    if (connect_s(malv_ip, malv_port) < 0) {
        misTrc(T_FLOW, "Failed Socket Connection");
        return(srvResults(99200, NULL));
    }

    char sbuf[100000];
    memset(sbuf, '\0', sizeof(sbuf));
    strcpy(sbuf, s_msg);
    strcat(sbuf, &term_char);
    
    misTrc(T_FLOW, "Sending message: %s", sbuf);
    if (send_s(sbuf) < 0) {
        misTrc(T_FLOW, "Failed sending message");
        return(srvResults(99201, NULL));
    }

    /*
    misTrc(T_FLOW, "Sending termchar");
    if (send_s(&term_char) < 0) {
        misTrc(T_FLOW, "Failed sending message");
        return(srvResults(99201, NULL));
    }
    */

    misTrc(T_FLOW, "Getting Results");
    recv_s();
    
    misTrc(T_FLOW, "r_msg: %s", retbuf);

    if (close(sock_fd) < 0) {
        misTrc(T_FLOW, "Failed to close socket");
    }

    return srvResults(eOK,
                      "r_msg", COMTYP_CHAR, RECV_BUF, retbuf,
                      NULL);
}
