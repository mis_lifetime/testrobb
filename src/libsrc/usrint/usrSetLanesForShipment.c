/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999 McHugh Software International, Inc.
 *  Shelton, Connecticut,  U.S.A.
 *  All rights reserved.
 *
 *  Application:   tmint
 *  Created:	07/08/99
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>

#include <applib.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>

typedef struct tm_row_struct {
  char ship_line_id[SHIP_LINE_ID_LEN + 1];
  long pckqty;
  long untcas;
  long untpak;
  moca_bool_t case_splflg;
  moca_bool_t pack_stdflg;

  }  TM_ROW_STRUCT;

static double def_max_volume = -10.0;
static char split_stategy[50];
static int sGetNextLane(void);
static void sInitialize(void);

static void sInitialize(void)
{
	long ret_status;
    char sqlBuffer[1000];
    mocaDataRes *res;
    mocaDataRow *row;
	char polval[100];

    if (def_max_volume < 0)
    {
	char polval[100];
        /* it wasn't passed in, so try the policy */
    sprintf(sqlBuffer,  "select * from poldat where polcod = 'VAR' and "
			"polvar = 'SHIPMENT-LANE-SPLITTING' and polval = 'DEFAULT-VOLUME' ");

    ret_status = sqlExecStr(sqlBuffer, &res);
    if (ret_status == eOK)
		{
    		for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
		   {
		   strcpy(polval, sqlGetString(res, row, "polval"));
		   if (!strcmp(polval, "DEFAULT-VOLUME"))
		   	def_max_volume = sqlGetFloat(res, row, "rtflt1");
  		   }
		}
	else			
        {
            /* No policy either, use default */
        def_max_volume = 150000; 
        }
    }

}

/* Consoldiate and split shipment lines based on volume */
LIBEXPORT 
RETURN_STRUCT *usrSetLanesForShipment(char *ship_id_i, long *laneSelected_i, double *max_volume_i, long *relflg_i)
{
    int ret_status;
    char ship_id[SHIP_ID_LEN + 1];
    long laneSelected;
    char cons_batch[CONS_BATCH_LEN + 1];
    char sqlBuffer[1000];
    double shpvol;
    double cur_volume;
    double palvol;
    mocaDataRes *res;
    mocaDataRow *row;
    mocaDataRes *laneOrderRes;
    mocaDataRow *laneOrderRow;
    long NextLane;
    long relflg;
    char Lane[STOLOC_LEN+1];
    char *NextSeq;
 	
    if (!ship_id_i && !misTrimLen(ship_id_i, SHIP_ID_LEN))
        return (APPMissingArg("ship_id"));

    laneSelected = 0;
    if (laneSelected_i)
  	laneSelected = *laneSelected_i;	

    relflg = 0;
    if (relflg_i)
  	relflg = *relflg_i;	

   misTrimcpy(ship_id, ship_id_i, SHIP_ID_LEN);

   if (laneSelected > 0)
    misTrc(T_FLOW, "UsrSetLane with ship_id: %s and lane %ld (in %ld)",ship_id, laneSelected, &laneSelected_i); 

    sInitialize();

   if (laneSelected > 0)
   {
    sprintf(sqlBuffer,
        "select distinct 1  from var_wavpar_dtl wd, cstmst, ord, shipment_line where ship_id = '%s' "
        "and shipment_line.client_id = ord.client_id and shipment_line.ordnum = ord.ordnum "
        "and cstnum in (rtcust, stcust, btcust) and cstmst.cstnum = wd.vc_cstnum and rownum < 2"
        " union select 1 from shipment, cardtl where ship_id = '%s' and shipment.carcod = cardtl.carcod "
        "and shipment.srvlvl = cardtl.srvlvl and cardtl.cartyp = 'S'"
	" union select distinct 1 from shipment_line where uc_seqnum is not null and ship_id = '%s'", ship_id, ship_id, ship_id);


      ret_status = sqlExecStr(sqlBuffer, &res);
      if (ret_status == eOK)
      {
	sqlFreeResults(res);
        return (srvResults(eOK, NULL));
      }
    sqlFreeResults(res);
    }

    sprintf(sqlBuffer,
        "select distinct vc_palvol  from shipment_line, ord, cstmst where ship_id = '%s' "
   	"and shipment_line.client_id = ord.client_id and shipment_line.ordnum = ord.ordnum "
	"and cstnum in (rtcust, stcust, btcust) and vc_palvol is not null and rownum < 2", ship_id);
    ret_status = sqlExecStr(sqlBuffer, &res);

    if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
    {
	sqlFreeResults(res);
        return (srvResults(ret_status, NULL));
    }
   
    if (ret_status == eOK)
   	{
        row = sqlGetRow(res);
	palvol = sqlGetFloat(res, row, "vc_palvol");
 	sqlFreeResults(res);
	}
    else
        palvol = def_max_volume;

   
    sprintf(sqlBuffer,
      "select z.ship_line_id, z.wrkref, z.shpvol from "
      "(select min(ship_line_id) ship_line_id, wrkref,  sum(ctnlen*ctnwid*ctnhgt) shpvol       from ctnmst, pckwrk where "
      "pckwrk.ship_id = '%s' and wrktyp = 'K' and lodlvl = 'S' and "
      "pckwrk.ctncod = ctnmst.ctncod group by ship_line_id, wrkref "
      "union "
      "select  ship_line_id, wrktyp, sum((caslen*caswid*cashgt) * (pckwrk.pckqty/pckwrk.untcas)) shpvol"
      " from ftpmst, pckwrk where "
      "  pckwrk.ship_id = '%s' and wrktyp = 'P' and lodlvl = 'S' and lodflg != 1 and "
      "  pckwrk.ftpcod = ftpmst.ftpcod group by ship_line_id, wrktyp ) z "
      "  where not exists (select 'x' from pckwrk pw2 where pw2.ship_line_id = z.ship_line_id "
      "  and pw2.pcksts = 'R' and wrktyp in ('P', 'K') and lodlvl = 'S' and lodflg != 1 )", ship_id, ship_id);
	 
    
    ret_status = sqlExecStr(sqlBuffer, &res);
 
    if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
    {
	sqlFreeResults(res);
        return (srvResults(ret_status, NULL));
    }
  
    sprintf(Lane, "LANE%03d", laneSelected);

    NextSeq = sGetNextSeq(Lane, ship_id);
    if (!strlen(NextSeq))
        return (srvResults(eDB_NO_ROWS_AFFECTED, NULL));

    misTrc(T_FLOW, "Beginning with Seq: %s Lane %ld",NextSeq, laneSelected); 
    cur_volume = 0;
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	shpvol = sqlGetFloat(res, row, "shpvol");
	
        misTrc(T_FLOW, "Checking Volume: %f against %f", shpvol+cur_volume, palvol); 
	if ((shpvol + cur_volume) > palvol)
	    {
            misTrc(T_FLOW, "Getting Next Lane");
    	    NextSeq = sGetNextSeq(Lane, ship_id);
	    if (!strlen(NextSeq))
        	return (srvResults(eDB_NO_ROWS_AFFECTED, NULL));
    	    misTrc(T_FLOW, "New Seq: %d",NextSeq); 
	    cur_volume = shpvol;
	    }	
	else
	    cur_volume += shpvol;
	   
        sprintf(sqlBuffer,
        "update shipment_line set vc_wave_lane = %ld, uc_seqnum = '%s', uc_lanflg = '1' where ship_line_id = '%s'", 
		laneSelected, NextSeq, sqlGetString(res, row, "ship_line_id"));

        ret_status = sqlExecStr(sqlBuffer, NULL);
    }

    if (relflg == 0)
       {
 	/* okay so I guess this looks odd - but if are not in release mode
	   we ran all that to see if it looks like we might have enough space
	   (not counting cartons) - so rollback and set the flags so we get 
	   back in here at release time */

       sqlRollback();
        sprintf(sqlBuffer,
        "update shipment_line set vc_wave_lane = %d, uc_lanflg = '1' where ship_id = '%s'", laneSelected, 
		ship_id );

        ret_status = sqlExecStr(sqlBuffer, NULL);
        sqlCommit(); /* have to commit just in case we are doing multiple */
	}
    
    sqlFreeResults(res);
    return (srvResults(eOK, NULL));
}

static int sGetNextSeq(char *Lane, char *ship_id)
{
    int ret_status;
    char *sqlBuffer1[1000];
    static mocaDataRes *res;
    static mocaDataRow *row;
    mocaDataRes *res1;
    mocaDataRow *row1;
    int RetSeq;
    static char NextSeq[STOLOC_LEN+1];
    static char RetLane[STOLOC_LEN+1], NextLane[STOLOC_LEN+1], MaxLane[STOLOC_LEN+1], MinLane[STOLOC_LEN+1];

    int notFound;

    sprintf(sqlBuffer1, "select stoloc from locmst where arecod = 'PALLET' and aisle_id = '%s' and rescod is null",
	Lane);
    ret_status = sqlExecStr(sqlBuffer1, &res);
    if (ret_status != eOK)
   	{
	sqlFreeResults(res);
	memset(NextSeq, 0, sizeof(NextSeq));
	return (NextSeq);
	}
    else
	{
    	row = sqlGetRow(res);
        strcpy(NextSeq, sqlGetString(res, row, "stoloc"));
	sqlFreeResults(res);
        sprintf(sqlBuffer1, "update locmst set rescod = '%s' where stoloc = '%s'", ship_id, NextSeq);
        ret_status = sqlExecStr(sqlBuffer1, NULL);
        if (ret_status != eOK)
		memset(NextSeq, 0, sizeof(NextSeq));
			
	return (NextSeq);
	}
} 
	


