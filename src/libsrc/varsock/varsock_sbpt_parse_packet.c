static char RCS_Id[] = "$Id: varsock_sbpt_parse_packet.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/libsrc/varsock/varsock_sbpt_parse_packet.c,v $
 *  $Revision: 1.1.1.1 $
 *  $Author: lh51sh $
 *
 *  Description: VARSOCK-level command. Contains Skill Bosch specific protocol details.
 *
 *  $McHugh_Copyright-Start$
 *
 *  Copyright (c) 1999
 *  McHugh Software International
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by McHugh Software
 *  International.
 *
 *  McHugh Software International assumes no responsibility for the use of
 *  the software described in this document on equipment which has not been
 *  supplied or approved by McHugh Software International.
 *
 *  $McHugh_Copyright-End$
 *
 *#END************************************************************************/

#include <moca.h>

#include <stdio.h>
#include <string.h>

#include <mocaerr.h>
#include <srvlib.h>
#include <applib.h>

#include <varsock_colwid.h>
#include <varsock_err.h>

#include <varsock_util.h>
#include <varsock_sbpt.h>

/* Manifest constants used in this module only */
/* names of the input parameters, used in validation of input */
static const char *const MESSAGE_STRING     = "message_string";
static const char *const MESSAGE_LENGTH     = "message_length";

/* columns names */
static const char *const READ_MORE   = "read_more";
static const char *const STATUS      = "status";
static const char *const DATA_STRING = "data_string";
static const char *const DATA_LENGTH = "data_length";
static const char *const ACK_STRING  = "ack_string";
static const char *const ACK_LENGTH  = "ack_length";
static const char *const NAK_STRING  = "nak_string";
static const char *const NAK_LENGTH  = "nak_length";

/* forward declaration of local static functions. */
static RETURN_STRUCT *sGetResult(long read_more, long status,
		const char *data_string, long data_length, const char *ack_string,
		long ack_length, const char *nak_string, long nak_length);
static void sCheckMessageSyntax(const char *msg, long len, int *p_read_more, int *p_status,
		char *data, long *p_data_len, char *mbx);

/* The main implementation */
LIBEXPORT 
RETURN_STRUCT *varsock_sbpt_parse_packet (
	char *message_string_i, /* The message packet that was send to host */
	long *message_length_i  /* The length of the message packet. This is to allow
							 for message having nulls get passed around. */
)
{
	/* locals for storing input variables */
	long message_length;

	/* The variables that contain tha return values. */
	long read_more = 1; /* by default assume that we do NOT have complete message. */
	long status = 0; /* by default assume that message does NOT have correct syntax. */
	char *data_string = 0;
	long data_length;
	char ack_string [ ACK_NAK_MSG_LEN + 1 ];
	long ack_length;
	char nak_string [ ACK_NAK_MSG_LEN + 1 ];
	long nak_length;
	char mailbox_name[ MAILBOX_NAME_LEN + 1 ];

	/* scratch variables -- may get used in more than one context. */
	long retVal = !eOK;
	char buffer[ 1024 ];

	/* Other variables */
	RETURN_STRUCT *Result = 0;
	const char *const fn = "var_sl_socket_prase_packet_sbpt";

	misTrc (T_FLOW, "%s - entered", fn);

	/* firewall the input arguments. */
	/* Even though we do NOT use the message_string we must check it to make sure
	 * that socket adapter code pass it in. */
	if (!message_string_i) {
		/* do NOT check for trimmed length as message may contain leading spaces! */
		misTrc (T_FLOW, "%s - Required argument %s was not specified!  Exiting...", fn, MESSAGE_STRING);
		return APPMissingArg(MESSAGE_STRING);
	}
	else {
		/* NOTE that though we require that the argument itself be supplied we allow it to poing to a 
		 * NULL string.  While the adapter code should NEVER have to call this component with a NULL
		 * string we are lax in our checking in the hopes that in case of a problem someone will look
		 * at the trace and figure out what is causing the problem. */
		/* do NOT do trimmed copy in a local variable either, as the message can contain spaces. */
		misTrc(T_FLOW, "%s - Argument %s(%s) checks out fine.", fn, MESSAGE_STRING, message_string_i);
	}

	/* message_length parameter is required and it should be more than 0 */
	if (!message_length_i) {
		misTrc(T_FLOW, "%s - Required argument %p was not specified! Exiting...", fn, MESSAGE_LENGTH, message_length_i);
		return APPMissingArg(MESSAGE_STRING);
	}
	else {
		/* NOTE that since we allow a null string be passed in as a message_string we allow message_length to be 0. */
		message_length = *message_length_i;
		misTrc(T_FLOW, "%s - checking is argument %s(%ld) is valid.", fn, MESSAGE_LENGTH, message_length);
		if (message_length < 0) {
			ZAP(buffer);
			sprintf(buffer, "%s specified as %ld.  It must NOT be less than 0.", MESSAGE_LENGTH, message_length);
			misTrc(T_FLOW, "%s - %s", buffer);
			return APPInvalidArg(MESSAGE_LENGTH, buffer);
		}
		misTrc(T_FLOW, "%s - argument %s(%ld) checks out fine!", fn, MESSAGE_LENGTH, message_length);
	}

	/* Log the value of input parameters */
	misTrc(T_FLOW, "%s - input values: %s(%s) %s(%ld)", fn,
			MESSAGE_STRING, message_string_i, MESSAGE_LENGTH, message_length);

	misTrc(T_FLOW, "MOCA does not allow passing around strings containing embedded NULLs");
	misTrc(T_FLOW, "Any NULLs in the string would have been replaced before sending it in to this function.");
	misTrc(T_FLOW, "Hence, we must now convert back to get the original string.");
	sChagneIntoOriginalString(message_string_i, message_length);

	misTrc(T_FLOW, "%s - New input values: %s(%s) %s(%ld)", fn,
			MESSAGE_STRING, message_string_i, MESSAGE_LENGTH, message_length);

	/* setup the return values. */
	read_more = 1;      /* by default assume that we do NOT have complete message. */
	status = 0;         /* by default assume that message does NOT have correct syntax. */
	/* data portion contained in the message string passed in can NOT be any more than the message string itself. */
	data_string = (char *)malloc(message_length * sizeof(char) + 1); /* allocate the data string to hold the user data. */
	memset(data_string, 0, message_length + 1); /* zero the data_string. */
	data_length = 0;    /* hence, current length of the data_string is 0. */

	ZAP( ack_string );              /* zero the ack_string. */
	strcpy(ack_string, ACK); /* fill in the ack_string */
	ack_length = ACK_STRING_LEN;    /* and its current length, i.e. 1. */

	ZAP( nak_string );              /* zero the nak_string. */
	strcpy(nak_string, NAK); /* fill in the nak_string */
	nak_length = NAK_STRING_LEN;    /* and its current length, i.e. 1. */

	sCheckMessageSyntax(message_string_i, message_length, &read_more, &status,
			data_string, &data_length, mailbox_name);

	Result = sGetResult(read_more, status, data_string, data_length,
			ack_string, ack_length, nak_string, ack_length);

	/* free any allocated memory before leaving */
	free(data_string);

	return Result;
}

/* this function takes the result values as input and sets up the results,
 * stuffs the result values in an returns the return struct back to caller. */
RETURN_STRUCT *sGetResult(
		long read_more,
		long status,
		const char *data_string,
		long data_length,
		const char *ack_string,
		long ack_length,
		const char *nak_string,
		long nak_length)
{
	const char *const fn = "varsock_sbpt_parse_packet::sGetResult";
	/* pointer whoes value would be return to the caller. */
	RETURN_STRUCT *Result = 0;
	/* scratch varibles. */
	long retVal = !eOK;

	misTrc(T_FLOW, "%s - entered", fn);
	/* setup the column names in the results touple */
	Result = srvResultsInit(eOK,
			READ_MORE,   COMTYP_INT,  sizeof(long),
			STATUS,      COMTYP_INT,  sizeof(long),
			DATA_STRING, COMTYP_CHAR, DATA_STRING_LEN,
			DATA_LENGTH, COMTYP_INT,  sizeof(long),
			ACK_STRING,  COMTYP_CHAR, ACK_NAK_MSG_LEN,
			ACK_LENGTH,  COMTYP_INT,  sizeof(long),
			NAK_STRING,  COMTYP_CHAR, ACK_NAK_MSG_LEN,
			NAK_LENGTH,  COMTYP_INT,  sizeof(long),
			NULL);

	if (!Result) {
		/* srvResultInit failed! */
		misTrc(T_FLOW, "%s - Unexpected error! srcResultsInit() failed.  Exiting...", fn);

		/* return error to calling process. */
		return srvResults(eVAR_SRV_RESULTS_INIT_FAILED, NULL);
	}

	retVal = srvResultsAdd(Result, read_more, status, data_string, data_length,
			ack_string, ack_length, nak_string, nak_length);
	if (retVal != eOK) {
		misTrc(T_FLOW, "%s - unexpected error! srvResultsAdd returned error(%ld)!"
				"  Exiting...", fn, retVal);
		return srvResults(eVAR_SRV_RESULTS_ADD_FAILED, NULL);
	}

	misTrc(T_FLOW, "%s - sending back %s(%ld), %s(%ld), %s(%s), %s(%ld),"
			" %s(%s), %s(%ld) %s(%s) %s(%ld)", fn, READ_MORE, read_more, STATUS, status,
			DATA_STRING, data_string, DATA_LENGTH, data_length,
			ACK_STRING, ack_string, ACK_LENGTH, ack_length,
			NAK_STRING, nak_string, NAK_LENGTH, nak_length);

	return Result;
}

/* Takes the packet message string as an input and extracts the user data string and
 * mailbox name out of it.  Currently we have no need for the mailbox_name, but we still
 * extract and send it back to the caller.
 * If the message is not in a correct format, i.e.
 * - mailbox name is padded to the left with spaces
 * - the last character is NOT a NULL - '\0' - character
 * - the data length portion of the header is not composed of valud hex digit characters,
 *   i.e. /[0123456789abcdefABCDEF]/.
 * - the message_length != (HEADER_LENGTH + data portion length)
 * then the data is set to null string and data_length is set to 0.
 * The function returns void. */
static void sCheckMessageSyntax(const char *msg, long len, int *p_read_more, int *p_status,
		char *data, long *p_data_len, char *mbx)
{
	const char *const fn = "varsock_sbpt_parse_packet::sCheckMessageSyntax";
	long retVal = !eOK;
	struct msg_header header;
	int misc_retval;
	/* sscanf read a "%x" hex value into an int, so we need this temp int. */
	int data_len = 0;

	misTrc(T_FLOW, "%s - entered", fn);
	/* by default assume that
	 * - the message syntax is screwed up.
	 * - The message format is so bad that it is not possible to ascertain user data passed in
	 * - we cannot ascertain the mailbox name from the packet. */
	*p_read_more = 1; /* we need to read more data */
	*p_status = 0;    /* the syntax is screwed up */
	*data = 0;        /* zap the user data string */
	*p_data_len = 0;  /* hence, the user data length is zero. */
	*mbx = 0;         /* we cannot ascertain the mailbox name from the header */

	misTrc(T_FLOW, "%s - have we read in one complete header yet? msg(%s), len(%ld).", fn, msg, len);
	if (len < HEADER_LENGTH) {
		misTrc(T_FLOW, "%s - we have not read one complete header yet! Need to read more characters.", fn);
		/* we need to read more characters for the complete message, and hence status is a non-issue. */
		*p_read_more = 1; *p_status = 0;
		return;
	}

	/* copy the entire message (len bytes) into a local struct to easily access its components. */
	memset(&header, 0, sizeof(header)); /* zap the header, too -- just in case. */
	memcpy(&header, msg, len);

	misTrc(T_FLOW, "%s - check if the mailbox name portion of the header is correctly formatted.", fn);
	memcpy(mbx, header.mailbox_name, MAILBOX_NAME_LEN); /* copy the first 31 bytes */
	mbx[MAILBOX_NAME_LEN] = 0; /* terminate the string */

	misTrc(T_FLOW, "%s - ensure that the mailbox name (%s) does not have leading spaces.", fn, mbx);
	if ( (misc_retval = strspn(mbx, " ")) != 0 ) {
		misTrc(T_FLOW, "%s - strspn: retval(%i)", fn, misc_retval);
		misTrc(T_FLOW, "%s - mailbox name portion of message header (%s) is prefixed with spaces."
				" This is wrong!", fn, mbx);
		misTrc(T_FLOW, "%s - if mailbox name is less than %i long pad it with space to the RIGHT.", fn, MAILBOX_NAME_LEN);
		/* do NOT read any more characters, since the sytax is wrong. */
		*p_read_more = 0; *p_status = 0;
		return;
	}

	misTrc(T_FLOW, "%s - trim off the trailing spaces from the mailbox name(%s)", fn, mbx);
	misTrim(mbx);
	misTrc(T_FLOW, "%s - trimmed mailbox name is (%s).", fn, mbx);

	/* we have to check for last character being null before trying to read the data portion length because
	 * otherwise it would NOT be a null terminated string. */
	misTrc(T_FLOW, "%s - check if the last character of the header (%i) is a NULL.", fn, (int)header.null_term);
	if (header.null_term != 0) {
		misTrc(T_FLOW, "%s - last character of the message header is NOT a NULL. Illegal header format", fn);
		/* do NOT read any more characters, since the sytax is wrong. */
		*p_read_more = 0; *p_status = 0;
		return;
	}

	misTrc(T_FLOW, "%s - check if the data message length portio (%s) has valid hex characters", fn, header.len_str);
	if (strspn(header.len_str, "0123456789ABCDEFabcdef") != 3) {
		misTrc(T_FLOW, "%s - data length portion of the message header (%s) is does NOT contain a valid"
				" hex digit string", fn, header.len_str);
		misTrc(T_FLOW, "%s - illegal message header!", fn);
		/* do NOT read any more characters, since the sytax is wrong. */
		*p_read_more = 0; *p_status = 0;
		return;
	}

	misTrc(T_FLOW, "%s - convert the length string to integer value", fn);
	misc_retval = sscanf(header.len_str, "%x", &data_len);
	if (misc_retval != 1) {
		misTrc(T_FLOW, "%s - sscanf: retval (%i): sscanf canot covert the hex string to int.", fn, misc_retval);
		misTrc(T_FLOW, "%s - this is an SERIOUS unexpected error! For now, treat it as incorrect syntax.", fn);
		*p_read_more = 0; *p_status = 0;
		return;
	}

	misTrc(T_FLOW, "%s - length of the data portion of this message is (%i).", fn, data_len);
	*p_data_len = data_len;

	misTrc(T_FLOW, "%s - check if the message length (%ld) is what we expect it to be (%i).", fn,
			len, (int)(HEADER_LENGTH + *p_data_len));
	if ( len < (HEADER_LENGTH + *p_data_len) ) {
		misTrc(T_FLOW, "%s - insufficient or incomplete message data (%s<NULL>%s) message length(%ld)", fn,
				header.mailbox_name, header.data_portion, len);
		/* we need to read more characters for the complete message, and hence status is a non-issue. */
		*p_read_more = 1; *p_status = 0;
		return;
	}
	else if ( len > (HEADER_LENGTH + *p_data_len) ) {
		misTrc(T_FLOW, "%s - we have read in more than what we need!  This should NEVER have happened!", fn);
		misTrc(T_FLOW, "%s - there are SERIOUS PROBLEMS in the code/format.  For now assume an invalid syntax.", fn);
		*p_read_more = 0; *p_status = 0;
		return;
	}

	misTrc(T_FLOW, "%s - extract the data portion from the message string(%s<NULL>%s)", fn,
			header.mailbox_name, header.data_portion);
	memcpy(data, header.data_portion, *p_data_len);
	data[*p_data_len] = 0;
	misTrc(T_FLOW, "%s - extracted data portion is (%s).", fn, data);
	/* all's well: we have a complete message and it has correct syntax. */
	*p_read_more = 0; *p_status = 1;
	return;
}

