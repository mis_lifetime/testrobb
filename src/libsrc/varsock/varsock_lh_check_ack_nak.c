static char RCS_Id[] = "$Id: varsock_lh_check_ack_nak.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/libsrc/varsock/varsock_lh_check_ack_nak.c,v $
 *  $Revision: 1.1.1.1 $
 *  $Author: lh51sh $
 *
 *  Description: Simple component to test a VAR-level command.
 *
 *  $McHugh_Copyright-Start$
 *
 *  Copyright (c) 1999
 *  McHugh Software International
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by McHugh Software
 *  International.
 *
 *  McHugh Software International assumes no responsibility for the use of
 *  the software described in this document on equipment which has not been
 *  supplied or approved by McHugh Software International.
 *
 *  $McHugh_Copyright-End$
 *
 *#END************************************************************************/

#include <moca.h>

#include <stdio.h>
#include <string.h>

#include <mocaerr.h>
#include <srvlib.h>
#include <oslib.h>
#include <applib.h>

#include <varsock_colwid.h> /* varsock specific colwid */
#include <varsock_err.h>    /* varsock specific err */
#include <varsock_util.h>   /* misc for socket adapter */
#include <varsock_lh.h>     /* specific to Lifetime Hoan protocol. */

/* Manifest constants used in this module only */
/* names of the input parameters, used in validation of input */
static const char *const MESSAGE_STRING    = "message_string";
static const char *const MESSAGE_LENGTH    = "message_length";
static const char *const RESPONSE_STRING   = "response_string";
static const char *const RESPONSE_LENGTH   = "response_length";

/* columns names */
static const char *const READ_MORE         = "read_more";
static const char *const ACK_COL_NAME      = "ack";

/* other #defines go here */

/* forward declaration of local functions. */

/* global variables. */

/* static functions */
static int sCompareSequenceNumbers(const char *s2, const char *s1);

/* In this protocol the length of the ACK/NAK string is fixed.  Hence, the outbound
 * socket adapter should already have the MIN_ACK_NAK_LEN available to it when it
 * would have called the the server command to get the PREFIX and SUFFIX for sending
 * the message out over the socket.  Hence, in practise it should never happen that this
 * command would get called with insufficient number of characters passed in the 
 * response string.  However, still this component defensibly checks for the length
 * of the response string received and accordingly sets and returns the READ_MORE variable.
 */
/* The main implementation */
LIBEXPORT 
RETURN_STRUCT *varsock_lh_check_ack_nak (
	char *message_string_i,  /* The message packet that was send to host */
	long *message_length_i,  /* length of the character string pointed to by message_string_i */
	char *response_string_i, /* Response received from the host system. */
	long *response_length_i  /* Length of the character string pointed to by response_string_i */
)
{
	/* locals for storing input variables */
	char message_string [ MESSAGE_STRING_LEN + 1 ];
	long message_length;
	char response_string [ RESPONSE_STRING_LEN + 1 ];
	long response_length;

	/* scratch variables -- may get used in more than one context. */
	char buffer[1024];
	long retVal = !eOK;

	/* Other variables */
	RETURN_STRUCT *Result = 0;
	const char *const fn = "varsock_lh_check_ack_nak";
	int sequence_number = 1;
	/* by default assume that we were passed in sufficient number of characters as the ACK/NAK string */
	int read_more = 0;
	/* by default assume that it is a NAK */
	int isItAck = 0;

	misTrc (T_FLOW, "In %s()", fn);

	/* firewall the input arguments. */
	/* message_string is required. */
	if (!message_string || !misTrimLen(message_string, MESSAGE_STRING_LEN)) {
		misTrc (T_FLOW, "%s - Required argument %s was not specified!  Exiting...", fn, MESSAGE_STRING);
		return APPMissingArg(MESSAGE_STRING);
	}
	else {
		ZAP(message_string);
		misTrimcpy(message_string, message_string_i, MESSAGE_STRING_LEN);
		misTrc(T_FLOW, "%s - Argument %s(%s) checks out fine.", fn, MESSAGE_STRING, message_string);
	}

	/* message_length is required. */
	if (!message_length_i) {
		misTrc(T_FLOW, "%s - Required argument %s was not specified!  Exiting...", fn, MESSAGE_LENGTH);
		return APPMissingArg(MESSAGE_LENGTH);
	}
	else {
		message_length = *message_length_i;
		misTrc(T_FLOW, "%s - checking if the argument %s(%ld) is valid", fn, MESSAGE_LENGTH, message_length);
		if (message_length <= 0) {
			ZAP(buffer);
			sprintf(buffer, "argument %s was specified as %ld.  It should me more than 0",
					MESSAGE_LENGTH, message_length);
			misTrc(T_FLOW, "%s - %s", fn, buffer);
			return APPInvalidArg(MESSAGE_LENGTH, buffer);
		}
		misTrc(T_FLOW, "%s - argument %s(%ld) checks out fine.", fn, MESSAGE_LENGTH, message_length);
	}

	/* response_string is required. */
	if (!response_string_i || !*response_string_i) {
		misTrc (T_FLOW, "%s - Required argument %s was not specified!  Exiting...", fn, RESPONSE_STRING);
		return APPMissingArg(RESPONSE_STRING);
	}
	else {
		ZAP(response_string);
		misTrimcpy(response_string, response_string_i, RESPONSE_STRING_LEN);
		misTrc(T_FLOW, "%s - Argument %s(%s) checks out fine.", fn, RESPONSE_STRING, response_string);
	}

	/* response_length is required. */
	if (!response_length_i) {
		misTrc(T_FLOW, "%s - required argument %s was not specified.", fn, RESPONSE_LENGTH);
		return APPMissingArg(RESPONSE_LENGTH);
	}
	else {
		response_length = *response_length_i;
		misTrc(T_FLOW, "%s - checking if argument %s(%ld) is valid.", fn, RESPONSE_LENGTH, response_length);
		if (response_length <= 0) {
			ZAP(buffer);
			sprintf(buffer, "argument %s is specified as %ld.  It should be more than 0",
					RESPONSE_LENGTH, response_length);
			misTrc(T_FLOW, "%s - %s", fn, buffer);
			return APPInvalidArg(RESPONSE_LENGTH, buffer);
		}
		misTrc(T_FLOW, "%s - argument %s(%ld) checks out fine.", fn, RESPONSE_LENGTH, response_length);
	}
	
	/* Log the value of input parameters */
	misTrc(T_FLOW, "%s - input values: %s(%s), %s(%ld), %s(%s), %s(%ld)",
			fn, MESSAGE_STRING, message_string, MESSAGE_LENGTH, message_length,
			RESPONSE_STRING, response_string, RESPONSE_LENGTH, response_length);

	/* the length of the response string should be exactly [<STX>99999<ACK><ETX> = ] 8 characters long */
	if (response_length != ACK_NAK_MSG_LEN) {
		misTrc(T_FLOW, "%s - incorrect %s(%ld).  Correct length is %i."
				" Invalid ACK/NAK string received.  Assuming a NAK!", fn,
				RESPONSE_LENGTH, response_length, ACK_NAK_MSG_LEN);
		read_more = 1;
	}
	/* make sure that the message string had at least one data bit in it, i.e.
	 * that it was at least 8 characters long.  Else it is an illegal message string! */
	else if (message_length < ACK_NAK_MSG_LEN) {
		misTrc(T_FLOW, "%s - incorrect length (%ld) of message string!"
				"  A message string smaller than %i characters is illegal."
				"  Assumin a NAK!", fn, message_length, ACK_NAK_MSG_LEN);
	}
	/* check that the response starts with an <STX> */
	else if (response_string[0] != STX) {
		misTrc(T_FLOW, "%s - response_string does not start with <STX>."
				"  It starts with %i where as it should be %i."
				"  Invalid ACK/NAK string received.  Assuming a NAK!", fn,
				(int)response_string[0], (int)STX);
	}
	/* check that the response string ends with an <ETX>. */
	else if (response_string[7] != ETX) {
		misTrc(T_FLOW, "%s - ACK/NAK response_string does NOT end with <ETX>."
				"  Invalid format of the response string.  Assuming a NAK!", fn);
	}
	else if (!sCompareSequenceNumbers(message_string, response_string)) {
		misTrc(T_FLOW, "%s - Sequence numbers on the message and response strings do not match."
				"  Invalid response string.  Assuming a NAK!", fn);
	}
	else if (response_string[6] == NAK) {
		misTrc(T_FLOW, "%s - host has sent a <NAK> character in data portion"
				" of the response_string.", fn);
	}
	else if (response_string[6] == ACK) {
		misTrc(T_FLOW, "%s - format of the ACK/NAK response string is correct and"
				" the host has sent an <ACK> character back in the data portion.", fn);
		isItAck = 1;
	}
	else { /* (response_string[6] != NAK && response_string != ACK) */
		misTrc(T_FLOW, "%s - data portion of the ACK/NAK string is neither an"
				" <ACK> character nor a <NAK> character!  Invalid message data."
				"  Assuming a NAK!", fn);
	}

	/* setup the column names in the results touple */
	Result = srvResultsInit(eOK,
			READ_MORE, COMTYP_INT, sizeof(long),
			ACK_COL_NAME, COMTYP_INT, sizeof(long),
			NULL);

	if (!Result) {
		/* srvResultInit failed! */
		misTrc(T_FLOW, "%s - Unexpected error! srcResultsInit() failed.  Exiting...", fn);

		/* return error to calling process. */
		return srvResults(eVAR_SRV_RESULTS_INIT_FAILED, NULL);
	}

	retVal = srvResultsAdd(Result, read_more, isItAck);
	if (retVal != eOK) {
		misTrc(T_FLOW, "%s - unexpected error! srvResultsAdd() returned error(%ld)!"
				"  Exiting...", fn, retVal);

		srvFreeMemory(SRVRET_STRUCT, Result);
		return srvResults(eVAR_SRV_RESULTS_ADD_FAILED, 
				  NULL);
	}

	return Result;
}

static int sCompareSequenceNumbers(const char *s2, const char *s1)
{
	/* characters 2 through 6 of the message and response string
	 * represent the sequence number.  So compare if these 5 characters
	 * are the same in two strings or not. */
	return !(strncmp(s1+1, s2+1, 5));
}

