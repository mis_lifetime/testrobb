/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 2002
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

LIBEXPORT 
RETURN_STRUCT *varRegisterCountAtLocation(char *stoloc_i,
				 char *cntbat_i,
				 char *cntgrp_i,
				 char *prtnum_i,
				 char *prt_client_id_i,
				 char *inval1_i,
				 char *inval2_i,
				 char *inval3_i,
				 long *cntqty_i,
				 char *usr_id_i,
				 char *devcod_i)
{
    mocaDataRow *row;
    mocaDataRes *res;

    long ret_status;
    char cntbat[CNTBAT_LEN + 1];
    char cntgrp[CNTGRP_LEN + 1];
    char usr_id[USR_ID_LEN + 1];
    char stoloc[STOLOC_LEN + 1];
    char prtnum[PRTNUM_LEN + 1];
    char prt_client_id[CLIENT_ID_LEN + 1];
    char inval1[INVVAL_LEN + 1];
    char inval2[INVVAL_LEN + 1];
    char inval3[INVVAL_LEN + 1];
    char devcod[DEVCOD_LEN + 1];
    long cntqty;

    char columns[1000];
    char buffer[2000];
    char tmpstr[100];
    memset(usr_id, 0, sizeof(usr_id));
    memset(devcod, 0, sizeof(devcod));
    memset(cntgrp, 0, sizeof(cntgrp));
    memset(cntbat, 0, sizeof(cntbat));
    memset(stoloc, 0, sizeof(stoloc));
    memset(prtnum, 0, sizeof(prtnum));
    memset(prt_client_id, 0, sizeof(prt_client_id));
    memset(inval1, 0, sizeof(inval1));
    memset(inval2, 0, sizeof(inval2));
    memset(inval3, 0, sizeof(inval3));
    
    cntqty = 0;

    if (stoloc_i && misTrimLen(stoloc_i, STOLOC_LEN))
	misTrimcpy(stoloc, stoloc_i, STOLOC_LEN);
    else
	return (srvSetupReturn(eINVALID_ARGS, ""));

    if (prtnum_i && misTrimLen(prtnum_i, PRTNUM_LEN))
	misTrimcpy(prtnum, prtnum_i, PRTNUM_LEN);

    if (cntbat_i && misTrimLen(cntbat_i,CNTBAT_LEN))
        misTrimcpy(cntbat, cntbat_i, CNTBAT_LEN);

    if (cntgrp_i && misTrimLen(cntgrp_i,CNTGRP_LEN))
        misTrimcpy(cntgrp, cntgrp_i, CNTGRP_LEN);

    /*
     * Get the client Id Value.
     */

    ret_status = appGetClient(prt_client_id_i, prt_client_id);
    if (ret_status != eOK && ret_status != eINVALID_ARGS)
	return (srvSetupReturn(ret_status, ""));

    if (inval1_i && misTrimLen(inval1_i, INVVAL_LEN))
	misTrimcpy(inval1, inval1_i, INVVAL_LEN);

    if (inval2_i && misTrimLen(inval2_i, INVVAL_LEN))
	misTrimcpy(inval2, inval2_i, INVVAL_LEN);

    if (inval3_i && misTrimLen(inval3_i, INVVAL_LEN))
	misTrimcpy(inval3, inval3_i, INVVAL_LEN);

    if (usr_id_i && misTrimLen(usr_id_i, USR_ID_LEN))
	misTrimcpy(usr_id, usr_id_i, USR_ID_LEN);
    else
	strncpy(usr_id, 
		osGetVar(LESENV_USR_ID) ? osGetVar(LESENV_USR_ID) : "", 
		USR_ID_LEN);

    if (devcod_i && misTrimLen(devcod_i, DEVCOD_LEN))
	misTrimcpy(devcod, devcod_i, DEVCOD_LEN);
    else
	strncpy(devcod, 
		osGetVar(LESENV_DEVCOD) ? osGetVar(LESENV_DEVCOD) : "",
		DEVCOD_LEN);


    if (!cntqty_i || *cntqty_i < 0)
	return (srvSetupReturn(eINVALID_ARGS, ""));
    else
	cntqty = *cntqty_i;
    
    /* MR 4361,  RP. Do not set the cntsts to 'C'. We will allow complete counts
       to do it for us.
    sprintf(buffer,
	    "update cntwrk "
	    "   set cntqty = '%ld', "
	    "       cnt_usr_id = '%s', "
	    "       cntdev = '%s', "
	    "       cntsts = '%s' "
	    " where stoloc = '%s' ",
	    cntqty, usr_id, devcod, CNTSTS_COMPLETE, stoloc); */
	    
    sprintf(buffer,
	    "update cntwrk "
	    "   set cntqty = '%ld', "
	    "       cnt_usr_id = null, "
	    "       cntdev = '%s' "
	    " where stoloc = '%s' ",
	    cntqty, devcod, stoloc); 
	    
    if (strlen(prtnum))
    {
	sprintf(tmpstr, " and prtnum = '%s'", prtnum);
	strcat(buffer, tmpstr);
    }

    if (strlen(prt_client_id))
    {
	sprintf(tmpstr, " and prt_client_id = '%s'", prt_client_id);
	strcat(buffer, tmpstr);
    }

    if (strlen(inval1))
    {
	sprintf(tmpstr, " and inval1 = '%s'", inval1);
	strcat(buffer, tmpstr);
    }
    if (strlen(inval2))
    {
	sprintf(tmpstr, " and inval2 = '%s'", inval2);
	strcat(buffer, tmpstr);
    }
    if (strlen(inval3))
    {
	sprintf(tmpstr, " and inval3 = '%s'", inval3);
	strcat(buffer, tmpstr);
    }
    if (strlen(cntbat))
    {
	sprintf(tmpstr, " and cntbat = '%s'", cntbat);
	strcat(buffer, tmpstr);
    }
    if (strlen(cntgrp))
    {
	sprintf(tmpstr, " and cntgrp = '%s'", cntgrp);
	strcat(buffer, tmpstr);
    }

    ret_status = sqlExecStr(buffer, NULL);
    if (eOK != ret_status)
    {
	/* Update wasn't there...we prompted for a given count,
	   but counted something entirely different... */

	sprintf(buffer,
		"select * from cntwrk "
		" where stoloc = '%s' ",
		stoloc);

	if (strlen(cntbat))
	{
	    sprintf(tmpstr, " and cntbat = '%s' ", cntbat);
	    strcat(buffer, tmpstr);
	}
	if (strlen(cntgrp))
	{
	    sprintf(tmpstr, " and cntgrp = '%s' ", cntgrp);
	    strcat(buffer, tmpstr);
	}

	ret_status = sqlExecStr(buffer, &res);
	if (eOK != ret_status)
	{
	    sqlFreeResults(res);
	    misTrc(T_FLOW, 
		   "Could not determine a count information for location");
	    return(srvSetupReturn(ret_status, ""));
	}

	row = sqlGetRow(res);

	columns[0] = '\0';
	buffer[0] = '\0';
	if (inval1[0])
	{
	    strcat(columns, "inval1");
	    sprintf(tmpstr, "'%s'", inval1);
	    strcat(buffer, tmpstr);
	}
	if (inval2[0])
	{
	    if (columns[0])
	    {
		strcat(columns, ",");
		strcat(buffer, ",");
	    }
	    strcat(columns, "inval2");
	    sprintf(tmpstr, "'%s'", inval2);
	    strcat(buffer, tmpstr);
	}
	if (inval3[0])
	{
	    if (columns[0])
	    {
		strcat(columns, ",");
		strcat(buffer, ",");
	    }
	    strcat(columns, "inval3");
	    sprintf(tmpstr, "'%s'", inval3);
	    strcat(buffer, tmpstr);
	}
	if (prtnum[0])
	{
	    if (columns[0])
	    {
		strcat(columns, ",");
		strcat(buffer, ",");
	    }
	    strcat(columns, "prtnum");
	    sprintf(tmpstr, "'%s'", prtnum);
	    strcat(buffer, tmpstr);
	}

	if (prt_client_id[0])
	{
	    if (columns[0])
	    {
		strcat(columns, ",");
		strcat(buffer, ",");
	    }
	    strcat(columns, "prt_client_id");
	    sprintf(tmpstr, "'%s'", prt_client_id);
	    strcat(buffer, tmpstr);
	}

	if (columns[0])
	{
	    strcat(columns, ",");
	    strcat(buffer, ",");
	}
	strcat(columns, "cntqty");
	sprintf(tmpstr, "'%d'", cntqty);
	strcat(buffer, tmpstr);

        /* Setting the cnt_usr_id to null. RP, MR 4361.
           In order to allow recovery. Also, we need to
           set the cntsts to 'INPROGRESS' */
	if (columns[0])
	{
	    strcat(columns, ",");
	    strcat(buffer, ",");
	}
	strcat(columns, "cnt_usr_id");
	sprintf(tmpstr, "null");
	strcat(buffer, tmpstr);

	if (columns[0])
	{
	    strcat(columns, ",");
	    strcat(buffer, ",");
	}
	strcat(columns, "cntdev");
	sprintf(tmpstr, "'%s'", devcod);
	strcat(buffer, tmpstr);
	
	if (columns[0])
	{
	    strcat(columns, ",");
	    strcat(buffer, ",");
	}
	strcat(columns, "cntsts");
	sprintf(tmpstr, "'%s'", CNTSTS_INPROGRESS);
	strcat(buffer, tmpstr);

	if (columns[0])
	{
	    strcat(columns, ",");
	    strcat(buffer, ",");
	}
	strcat(columns, "untqty");
	strcat(buffer, "'0'");

	ret_status = appGenerateTableEntry("cntwrk",
					   res, 
					   row,
					   columns,
					   buffer);
	sqlFreeResults(res);
	if (eOK != ret_status)
	{
	    return(srvSetupReturn(ret_status, ""));
	}
	

    }
    return (srvSetupReturn(eOK, ""));
}
