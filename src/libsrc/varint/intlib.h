/*#START***********************************************************************
 *
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  $Source: /cvs/les/wm/src/libsrc/dcsint/intlib.h,v $
 *  $Revision: 1.7 $
 *  $Id: intlib.h,v 1.7 1999/11/09 01:54:10 chavez Exp $
 *
 *  Application:  Intrinsic Library
 *  Created:   31-Jan-1994
 *  $Author: chavez $
 *
 *  Purpose:   Structure defs for INTLIB
 *
 *#END************************************************************************/

#ifndef INTLIB_H
#define INTLIB_H

#include <applib.h>

RETURN_STRUCT *int_UpdateReceiveLine(char *srcloc_i,
				     char *dstloc_i, 
				     char *client_id_i,
				     char *supnum_i,
				     char *invnum_i,
				     char *lodnum_i,
				     char *subnum_i,
				     char *dtlnum_i,
				     char *updmod_i);

#endif

