static const char *rcsid = "$Id: varSplitShipmentLoad.c,v 1.6 2002/01/23 15:45:34 prod Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/libsrc/varint/varSplitShipmentLoad.c,v $
 *  $Revision: 1.6 $
 *
 *  Application:   intlib
 *  Created:       20-Sep-1996
 *  $Author: prod $
 *
 *
 #END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <applib.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>
#include <varerr.h>


LIBEXPORT 
RETURN_STRUCT *varSplitShipmentLoad(char *lodnum_i,
				    char *subnum_i,
				    char *new_ship_id_i)
{
    long            ret_status;
    long	    count = 0;
    RETURN_STRUCT  *CurPtr;
    mocaDataRes    *res; 
    mocaDataRow    *row;

    char            buffer[1000];
    char	    stgdte[18] ;
    char            ship_id_src[SHIP_ID_LEN + 1];
    char            new_ship_id[SHIP_ID_LEN + 1];
    
    char	    lodnum[LODNUM_LEN] ;
    char	    subnum[SUBNUM_LEN] ;
   
    memset (lodnum, 0, sizeof (lodnum));
    memset (subnum, 0, sizeof (subnum));
    /* memset (stgdte, 0, sizeof (stgdte));  */

    if ((!lodnum_i || 0 == misTrimLen(lodnum_i, LODNUM_LEN)) &&
	(!subnum_i || 0 == misTrimLen(subnum_i, SUBNUM_LEN)))
    {
	return srvSetupReturn(eINT_REQ_PARAM_MISSING, "");
    }

  
    if (new_ship_id_i && misTrimLen(new_ship_id_i, SHIP_ID_LEN))
        misTrimcpy(new_ship_id, new_ship_id_i, SHIP_ID_LEN);
    else
	return APPMissingArg("ship_id");
   
    /*
     * Use details if available, otherwise, join with sub to get
     * ship_line_id's
     *
     * Join with details to get lodnum
     */

    if (subnum_i && 0 != misTrimLen(subnum_i, SUBNUM_LEN))
    {
	strcpy ( subnum, subnum_i) ;
	sprintf(buffer, 
		"  select distinct shipment.ship_id ship_id_src   "
		"    from invdtl, shipment_line, shipment  " 
		"   where invdtl.ship_line_id = shipment_line.ship_line_id " 
		"     and shipment_line.ship_id = shipment.ship_id "  
		"     and invdtl.subnum = '%s'  ", 
		subnum);
    }
    else
    {
	strcpy ( lodnum, lodnum_i) ;
	sprintf(buffer, 
		"  select distinct shipment.ship_id ship_id_src   "
		"    from invsub, invdtl, shipment_line, shipment  " 
		"   where invdtl.ship_line_id = shipment_line.ship_line_id " 
		"     and invsub.subnum = invdtl.subnum  " 
		"     and shipment_line.ship_id = shipment.ship_id "  
		"     and invsub.lodnum = '%s'  ", 
		lodnum);
    }

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status, ""));
    }


    /* make sure we found only one ship id */ 
    if (sqlGetNumRows (res) > 1)
    {
	sqlFreeResults(res);
	return (srvSetupReturn(eINT_TOO_MANY_SHIP_ID,"")) ;
    }

    row = sqlGetRow (res);
    strcpy (ship_id_src, sqlGetString(res,row,"ship_id_src")); 
    
    /* Verify that We are not dealing with a kit part (Cartonized) */

    sprintf (buffer, 
	     "Select  count(*)  count   "
	     "From    pckwrk  p         "
	     "Where   p.wrktyp = '%s'   "
	     "And     p.subnum in       "
	     "        (select  subnum   "
	     "         from    invsub  ib "
	     "         where   ( ib.lodnum = '%s' "
	     "                or ib.subnum = '%s' ))",
	     WRKTYP_KIT, 
	     lodnum, 
	     subnum);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status, ""));
    }

    row = sqlGetRow (res);
    count = sqlGetLong (res, row, "count");
    sqlFreeResults(res);

    if  (count > 0)
    {
       return (srvSetupReturn(eVAR_CANNOT_SPLIT_KITS, ""));         
    }

    sprintf(buffer, 
	    " select to_char(stgdte, 'YYMMDD HH:MI:SS')  stgdte  " 
	    "   from shipment "
	    " where ship_id = '%s' ",
	    ship_id_src);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status, ""));
    }

    if (ret_status == eOK) 
    {
	row = sqlGetRow (res);
	strcpy (stgdte, sqlGetString(res,row,"stgdte")); 
    }

    /* Un-stage ( if already staged)  the ship id, by un setting the stgdte  */

    if (stgdte) 
    {
    sprintf(buffer, 
	    "update shipment "
	    "   set stgdte = NULL  "
	    " where ship_id = '%s' ",
	    ship_id_src);

    ret_status = sqlExecStr(buffer, NULL) ;
    } 
    if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
    {
	sqlFreeResults(res);  
	return (srvSetupReturn(ret_status,"")) ;
    }

    /* we are all set to call Create New Ship Sequence */ 

    sprintf(buffer, 
	    "Change Inventory Ship Sequence  "
	    " where  lodnum  = '%s'  "
	    "   and  subnum  = '%s'  "
	    "   and  new_ship_id = '%s' "
	    "   and newdst  = 'Y' " ,
	    lodnum, 
	    subnum, 
	    new_ship_id);

    ret_status = srvInitiateCommand (buffer,NULL);

    if (ret_status !=eOK) 
    {
	sqlFreeResults(res);  
	return (srvSetupReturn(ret_status,"")) ;
    }

    
    /* if the shipment was staged, stage it back */     
    if (stgdte) 
    {
	sprintf(buffer, 
		"update shipment "
		"   set stgdte = to_date('%s', 'YYMMDD HH:MI:SS')  "
		" where ship_id = '%s' ",
		stgdte,
		ship_id_src);

	ret_status = sqlExecStr(buffer, NULL) ;
    } 

    if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
    {
	sqlFreeResults(res);  
	return (srvSetupReturn(ret_status,"")) ;
    }
    return (srvSetupReturn(eOK,""));

}
