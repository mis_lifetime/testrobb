static char    *rcsid = "$Id: varAllocateCaseByOrder.c,v 1.11 2002/04/23 00:25:52 prod Exp $";
/*#START***********************************************************************
* McHugh Software International
* Copyright 2001
* Waukesha, Wisconsin, U.S.A.
* All rights reserved.
* DESCRIPTION  :  This function will allocate inventory in such a way that
*                 order's cases will not be split.  We do not want to allocate
*                  a load level pick that has a fraction of a case for an order
*                 This function calls the standard allocate inventory multiple
*                 times to prevent this from happening.  
*#END*************************************************************************/

#include <moca_app.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <applib.h>
#include <dcscolwid.h>
#include <dcsgendef.h>
#include <srvconst.h>
#include <dcserr.h>    

#define LENGTH_OF_LIST 25000
#define LENGTH_OF_SQL_BUFFER 350000

/*Returns the Xth Element from a List seperated by CONCAT_CHAR*/
/*when x == 1, x is the first element in the list*/
char *sGetXElementFromList(char *List, long x)
    {
    char * ptr = NULL;
    misTrc(T_FLOW, "sGetXElementFromList");
    ptr = strtok(List, CONCAT_CHAR);
    x = x -1;
    while (x > 0 && ptr)
        {
        x = x-1;
        ptr = strtok(NULL, CONCAT_CHAR);
        }
    misTrc(T_FLOW, "exit sGetXElementFromList");
    return ptr;
    }
/*Counts the Elements of a List seperated by CONCAT_CHAR*/
long sCountElementsFromList(char *List)
    {
    char *  ptr = NULL;
    long counter = 1;
    misTrc(T_FLOW, "sCountElementsFromList");
    ptr = strtok(List, CONCAT_CHAR);
    while (ptr)
        {
        counter = counter + 1;
        ptr = strtok(NULL, CONCAT_CHAR);
        } 
    return counter;
    }
/*Adds the and clause of allocate inventory.  The buffer is the modified Piece*/
char * sAddToList(char * field_name_list,const long position , char * field_text, char* buffer, char * prefix)
    {
    char  List[LENGTH_OF_LIST];
    char * field_value = NULL;
    memset(List,0,sizeof(List));
    /* misTrimcpy(List, field_name_list, strlen(field_name_list)); */
    field_value = sGetXElementFromList(List, position);
    sprintf(buffer,"%s %s %s = '%s'", buffer, prefix, field_text, field_value);
    return buffer;
    }
/*Adds 1 Element to a List*/
char * sExtendFieldList(char *new_field_list, char * orginal_field_list,long position)
    {
    char  List[LENGTH_OF_LIST];
    char  List2[LENGTH_OF_LIST];
    char * field_value;
    memset(List,0,sizeof(List));
    memset(List2,0,sizeof(List2));
    /*misTrimcpy(List, orginal_field_list, strlen(orginal_field_list));*/
    if (new_field_list && (strlen(new_field_list)>0))
        {
        sprintf(List2,"%s|", new_field_list);
        sprintf(new_field_list, List2);   
        }
    field_value = sGetXElementFromList(List, position);
    if (new_field_list) 
        sprintf(new_field_list,"%s%s", new_field_list, field_value);
    else
        sprintf(new_field_list,"%s", field_value);
    return new_field_list;
    }

LIBEXPORT 
RETURN_STRUCT *varAllocateCaseByOrder(char *ordnum_i, char *ship_id_i, char *ship_line_id_i, char *segqty_i, char * schbat_i, long * pckqty_i, long * untcas_i, char * pcktyp_i,char * prtnum_i,char * prt_client_id_i,char * orgcod_i,char * revlvl_i, char *lotnum_i,char* invsts_prg_i,char* client_id_i,char* stcust_i,char* rtcust_i,char* ordlin_i, char * ordsln_i, char* concod_i,char* dstare_i,char* pcksts_i,char* dstloc_i,long* untpak_i,long* untpal_i,char* pipcod_i, char *trace_suffix_i,long* skip_invlkp_i, moca_bool_t * splflg_i)
    {
    mocaDataRes    *res = NULL;
    mocaDataRow    *row = NULL; 
    long untcas = 0;
    long ret_status;
    long new_pckqty;
    long splflg = 1;
    RETURN_STRUCT *CmdRes=NULL;
    char sqlBuffer[LENGTH_OF_SQL_BUFFER]; 
    char sqlBuffer2[1000];
    char ordnum_new_list[LENGTH_OF_LIST];
    char schbat_new_list[LENGTH_OF_LIST];
    char  ship_id_new_list[LENGTH_OF_LIST];
    char ship_line_id_new_list[LENGTH_OF_LIST];
    char client_id_new_list[LENGTH_OF_LIST];
    char stcust_new_list[LENGTH_OF_LIST] ;
    char rtcust_new_list[LENGTH_OF_LIST];
    char ordlin_new_list[LENGTH_OF_LIST];
    char ordsln_new_list[LENGTH_OF_LIST];
    char concod_new_list[LENGTH_OF_LIST];
    char dstloc_new_list[LENGTH_OF_LIST];
    char dstare_new_list[LENGTH_OF_LIST];
    char  temp_list[LENGTH_OF_LIST];
    long seqqty_new_value;
    long segqty_cur_value;
    long remainder;
    long i;
    long list_length = 0;
    char segqty_orginal_list[LENGTH_OF_LIST];
    char  segqty_new_list[LENGTH_OF_LIST];
    char dstloc[STOLOC_LEN+1];
    misTrc(T_FLOW, "Entering Function");
    
    memset(sqlBuffer,0,strlen(sqlBuffer));
    memset(sqlBuffer2,0,strlen(sqlBuffer2));
    memset(temp_list, 0, strlen(temp_list));
    memset(segqty_orginal_list, 0, strlen(segqty_orginal_list));
    memset(ordnum_new_list, 0, strlen(ordnum_new_list));
    memset(segqty_new_list, 0, strlen(segqty_new_list));
    memset(schbat_new_list, 0, strlen(schbat_new_list)); 
    memset(ship_line_id_new_list, 0, strlen(ship_line_id_new_list));
    memset(client_id_new_list, 0, strlen(client_id_new_list));
    memset(stcust_new_list, 0, strlen(stcust_new_list));
    memset(rtcust_new_list, 0, strlen(rtcust_new_list));
    memset(ordlin_new_list, 0, strlen(ordlin_new_list));
    memset(ordsln_new_list, 0, strlen(ordsln_new_list));
    memset(concod_new_list, 0, strlen(concod_new_list));
    memset(ship_id_new_list, 0, strlen(ship_id_new_list));
    memset(dstare_new_list, 0, strlen(dstare_new_list));
    memset(dstloc_new_list, 0, strlen(dstloc_new_list));
    if (!temp_list || !ordnum_new_list || 
!segqty_orginal_list  ||
 !schbat_new_list || !schbat_new_list || !ship_line_id_new_list || !client_id_new_list || !stcust_new_list || !rtcust_new_list || !ordlin_new_list || !ordsln_new_list || !concod_new_list || !concod_new_list || !dstare_new_list || !dstloc_new_list)
     {
     misTrc(T_FLOW, "failed");
     }
     
    if (splflg_i)
        {
        if (splflg_i == BOOLEAN_FALSE)
        splflg = 0;
        }

    if (!sqlBuffer)
        {
        misTrc(T_FLOW, "failed");
        }
    misTrimcpy(segqty_orginal_list, segqty_i, strlen(segqty_i));
    new_pckqty = *pckqty_i;
    untcas = *untcas_i;
    if (untcas==0) 
        {
        sprintf(sqlBuffer2, "select prtmst.untcas untcas from prtmst where prt_client_id = '----' and prtnum = '%s' ", prtnum_i);
       ret_status = sqlExecStr (sqlBuffer2, &res);
       row = sqlGetRow (res);
       untcas = sqlGetLong (res, row, "untcas");
       sqlFreeResults(res);
       
        }
    list_length = sCountElementsFromList(segqty_orginal_list);

    for (i =1; i <= list_length; i++)
        {
        misTrimcpy(segqty_orginal_list, segqty_i, strlen(segqty_i)); 
        segqty_cur_value = atoi(sGetXElementFromList(segqty_orginal_list, i));
        if (segqty_cur_value != 0)
        {
        sprintf(sqlBuffer, "looping segqty_cur_value =  %ld and pckqty is = %ld and l= '%ld' ", segqty_cur_value, *pckqty_i, list_length); 
        misTrc(T_FLOW, sqlBuffer); 
        remainder = segqty_cur_value - untcas * (segqty_cur_value / untcas);
        segqty_cur_value = segqty_cur_value - remainder;
        new_pckqty = new_pckqty - remainder;
        sprintf(sqlBuffer, "remainder = '%ld' cur_value = '%ld' untcas = '%ld'", remainder, segqty_cur_value, untcas);
        misTrc(T_FLOW, sqlBuffer);
        if (remainder > 0)
            {
            sprintf(sqlBuffer, "allocate inventory ");
            sAddToList(ordnum_i, i, "ordnum", sqlBuffer, " where "); 
            sAddToList(schbat_i, i, "schbat", sqlBuffer, " and ");
            sAddToList(ship_id_i, i, "ship_id", sqlBuffer, " and ");
            sAddToList(ship_line_id_i, i, "ship_line_id", sqlBuffer, " and ");
            sAddToList(client_id_i, i, "client_id", sqlBuffer, " and ");
            sAddToList(stcust_i, i, "stcust", sqlBuffer, " and ");
            sAddToList(rtcust_i, i, "rtcust", sqlBuffer, " and ");
            sAddToList(ordlin_i, i, "ordlin", sqlBuffer, " and ");
            sAddToList(ordsln_i, i, "ordsln", sqlBuffer, " and ");
            sAddToList(concod_i, i, "concod", sqlBuffer, " and ");
            sAddToList(dstare_i, i, "dstare", sqlBuffer, " and ");
            sAddToList(dstloc_i, i, "dstloc", sqlBuffer, " and ");
            sprintf(sqlBuffer,"%s and pcktyp = '%s'", sqlBuffer, pcktyp_i);
            sprintf(sqlBuffer,"%s and prtnum = '%s'", sqlBuffer, prtnum_i); 
            sprintf(sqlBuffer,"%s and prt_client_id = '%s'", sqlBuffer, prt_client_id_i);
            sprintf(sqlBuffer,"%s and orgcod = '%s'", sqlBuffer, orgcod_i);
            sprintf(sqlBuffer,"%s and revlvl = '%s'", sqlBuffer, revlvl_i);
            sprintf(sqlBuffer,"%s and lotnum = '%s'", sqlBuffer, lotnum_i);
            sprintf(sqlBuffer,"%s and invsts_prg = '%s'", sqlBuffer, invsts_prg_i);
            sprintf(sqlBuffer,"%s and pcksts = '%s'", sqlBuffer, pcksts_i);
            sprintf(sqlBuffer,"%s and splflg = '%ld'", sqlBuffer, splflg);
            sprintf(sqlBuffer,"%s and untcas = '%ld'", sqlBuffer, untcas);
            sprintf(sqlBuffer,"%s and untpak = '%ld'", sqlBuffer, *untpak_i);
            sprintf(sqlBuffer,"%s and untpal = '%ld'", sqlBuffer, *untpal_i);
            sprintf(sqlBuffer,"%s and pipcod = '%s'", sqlBuffer, pipcod_i);
            sprintf(sqlBuffer,"%s and trace_suffix = '%s'", sqlBuffer, trace_suffix_i); 
            sprintf(sqlBuffer,"%s and skip_invlkp = '%s'", sqlBuffer, skip_invlkp_i); 
            sprintf(sqlBuffer, "%s and pckqty = '%ld'", sqlBuffer, remainder);
            sprintf(sqlBuffer, "%s and allocate_normal = 1", sqlBuffer);
            sprintf(sqlBuffer, "%s and segqty = '%ld'", sqlBuffer, remainder);
            misTrc(T_FLOW, sqlBuffer);
            if (CmdRes)
               srvFreeMemory (SRVRET_STRUCT, CmdRes); 
            ret_status = srvInitiateInline (sqlBuffer, &CmdRes);
            if (ret_status != eOK)
               {
               misTrc(T_FLOW, "The call to allocate inventory in varAllocateCaseByOrder.c failed in the remainder portion of the loop");
               if (CmdRes)
                   srvFreeMemory (SRVRET_STRUCT, CmdRes);
              return (APPError(ret_status));  
              }  
           }
           sprintf(sqlBuffer, "adding to list");
           misTrc(T_FLOW, sqlBuffer);
           if (segqty_cur_value > 0)
                {
                if (segqty_new_list && (strlen(segqty_new_list) >0))
                   {
                   sprintf(temp_list,"%s|", segqty_new_list);
                   sprintf(segqty_new_list, temp_list);
                   }
                sprintf(segqty_new_list,"%s%ld", segqty_new_list, segqty_cur_value); 
                sExtendFieldList(ordnum_new_list,ordnum_i,i);
                sExtendFieldList(schbat_new_list,schbat_i,i); 
                sExtendFieldList(ship_id_new_list,ship_id_i,i); 
                sExtendFieldList(ship_line_id_new_list,ship_line_id_i,i); 
                sExtendFieldList(client_id_new_list,client_id_i,i); 
                sExtendFieldList(stcust_new_list,stcust_i,i); 
                sExtendFieldList(rtcust_new_list,rtcust_i,i); 
                sExtendFieldList(ordlin_new_list,ordlin_i,i); 
                sExtendFieldList(ordsln_new_list,ordsln_i,i); 
                sExtendFieldList(concod_new_list,concod_i,i); 
                sExtendFieldList(dstare_new_list,dstare_i,i); 
                sExtendFieldList(dstloc_new_list,dstloc_i,i); 
                }
           sprintf(sqlBuffer, "finished loop on interation '%ld'", i);
           misTrc(T_FLOW, sqlBuffer);    
           } /* End if about segqty */
           } /* End for loop */
    sprintf(sqlBuffer, "finished for loop new pckqty '%ld'", new_pckqty);
    misTrc(T_FLOW, sqlBuffer);
    if (new_pckqty > 0)
    {
    sprintf(sqlBuffer, "allocate inventory ");
    sprintf(sqlBuffer,"%s where pcktyp = '%s'", sqlBuffer, pcktyp_i); 
    sprintf(sqlBuffer,"%s and prtnum = '%s'", sqlBuffer, prtnum_i);
    sprintf(sqlBuffer,"%s and prt_client_id = '%s'", sqlBuffer, prt_client_id_i);
    sprintf(sqlBuffer,"%s and orgcod = '%s'", sqlBuffer, orgcod_i);
    sprintf(sqlBuffer,"%s and revlvl = '%s'", sqlBuffer, revlvl_i);
    sprintf(sqlBuffer,"%s and lotnum = '%s'", sqlBuffer, lotnum_i);
    sprintf(sqlBuffer,"%s and invsts_prg = '%s'", sqlBuffer, invsts_prg_i);
    sprintf(sqlBuffer,"%s and dstare = '%s'", sqlBuffer, dstare_new_list); 
    sprintf(sqlBuffer,"%s and pcksts = '%s'", sqlBuffer, pcksts_i);
    sprintf(sqlBuffer,"%s and splflg = '%ld'", sqlBuffer, splflg);
    sprintf(sqlBuffer,"%s and dstloc = '%s'", sqlBuffer, dstloc_new_list);
    sprintf(sqlBuffer,"%s and untcas = '%ld'", sqlBuffer, untcas); 
    sprintf(sqlBuffer,"%s and untpak = '%ld'", sqlBuffer, *untpak_i); 
    sprintf(sqlBuffer,"%s and untpal = '%ld'", sqlBuffer, *untpal_i);
    sprintf(sqlBuffer,"%s and pipcod = '%s'", sqlBuffer, pipcod_i); 
    sprintf(sqlBuffer,"%s and trace_suffix = '%s'", sqlBuffer, trace_suffix_i);
    sprintf(sqlBuffer,"%s and skip_invlkp = '%s'", sqlBuffer, skip_invlkp_i);
    sprintf(sqlBuffer, "%s and pckqty = '%ld'", sqlBuffer, new_pckqty); 
    sprintf(sqlBuffer, "%s and allocate_normal = 1", sqlBuffer);  
    sprintf(sqlBuffer, "%s and segqty = '%s'", sqlBuffer, segqty_new_list);
    sprintf(sqlBuffer,"%s and ordnum = '%s'", sqlBuffer, ordnum_new_list); 
    sprintf(sqlBuffer,"%s and schbat = '%s'", sqlBuffer, schbat_new_list); 
    sprintf(sqlBuffer,"%s and ship_id = '%s'", sqlBuffer, ship_id_new_list); 
    sprintf(sqlBuffer,"%s and ship_line_id = '%s'", sqlBuffer, ship_line_id_new_list); 
    sprintf(sqlBuffer,"%s and client_id = '%s'", sqlBuffer, client_id_new_list); 
    sprintf(sqlBuffer,"%s and stcust = '%s'", sqlBuffer, stcust_new_list); 
    sprintf(sqlBuffer,"%s and rtcust = '%s'", sqlBuffer, rtcust_new_list); 
    sprintf(sqlBuffer,"%s and ordlin = '%s'", sqlBuffer, ordlin_new_list); 
    sprintf(sqlBuffer,"%s and ordsln = '%s'", sqlBuffer, ordsln_new_list); 
    sprintf(sqlBuffer,"%s and concod = '%s'", sqlBuffer, concod_new_list); 
    if (CmdRes)
        srvFreeMemory (SRVRET_STRUCT, CmdRes);
    ret_status = srvInitiateInline (sqlBuffer, &CmdRes); 
    misTrc(T_FLOW, "executed allocate inventory");
    if (ret_status !=eOK)
        {
        if (CmdRes)
             srvFreeMemory (SRVRET_STRUCT, CmdRes);
        misTrc(T_FLOW, "The call to allocate inventory in varAllocateCaseByOrder.c failed in the last part of the function");
        return (APPError(ret_status));
        }
    }
    misTrc(T_FLOW, "begin free");
    misTrc(T_FLOW, "end free");
    return (CmdRes);
    }
