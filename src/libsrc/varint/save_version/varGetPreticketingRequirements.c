static const char *rcsid = "$Id: varGetPreticketingRequirements.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************

 *  SAI Development Systems
 *  Copyright 1993-1997 Software Architects, Inc.
 *  Brookfield, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/libsrc/varint/varGetPreticketingRequirements.c,v $
 *  $Revision: 1.1.1.1 $
 *
 *  Application:
 *  Created:
 *  $Author: lh51sh $
 *
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <srvconst.h>
#include <dcserr.h>
#include <dcslib.h>
#include <varerr.h>
#include <common.h>
#include <varcolwid.h>
#include <vargendef.h>        

#define VC_PTKSTK_LEN	20
#define VC_PRETCK_LEN	10

void *varGetPreticketingRequirements(char *wrkref_i,
			     	     char *ordnum_i,
			     	     char *prtnum_i,
			     	     char *lotnum_i)
{
    RETURN_STRUCT *CurPtr;
    mocaDataRow    *next = 0;
    mocaDataRes    *ptr;
    long            errcode;
    char            datatypes[20];
    char            buffer[5000], tmpbuf[2000];
    char            Wrkref[WRKREF_LEN + 1];
    char            Subsid[SUBSID_LEN + 1];
    char            Ordnum[ORDNUM_LEN + 1];
    char            Prtnum[PRTNUM_LEN + 1];
    char            Lotnum[LOTNUM_LEN + 1];
    long	    xx;
    char	    Colnam[RTSTR1_LEN + 1];
    char	    VarPretck[VC_PRETCK_LEN + 1];
    char	    Cstnum[CSTNUM_LEN + 1];
    long	    VarPrenum;
    char	    VarPtkstk[VC_PTKSTK_LEN + 1];
    char	    Datatypes[10];

    memset(Wrkref, 0, sizeof(Wrkref));
    memset(Subsid, 0, sizeof(Subsid));
    memset(Ordnum, 0, sizeof(Ordnum));
    memset(Prtnum, 0, sizeof(Prtnum));
    memset(Lotnum, 0, sizeof(Lotnum));
    memset(Datatypes, 0, sizeof(Datatypes));

    if (wrkref_i && misTrimLen(wrkref_i, WRKREF_LEN))
	strcpy(Wrkref, wrkref_i);

    if (ordnum_i && misTrimLen(ordnum_i, ORDNUM_LEN))
	strcpy(Ordnum, ordnum_i);

    if (prtnum_i && misTrimLen(prtnum_i, PRTNUM_LEN))
	strcpy(Prtnum, prtnum_i);

    if (lotnum_i && misTrimLen(lotnum_i, LOTNUM_LEN))
	strcpy(Lotnum, lotnum_i);

    /*  If we didn't get either the wrkref or all the other information,
	return Insufficient Args */
    if ((strlen(Wrkref) == 0)  && 
        (strlen(Ordnum) == 0 ||
	 strlen(Prtnum) == 0))
	return (srvSetupReturn(eSRV_INSUFF_ARGUMENTS, ""));

    /*  Get the information as needed */
    if (strlen(Wrkref))
    {
        /* LHCSTART 11.01 <Ira Laksmono> 
         * Desc: Changed the select statement.
         * If the wrkref is associated with kitpart, then 
         * we won't get the information as needed.
         * original code:
         *sprintf(buffer,
	 *   "select ordnum, lotnum, prtnum from pckwrk "
	 *  "where wrkref = '%s' ",
	 *  Wrkref);
         */
        sprintf(buffer,
        	"select ordnum, lotnum, prtnum              "
                "  from pckwrk                              "                
                " where ctnnum = (select subnum from pckwrk " 
                "       where wrkref = '%s')                "
                "   and wrktyp != 'K'                       "
                " union                                     "
                "select ordnum, lotnum, prtnum              "
                "  from pckwrk                              "
                " where wrkref = '%s'                       " 
                "   and wrktyp != 'K'                       ", 
                Wrkref, Wrkref);
        /*LHCEND 11.01 <Ira Laksmono> */
        errcode = sqlExecStr(buffer, &ptr);
        if (errcode != eOK)
	    return (srvSetupReturn(errcode, ""));

	next = sqlGetRow(ptr);
	strncpy(Ordnum, sqlGetValue(ptr, next, "ordnum"),
		misTrimLen(sqlGetValue(ptr, next, "ordnum"), ORDNUM_LEN));
	strncpy(Lotnum, sqlGetValue(ptr, next, "lotnum"),
		misTrimLen(sqlGetValue(ptr, next, "lotnum"), LOTNUM_LEN));
	strncpy(Prtnum, sqlGetValue(ptr, next, "prtnum"),
		misTrimLen(sqlGetValue(ptr, next, "prtnum"), PRTNUM_LEN));
	sqlFreeResults(ptr);
    }

    /*  We need to find out if preticketing is required. */
    if (strcmp(Lotnum, "----") == 0)
	memset (Lotnum, 0, sizeof(Lotnum));
    if (strcmp(Lotnum, "NOLOT") == 0)
	memset (Lotnum, 0, sizeof(Lotnum));
    if (strlen(Lotnum) == 0)
    {
    	sprintf(buffer,
	    "select rtstr1 "
	    "  from poldat "
	    " where polcod = 'CUSTOMER-REQUIREMNT-PROCESSING'"
	    "   and polvar = 'PREFERENCE'"
	    "   and polval = 'SORT-ORDER' order by srtseq");
        errcode = sqlExecStr(buffer, &ptr);
        if (errcode == eOK)
        {
	    memset (buffer, 0, sizeof(buffer));
            xx=1;
            next = sqlGetRow(ptr);
            do
            {
                sprintf (Colnam, "%s", sqlGetValue (ptr, next, "rtstr1"));
                misTrim (Colnam);
                sprintf (tmpbuf, "select %ld, cstnum, vc_pretck, vc_prenum, "
		    "'%s' colnam from cstprq "
	            "where cstnum=(select distinct %s from ord "
                    "  where ordnum = '%s') "
                    "  and prtnum = '%s' "
                    "  and vc_pretck is not null "
                    "  and vc_prenum is not null "
	            "  and length(ltrim(rtrim(vc_pretck))) > 0 " 
	            "  and vc_prenum > 0 ", 
	            xx, Colnam, Colnam, Ordnum, Prtnum);

                if (strlen(buffer))
                {
                    strcat (buffer, " union ");
                }
                strcat (buffer, tmpbuf);
                xx++;
            } while ((next = sqlGetNextRow(next)) != NULL);
            strcat (buffer, " order by 1");
    	    sqlFreeResults(ptr);
            errcode = sqlExecStr(buffer, &ptr);
            if (errcode != eOK)
    	    {
        	sqlFreeResults(ptr);
	        return (srvSetupReturn(errcode, ""));
	    }

	    next = sqlGetRow(ptr);
    	    memset(Cstnum, 0, sizeof(Cstnum));
    	    strncpy(Cstnum, sqlGetValue(ptr, next, "cstnum"),
		misTrimLen(sqlGetValue(ptr, next, "cstnum"), 
		    CSTNUM_LEN));
    	    memset(VarPretck, 0, sizeof(VarPretck));
    	    strncpy(VarPretck, sqlGetValue(ptr, next, "vc_pretck"),
		misTrimLen(sqlGetValue(ptr, next, "vc_pretck"), 
		    VC_PRETCK_LEN));
	    VarPrenum = (long ) *(double *)sqlGetValue(ptr,next,"vc_prenum");
    	    memset(Colnam, 0, sizeof(Colnam));
    	    strcpy(Colnam, sqlGetValue(ptr, next, "colnam"));
    	    sqlFreeResults(ptr);
        }
	else
	{
	    return (srvSetupReturn(errcode, ""));
	}
    }
    else
	return (srvSetupReturn(eDB_NO_ROWS_AFFECTED, ""));

    sprintf (buffer, "select vc_ptkstk "
	"from cstmst "
	 "where cstnum=(select distinct %s from ord "
         "  where ordnum = '%s') "
         "  and vc_ptkstk is not null "
	 "  and length(ltrim(rtrim(vc_ptkstk))) > 0 ",
	 Colnam, Ordnum);

    memset(VarPtkstk, 0, sizeof(VarPtkstk));
    errcode = sqlExecStr(buffer, &ptr);
    if (errcode == eOK)
    {
        next = sqlGetRow(ptr);
        memset(VarPtkstk, 0, sizeof(VarPtkstk));
        strncpy(VarPtkstk, sqlGetValue(ptr, next, "vc_ptkstk"),
	    misTrimLen(sqlGetValue(ptr, next, "vc_ptkstk"), 
	        VC_PTKSTK_LEN));
    }
    sqlFreeResults(ptr);

    srvSetupColumns(4);
    xx = 0;
    Datatypes[xx]=srvSetColName(xx+1,"cstnum",COMTYP_CHAR, CSTNUM_LEN);
    xx++;
    Datatypes[xx]=srvSetColName(xx+1,"vc_pretck",COMTYP_CHAR, VC_PRETCK_LEN);
    xx++;
    Datatypes[xx]=srvSetColName(xx+1,"vc_prenum",COMTYP_INT, sizeof(int));
    xx++;
    Datatypes[xx]=srvSetColName(xx+1,"vc_ptkstk",COMTYP_CHAR, VC_PTKSTK_LEN);
    xx++;
    return (srvSetupReturn(eOK, Datatypes, Cstnum, VarPretck, VarPrenum, 
				VarPtkstk));

}
