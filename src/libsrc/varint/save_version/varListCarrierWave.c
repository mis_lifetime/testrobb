static const char *rcsid = "$Id: varListCarrierWave.c,v 1.5 2002/01/05 04:47:08 test Exp $";
/*#START****************************************************************
 *  McHugh Software International
 *  Copyright 2001
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 * *********************************************************************
 * 8.05 Carrier Wave Query -- Shows carriers assigned to waves, and
 *  the waves that are assigned to carriers.
 *#END******************************************************************/
#include <moca_app.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <srvconst.h>
#include <dcserr.h>
#include <varcolwid.h>
 
LIBEXPORT RETURN_STRUCT *varListCarrierWave(char *carcod_i,
					    char *schbat_i)
{
    RETURN_STRUCT *CurPtr, *CmdRes;
 
    long ret_status;
    char vc_wavdte[8];
    char buffer[2000];
    char schbat[RTSTR1_LEN +1];
    char carcod[CARCOD_LEN +1];
    char batsts[RTSTR1_LEN +1]; 
    char ship_id[SHIP_ID_LEN +1];
    //char ordnum[ORDNUM_LEN +1];
    char tmpbuf[20];
    long wave_lane=0, sum_ordnum=0, sum_ctnnum=0, sum_totpcs=0;
    double sum_estwgt=0, sum_palvol=0;
 
    mocaDataRes   *res, *res2, *res3;
    mocaDataRow   *row, *row2, *row3;

    memset(carcod, 0, sizeof(carcod));
    memset(schbat, 0, sizeof(schbat));
    if (carcod_i && misTrimLen(carcod_i, CARCOD_LEN))
	misTrimcpy(carcod,carcod_i,CARCOD_LEN);
    if (schbat_i && misTrimLen(schbat_i, SCHBAT_LEN))
	misTrimcpy(schbat,schbat_i,SCHBAT_LEN);
 
    CurPtr = NULL;
    sprintf(buffer, "select distinct s.carcod, "
	"substr(sl.schbat, 1,2)|| '/' || substr(sl.schbat, 3,2) "
	"|| '/' || substr(sl.schbat, 5,2) vc_wavdte, "
	"sl.schbat, p.batsts, sl.vc_wave_lane "
	"from shipment s, shipment_line sl, pckbat p "
	"where s.ship_id = sl.ship_id and sl.schbat = p.schbat");
    
    if (misTrimLen(carcod, CARCOD_LEN))
	sprintf(&buffer[strlen(buffer)]," and s.carcod='%s'",carcod);
    if (misTrimLen(schbat, SCHBAT_LEN))
	sprintf(&buffer[strlen(buffer)]," and sl.schbat='%s'",schbat);
 
    strcat(buffer," order by schbat, vc_wave_lane");
    ret_status = sqlExecStr(buffer, &res);
    if (eDB_NO_ROWS_AFFECTED != ret_status && eOK != ret_status) {
	sqlFreeResults(res);
	misTrc(T_FLOW, "Exiting. Status = %ld",ret_status);
	return(srvSetupReturn(ret_status,""));
    }
    else if (eDB_NO_ROWS_AFFECTED == ret_status) {
	misTrc(T_FLOW, "Exiting, no rows affected");
	sqlFreeResults(res);
	CurPtr = srvResultsInit(ret_status,
		"carcod",      COMTYP_STRING, sizeof(carcod),
		"vc_wavdte",   COMTYP_STRING, sizeof(vc_wavdte),
		"schbat",      COMTYP_STRING, sizeof(schbat),
		"batsts",      COMTYP_STRING, sizeof(batsts),
		"vc_wave_lane",COMTYP_INT,    sizeof(long),
		"sum_ordnum",  COMTYP_INT,    sizeof(long),
		"sum_ctnnum",  COMTYP_INT,    sizeof(long),
		"sum_totpcs",  COMTYP_INT,    sizeof(long),
		"sum_estwgt",  COMTYP_FLOAT,  sizeof(double),
		"sum_palvol",  COMTYP_FLOAT,  sizeof(double),NULL);
	return(CurPtr);
    }
    misTrc(T_FLOW, "starting loop of carrier/lane for wave/batch ");
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row)) {
	memset (carcod, 0, sizeof (carcod));
	memset (schbat, 0, sizeof (schbat));
	memset (batsts, 0, sizeof (batsts));
	wave_lane= 0;
 
	if(!sqlIsNull(res, row, "schbat"))
	    misTrimcpy(schbat,sqlGetString(res,row,"schbat"),SCHBAT_LEN);
	if(!sqlIsNull(res, row, "vc_wavdte"))
	    misTrimcpy(vc_wavdte,sqlGetString(res,row,"vc_wavdte"),8);
	if(!sqlIsNull(res, row, "carcod"))
	    misTrimcpy(carcod,sqlGetString(res,row,"carcod"),CARCOD_LEN);
	if(!sqlIsNull(res, row, "batsts"))
	    misTrimcpy(batsts,sqlGetString(res,row,"batsts"),BATSTS_LEN);
	if(!sqlIsNull(res, row, "vc_wave_lane"))
	    wave_lane = sqlGetLong(res, row, "vc_wave_lane");
 
	sprintf(buffer,"",'\0');	// count cartons for schbat/carcod
	sprintf(buffer, "select count(distinct subucc) cnt_ctnnum "
		"from pckwrk p, shipment_line sl, shipment s "
		"where p.ship_line_id=sl.ship_line_id "
		"and p.subucc is not null and p.ship_id=sl.ship_id "
		"and s.ship_id=sl.ship_id "
		"and sl.schbat='%s' ", schbat);
	if (strlen(carcod)) {
	    sprintf(&buffer[strlen(buffer)]," and s.carcod='%s'",carcod);
	}
	else {
	    sprintf(&buffer[strlen(buffer)]," and s.carcod is null");
	}
	ret_status = sqlExecStr(buffer, &res2);
 
	if (eDB_NO_ROWS_AFFECTED != ret_status && eOK != ret_status) {
	    sqlFreeResults(res);
	    sqlFreeResults(res2);
	    misTrc(T_FLOW, "Exiting. Status = %ld",ret_status);
	    return(srvSetupReturn(ret_status, ""));
	}
	sum_ctnnum = sqlGetLong(res2, sqlGetRow(res2), "cnt_ctnnum");
	sqlFreeResults(res2);
 //-------------------------------------------------------------

	/*MR943 Ira Laksmono Dec 12,01 
	 *get order pick estimate doesn't get the correct values for #ofunit, weight and volume. 
	 *Change the select statement to get the total order for the wave/lane. 
	 *Use get shipment pick estimate to get the values. 
	 *Need to get the ship_id instead of ordnum. 
	 */
	sprintf (buffer, "select count(distinct sl.ordnum) cnt_ordnum "
		   	    	"from shipment_line sl, shipment s "
		"where sl.schbat='%s' and sl.ship_id=s.ship_id ",schbat);
	if (strlen(carcod)) {
	    sprintf(&buffer[strlen(buffer)]," and s.carcod='%s'",carcod);
	}
	else {
	    sprintf(&buffer[strlen(buffer)]," and s.carcod is null");
	}
	if (wave_lane>0) {
	    sprintf(&buffer[strlen(buffer)],
			" and sl.vc_wave_lane=%ld",wave_lane);
	}
	else {
	    sprintf(&buffer[strlen(buffer)]," and sl.vc_wave_lane is null");
	}

	ret_status = sqlExecStr (buffer, &res2);
	if (ret_status != eOK) {
	    sqlFreeResults(res);
	    sqlFreeResults(res2);
	    return (srvSetupReturn(ret_status,""));
	} 
        sum_ordnum = sqlGetLong(res2, sqlGetRow(res2), "cnt_ordnum"); 
        sqlFreeResults(res2); 
 
	// LH get the ship_id in, then get unit, wgt and vol 
	memset (ship_id, 0, sizeof (ship_id));
	sprintf (buffer, "select distinct sl.ship_id ship_id " 
		   	    	"from shipment_line sl, shipment s " 
		"where sl.schbat='%s' and sl.ship_id=s.ship_id ",schbat); 
	if (strlen(carcod)) { 
	    sprintf(&buffer[strlen(buffer)]," and s.carcod='%s'",carcod); 
	} 
	else { 
	    sprintf(&buffer[strlen(buffer)]," and s.carcod is null"); 
	} 
	if (wave_lane>0) { 
	    sprintf(&buffer[strlen(buffer)], 
			" and sl.vc_wave_lane=%ld",wave_lane); 
	} 
	else { 
	    sprintf(&buffer[strlen(buffer)]," and sl.vc_wave_lane is null"); 
	} 
 
	ret_status = sqlExecStr (buffer, &res2); 
	if (ret_status != eOK) { 
	    sqlFreeResults(res); 
	    sqlFreeResults(res2); 
	    return (srvSetupReturn(ret_status,"")); 
	} 
        row2=sqlGetRow(res2);     
        strncpy(ship_id, sqlGetString(res2,row2, "ship_id"), SHIP_ID_LEN); 
	sqlFreeResults(res2); 

	sum_totpcs = 0;
	sum_estwgt = 0;
	sum_palvol = 0;
 
	sprintf(buffer, "get shipment pick estimate where ship_id = '%s'", ship_id); 
	ret_status = srvInitiateCommand (buffer, &CmdRes); 
	if (ret_status !=eOK) { 
		sqlFreeResults(res); 
		srvFreeMemory (SRVRET_STRUCT, CmdRes); 
		return (srvSetupReturn(ret_status,"")); 
	} 
	res3 = CmdRes->ReturnedData; 
	row3 = sqlGetRow (res3); 
	sum_totpcs = sqlGetLong (res3, row3, "totpcs"); 
	sum_estwgt = sqlGetFloat(res3, row3, "estwgt"); 
	sum_palvol = sqlGetFloat(res3, row3, "palvol"); 
	srvFreeMemory (SRVRET_STRUCT, CmdRes);
	 
	/* Start LH ,Ira commented out since we don't call get order pick est. 
	    for(row2=sqlGetRow(res2); row2; row2=sqlGetNextRow(row2)) {
	    strncpy(ordnum,sqlGetString(res2,row2,"ordnum"),ORDNUM_LEN);
	    sum_ordnum++; 		//count orders for wave/lane
	    sprintf(buffer,
		"get order pick estimate where ordnum='%s'",ordnum);
	    ret_status = srvInitiateCommand (buffer, &CmdRes);
	    if (ret_status !=eOK) {
		sqlFreeResults(res);
		sqlFreeResults(res2);
		srvFreeMemory (SRVRET_STRUCT, CmdRes);
		return (srvSetupReturn(ret_status,""));
	    }
	    res3 = CmdRes->ReturnedData;
	    row3 = sqlGetRow (res3);
	    sum_totpcs += sqlGetLong (res3, row3, "totpcs");
	    sum_estwgt += sqlGetFloat(res3, row3, "estwgt");
	    sum_palvol += sqlGetFloat(res3, row3, "palvol");
	    srvFreeMemory (SRVRET_STRUCT, CmdRes);
	    }  // end ship_id for wave/lane loop 
	*/ // End LH  

	sprintf(tmpbuf,"%.2f",sum_estwgt); 	//round the floating part
	sum_estwgt = atof(tmpbuf);
	sprintf(tmpbuf,"%.2f",sum_palvol);
	sum_palvol = atof(tmpbuf);
	 
	misTrc(T_FLOW,
		"carcod=%s,schbat=%s,batsts=%s", carcod,schbat,batsts);
	misTrc(T_FLOW,"wave_lane=%ld, sum_ordnum=%ld, sum_ctnnum=%ld",
			wave_lane, sum_ordnum,sum_ctnnum);
	misTrc(T_FLOW,"totpcs=%ld, estwgt=%ld, palvol=%f",
			sum_totpcs, sum_estwgt, sum_palvol);
	if(!CurPtr) {
	    CurPtr= srvResultsInit(ret_status,
		"carcod",      COMTYP_STRING,sizeof(carcod),
		"vc_wavdte",   COMTYP_STRING,sizeof(vc_wavdte),
		"schbat",      COMTYP_STRING,sizeof(schbat),
		"batsts",      COMTYP_STRING,sizeof(batsts),
		"vc_wave_lane",COMTYP_INT,   sizeof(long),
		"sum_ordnum",  COMTYP_INT,   sizeof(long),
		"sum_ctnnum",  COMTYP_INT,   sizeof(long),
		"sum_totpcs",  COMTYP_INT,   sizeof(long),
		"sum_estwgt",  COMTYP_FLOAT, sizeof(double),
		"sum_palvol",  COMTYP_FLOAT, sizeof(double),NULL);
	}
	ret_status=srvResultsAdd(CurPtr,carcod, 
					vc_wavdte,
					schbat,
					batsts,
					wave_lane,
					sum_ordnum,
					sum_ctnnum,
					sum_totpcs,
					sum_estwgt,
					sum_palvol);
    } //end lane/wave loop
    if(!CurPtr) {
	CurPtr = srvResultsInit(ret_status,
		"carcod",      COMTYP_STRING,sizeof(carcod),
		"vc_wavdte",   COMTYP_STRING,sizeof(vc_wavdte),
		"schbat",      COMTYP_STRING,sizeof(schbat),
		"batsts",      COMTYP_STRING,sizeof(batsts),
		"vc_wave_lane",COMTYP_INT,   sizeof(long),
		"sum_ordnum",  COMTYP_INT,   sizeof(long),
		"sum_ctnnum",  COMTYP_INT,   sizeof(long),
		"sum_totpcs",  COMTYP_INT,   sizeof(long),
		"sum_estwgt",  COMTYP_FLOAT, sizeof(double),
		"sum_palvol",  COMTYP_FLOAT, sizeof(double),NULL);
    }
    sqlFreeResults(res);
    return(CurPtr);
}
