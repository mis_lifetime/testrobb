static const char *rcsid = "$Id: varChangeInventoryAdjOptions.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 2001 
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *#END************************************************************************/

#include <moca_app.h>
#include <dcscolwid.h>
#include <dcsgendef.h>
#include <srvconst.h>
#include <dcserr.h>
#include <vargendef.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/* #include "intlib.h"
#include "intCntLib.h"
*/


/*LIBEXPORT RETURN_STRUCT *varChangeInventoryAdjOptions(long *InvAdjLim_i,
						  long *chkinvadjopt_i,
						  long *chkshowdlramt_i)
*/
LIBEXPORT RETURN_STRUCT *varChangeInventoryAdjOptions(long *InvAdjLim_i,
						  long *chkinvadjopt_i)
						  
{
	long	ret_status, i;
	char	rtstr1[RTSTR1_LEN + 1];
	char	rtstr2[RTSTR2_LEN + 1];
	long	rtnum1, rtnum2;
	long	InvAdjLim;
	long	chkinvadjopt;
//	long	chkshowdlramt;

	

	/* Assign values to local variables */
	InvAdjLim	= InvAdjLim_i 		? *InvAdjLim_i		: -1;
//	chkshowdlramt   = chkshowdlramt_i	? *chkshowdlramt_i	: -1;
	chkinvadjopt    = chkinvadjopt_i	? *chkinvadjopt_i	: -1;

 	misTrc(T_FLOW, "tpg.  Starting Program");

	/* The sequence for all these is 0 */
	i = 0;

	/* First deal with the Discrepancy Limit textbox */
	if (InvAdjLim >= 0)
	{
        	misTrc(T_FLOW, "tpg. Start of InvAdjLim logic");

		memset (rtstr1, 0, sizeof(rtstr1));
		memset (rtstr2, 0, sizeof(rtstr2));
 		rtnum1 = 0;
		rtnum2 = 0;
 
		ret_status = int_CntPolicyExists(POLVAR_CYCCNT_INVENTORY_ADJ_OPTIONS,
						 POLVAL_CYCCNT_DISCREPANCY_LIMIT,
						 rtstr1,
						 rtstr2,
						 &rtnum1,
						 &rtstr2);
		if (1 == ret_status)
		{
			/* Update */
			rtnum1 = InvAdjLim;
			ret_status = int_CntPolicyUpdate(POLVAR_CYCCNT_INVENTORY_ADJ_OPTIONS,
							 POLVAL_CYCCNT_DISCREPANCY_LIMIT,
							 NULL,
							 NULL,
							 rtstr1,
							 rtstr2,
							 rtnum1,
							 rtnum2);
		}
		else if (0 == ret_status)
		{
			/* Insert it */
			rtnum1 = InvAdjLim;
			ret_status = int_CntPolicyExists(POLVAR_CYCCNT_INVENTORY_ADJ_OPTIONS,
							 POLVAL_CYCCNT_DISCREPANCY_LIMIT,
							 rtstr1,
							 rtstr2,
							 rtnum1,
							 rtstr2,
							 i);
		}
		if (eOK != ret_status)
			return (srvSetupReturn(ret_status, ""));
	}

	
	/* First see if it is there */
	if (chkinvadjopt >= 0)
	{
         	misTrc(T_FLOW, "tpg.  Start of chkinvadjopt logic");		
		memset (rtstr1, 0, sizeof(rtstr1));
		memset (rtstr2, 0, sizeof(rtstr2));
  		rtnum1 = 0;
		rtnum2 = 0; 
		
		ret_status = int_CntPolicyExists(POLVAR_CYCCNT_INVENTORY_ADJ_OPTIONS,
						 POLVAL_CYCCNT_DISCREPANCY_TYPE,
						 rtstr1,
						 rtstr2,
						 &rtnum1,
						 &rtstr2);
		if (1 == ret_status)
		{
			/* Update */
			rtnum1 = chkinvadjopt;
			ret_status = int_CntPolicyUpdate(POLVAR_CYCCNT_INVENTORY_ADJ_OPTIONS,
							 POLVAL_CYCCNT_DISCREPANCY_TYPE,
						  	 NULL,
							 NULL,
							 rtstr1,
							 rtstr2,
							 rtnum1,
							 rtnum2);
		}
		else if (0 == ret_status)
		{
			/* Insert it */
			rtnum1 = chkinvadjopt;
			ret_status = int_CntPolicyExists(POLVAR_CYCCNT_INVENTORY_ADJ_OPTIONS,
							 POLVAL_CYCCNT_DISCREPANCY_TYPE,
							 rtstr1,
							 rtstr2,
							 rtnum1,
							 rtstr2,
							 i);
		}
		if (ret_status != eOK)
			return (srvSetupReturn(ret_status, ""));
	}

	/* See if it is there */
/*	if (chkshowdlramt >= 0)
	{
	 	misTrc(T_FLOW, "tpg.  Start of chkshowdlramt logic");
		memset (rtstr1, 0, sizeof(rtstr1));
		memset (rtstr2, 0, sizeof(rtstr2));
  		rtnum1 = 0;
		rtnum2 = 0;

		ret_status = int_CntPolicyExists(POLVAR_CYCCNT_INVENTORY_ADJ_OPTIONS,
						 POLVAL_CYCCNT_SHOW_DOLLAR_AMT,
						 rtstr1,
						 rtstr2,
						 &rtnum1,
						 &rtstr2);
		if (ret_status == 1)
		{
*/
		/* Update */
/*			rtnum1 = chkshowdlramt;
			ret_status = int_CntPolicyUpdate(POLVAR_CYCCNT_INVENTORY_ADJ_OPTIONS,
							 POLVAL_CYCCNT_SHOW_DOLLAR_AMT,
							 NULL,
							 NULL,
							 rtstr1,
							 rtstr2,
							 rtnum1,
							 rtnum2);
		}
		else if (ret_status == 0)
		{
*/
		/* Insert it */
/*			rtnum1 = chkshowdlramt;
			ret_status = int_CntPolicyExists(POLVAR_CYCCNT_INVENTORY_ADJ_OPTIONS,
							 POLVAL_CYCCNT_SHOW_DOLLAR_AMT,
							 rtstr1,
							 rtstr2,
							 rtnum1,
							 rtstr2,
							 i);
		}
		if (ret_status != eOK)
			return (srvSetupReturn(ret_status, ""));
	}
*/
	/* Now setup the return structure and exit */
	return(srvSetupReturn(eOK, ""));
}


