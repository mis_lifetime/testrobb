static const char *rcsid = "$Id: varGetLabelFormat.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/libsrc/varint/varGetLabelFormat.c,v $
 *  $Revision: 1.1.1.1 $
 *
 *  Application:
 *  Created:
 *  $Author: lh51sh $
 *
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include <dcscolwid.h>
#include <vargendef.h>
#include <dcsgendef.h>
#include <dcserr.h>

#define KEYWORD_NEXT "NEXT="
#define KEYWORD_DATA "DATA="
#define KEYWORD_SELECT "SELECT="
#define KEYWORD_QUANTITY "QUANTITY="
#define PREFIX_VARIABLE "@"
#define PREFIX_DESTINATION ":"
#define OPEN_PAREN_CHAR '('
#define CLOSE_PAREN_STR ")"
#define BACKSLASH_CHAR '\\'
#define NL 	10
#define NP 	12
#define CR 	13
#define TA  9

#define SINGLE_QUOTE 0x27
#define DOUBLE_QUOTE 0x22

static char    *_GetDataElement(char *String, char *);
static long     DoSelects(void);
static long     ExpandString(char *SourceString, char *Data);
static long     ProcessIfTest(char *IfClause);
static char    *_TrimZeros(char *string);


typedef struct _SELECTS
{
    char           *select;
    char           *IntoValue;
    char           *IfTest;
    mocaDataRow    *nextptr;
    mocaDataRes     *ptr;
    long            errcode;
    struct _SELECTS *Next;
    struct _SELECTS *Prev;
} SELECTS;

typedef struct _FORMATS
{
    char            Filename[FILNAM_LEN + 1];
    char           *Data;
    int             LblQty;
    SELECTS        *Selects;
    struct _FORMATS *Next;
} FORMATS;

static SELECTS *Selects = NULL, *NextSelect = NULL, *PreviousSelect = NULL;


LIBEXPORT 
RETURN_STRUCT  *varGetLabelFormat(char *devcod_i,
				  char *prtadr_i,
				  char *frmtnm_i,
				  char *wrkref_i,
				  moca_bool_t *notype_i)
{
    RETURN_STRUCT  *CurPtr;
    SELECTS        *tmpSel, *last;
    FORMATS        *tmpFormat;

    mocaDataRow    *next = 0, *next2 = 0;
    mocaDataRes     *ptr, *ptr2;
    long            errcode;
    char            datatypes[20];
    char            buffer[5000], tmpbuf[2000];
    char            DevCode[DEVCOD_LEN + 1];
    char            PrtAdr[PRTADR_LEN + 1];
    char            FormatName[FRMTNM_LEN + 1];
    char            WrkRef[WRKREF_LEN + 1];
    FILE           *FormatFilePtr;
    char            PrtTyp[PRTTYP_LEN + 1];
    char            Filename[FILNAM_LEN + 1];
    char            Format[FILNAM_LEN + 1];
    char            U_Format[FILNAM_LEN + 1];
    char            Data[4096];
    char            WorkString[100];
    char            TempString[200];
    long            Pos;
    char            DataType;
    long            NeedToSelect;
    long            LiteralOpened;
    struct
    {
	char            Type;
	char            Data[RTSTR1_LEN + 1];
	char           *DataPtr;
    } DataToFill[20];

    long            FillItems, i;
    char            Line[2000];
    long            Done;
    char           *OldPos;
    char           *TempPtr, *TempPtr2;
    long            SegLength;
    long            lblqty;
    void           *GenericPtr;
    char           *IfPtr;
    char            Prefix[RTSTR1_LEN + 1];
    char            Suffix[RTSTR1_LEN + 1];
    char           *ship_id;
    FORMATS *Formats = NULL, *NextFormat = NULL;
    long	    file_found;
    char	    colnam[RTSTR1_LEN + 1];
    char	    varbuf[1024];
    long	    xx;
    int             ignore_type = 0;

    Selects = NULL;

    memset(DevCode, 0, sizeof(DevCode));
    memset(PrtAdr, 0, sizeof(PrtAdr));
    memset(Format, 0, sizeof(Format));
    memset(U_Format, 0, sizeof(Format));
    memset(WrkRef, 0, sizeof(WrkRef));
    memset(Filename, 0, sizeof(Filename));

    if (wrkref_i && misTrimLen(wrkref_i, WRKREF_LEN))
	misTrimcpy(WrkRef, wrkref_i, WRKREF_LEN);

    if (prtadr_i && misTrimLen(prtadr_i, PRTADR_LEN))
	misTrimcpy(PrtAdr, prtadr_i, PRTADR_LEN);

    if (devcod_i && misTrimLen(devcod_i, DEVCOD_LEN))
	misTrimcpy(DevCode, devcod_i, DEVCOD_LEN);
    else if (!strlen(PrtAdr) && 
	     !strlen(WrkRef) &&
	     osGetVar(LESENV_DEVCOD))
	strcpy(DevCode, osGetVar(LESENV_DEVCOD));

    if (notype_i && (BOOLEAN_TRUE == *notype_i))
	ignore_type = 1;

    if (frmtnm_i && misTrimLen(frmtnm_i, FRMTNM_LEN))
	misTrimcpy(Format, frmtnm_i, FRMTNM_LEN);
    else
	return (srvSetupReturn(eSRV_INSUFF_ARGUMENTS, ""));

    for (i = 0; i < (long) strlen(Format); i++)
	U_Format[i] = toupper(Format[i]);

    /*  If we didn't get a formatname or enough information to determin
       where to print the label then return  Insufficient Args */

    if (strlen(PrtAdr) == 0 && (strlen(DevCode) == 0))
	return (srvSetupReturn(eSRV_INSUFF_ARGUMENTS, ""));

    /*  We need to find out if a policy exists which will determine if
	we need to check the customer requirements table for a filename. */
    file_found = FALSE;

    FormatFilePtr = NULL;

    if ((strcmp(Format, "shplbl") == 0) &&
    	(appIsInstalled(POLCOD_CUST_REQ_PROC)))
    {
        sprintf(buffer,
	    "select rtstr1 "
	    "  from poldat "
	    " where polcod = '%s'"
	    "   and polvar = 'PREFERENCE'"
	    "   and polval = 'SORT-ORDER' order by srtseq",
		POLCOD_CUST_REQ_PROC);
	errcode = sqlExecStr(buffer, &ptr);
    	if (errcode == eOK)
	{
	    memset (buffer, 0, sizeof(buffer));
	    xx=1;
	    next = sqlGetRow(ptr);
	    do
	    {
		sprintf(colnam, "%s", sqlGetValue (ptr, next, "rtstr1"));
		misTrim(colnam);
		sprintf(tmpbuf, 
		  "select %ld, shplbl "
		  "  from cstmst "
		  " where cstnum in "
		  "  (select %s " 
		  "     from ord, shipment_line, pckwrk "
		  "    where shipment_line.ship_line_id = pckwrk.ship_line_id "
		  "      and pckwrk.wrkref              ='%s' "
		  "      and shipment_line.client_id    = cstmst.client_id "
		  "      and shipment_line.client_id    = ord.client_id "
		  "      and shipment_line.ordnum       = ord.ordnum) "
		  "   and shplbl is not null "
		  "   and length(rtrim(shplbl)) > 0 ", 
		  xx, 
		  colnam, WrkRef);
		if (strlen(buffer))
		{
		    strcat (buffer, " union ");
		}
		strcat (buffer, tmpbuf);
		xx++;
	    } while ((next = sqlGetNextRow(next)) != NULL);
       	    sqlFreeResults(ptr);

	    strcat (buffer, " order by 1");
	    errcode = sqlExecStr(buffer, &ptr);
    	    if (errcode == eOK)
	    {
	        next = sqlGetRow(ptr);
		strcpy(Format, sqlGetValue(ptr, next, "shplbl"));
		misTrim(Format);
		file_found = TRUE;
       	        sqlFreeResults(ptr);
	    }
	}
    }

    /*  Okay so now we need to determine the format file to open and see
       if the file is there */
    if (file_found == FALSE)
    {
        sprintf(buffer,
	    "select rtstr1 "
	    "  from poldat "
	    " where polcod = 'LABEL-PRINTERS'"
	    "   and polvar = 'LABEL-FORMAT-NAMES'"
	    "   and polval = '%s' order by srtseq", U_Format);
    	errcode = sqlExecStr(buffer, &ptr);
    	if (errcode != eDB_NO_ROWS_AFFECTED && errcode != eOK)
    	{
	    misLogError("Error %d reading for the network code policy!",
		    errcode);
    	}

    	if (errcode == eOK)
    	{
	    next = sqlGetRow(ptr);
	    do
	    {
	    	memset(DataToFill, 0, sizeof(DataToFill));
	    	strcpy(WorkString, sqlGetValue(ptr, next, "rtstr1"));
	    	misTrim(WorkString);
	    	memset(TempString, 0, sizeof(TempString));
	    	Pos = FillItems = LiteralOpened = 0;
	    	while (Pos < (long) strlen(WorkString))
	    	{
		    if (isalnum(WorkString[Pos]))
		    	TempString[strlen(TempString)] = 
			    tolower(WorkString[Pos]);
		    else
		    {
		    	if (WorkString[Pos] == SINGLE_QUOTE)
		    	{
			    if (strlen(TempString))
			    {
			    	strcpy(DataToFill[FillItems].Data, TempString);
			    	memset(TempString, 0, sizeof(TempString));
			    	if (WorkString[Pos] == SINGLE_QUOTE)
			    	{
				    if (LiteralOpened)
				    {
				    	DataToFill[FillItems].Type = TRUE;
				    	LiteralOpened = FALSE;
				    }
				    else
				    	LiteralOpened = TRUE;
			    	}
			    	FillItems++;
			    }
			    else
			    {
			    	if (WorkString[Pos] == SINGLE_QUOTE)
			    	{
				    if (LiteralOpened)
				    	LiteralOpened = 0;
				    else
				    	LiteralOpened = 1;
			    	}
			    }
		        }
		        else
		        {
			    if (LiteralOpened)
			    	TempString[strlen(TempString)] =
				tolower(WorkString[Pos]);
		    	}
		    }
		    Pos++;
	    	}
	    	if (strlen(TempString))
	    	{
		    strcpy(DataToFill[FillItems].Data, TempString);
		    memset(TempString, 0, sizeof(TempString));
		    if (WorkString[Pos] == SINGLE_QUOTE)
		    {
		    	if (LiteralOpened)
		    	{
			    DataToFill[FillItems].Type = TRUE;
			    LiteralOpened = FALSE;
		    	}
		    	else
			    LiteralOpened = TRUE;
		        FillItems++;
		    }
		    else
		        FillItems++;
	    	}

	    	NeedToSelect = FALSE;
	    	for (i = 0; i < FillItems; i++)
	    	{
		    if (DataToFill[i].Type == 0)
		    {
		        if (srvGetNeededElement(DataToFill[i].Data, "",
				   &DataType,
				   (void **) &DataToFill[i].DataPtr) != eOK)
			NeedToSelect = TRUE;
		    }
	    	}

	    	if (NeedToSelect)
	    	{
		    sprintf(buffer,
		     "select * from pckwrk, shipment_line, shipment "
		     " where wrkref = '%s' "
		     "   and shipment.ship_id    = shipment_line.ship_id "
		     "   and pckwrk.ship_line_id = shipment_line.ship_line_id ",
		     WrkRef);

		    errcode = sqlExecStr(buffer, &ptr2);
		    if (errcode != eDB_NO_ROWS_AFFECTED && errcode != eOK)
		    {
		        misLogError("Error %d reading printer type", errcode);
		        return (srvSetupReturn(errcode, ""));
		    }

	 	    /*  We may not have a wrkref, so just try and get the 
		        data with the ship_id */

		    if (srvGetNeededElement("ship_id", "", &DataType,
					(void **) &ship_id) == eOK)
		    {
		    	sprintf(buffer, "select * from shipment_line where "
			    " ship_id = '%s'",
			    ship_id);

		        errcode = sqlExecStr(buffer, &ptr2);
		    }

		    if (errcode != eOK)
		    	return (srvSetupReturn(eSRV_INSUFF_ARGUMENTS, ""));
		    else
		    	next2 = sqlGetRow(ptr2);
	        }

	        sprintf(Filename, "%s%c", 
			misExpandVars(varbuf, LES_LABELS, sizeof(varbuf), NULL),
		        PATH_SEPARATOR);
	        memset(FormatName, 0, sizeof(FormatName));
	        for (i = 0; i < FillItems; i++)
	        {
		    if (DataToFill[i].Type)
		    {
		        strcat(Filename, DataToFill[i].Data);
		        strcat(FormatName, DataToFill[i].Data);
		    }
		    else
		    {
		    	if (DataToFill[i].DataPtr)
		    	{
			    strcat(Filename, DataToFill[i].DataPtr);
			    strcat(FormatName, DataToFill[i].DataPtr);
		    	}
		    	else
		        {
			    strcat(Filename,
			       misTrim(sqlGetValue(ptr2, next2, DataToFill[i].Data)));
			    strcat(FormatName,
			       misTrim(sqlGetValue(ptr2, next2, DataToFill[i].Data)));
		    	}
		    }
	    	}

	    	if (strlen(FormatName))
		    strcpy(Format, FormatName);
	    	if (NeedToSelect)
		    sqlFreeResults(ptr2);

	    	if (misFileExists(Filename) == eOK)
		    break;
	    } while ((next = sqlGetNextRow(next)) != NULL);

	    sqlFreeResults(ptr);
    	}
    }

    memset(PrtTyp, 0, sizeof(PrtTyp));
    if (!ignore_type)
    {
	if (strlen(PrtAdr))
	{
	    /* get the printer type from the prsmst */
	    sprintf(buffer,
		    "select prttyp "
		    "  from prsmst "
		    " where prtadr = '%s'",
		    PrtAdr);

	    errcode = sqlExecStr(buffer, &ptr);
	    if (errcode != eDB_NO_ROWS_AFFECTED && errcode != eOK)
	    {
		misLogError("Error %d reading printer type", errcode);
	    }
	    if (errcode == eOK)
	    {
		next = sqlGetRow(ptr);
		strcpy(PrtTyp, sqlGetValue(ptr, next, "prttyp"));
		sqlFreeResults(ptr);
	    }
	}

	if (!strlen(PrtTyp))
	{
	    /* get the printer type from devcod  */
	    sprintf(buffer,
		    "select prsmst.prttyp, devmst.prtadr "
		    "  from devmst, prsmst "
		    " where devmst.devcod = '%s'"
		    "   and devmst.prtadr = prsmst.prtadr", DevCode);

	    errcode = sqlExecStr(buffer, &ptr);
	    if (errcode != eDB_NO_ROWS_AFFECTED && errcode != eOK)
	    {
		misLogError("Error %d reading printer type", errcode);
	    }

	    if (errcode == eOK)
	    {
		next = sqlGetRow(ptr);
		strcpy(PrtTyp, sqlGetValue(ptr, next, "prttyp"));
		if (strlen(PrtAdr) == 0)
		    misTrimcpy(PrtAdr,
			 (char *) sqlGetValue(ptr, next, "prtadr"), PRTADR_LEN);

		sqlFreeResults(ptr);
	    }
	}

	if (!strlen(PrtTyp) && strlen(WrkRef))
	{
	    /* get the printer based on the wrkref  */
	    sprintf(buffer,
		    "select prsmst.prttyp "
		    "  from devmst, prsmst, pckwrk "
		    " where pckwrk.wrkref = '%s' "
		    "   and devmst.devcod = pckwrk.devcod "
		    "   and devmst.prtadr = prsmst.prtadr", WrkRef);

	    errcode = sqlExecStr(buffer, &ptr);
	    if (errcode != eDB_NO_ROWS_AFFECTED && errcode != eOK)
	    {
		misLogError("Error %d reading printer type", errcode);
	    }
	    if (errcode == eOK)
	    {
		next = sqlGetRow(ptr);
		strcpy(PrtTyp, sqlGetValue(ptr, next, "prttyp"));
		sqlFreeResults(ptr);
	    }
	    else
	    {
		/* we get it off of the devcod from the srcloc on the pick */

		sprintf(buffer,
			"select prsmst.prttyp "
			"  from locmst, devmst, prsmst, pckwrk "
			" where pckwrk.wrkref = '%s' "
			"   and locmst.stoloc = pckwrk.srcloc "
			"   and devmst.devcod = locmst.devcod "
			"   and devmst.prtadr = prsmst.prtadr", WrkRef);

		errcode = sqlExecStr(buffer, &ptr);
		if (errcode != eDB_NO_ROWS_AFFECTED && errcode != eOK)
		{
		    misLogError("Error %d reading printer type", errcode);
		}

		if (errcode == eOK)
		{
		    next = sqlGetRow(ptr);
		    strcpy(PrtTyp, sqlGetValue(ptr, next, "prttyp"));
		    sqlFreeResults(ptr);
		}
	    }
	}
    }

    if (!strlen(PrtTyp))
    {
	memset(Prefix, 0, sizeof(Prefix));
	memset(Suffix, 0, sizeof(Suffix));
    }
    else
    {
	sprintf(buffer,
		"select  rtstr1, rtstr2 "
		"  from poldat "
		" where polcod = 'LABEL-PRINTERS'"
		"   and polvar = '%s'"
	 "   and polval = 'LABEL-PREFIX-N-SUFFIX' order by srtseq", PrtTyp);
	errcode = sqlExecStr(buffer, &ptr);
	if (errcode != eDB_NO_ROWS_AFFECTED && errcode != eOK)
	{
	    misLogError("Error %d reading for the network code policy!",
			errcode);
	}

	if (errcode == eOK)
	{
	    next = sqlGetRow(ptr);
	    strcpy(Prefix, misTrim(sqlGetValue(ptr, next, "rtstr1")));
	    strcpy(Suffix, misTrim(sqlGetValue(ptr, next, "rtstr2")));
	    sqlFreeResults(ptr);
	}
    }

    if (strlen(Filename))
    {
	for (i = 0; i < (long) strlen(Filename); i++)
	    if (Filename[i] > 0) Filename[i] = tolower(Filename[i]);
    }
    if (strlen(Format))
    {
	for (i = 0; i < (long) strlen(Format); i++)
	    if (Format[i] > 0) Format[i] = tolower(Format[i]);
    }

    if (!strlen(Filename) || (FormatFilePtr = fopen(Filename, "r")) == NULL)
    {
	memset(Filename, 0, sizeof(Filename));
	sprintf(FormatName, "%s%s%s", Prefix, Format, Suffix);
	sprintf(Filename, "%s%c%s",
		misExpandVars(varbuf, LES_LABELS, sizeof(varbuf), NULL),
		PATH_SEPARATOR, FormatName);
	if ((FormatFilePtr = fopen(Filename, "r")) == NULL)
	{
	    memset(Filename, 0, sizeof(Filename));
	    strcpy(FormatName, Format);
	    sprintf(Filename, "%s%c%s",
		    misExpandVars(varbuf, LES_LABELS, sizeof(varbuf), NULL),
		    PATH_SEPARATOR, Format);
	    if ((FormatFilePtr = fopen(Filename, "r")) == NULL)
	    {
		return (srvSetupReturn(eFILE_OPENING_ERROR, ""));
	    }
	}
    }

    Done = FALSE;
    NextFormat = Formats;
    while (!Done && NextFormat)
    {
	if (!strcmp(NextFormat->Filename, Filename))
	{
	    /* We've already done this file before so we have everything we
	       need to process the data */

	    if (NextFormat->Selects)
	    {
		Selects = NextFormat->Selects;
		if ((errcode = DoSelects()) != eOK)
		{
		    if (FormatFilePtr)
			fclose(FormatFilePtr);
		    return (srvSetupReturn(errcode, ""));
		}
	    }
	    ExpandString(NextFormat->Data, Data);
	    lblqty = NextFormat->LblQty;
	    Done = TRUE;
	}
	NextFormat = NextFormat->Next;
    }

    if (!Done)
    {

	/* Ok, so now we have an opened file.  We read thru it a grab the 
	   approp. information. We are looking only at lines starting with # 
	   (comment) and know about with keywords DATA= and SELECT= and we 
	   start building our datasets.  SELECT commands must fall before 
	   DATA commands if you are going to use a SELECT command. Selects 
	   will be built dynamically - using buffer as temporary storage, 
	   and data is moved into the Data variable.

	   The command will PUBLISH the Format File name,
	   Net Code, Printer Address,
	   and the Data. */

	NextFormat = calloc(1, sizeof(FORMATS));
	NextFormat->Next = Formats;
	Formats = NextFormat;
	strcpy(Formats->Filename, Filename);

	lblqty = 1;
	memset(Data, 0, sizeof(Data));
	while (fgets(Line, sizeof(Line) - 1, FormatFilePtr) != NULL)
	{
	    if (Line[strlen(Line) - 1] > ' ')
		Line[strlen(Line) - 1] = 0;

	    if (Line[0] == PRSFRM_COMMENT)
	    {
		if (strstr(Line, KEYWORD_NEXT))
		{
		    OldPos = strstr(Line, "=") + 1;
		    lblqty = atol(OldPos);
		    if (lblqty < 1)
			lblqty = 1;
		    Formats->LblQty = lblqty;
		}
		if (strstr(Line, KEYWORD_QUANTITY))
		{
		    OldPos = strstr(Line, "=") + 1;
		    lblqty = atol(OldPos);
		    if (lblqty < 1)
			lblqty = 1;
		    Formats->LblQty = lblqty;
		}
		if (strstr(Line, KEYWORD_DATA))
		{
		    if (Selects)
		    {
			if ((errcode = DoSelects()) != eOK)
			{
			    /* If we're getting out of here, 
			       then we need to drop this 
			       format from the list as we 
			       really haven't finished processing
			       it successfully... */
			    if (Formats->Data)
				free(Formats->Data);

			    last = NULL;
			    for (tmpSel = Formats->Selects; tmpSel;
				 tmpSel = tmpSel->Next)
			    {
				if (last)
				    free(last);
				last = tmpSel;
			    }
			    if (last)
				free(last);
			    tmpFormat = Formats;
			    Formats = Formats->Next;
			    free(tmpFormat);
                            fclose(FormatFilePtr);
			    return (srvSetupReturn(errcode, ""));
			}
		    }

		    misTrim(Line);
		    strcat(Line, " ");
		    OldPos = (char *) Line + sizeof(KEYWORD_DATA);
		    while (OldPos &&
			 (TempPtr = strchr(OldPos, BACKSLASH_CHAR)) != NULL)
		    {
			switch (*(TempPtr + 1))
			{
			case BACKSLASH_CHAR:
			    OldPos = TempPtr + 2;
			    continue;

			case 'f':
			    *TempPtr = NP;
			    strcpy((TempPtr + 1), TempPtr + 2);
			    break;

			case 'r':
			    *TempPtr = CR;
			    strcpy((TempPtr + 1), TempPtr + 2);
			    break;

			case 'n':
			    *TempPtr = NL;
			    strcpy((TempPtr + 1), TempPtr + 2);
			    break;

			case 'p':
			    OldPos = TempPtr + 1;
			    continue;

			case 't':
			    *TempPtr = TA;
			    strcpy((TempPtr + 1), TempPtr + 2);
			    break;
			}
			OldPos = TempPtr + 1;
		    }

		    OldPos = (char *) Line + sizeof(KEYWORD_DATA);
		    Formats->Data = calloc(1, strlen(OldPos) + 1);
		    if (!Formats->Data)
		    {
			if (FormatFilePtr)
			    fclose(FormatFilePtr);
			return (srvSetupReturn(eNO_MEMORY, ""));
		    }
		    strcpy(Formats->Data, OldPos);
		    ExpandString(OldPos, Data);
		}
		else if (strstr(Line, KEYWORD_SELECT))
		{
		    if (Selects)
		    {
			NextSelect = Selects;
			PreviousSelect = NULL;
			while (NextSelect)
			{
			    PreviousSelect = NextSelect;
			    NextSelect = NextSelect->Next;
			}
			NextSelect = calloc(1, sizeof(SELECTS));
			PreviousSelect->Next = NextSelect;
			NextSelect->Prev = PreviousSelect;
		    }
		    else
		    {
			Selects = calloc(1, sizeof(SELECTS));
			NextSelect = Selects;
		    }

		    if (!NextSelect)
		    {
			if (FormatFilePtr)
			    fclose(FormatFilePtr);
			return (srvSetupReturn(eNO_MEMORY, ""));
		    }

		    misTrim(Line);
		    strcat(Line, " ");
		    OldPos = (char *) Line + sizeof(KEYWORD_SELECT);

		    IfPtr = NULL;
		    if (strstr(Line, " if ") || strstr(Line, " if("))
		    {
			IfPtr = strstr(Line, " if ");
			if (!IfPtr)
			    IfPtr = strstr(Line, " if(");
			*IfPtr = 0;
			IfPtr++;
		    }

		    NextSelect->select = calloc(1, strlen(Line) + 1);
		    if (!NextSelect->select)
		    {
                        fclose(FormatFilePtr);
			return (srvSetupReturn(eNO_MEMORY, ""));
		    }

		    i = 0;
		    while ((TempPtr =
			    strstr(OldPos, PREFIX_DESTINATION)) != NULL)
		    {
			SegLength = (int) (TempPtr - OldPos);
			strncat(NextSelect->select, OldPos, SegLength);
			if (*(char *) (TempPtr + 1) == ' ')
			{
			    strncat(NextSelect->select, TempPtr, 1);
			    OldPos = TempPtr + 1;
			    continue;
			}

			if (strstr(NextSelect->select, "into "))
			    *(strstr(NextSelect->select, "into ")) = 0;

			TempPtr2 = strpbrk(TempPtr, "'%,\" ");
			if (TempPtr2)
			    SegLength = (int) (TempPtr2 - (TempPtr + 1));
			else
			    SegLength = strlen(TempPtr + 1);
			strncpy(TempString, TempPtr + 1, SegLength);
			TempString[SegLength] = 0;
			NextSelect->IntoValue = calloc(1,
						    strlen(TempString) + 1);
			if (!NextSelect->IntoValue)
			{
                            fclose(FormatFilePtr);
			    return (srvSetupReturn(eNO_MEMORY, ""));
			}
			strcpy(NextSelect->IntoValue, TempString);
			OldPos = TempPtr2;
		    }
		    strcat(NextSelect->select, OldPos);
		    if (IfPtr)
		    {
			NextSelect->IfTest = calloc(1, strlen(IfPtr) + 1);
			if (!NextSelect->IfTest)
			{
                            fclose(FormatFilePtr);
			    return (srvSetupReturn(eNO_MEMORY, ""));
			}

			strcpy(NextSelect->IfTest, IfPtr);
		    }
		}
	    }
	}
	Formats->Selects = Selects;
    }

    fclose(FormatFilePtr);
    FormatFilePtr = NULL;

    NextSelect = Selects;
    while (NextSelect)
    {
	if (NextSelect->ptr)
	{
	    sqlFreeResults(NextSelect->ptr);
	    NextSelect->ptr = NULL;
	    NextSelect->nextptr = NULL;
	}
	NextSelect = NextSelect->Next;
    }

    memset(datatypes, 0, sizeof(datatypes));
    if (srvGetNeededElement("lblqty", "", &DataType, &GenericPtr) != eOK)
    {
	srvSetupColumns(5);
	i = 0;
	datatypes[i] = srvSetColName(i + 1, "format", COMTYP_CHAR, FILNAM_LEN);
	i++;
	datatypes[i] = srvSetColName(i + 1, "prttyp", COMTYP_CHAR, PRTTYP_LEN);
	i++;
	datatypes[i] = srvSetColName(i + 1, "data", COMTYP_CHAR, strlen(Data));
	i++;
	datatypes[i] = srvSetColName(i + 1, "prtadr", COMTYP_CHAR, PRTADR_LEN);
	i++;
	datatypes[i] = srvSetColName(i + 1, "lblqty", COMTYP_INT, sizeof(long));
	i++;

	CurPtr = (RETURN_STRUCT *) srvSetupReturn(eOK, datatypes,
						  Format, PrtTyp,
						  Data, PrtAdr,
						  lblqty);
    }
    else
    {
	srvSetupColumns(4);
	i = 0;
	datatypes[i] = srvSetColName(i + 1, "format", COMTYP_CHAR, FILNAM_LEN);
	i++;
	datatypes[i] = srvSetColName(i + 1, "prttyp", COMTYP_CHAR, PRTTYP_LEN);
	i++;
	datatypes[i] = srvSetColName(i + 1, "data", COMTYP_CHAR,
				     misTrimLen(Data, sizeof(Data)));
	i++;
	datatypes[i] = srvSetColName(i + 1, "prtadr", COMTYP_CHAR, PRTADR_LEN);
	i++;

	CurPtr = (RETURN_STRUCT *) srvSetupReturn(eOK, datatypes,
						  Format, PrtTyp,
						  Data, PrtAdr);
    }
    return (CurPtr);
}

static char    *_GetDataElement(char *String, char *ReturnType)
{
    static char     TempData[500];
    void           *GenericPtr;
    long           *LongPtr;
    double         *DoublePtr;
    long            TestInt;
    char            DataType;
    int             i, j;
    int             LookForIndex;
    char           *Index;
    int             Column;
    int             LookForIntoValue;
    char           *IntoValue;


    LookForIndex = LookForIntoValue = FALSE;
    if ((Index = strchr(String, '.')) != NULL)
    {
	*Index = 0;
	IntoValue = String;
	LookForIntoValue = TRUE;
	String = Index + 1;
    }

    if ((Index = strchr(String, '[')) != NULL)
    {
	*Index = 0;
	Index = strtok(Index + 1, "]");
	if (Index)
	    LookForIndex = atoi(Index);
    }

    memset(TempData, 0, sizeof(TempData));
    NextSelect = Selects;
    Column = -1;
    while (NextSelect)
    {
	PreviousSelect = NextSelect;
	NextSelect = NextSelect->Next;
    }
    NextSelect = PreviousSelect;

    while (NextSelect && !strlen(TempData))
    {
	if (LookForIntoValue)
	{
	    if (misCiStrcmp(NextSelect->IntoValue, IntoValue))
	    {
		NextSelect = NextSelect->Prev;
		continue;
	    }
	}


	Column = -1;
	for (i = 0; NextSelect->ptr && i < NextSelect->ptr->NumOfColumns &&
	     Column < 0; i++)
	{
	    if (!strcmp(String, NextSelect->ptr->ColName[i]))
		Column = i;
	}

	if (Column >= 0)
	{
	    if (LookForIndex)
	    {
		if (LookForIndex > NextSelect->ptr->NumOfRows - 1)
		{
		    memset(TempData, ' ', NextSelect->ptr->ActualMaxLen[Column]);
		    return (TempData);
		}
		NextSelect->nextptr = sqlGetRow(NextSelect->ptr);
		for (j = 0; j < LookForIndex; j++)
		    NextSelect->nextptr = sqlGetNextRow(NextSelect->nextptr);
	    }

	    i = Column;
	    if (NextSelect->ptr && NextSelect->nextptr)
	    {
		switch (NextSelect->ptr->DataType[i])
		{
		case COMTYP_INT:
		case COMTYP_LONG:
		    *ReturnType = ARGTYP_INT;
		    sprintf(TempData, "%ld",
			    *(long *) NextSelect->nextptr->DataPtr[i]);
		    break;

		case COMTYP_FLOAT:
		    *ReturnType = ARGTYP_FLOAT;
		    DoublePtr = (double *) NextSelect->nextptr->DataPtr[i];
		    TestInt = (long) *DoublePtr;
		    if ((double) TestInt == *DoublePtr)
			sprintf(TempData, "%ld", TestInt);
		    else
		    {
			sprintf(TempData, "%f", *DoublePtr);
			_TrimZeros(TempData);
		    }
		    break;

		case COMTYP_CHAR:
		case COMTYP_TEXT:
		case COMTYP_DATTIM:
		    *ReturnType = ARGTYP_STR;
		    strcpy(TempData,
			 misTrim((char *) NextSelect->nextptr->DataPtr[i]));
		    if (!strlen(TempData))
			strcpy(TempData, "");
		    break;
		}
	    }
	}

	if (LookForIndex && Column >= 0)
	{
	    NextSelect->nextptr = sqlGetRow(NextSelect->ptr);
	    if (!strlen(TempData))
		return (NULL);
	}

	NextSelect = NextSelect->Prev;
    }

    if (LookForIntoValue && !strlen(TempData))
	return (NULL);

    if (strlen(TempData))
	return (TempData);

    if (srvGetNeededElement(String, "", &DataType, &GenericPtr) != eOK)
	return (NULL);

    if (!GenericPtr)
	return (NULL);

    switch (DataType)
    {
    case ARGTYP_INT:
	LongPtr = (long *) GenericPtr;
	sprintf(TempData, "%d", (int) *LongPtr);
	break;

    case ARGTYP_FLOAT:
	DoublePtr = (double *) GenericPtr;
	TestInt = (long) *DoublePtr;
	/* If we are really dealing with an integer then don't send back
	   a value with the decimal places in it */

	if (TestInt == (long) *DoublePtr)
	    sprintf(TempData, "%ld", TestInt);
	else
	    sprintf(TempData, "%f", *DoublePtr);
	break;

    default:
	strcpy(TempData, misTrim((char *) GenericPtr));
    }
    return (TempData);
}

static long     ExpandString(char *SourceString, char *Data)
{
    char           *OldPos;
    char           *TempPtr, *TempPtr2;
    long            SegLength, i;
    int             GotParen, GotCR;
    char            TempString[200];
    char           *DataPtr;
    char            DataType;
    long            Position, Base;

    OldPos = SourceString;
    Data[0] = 0;
    while (OldPos && (TempPtr = strstr(OldPos, PREFIX_VARIABLE)) != NULL)
    {
	GotParen = FALSE;
	SegLength = (int) (TempPtr - OldPos);
	strncat(Data, OldPos, SegLength);
	if ((*(TempPtr + 1)) == OPEN_PAREN_CHAR)
	{
	    TempPtr++;
	    GotParen = TRUE;
	    TempPtr2 = strpbrk(TempPtr, CLOSE_PAREN_STR);
	}
	else
	    TempPtr2 = strpbrk(TempPtr, "\f\n\r\t'%,/~#$^(){}[]\\\" ");

	if (TempPtr2)
	    SegLength = (int) (TempPtr2 - (TempPtr + 1));
	else
	    SegLength = strlen(TempPtr + 1);

	strncpy(TempString, TempPtr + 1, SegLength);
	TempString[SegLength] = 0;
	for (i = 0; i < (long) strlen(TempString); i++)
	    TempString[i] = tolower(TempString[i]);
	DataPtr = _GetDataElement(TempString, &DataType);
	if (DataPtr)
	    strcat(Data, DataPtr);
	OldPos = TempPtr2;
	if (GotParen)
	    OldPos++;
    }

    if (OldPos && strlen(OldPos))
	strcat(Data, (char *) srvTrim(OldPos));

    OldPos = Data;
    while (OldPos && (TempPtr = strchr(OldPos, BACKSLASH_CHAR)) != NULL)
    {
	if (*(TempPtr + 1) == 'p')
	{
	    if (*(TempPtr + 2) == OPEN_PAREN_CHAR)
	    {
		OldPos = TempPtr + 3;
		GotParen = TRUE;
	    }
	    else
	    {
		OldPos = TempPtr + 2;
		GotParen = FALSE;
	    }
	    memset(TempString, 0, sizeof(TempString));
	    for (i = 0; isdigit(*OldPos) && i < 4; OldPos++)
		TempString[i++] = *OldPos;
	    if (GotParen)
		OldPos++;
	    Position = atol(TempString);
	    SegLength = (long) TempPtr - (long) Data - 1;
	    if (SegLength < 0)
		SegLength = 0;

	    GotCR = FALSE;
	    Base = 0;
	    for (i = SegLength; i > 0 && !GotCR; i--)
		if (Data[i] == NL || Data[i] == CR)
		{
		    Base = SegLength - (SegLength - i);
		    break;
		}

	    if (SegLength - Base < Position)
	    {
		memmove((char *) (Data + Base + Position), OldPos,
			(int) (strlen(OldPos) + 1));
		memset(TempPtr, ' ', Position - (SegLength - Base) - 1);
	    }
	    else
		memmove((char *) (Data + Base + Position - 1), OldPos,
			(int) (strlen(OldPos)));

	    OldPos = (char *) Data + Base + Position + 1;
	}
	else
	{
	    if (*(TempPtr + 1) == BACKSLASH_CHAR)
		OldPos = OldPos + 2;
	    else
		OldPos = OldPos + 1;
	}
    }
    return (eOK);
}

static long     DoSelects(void)
{
    long            errcode;
    char            Data[4096];
    SELECTS        *NextSel = NULL;

    srvSetServerTrimOff();
    NextSel = Selects;
    while (NextSel)
    {
	memset(Data, 0, sizeof(Data));
	if (NextSel->IfTest)
	{
	    if (!ProcessIfTest(NextSel->IfTest))
	    {
		NextSel = NextSel->Next;
		continue;
	    }
	}
	ExpandString(NextSel->select, Data);
	errcode = sqlExecStr(Data, &NextSel->ptr);
	misTrc(T_FLOW, "Select [%s] returned %ld", Data, errcode);
	NextSel->errcode = errcode;
	if (errcode == eOK)
	    NextSel->nextptr = sqlGetRow(NextSel->ptr);
	else
	{
	    srvSetServerTrimOn();
	    return (errcode);
	}
	NextSel = NextSel->Next;
    }
    srvSetServerTrimOn();
    return (eOK);
}

static char    *_TrimZeros(char *string)
{
    int             i;
    for (i = (int) strlen(string) - 1; i >= 0 && (string[i] == '0'); i--)
	string[i] = '\0';
    return (string);
}


#define IF_EXISTS 	0
#define IF_EQUAL  	1
#define IF_LSTHAN 	2
#define IF_GTTHAN 	4
#define IF_NOT		8
#define IF_GTEQUAL  5
#define IF_LTEQUAL 	3

static long     ProcessIfTest(char *IfClause)
{
    long            CurrentValue;
    char           *OrPtr;
    char           *AndPtr;
    char           *NextPtr;
    char           *Left;
    char           *Right;
    char           *Temp;
    long            Process, i;
    char           *Operators = "=<>!";
    char            TempIfClause[100];
    char            DataType;
    char           *VariablePtr;
    char           *IfPtr;
    long            Result;
    double          FloatResult;

    /* We'll start with something dirt simple, and add to it if we
       need more complexity (post order parser for example) */

    CurrentValue = 0;

    strcpy(TempIfClause, IfClause);
    IfPtr = strchr(TempIfClause, '@');
    if (!IfPtr)
	return (FALSE);
    IfPtr++;

    NextPtr = OrPtr = strstr(IfPtr, " or ");
    AndPtr = strstr(IfPtr, " and ");
    if (NextPtr == NULL || (AndPtr != NULL && AndPtr < NextPtr))
	NextPtr = AndPtr;

    /*  Okay, I was wrong and we need a bit more complexity, so we'll find out
       if we've got any more if's to test by looking for ' and ' and ' or '.
       If we find one, we'll trim the current string and recurse.  By the
       time we get back here, all of the other if test have been checked. */

    if (NextPtr)
    {
	*NextPtr = 0;
	if (NextPtr == OrPtr)
	    NextPtr = OrPtr = OrPtr + 3;
	else
	    NextPtr = AndPtr = AndPtr + 4;

	CurrentValue = ProcessIfTest(NextPtr);
	if (NextPtr == AndPtr && !CurrentValue)
	    return (CurrentValue);

	if (NextPtr == OrPtr && CurrentValue)
	    return (CurrentValue);
    }

    Left = Right = 0;
    Process = 0;

    for (i = 0; i < (long) strlen(Operators); i++)
    {
	if ((Temp = strchr(TempIfClause, Operators[i])) != NULL)
	{
	    if (!Left || Temp < Left)
		Left = Temp;

	    if (Temp > Right)
		Right = Temp;

	    Process += (1 << i);
	}
    }
    /* Get the Value(s) we need to check */

    if (!Left || Left < IfPtr)
    {
	Left = strchr(IfPtr, ')');
	if (Right == Left)
	    Right = 0;
    }
    if (Right)
	Right++;

    if (!Left)
	return (FALSE);
    *Left = 0;
    misTrim(IfPtr);

    for (i = 0; i < (long) strlen(IfPtr); i++)
	IfPtr[i] = tolower(IfPtr[i]);

    VariablePtr = _GetDataElement(IfPtr, &DataType);

    if (Process == IF_EXISTS && VariablePtr)
	return (TRUE);

    if (Process == IF_EXISTS && !VariablePtr)
	return (FALSE);

    if (Process == IF_NOT && VariablePtr)
	return (FALSE);

    if (Process == IF_NOT && !VariablePtr)
	return (TRUE);

    if (!Right)
	return (FALSE);

    if ((Temp = strchr(Right, ')')) != NULL)
	*Temp = 0;

    misTrimLR(Right);
    if (*Right == DOUBLE_QUOTE || *Right == SINGLE_QUOTE)
    {
	Right++;
	if ((Temp = strchr(Right, DOUBLE_QUOTE)) != NULL)
	    *Temp = 0;
	else if ((Temp = strchr(Right, SINGLE_QUOTE)) != NULL)
	    *Temp = 0;
    }

    if (VariablePtr)
    {
	switch (DataType)
	{
	case ARGTYP_INT:
	    Result = ((long) atoi(VariablePtr) - atol(Right));
	    break;

	case ARGTYP_FLOAT:
	    FloatResult = (atof(VariablePtr) - atof(Right));
	    if (FloatResult == 0)
		Result = 0;
	    if (FloatResult < 0)
		Result = -1;
	    if (FloatResult > 0)
		Result = 1;
	    break;

	default:
	    Result = ((long) strcmp(VariablePtr, Right));
	}
    }
    else
	Result = ((long) strcmp("", Right));

    /* First get rid of all the equal to cases and not equal */

    if (Process & IF_NOT)
    {
	if (Result == 0)
	    return (FALSE);
	else
	    return (TRUE);
    }

    if (Process & IF_EQUAL)
    {
	if (Result == 0)
	    return (TRUE);
	else
	    return (FALSE);
    }

    if (Process & IF_LSTHAN)
    {
	if (Result < 0)
	    return (TRUE);
	else
	    return (FALSE);
    }

    if (Process & IF_GTTHAN)
    {
	if (Result > 0)
	    return (TRUE);
	else
	    return (FALSE);
    }

    return (FALSE);
}
