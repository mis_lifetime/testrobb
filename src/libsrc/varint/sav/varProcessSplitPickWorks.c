static const char *rcsid = "$Id: varProcessSplitPickWorks.c,v 1.12 2003/06/05 15:03:25 prod Exp $"; 
/*#START*********************************************************************** 
 *  McHugh Software International 
 *  Copyright 1999 
 *  Waukesha, Wisconsin,  U.S.A. 
 *  All rights reserved. 
 * 
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/libsrc/varint/varProcessSplitPickWorks.c,v $ 
 *  $Revision: 1.12 $ 
 * 
 *  Application:   Lifetime Varlib 
 *  Created:       28-Jun-2001 
 *  $Author: prod $ 
 * 
 * 
 *#END************************************************************************/ 
 
#include <moca_app.h> 
 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
 
#include <dcscolwid.h> 
#include <dcsgendef.h> 
#include <dcserr.h> 
#include <trnlib.h> 
 
 
LIBEXPORT  
RETURN_STRUCT *varProcessSplitCasePickWorks(char *wrkref_i, 
				   char *pcksts_i) 
{  
	RETURN_STRUCT *CurPtr, *CmdRes; 
	char buffer[2000];  
	char wrkref[WRKREF_LEN+1];  
	char new_wrkref[WRKREF_LEN+1];  
	char new_cmbcod[CMBCOD_LEN+1];  
	char new_subucc[UCCCOD_LEN+1];  /*MR1210*/ 
	char new_subucc1[UCCCOD_LEN+1];  /*MR1210*/
	char pcksts[PCKSTS_LEN+1];  
	char tmpstr[100];  
	long ret_status;  
	long i;  
	mocaDataRes *res;  
	mocaDataRow *row; 
	mocaDataRes *movRes;  
	mocaDataRow *movRow;  
	mocaDataRes *res2; /*MR1210*/  
	mocaDataRow *row2; /*MR1210*/
    long number_iterations, untqty, untcas; 
    char fieldList[100]; 
    char valueList[300]; 
 
    memset(wrkref, 0, sizeof(wrkref)); 
 
    if (wrkref_i && strlen(wrkref_i)) 
	strncpy(wrkref, wrkref_i, WRKREF_LEN); 
 
    memset(pcksts, 0, sizeof(pcksts)); 
    if (pcksts_i && strlen(pcksts_i)) 
	strncpy(pcksts, pcksts_i, PCKSTS_LEN); 
 
    sprintf(buffer, 
	    "select * from pckwrk where wrkref = '%s' " 
	    " for update of pckwrk.pcksts", 
	    wrkref); 
     
    ret_status = sqlExecStr(buffer, &res); 
    if (ret_status != eOK) 
    { 
	sqlFreeResults(res); 
	return(srvSetupReturn(ret_status, "")); 
    } 
     
    row = sqlGetRow(res); 
 
    /* MOD 1 - MCF 
 
	if (strncmp(sqlGetValue(res, row, "lodlvl"),  
		LODLVL_SUB, LODLVL_LEN) != 0 || 
	strncmp(sqlGetValue(res, row, "wrktyp"),  
		WRKTYP_PICK, WRKTYP_LEN) != 0)  
 
	END MOD 1 */ 
 
    if (strncmp(sqlGetValue(res, row, "lodlvl"),  
		LODLVL_SUB, LODLVL_LEN) != 0 ) 
    { 
	/* Either not a case pick or is a case pick but is a kit */ 
	sqlFreeResults(res); 
	return(srvSetupReturn(eOK, "")); 
    } 
 
    untcas = sqlGetLong(res, row, "untcas"); 
    untqty = sqlGetLong(res, row, "pckqty"); 
     
    number_iterations = untqty / untcas; 
 	CurPtr = srvResultsInit(eOK, "wrkref", COMTYP_CHAR, WRKREF_LEN,NULL); 
 
    for (i = 0; i < number_iterations; i++) 
    { 
	if (untqty > untcas) 
	{ 
	    memset(new_wrkref, 0, sizeof(new_wrkref)); 
	    memset(new_cmbcod, 0, sizeof(new_cmbcod)); 
	    memset(new_subucc, 0, sizeof(new_subucc)); /*MR1210*/
	    appNextNum(NUMCOD_WRKREF, new_wrkref); 
	    appNextNum(NUMCOD_CMBCOD, new_cmbcod); 
         
            /*MR1210 Start Ira Laksmono Dec 17,01*/
            /*we need to call generate ucc128 identifier to get the valid ucc128,instead of
             *mocking the subucc.
             *old code:
	     * sprintf(valueList, "'%s', '%ld', '0011000000002%s', '%s'", new_wrkref, untcas, new_wrkref+3, new_cmbcod); 
             */
             sprintf(buffer,
                 "generate ucc128 identifier where ucclvl= 'S' and uccseq= 'uccnum' and uccman= '0053005' and sumflg= '1'"); 
             ret_status = srvInitiateCommand(buffer, &CmdRes);												 
             if (ret_status != eOK) { 
		sqlFreeResults(res);  
                srvFreeMemory(SRVRET_STRUCT, CmdRes); 
		return (srvSetupReturn(ret_status,""));
	     } 
	     res2 =  CmdRes->ReturnedData;
	     row2 = sqlGetRow(res2); 
	     strncpy(new_subucc, sqlGetString(res2, row2, "subucc"), UCCCOD_LEN); 
	     srvFreeMemory (SRVRET_STRUCT, CmdRes); 
             /*MR1210 End*/

	    sprintf(fieldList, "wrkref, pckqty, subucc, cmbcod"); 
    /* MOD 2 - MCF */ 
	    sprintf(valueList, "'%s', '%ld', '%s', '%s'", 
		    new_wrkref, untcas, new_subucc, new_cmbcod); 
    /* END MOD 2 */ 
	    /* 
	    if (strlen(pcksts)) 
	    { 
		strcat(fieldList, ", pcksts"); 
		sprintf(tmpstr, ", '%s'", pcksts); 
                MR 789, Ira Laksmono 10/24/01 
                 *if we pass in pcksts, pcksts is not get inserted 
                 *correctly into the pckwrk. The reason is that pcksts is not 
                 *in the valueList. 
		strcat(valueList, tmpstr); 
                MR 789 End, Ira Laksmono 10/24/01
	   misLogInfo(fieldList); 
	   misLogInfo(valueList); 
	    } */ 
            /* Making the pcksts a default of 'R' for Process split case pick works. MR 5433 */
     	    strcat(fieldList, ", pcksts"); 
	    sprintf(tmpstr, ", 'R'"); 
	    strcat(valueList, tmpstr);

            /* End MR 5433 */

	    ret_status = appGenerateTableEntry("pckwrk", 
					       res,  
					       row, 
					       fieldList, 
					       valueList); 
               /*MR1084 Ira Laksmono Dec 10, 01
                *Commented out since causing new wrkref to be publish more than one.
                *Then it also cause print label more than once.
		*srvResultsAdd(CurPtr, new_wrkref); 
                *MR 1084 End Dec 10, 01
                */
	    if (ret_status != eOK) 
	    { 
		sqlFreeResults(res); 
		srvFreeMemory(SRVRET_STRUCT, CurPtr); 
		return(srvSetupReturn(ret_status, "")); 
	    } 
 
    	sprintf(buffer, "select * from pckmov where cmbcod = '%s' ",  
			sqlGetString(res, row, "cmbcod")); 
     
   		ret_status = sqlExecStr(buffer, &movRes); 
    	if (ret_status != eOK) 
    		{ 
			sqlFreeResults(res); 
			sqlFreeResults(movRes); 
			return(srvSetupReturn(ret_status, "")); 
    		} 
 
    	movRow = sqlGetRow(movRes); 
		do  
		{ 
	     	strcpy(fieldList, "cmbcod"); 
	     	sprintf(valueList,"'%s'", new_cmbcod); 
		 	ret_status = appGenerateTableEntry("pckmov", 
					       movRes,  
					       movRow, 
					       fieldList, 
					       valueList); 
	     
	    	if (ret_status != eOK) 
	    	{ 
			sqlFreeResults(res); 
			srvFreeMemory(SRVRET_STRUCT, CurPtr); 
			return(srvSetupReturn(ret_status, "")); 
	    	} 
			movRow = sqlGetNextRow(movRow); 
 
		} while (movRow); 
 
		sqlFreeResults(movRes); 
		srvResultsAdd(CurPtr, new_wrkref); 
	    untqty -= untcas; 
	} 
    }  
    /*MR1210 Start Ira Laksmono Dec 17, 01 */ 
	memset(new_subucc1, 0, sizeof(new_subucc1));
    sprintf(buffer, 
              "generate ucc128 identifier where ucclvl= 'S' and uccseq= 'uccnum' and uccman= '0053005' and sumflg= '1'");  
    ret_status = srvInitiateCommand(buffer, &CmdRes);												  
    if (ret_status != eOK) {  
	sqlFreeResults(res);   
        srvFreeMemory(SRVRET_STRUCT, CmdRes);  
        return (srvSetupReturn(ret_status,"")); 
     }  
     res2 =  CmdRes->ReturnedData; 
     row2 = sqlGetRow(res2);  
     strncpy(new_subucc1, sqlGetString(res2, row2, "subucc"), UCCCOD_LEN);  
     srvFreeMemory (SRVRET_STRUCT, CmdRes);  
     /*MR1210 End*/
    sprintf(buffer, 
	    "update pckwrk " 
	    "   set pckqty = '%d', subucc = '%s' " 
	    " where wrkref = '%s' ", 
	    untcas, new_subucc1, wrkref); 
    ret_status = sqlExecStr(buffer, NULL); 
    if (ret_status != eOK) 
    { 
	sqlFreeResults(res); 
	srvFreeMemory(SRVRET_STRUCT, CurPtr); 
	return(srvSetupReturn(ret_status, "")); 
    } 
	srvResultsAdd(CurPtr, wrkref); 
 
    sqlFreeResults(res); 
    return(CurPtr); 
} 
       
