    static const char *rcsid = "$Id: varGetWaveLane.c,v 1.3 2001/12/03 20:27:07 lh51rp Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 2001
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 * DESCRIPTION  :  This functin gets a suitable wave lane based on the passed in
 *                 parameters 
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <applib.h>
#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include <vargendef.h>
#include <varcolwid.h>


LIBEXPORT 
RETURN_STRUCT *varGetWaveLane(char *carcod_i,
			      char *cstnum_i,
			      char *ship_id_i,
                              long *shp_wavcas_qty_i,
			      long *datdiff_i, 
			      char *wavnum_i,
			      char *preticket_flag_i,
			      char *small_parcel_flag_i,
			      char *sample_ord_flag_i,
			      long *pass_i)
{
    RETURN_STRUCT *CurPtr;
    RETURN_STRUCT *returnData=NULL;

    char  buffer[1500];
    char  carcod[CARCOD_LEN] = "" ;
    char  cstnum[CSTNUM_LEN] = "";
    char  carcod_s[CARCOD_LEN] = "" ;
    char  cstnum_s[CSTNUM_LEN] = "";
    char  ship_id[SHIP_ID_LEN];
    char  shpdte [LODCMD_DATE_LEN] = ""; 
    long  shp_wavcas_qty = 0;
    char  wavnum[WAVE_NUMBER_LEN] = ""; 
    long  max_cases_per_wave = 0;
    long  wave_lane = 0;
    long  ret_status = 0;
    long  fluid_check = 0;
    long  sum_wavqty = 0; 
    long  no_of_sids = 0; 
    long  fluid_load_flg = 0; 
    long  max_sid_per_wave = 0; 
    long  datdiff = 0; 
    long  flrlod_flg = 0;
    long  large_ship = 0; 
    long  found = BOOLEAN_FALSE;
    char  small_parcel_flag [FLAG_LEN] = NO_STR;
    char  preticket_flag [FLAG_LEN] = NO_STR;
    char  sample_ord_flag [FLAG_LEN] = NO_STR;
    long  next_wave_flg = BOOLEAN_FALSE;
    long  pass;

    mocaDataRes *res, *res1 , *res2;
    mocaDataRow *row , *case_row , *cur_row , *row1, *row2;
     
    CurPtr = NULL;

    if (carcod_i)
	misTrimcpy(carcod, carcod_i, CARCOD_LEN ) ; 
    if (cstnum_i)
	misTrimcpy(cstnum, cstnum_i, CSTNUM_LEN ) ;

    if (!ship_id_i )
    	return (APPMissingArg ("ship_id"));
    else
    	misTrimcpy(ship_id, ship_id_i, SHIP_ID_LEN) ; 
    
    if (shp_wavcas_qty_i == NULL)
	return (APPMissingArg ("shp_wavcas_qty"));
    else
	shp_wavcas_qty = *shp_wavcas_qty_i ;

    if (pass_i == NULL)
	return (APPMissingArg ("pass"));
    else
	pass = *pass_i;

    if (datdiff_i != NULL)
	datdiff = *datdiff_i;

    if (!wavnum_i) 
	return (APPMissingArg ("wavnum"));
    else
	misTrimcpy (wavnum, wavnum_i, WAVE_NUMBER_LEN);

    if (preticket_flag_i)
	misTrimcpy(preticket_flag, preticket_flag_i, FLAG_LEN); 

    if (small_parcel_flag_i)
	misTrimcpy(small_parcel_flag, small_parcel_flag_i, FLAG_LEN); 

    if (sample_ord_flag_i)
	misTrimcpy(sample_ord_flag, sample_ord_flag_i, FLAG_LEN); 

    /* If the wave plan date is the same as ship date, we
       should check the customer to see if the customer is
       fluid loaded */

    if (datdiff == 0)
    {
	/* The customer passed into the function is the btcust. 
	   We need to check the route to customer also to get 
	   the floor load flag. The order is  rtcust -> btcust
	*/
	sprintf ( buffer , 
		  " Select    distinct  " 
		  "           decode (c2.vc_flrlod_flg, NULL,  "
		  "                   nvl (c1.vc_flrlod_flg, 0) ,nvl (c2.vc_flrlod_flg, 0)) vc_flrlod_flg "
		  "   From    cstmst c1, cstmst c2, ord o, shipment_line sl  " 
		  "  Where    sl.ship_id = '%s'   "
		  "    And    sl.ordnum = o.ordnum  "
		  "    And    sl.client_id = o.client_id  "
		  "    And    o.btcust = c1.cstnum  "
		  "    And    o.client_id = c1.client_id  "
		  "    And    o.rtcust = c2.cstnum  (+)   "
		  "    And    o.client_id = c2.client_id (+)  ",
		  ship_id
		  ) ;

	ret_status = sqlExecStr(buffer, &res) ;

	if (ret_status !=eOK && ret_status != eDB_NO_ROWS_AFFECTED) 
	{
	    sqlFreeResults(res);
	    return (srvSetupReturn(ret_status,"")) ;
	}
	
	if (ret_status == eOK) 
	{
	    row = sqlGetRow(res);
	    flrlod_flg = sqlGetLong(res,row,"vc_flrlod_flg");
	    sqlFreeResults(res);
	}
    
    }

    /* We shall first try the best match based on the carrier and customer. */
   sprintf ( buffer , 
          " Select	vd.vc_carcod ,"
    	  "		decode(vd.vc_carcod,'%s', 2, 1) ,"
	  "		vd.vc_cstnum ,"
	  "	        decode(vd.vc_cstnum,'%s', 2, 1) ,"
	  "         vh.vc_wave_lane vc_wave_lane, " 
	  "		vh.vc_fluid_load_flg ," 
	  "		vh.vc_max_cases_per_wave ,"
	  "		vh.vc_max_sid_per_wave ,"
	  "	        nvl(round(sum(sl.vc_wavcas_qty)),0)  sum_wavqty "
	  "   From  var_wavpar_hdr vh ,"
	  "         var_wavpar_dtl vd,"
	  "		shipment_line sl  " 
	  "	 Where(    (vd.vc_carcod = '%s' and vd.vc_cstnum = '%s')  " 
	  "         or (vd.vc_carcod = '%s' and vd.vc_cstnum = '%s')  "
	  "         or (vd.vc_carcod = '%s' and vd.vc_cstnum = '%s') )"
	  "	   And  vh.vc_wave_lane  = sl.vc_wave_lane(+) " 
	  "    And  vh.vc_wave_lane = vd.vc_wave_lane "
	  "    And  sl.schbat (+) = '%s'   " 
	  "    And  vh.vc_non_wave_flg = 0 "
	  "  Group By	"
	  "		vd.vc_carcod, "
	  "		vd.vc_cstnum, "
	  "		vh.vc_fluid_load_flg, "
	  "         vh.vc_wave_lane, " 
	  "         vh.vc_max_cases_per_wave, " 
	  "         vh.vc_max_sid_per_wave " 
	  "Order By decode (vd.vc_carcod, '%s', 2, 1), "
	  "         decode (vd.vc_cstnum, '%s', 2, 1), "
	  "         nvl (round(sum(sl.vc_wavcas_qty)), 0)  ",
	  DEFAULT_NOT_NULL_STRING, 
	  DEFAULT_NOT_NULL_STRING , 
	  carcod, 
	  cstnum,
	  carcod,
	  DEFAULT_NOT_NULL_STRING, 
	  DEFAULT_NOT_NULL_STRING, 
	  cstnum, 
	  wavnum,
	  DEFAULT_NOT_NULL_STRING, 
	  DEFAULT_NOT_NULL_STRING ) ;
	ret_status = sqlExecStr(buffer, &res) ;
    if (ret_status !=eOK && ret_status != eDB_NO_ROWS_AFFECTED ) 
    {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }
    /* For the I pass, if we dont find by carrier/customer, we exit out */
    if (ret_status == eDB_NO_ROWS_AFFECTED) 
    {
	if (pass == 1)
	{
	    CurPtr = srvResultsInit(eOK, "vc_wave_lane",COMTYP_INT, sizeof(long), NULL);
	    /* Return 0 if no lane is found */
	    srvResultsAdd(CurPtr, 0);
	    sqlFreeResults (res);
	    return ( CurPtr );
	}
	found = BOOLEAN_FALSE;
    }
    else
    {
	found = BOOLEAN_TRUE;
    }


    /* We found no lanes based on the carrier/customer. Check for large shipment lanes  */
    if (ret_status == eDB_NO_ROWS_AFFECTED)  
    { 
	sprintf (buffer , 
		 " Select  to_number(rtstr1)  large_ship  " 
		 "   From  poldat " 
		 "  Where  polcod = '%s'  " 
		 "         AND polvar = '%s' " 
		 "	   AND polval = '%s' " 
		 "	   AND rtstr1 is not NULL " , 
		 POLCOD_PLAN_WAVE  , 
		 POLVAR_MISC , 
		 POLVAL_LARGE_SHIPMENT  )  ; 

	ret_status = sqlExecStr(buffer, &res) ;

	if (ret_status !=eOK) 
	{
	    sqlFreeResults(res);
	    return (srvSetupReturn(ret_status,"")) ;
	}

	case_row   = sqlGetRow(res);
	large_ship =  sqlGetLong(res,case_row,"large_ship");
	sqlFreeResults(res);

	if ( shp_wavcas_qty >=  large_ship ) 
	{ 
	    sprintf ( buffer, 
		     "  Select	vd.vc_carcod ,"
	             "	        NULL,	"
		     "	        vd.vc_cstnum ,"
		     "	        NULL ,"
		     "          vh.vc_wave_lane vc_wave_lane, " 
		     "		vh.vc_fluid_load_flg ," 
		     "		vh.vc_max_cases_per_wave ,"
		     "		vh.vc_max_sid_per_wave ,"
		     "	        nvl(round(sum(sl.vc_wavcas_qty)),0) sum_wavqty "
	             "  From    var_wavpar_hdr vh,  "
		     "          var_wavpar_dtl vd,  "
		     "	        shipment_line  sl" 
		     "  Where   vh.vc_max_cases_per_wave >= %ld " 
		     "          AND vh.vc_wave_lane = sl.vc_wave_lane(+)"
		     "          AND vh.vc_wave_lane = vd.vc_wave_lane(+)"
		     "          AND sl.schbat(+) = '%s'   "
		     "          AND vh.vc_non_wave_flg = 0  "
		     "          AND not exists              "
		     "          (select 1 from var_wavpar_dtl vd2    "
		     "           where vd2.vc_wave_lane = vh.vc_wave_lane)   "
		     /* Added the fluid load flag bcoz, we dont want to allow these into
		        dedicated diverts -- Dt: 10/05/01 -- RP */
		     "          AND vh.vc_fluid_load_flg = 0 "
		     "  Group   By  " 
                     "          vh.vc_wave_lane, "
                     "          vh.vc_fluid_load_flg, "
		     "          vd.vc_carcod, vd.vc_cstnum,  "
		     "          vh.vc_max_cases_per_wave, vh.vc_max_sid_per_wave "
		     "  Order   By   "
		     "          sum_wavqty  ",
                     large_ship, 
		     wavnum);
		    


	    ret_status = sqlExecStr (buffer, &res);

	    if (ret_status !=eOK && ret_status != eDB_NO_ROWS_AFFECTED) 
	    {
		sqlFreeResults(res);
		return (srvSetupReturn(ret_status,"")) ;
	    }
	    if (ret_status != eDB_NO_ROWS_AFFECTED)
	    {
		found = BOOLEAN_TRUE;
	    }
	}
    } /* if pass = 2 */
    
    /* At this point lets do something if we have some data found */
    for (cur_row = sqlGetRow(res); cur_row && found == BOOLEAN_TRUE;cur_row = sqlGetNextRow(cur_row))
    {
	misTrimcpy (carcod_s, sqlGetString (res, cur_row, "vc_carcod"), CARCOD_LEN);
	misTrimcpy (cstnum_s, sqlGetString (res, cur_row, "vc_cstnum" ), CSTNUM_LEN);
	wave_lane = sqlGetLong (res, cur_row, "vc_wave_lane");
	fluid_load_flg =  sqlGetLong(res, cur_row, "vc_fluid_load_flg");
	sum_wavqty =  sqlGetLong(res, cur_row, "sum_wavqty");
	max_cases_per_wave =  sqlGetLong(res, cur_row, "vc_max_cases_per_wave") ; 
	max_sid_per_wave   =  sqlGetLong(res, cur_row, "vc_max_sid_per_wave") ;

        sprintf(buffer , 
	        " Select  count (distinct ship_id)  no_of_sids " 
	        "   From  shipment_line  "	
	        "  Where  schbat = '%s' "
		"    And  vc_wave_lane = %ld ",
	        wavnum, 
		wave_lane );

	ret_status	= sqlExecStr(buffer, &res2);
	if (ret_status !=eOK) 
    	{
	    sqlFreeResults(res);
	    return (srvSetupReturn(ret_status,"")) ;
	}

	row		= sqlGetRow(res2);
	no_of_sids	= sqlGetLong(res2,row,"no_of_sids") ;

	sqlFreeResults (res2);

	/* If we have a preticket order or a sample order, we cannot send it down the
	   fluid lanes */
/* MR1042 adding debug msg to see why assigned fluid lane is being chosen on 2nd pass*/
	misLogInfo(" MR1042dbg1: wave_lane=%ld, fluid_load_flg=%ld ",
		wave_lane , fluid_load_flg);
	misLogInfo(" MR1042dbg2: shp_wavcas_qty=%ld, sum_wavqty=%ld ",
		shp_wavcas_qty, sum_wavqty);
	misLogInfo(" MR1042dbg3: max_cases_per_wave=%ld, max_sid_per_wave=%ld ",
		max_cases_per_wave, max_sid_per_wave);
	misLogInfo(" MR1042dbg4: no_of_sids=%ld, preticket_flag=%s, sample_ord_flag=%s ",
		no_of_sids, preticket_flag, sample_ord_flag);
	misLogInfo(" MR1042dbg5: small_parcel_flag=%s, flrlod_flg=%ld ",
		small_parcel_flag, flrlod_flg);
/* MR1042 end of debug for checking logic */
	
	if (((strncmp(preticket_flag, YES_STR, FLAG_LEN) == 0)
	     || (strncmp(sample_ord_flag, YES_STR, FLAG_LEN) == 0)) 
	     && (fluid_load_flg == 1))
	{
	    continue;
	}

        /* If we have a non-small parcel carrier going down a fluid load lane and
	   customer does not accept floor loading, we back out. 
	*/
 
	if ((strncmp(small_parcel_flag, YES_STR, FLAG_LEN) != 0) 
	     && fluid_load_flg == 1 && flrlod_flg == 0)
	{
	    continue;
	}

	/* MR 1042 -- If we have a dedicated lane and there is already 
	   a 'dedicated' shipment in that lane, lets ignore the lane 
	   Dt: 11/17/01 -- RP. We ignore dedicated lanes. So, we
	   wont encounter this. 
        */

	/* if ((no_of_sids > 0) && (pass > 1)
	    && ((misTrimLen (carcod_s, CARCOD_LEN) > 0)
	        || (misTrimLen (cstnum_s, CSTNUM_LEN) > 0)))
	{
	    memset (buffer, 0, sizeof(buffer));
	    sprintf (buffer, 
		     "Select 1  "
		     "From   shipment_line sl2, shipment s2, "
		     "       ord o2, var_wavpar_dtl vd2      "
		     "Where  s2.ship_id = sl2.ship_id     "
		     "And    sl2.ordnum = o2.ordnum        "
		     "And    sl2.client_id = o2.client_id  "
		     "And    sl2.vc_wave_lane = vd2.vc_wave_lane  "
		     "And    sl2.schbat = '%s' "
		     "And    ((s2.carcod = '%s' "
		     "         and vd2.vc_cstnum = '%s'  "
		     "         and s2.carcod != '%s')    "
		     "        or            "
		     "        (o2.btcust = '%s'  "
		     "         and vd2.vc_carcod = '%s'   "
		     "         and o2.btcust != '%s')  "
		     "        or            "
		     "        (s2.carcod = '%s' "
		     "         and o2.btcust = '%s'  "
		     "         and (s2.carcod != '%s'  "
		     "              or o2.btcust != '%s' )))  ",
		     wavnum,
		     carcod_s, DEFAULT_NOT_NULL_STRING, carcod,
		     cstnum_s, DEFAULT_NOT_NULL_STRING, cstnum,
		     carcod_s, cstnum_s, carcod, cstnum
		     );
	    ret_status = sqlExecStr (buffer, &res2);
	    switch (ret_status)
	    {
	    case eOK:
		This means there is another shipment in the lane. Ignore lane 
		sqlFreeResults (res2);
		continue;
		break;

	    case eDB_NO_ROWS_AFFECTED:
		break;

	    default:
		sqlFreeResults (res);
		return (srvSetupReturn (ret_status, ""));
		break;
	    }
	} */
	/* End MR 1042 */
	

	/* For a Non large shipment */
	if (large_ship == 0)
	{
	    if ((sum_wavqty + shp_wavcas_qty <= max_cases_per_wave ) 
	    && (max_sid_per_wave > no_of_sids) 
	    && (!CurPtr))
	    {
		CurPtr = srvResultsInit(eOK, "vc_wave_lane",COMTYP_INT, sizeof(long), NULL);
		
		srvResultsAdd(CurPtr, sqlGetLong(res, cur_row, "vc_wave_lane"));
	        sqlFreeResults (res);
		return ( CurPtr ) ;
	    }
	    /* As long as the shipment wave case qty is less than the max cases
	       per wave for the lane, we can send it across another wave. This is
	       to make sure that we can actually send the shipment down 1 wave.*/
	    if (shp_wavcas_qty <= max_cases_per_wave)
		next_wave_flg = BOOLEAN_TRUE;
	}

	if (shp_wavcas_qty >= large_ship && large_ship > 0)
	{
	    if ((max_sid_per_wave > no_of_sids ) 
		&& (!CurPtr))
	    {
		CurPtr = srvResultsInit(eOK, "vc_wave_lane",COMTYP_INT, sizeof(long) ,NULL);
		
		srvResultsAdd(CurPtr, sqlGetLong(res, cur_row, "vc_wave_lane"));
		sqlFreeResults (res);
		return ( CurPtr ) ;
	    }
	    next_wave_flg = BOOLEAN_TRUE;
	}
	
    } /* for */
    
    /* If this is the first pass and I dont have a lane, return */
    if ((pass == 1) && (!CurPtr))
    {
	CurPtr = srvResultsInit(eOK, "vc_wave_lane",COMTYP_INT, sizeof(long), NULL);
	/* Return 0 if no lane is found */
	srvResultsAdd(CurPtr, 0);
	sqlFreeResults (res);
	return ( CurPtr ) ;
    }

    /* Try to get a lane if we dont have one already and if we can send it
       across the same wave. */
    if ((!CurPtr) && (next_wave_flg == BOOLEAN_FALSE))
    {  
	/*   sqlFreeResults(res);    */
	sprintf ( buffer,
	    " Select vd.vc_carcod , "
	    "        vd.vc_cstnum , "
	    "        vh.vc_fluid_load_flg  vc_fluid_load_flg, "
	    "	     vh.vc_wave_lane vc_wave_lane,  "
	    "        vh.vc_max_sid_per_wave,  "
	    "        vh.vc_max_cases_per_wave ,"
	    "	     nvl(round(sum(sl.vc_wavcas_qty)),0)  sum_wavqty "
	    "  From  var_wavpar_hdr vh, "
	    "        var_wavpar_dtl vd, "
	    "        shipment_line sl   "
	    " Where  vh.vc_wave_lane = sl.vc_wave_lane(+)" 
	    "   And  vh.vc_wave_lane = vd.vc_wave_lane (+)  "
	    "        AND sl.schbat (+) = '%s'   "
	    "        AND  vh.vc_non_wave_flg = 0  "
	    /* Added the fluid load flag bcoz, we dont want to allow these into
               dedicated diverts -- Dt: 10/05/01 -- RP */
            "        AND vh.vc_fluid_load_flg = 0 "  
	    "        AND not exists               "
	    "        (select 1 from var_wavpar_dtl vd2   "
	    "         where vd2.vc_wave_lane = vh.vc_wave_lane) "
	    " Group By  vd.vc_carcod , "
    	    "	     vd.vc_cstnum , "
	    "	     vh.vc_wave_lane, "
	    "        vh.vc_fluid_load_flg,  "
	    "	     vh.vc_max_sid_per_wave, "
	    "	     vh.vc_max_cases_per_wave ",
	    wavnum);
	    
	 /* If it is a small Preticket order/sample order shipment, we try to send 
	    it down the same lanes. So dont order it by Lane Qty. The order by
	    lists the blank lanes in the top and the Non-blank lanes at
	    the bottom.*/

	/*if ((strncmp(sample_ord_flag, YES_STR, FLAG_LEN) == 0)
	    || 
	    ((strncmp(small_parcel_flag, YES_STR, FLAG_LEN) == 0)
	      && (strncmp(preticket_flag, YES_STR, FLAG_LEN) == 0)))
	{
	    strcat (buffer, 
		    "  Order By   ");
	    strcat (buffer, 
		    "        vh.vc_max_sid_per_wave desc, "
		    "        vh.vc_max_cases_per_wave desc, "
		    "        vd.vc_carcod desc , "
		    "        vd.vc_cstnum desc , "
		    "        vh.vc_wave_lane ");               

	}
	else
	{
	}
	*/
	strcat (buffer, 
	    "  Order By   ");
	strcat (buffer, 
	    "        sum_wavqty,   "
	    "        vd.vc_carcod desc , "
	    "        vd.vc_cstnum desc , "
	    "        vh.vc_wave_lane ");
	
	sqlFreeResults(res);
	ret_status = sqlExecStr(buffer, &res);
	if (ret_status !=eOK) 
	{
	    sqlFreeResults(res);
	    return (srvSetupReturn(ret_status,"")) ;
	}

	next_wave_flg = BOOLEAN_FALSE;

	for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	{
	    misTrimcpy (carcod_s, sqlGetString (res, row, "vc_carcod"), CARCOD_LEN);
	    misTrimcpy (cstnum_s, sqlGetString (res, row, "vc_cstnum" ), CSTNUM_LEN);
	    wave_lane = sqlGetLong (res, row, "vc_wave_lane");
            sum_wavqty         = sqlGetLong(res, row, "sum_wavqty");
	    max_cases_per_wave = sqlGetLong(res, row, "vc_max_cases_per_wave") ; 
	    max_sid_per_wave   = sqlGetLong(res, row, "vc_max_sid_per_wave") ;
	    fluid_load_flg =  sqlGetLong(res, row, "vc_fluid_load_flg");

	    sprintf(buffer , 
	        " Select  count (distinct ship_id)  no_of_sids " 
	        "   From  shipment_line  "	
	        "  Where  schbat = '%s' "
		"    And  vc_wave_lane = %ld ",
	        wavnum, 
		wave_lane );

	    ret_status	= sqlExecStr(buffer, &res2);
	    if (ret_status !=eOK) 
    	    {
		sqlFreeResults(res);
		return (srvSetupReturn(ret_status,"")) ;
	    }

	    row2		= sqlGetRow(res2);
	    no_of_sids	= sqlGetLong(res2, row2, "no_of_sids") ;
	    sqlFreeResults (res2);

/* MR1042 adding debug msg to see why assigned fluid lane is being chosen on 2nd pass*/
	misLogInfo(" MR1042dbgM1: wave_lane=%ld, fluid_load_flg=%ld ",
		wave_lane , fluid_load_flg);
	misLogInfo(" MR1042dbgM2: shp_wavcas_qty=%ld, sum_wavqty=%ld ",
		shp_wavcas_qty, sum_wavqty);
	misLogInfo(" MR1042dbgM3: max_cases_per_wave=%ld, max_sid_per_wave=%ld ",
		max_cases_per_wave, max_sid_per_wave);
	misLogInfo(" MR1042dbgM4: no_of_sids=%ld, preticket_flag=%s, sample_ord_flag=%s ",
		no_of_sids, preticket_flag, sample_ord_flag);
	misLogInfo(" MR1042dbgM5: small_parcel_flag=%s, flrlod_flg=%ld ",
		small_parcel_flag, flrlod_flg);
/* MR1042 end of debug for checking logic */

	    /* Sample or preticketed orders cannot be fluid loaded */
            if (((strncmp(preticket_flag, YES_STR, FLAG_LEN) == 0)
	     || (strncmp(sample_ord_flag, YES_STR, FLAG_LEN) == 0)) 
	     && (fluid_load_flg == 1))
	    {
		continue;
	    }

	    /* We will not send Non-Floor load customers down a
	       fluid load lane */

	    if (flrlod_flg == 0 && fluid_load_flg == 1)
	    {
		continue;
	    }

	    /* MR 1042 -- If we have a dedicated lane and there is already 
	       a 'dedicated' shipment in that lane, lets ignore the lane 
	    */

	    /* if ((no_of_sids > 0) && (pass > 1)
	        && ((misTrimLen (carcod_s, CARCOD_LEN) > 0)
	             || (misTrimLen (cstnum_s, CSTNUM_LEN) > 0))
	        && ((strncmp(sample_ord_flag, YES_STR, FLAG_LEN) == 0)
	             ||((strncmp(preticket_flag, YES_STR, FLAG_LEN) == 0)
		        && (strncmp(small_parcel_flag, YES_STR, FLAG_LEN) == 0))))
	    {
		memset (buffer, 0, sizeof(buffer));
		sprintf (buffer, 
			 "Select 1  "
			 "From   shipment_line sl2, shipment s2, "
			 "       ord o2, var_wavpar_dtl vd2      "
			 "Where  s2.ship_id = sl2.ship_id     "
			 "And    sl2.ordnum = o2.ordnum        "
			 "And    sl2.client_id = o2.client_id  "
			 "And    sl2.vc_wave_lane = vd2.vc_wave_lane  "
			 "And    sl2.schbat = '%s'  "
			 "And    ((s2.carcod = '%s' "
			 "         and vd2.vc_cstnum = '%s'  "
			 "         and s2.carcod != '%s')    "
			 "        or            "
			 "        (o2.btcust = '%s'  "
			 "         and vd2.vc_carcod = '%s'   "
			 "         and o2.btcust != '%s')  "
			 "        or            "
			 "        (s2.carcod = '%s' "
			 "         and o2.btcust = '%s'  "
			 "         and (s2.carcod != '%s'  "
			 "              or o2.btcust != '%s' )))  ",
			 wavnum,
			 carcod_s, DEFAULT_NOT_NULL_STRING, carcod,
			 cstnum_s, DEFAULT_NOT_NULL_STRING, cstnum,
			 carcod_s, cstnum_s, carcod, cstnum
			 );
		ret_status = sqlExecStr (buffer, &res2);
		switch (ret_status)
		{
		case eOK:
		    This means there is another shipment in the lane. Ignore lane
		    sqlFreeResults (res2);
		    continue;
		    break;

		case eDB_NO_ROWS_AFFECTED:
		    break;

		default:
		    sqlFreeResults (res);
		    return (srvSetupReturn (ret_status, ""));
		    break;
		}
	    }
	    */
	    if ((sum_wavqty + shp_wavcas_qty > max_cases_per_wave ) 
                  && (shp_wavcas_qty <= max_cases_per_wave))
	    {
                 next_wave_flg = BOOLEAN_TRUE;
	    }
	   

            if ((sum_wavqty + shp_wavcas_qty <= max_cases_per_wave ) 
         	   &&  (max_sid_per_wave > no_of_sids ))
	    {
	        CurPtr = srvResultsInit(eOK, "vc_wave_lane",COMTYP_INT, sizeof(long), NULL);
			
	        srvResultsAdd(CurPtr, sqlGetLong(res, row, "vc_wave_lane"));
	        sqlFreeResults(res);
	        return ( CurPtr ) ;
	    }
	} /* II for */
    }
    /* this should never happen . But if it does , we return blank */

    sqlFreeResults(res); 
    return (srvSetupReturn (eDB_NO_ROWS_AFFECTED, ""));
}
