/*#START**************************************************************
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *  Description: This processes deletes using Wave Parameter Maintenance. 
 *		 We do not check for waves that are planned already 
 *		 because a delete will affect only subsequent waves 
 *		 that will be planned.
 *  The following is added for MR1557 --
 *   a) upon lane delete, the corresponding slot locations are deleted 
 *   b) VAR policies for the lane are deleted 
 *#END*****************************************************************/
#include <moca_app.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include <varerr.h>

LIBEXPORT 
RETURN_STRUCT *varRemoveWaveParameter(long *vc_wave_lane_i)
{
    long ret_status;
    char buffer[1000];
    long vc_wave_lane = 0;
    mocaDataRes *res;
    mocaDataRow *row;
    long count_lane, count_inv;
    long lane_length;  			/* MR1557 length of lane name */
    char lane_name[STOLOC_LEN+1];
  
    if (!vc_wave_lane_i || *vc_wave_lane_i == 0) {
       return (srvSetupReturn(eINVALID_ARGS, ""));
    }
    vc_wave_lane = *vc_wave_lane_i;
	
    sprintf(buffer, "delete var_wavpar_hdr "	/* delete the header */
	    " where vc_wave_lane = '%ld'", vc_wave_lane);
    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status != eOK) {
	return (srvSetupReturn(ret_status, ""));
    }

    sprintf(buffer, "delete var_wavpar_dtl "	/* delete the detail */
	    " where vc_wave_lane = '%ld'", vc_wave_lane);
    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED) {
	return (srvSetupReturn(ret_status, ""));
    }

    /* if we have policies for this lane -- delete them*/
    sprintf(buffer, "select count(*) count_lane from poldat "
                " where polcod='VAR' and polvar='SHIP-STAGE-BUILD-LOAD' "
                " and polval like '%%'||lpad(%ld,3,'0') ", vc_wave_lane);
    ret_status = sqlExecStr(buffer,&res);
    if (ret_status != eOK) {
      sqlFreeResults(res);
      misLogInfo(" MR1557dbgDelete: prob counting policies...quit");
      return (srvSetupReturn(ret_status,"")) ;
    }
    row = sqlGetRow(res);
    count_lane = sqlGetLong(res, row, "count_lane");
    sqlFreeResults(res);

    if(count_lane > 0) {   /* delete the policies for the lane/slots */
      sprintf(buffer, "select min(polval) lane_id, "
                " length(rtrim(min(polval),'0123456789 ')) lane_len "
                " from poldat "
                " where polcod='VAR' and polvar='SHIP-STAGE-BUILD-LOAD' "
                " and polval like '%%'||lpad(%ld,3,'0') ", vc_wave_lane);
      ret_status = sqlExecStr(buffer,&res);
      row = sqlGetRow(res);
      lane_length=sqlGetLong(res,row,"lane_len");
      misTrimcpy(lane_name,sqlGetString(res,row,"lane_id"),lane_length);
      sqlFreeResults(res);

      sprintf(buffer, "delete from poldat "
                " where polcod='VAR' and polvar='SHIP-STAGE-BUILD-LOAD' "
                " and polval = '%s'||lpad(%ld,3,'0') ",
                lane_name, vc_wave_lane);
      ret_status = sqlExecStr(buffer,NULL);
      if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED) {
        misLogInfo(" MR1557dbgRemove: prob deleting policies");
      }
    }
    else {
      sprintf(lane_name,"LANE"); /* if no policies, we assume lane name */
      lane_length=4;
    }
    
    /* if we have inventory anywhere in this lane --  issue failure message */
    sprintf(buffer, "select count(*) count_inv from invlod, locmst "
                " where invlod.stoloc like '%s'||lpad(%ld,3,'0')||'%%' "
                " and invlod.stoloc = locmst.stoloc "
		" and arecod in ('FSTG','PALLET','OPALLET') ",
                lane_name,vc_wave_lane);
    ret_status = sqlExecStr(buffer,&res);
    if (ret_status != eOK) {
      sqlFreeResults(res);
      misLogInfo(" MR1557dbgDelete: prob counting inventory for lane...quit");
      return (srvSetupReturn(ret_status,"")) ;
    }
    row = sqlGetRow(res);
    count_inv = sqlGetLong(res, row, "count_inv");
    sqlFreeResults(res);

    if(count_inv!=0) 	/* error if inventory exists in lane*/ 
	return (srvSetupReturn(eVAR_LANE_HAS_INVENTORY, "")); 
    else{ 		/* delete lane if no inventory exists in it*/
      sprintf(buffer, "delete from locmst "
                " where stoloc like '%s'||lpad(%ld,3,'0')||'%%' "
		" and arecod in ('FSTG','PALLET','OPALLET') ",
                lane_name,vc_wave_lane);
      ret_status = sqlExecStr(buffer,NULL);
      if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED) {
	return (srvSetupReturn(ret_status, ""));
      }
      return (srvSetupReturn(eOK, ""));
    }
}
