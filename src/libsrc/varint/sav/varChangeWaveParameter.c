/*#START**************************************************************
 *  McHugh Software International
 *  Copyright 2001
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *  DESCRIPTION:  This function generates the syntax to update the 
 *		  var_wavpar_hdr table using the MCS Base component -
 *		  Process Table Action.
 *  For MR1557 when a lane that is added the policies and lane and
 *  slot locations are reset (varResetWaveLane)
 *#END****************************************************************/
#include <moca_app.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <applib.h>
#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

LIBEXPORT 
RETURN_STRUCT *varChangeWaveParameter(long *vc_wave_lane_i,
				      long *vc_max_sid_per_wave_i,
				      long *vc_max_cases_per_wave_i,
				      moca_bool_t *vc_fluid_load_flg_i,
				      moca_bool_t *vc_non_wave_flg_i)
{
    RETURN_STRUCT *CurPtr;
    char  tablename[100];
    char  tmpvar[1000];
    char  *wherelist;
    char  *updatelist;
    long  vc_wave_lane;
    long  vc_max_sid_per_wave;
    long  ret_status;
    mocaDataRes *res;
    mocaDataRow *row;
    long  fluid_lane = BOOLEAN_FALSE;	/*MR1557 need for varResetWaveLane*/

    CurPtr = NULL;
    if (!vc_wave_lane_i || *vc_wave_lane_i == 0) {
	return (APPMissingArg("vc_wave_lane"));
    }
    vc_wave_lane = *vc_wave_lane_i;

    sprintf(tmpvar, "select var_wavpar_hdr.* from var_wavpar_hdr "
	    " where var_wavpar_hdr.vc_wave_lane = '%ld'", vc_wave_lane);
    ret_status = sqlExecStr(tmpvar,&res);
    if(ret_status !=eOK) {
	sqlFreeResults(res);
	return(APPInvalidArg("?", "vc_wave_lane"));
    }
    row = sqlGetRow(res);
   
    strcpy(tablename, "var_wavpar_hdr");
    updatelist = wherelist = NULL;
    
    /* First, let's do the where clause ... */
    sprintf(tmpvar,"var_wavpar_hdr.vc_wave_lane = %ld ",vc_wave_lane);

    if(!misDynStrcat(&wherelist,tmpvar)) {
	sqlFreeResults(res);
	if (wherelist) free(wherelist);
	return(srvSetupReturn(eNO_MEMORY,""));
    }

    if(vc_max_sid_per_wave_i && 
      *vc_max_sid_per_wave_i 
	!= sqlGetLong(res,row,"vc_max_sid_per_wave")) {
      sprintf(tmpvar, 
	" vc_max_sid_per_wave = %ld,",*vc_max_sid_per_wave_i);
      if(!misDynStrcat(&updatelist,tmpvar)) {
	if (wherelist) free(wherelist);
	if (updatelist) free(updatelist);
	return(srvSetupReturn(eNO_MEMORY, ""));
      }
    }

    if(vc_max_cases_per_wave_i &&
      (sqlIsNull(res,row,"vc_max_cases_per_wave") ||
      *vc_max_cases_per_wave_i
	!=sqlGetLong(res,row,"vc_max_cases_per_wave"))){
      sprintf(tmpvar,
	" vc_max_cases_per_wave = %ld,",*vc_max_cases_per_wave_i);
      if(!misDynStrcat(&updatelist,tmpvar)) {
	if (wherelist) free(wherelist);
	if (updatelist) free(updatelist);
	return(srvSetupReturn(eNO_MEMORY, ""));
      }
    }
 
    fluid_lane=BOOLEAN_FALSE;	
    if(vc_fluid_load_flg_i && *vc_fluid_load_flg_i 
		!= sqlGetBoolean(res,row,"vc_fluid_load_flg")) {
	fluid_lane=*vc_fluid_load_flg_i;
	sprintf(tmpvar,
		" vc_fluid_load_flg = %ld ,",*vc_fluid_load_flg_i);
	if(!misDynStrcat(&updatelist,tmpvar)) {
	    if (wherelist) free(wherelist);
	    if (updatelist) free(updatelist);
	    return(srvSetupReturn(eNO_MEMORY, ""));
	}
	if(eOK != varResetWaveLane(vc_wave_lane,fluid_lane) ) {
	  misLogInfo(
	    "MR1557dbgChange: prob resetting lane policies and loc");
	} 
    }
    
    if(vc_non_wave_flg_i &&
      *vc_non_wave_flg_i != sqlGetBoolean(res,row,"vc_non_wave_flg")) {
	sprintf(tmpvar," vc_non_wave_flg = %ld ,",*vc_non_wave_flg_i);
	if(!misDynStrcat(&updatelist,tmpvar)) {
	    if (wherelist) free(wherelist);
	    if (updatelist) free(updatelist);
	    return(srvSetupReturn(eNO_MEMORY, ""));
	}
    }

    if (updatelist && strlen(updatelist)) 
	updatelist[strlen(updatelist)-1] = 0;
    
    sprintf(tmpvar,"%s",
	osGetVar(LESENV_USR_ID) ? osGetVar(LESENV_USR_ID):"");
    if(appBuildUpdateList("mod_usr_id",tmpvar,USR_ID_LEN,&updatelist)
	!= eOK ){
	if (updatelist) free(updatelist);
	if (wherelist) free(wherelist);
	return (srvSetupReturn(eNO_MEMORY, ""));
    }

    if(appBuildUpdateListDBKW("moddte","sysdate",8,&updatelist)
       != eOK ) {
	if (wherelist) free(wherelist);
	if (updatelist) free(updatelist);
	return(srvSetupReturn(eNO_MEMORY, ""));
    }

    /* Publish table, columns, and values for insert using */
    /* intProcessTableInsert (PROCESS TABLE INSERT) */
    CurPtr = srvResultsInit(eOK, "acttyp", COMTYP_CHAR, 1,
		"tblnam",COMTYP_CHAR,strlen(tablename),
		"updlst",COMTYP_CHAR,updatelist ? strlen(updatelist):0,
		"whrlst",COMTYP_CHAR,wherelist ? strlen(wherelist):0,NULL); 
    srvResultsAdd(CurPtr, ACTTYP_UPDATE,
		  tablename,
		  updatelist ? updatelist : "",
		  wherelist ? wherelist : "");
    if (updatelist) free(updatelist);
    if (wherelist) free(wherelist);

    /* Finally, don't forget to free our results set */
    sqlFreeResults(res);
    return (CurPtr);
}
