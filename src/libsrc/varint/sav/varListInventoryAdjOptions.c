static const char *rcsid = "$Id: varListInventoryAdjOptions.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999 
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <srvconst.h>
#include <dcserr.h>

/* #include <intlib.h> */

#include <vargendef.h>

#define DAYCOUNT 7
#define ABCCOUNT 3
#define DefaultDollarAmt "1000.00"
#define DefaultPercent	"10"

LIBEXPORT RETURN_STRUCT *varListInventoryAdjOptions(long *init_i)
{

	/* Return value meaning 
	 * InvAdjLim	-	Discrepancy limit in dollars or percent
	 * chkinvadjopt	-	checkbox to set dollar/percent option boxes
						percent = 1, dollar = 0
	 */

    long			ret_status;
    short			init;
    char			buffer[2000];
    long			InvAdjLim;
    long			chkinvadjopt;
    long			chkshowdlramt;
    long			returnMsg;

    mocaDataRes   *res;
    mocaDataRow   *row; 
    RETURN_STRUCT *CurPtr;

misTrc(T_FLOW, "TG - Begin program");

    misTrc(T_FLOW, "TG - checking init %d ", init_i);
    if (init_i != NULL)
    {
     	    misTrc(T_FLOW, "TG - *init = %ld ", *init_i);
	    /* See if we are initializing */
	    init = *init_i == 1;
    }
    else
           init = FALSE;

    misTrc(T_FLOW, "TG - init = %d", init);
    if (!init)
    {
	returnMsg = eOK;
        misTrc(T_FLOW, " tpg.  Getting Discrepancy_Type info (option boxes)");
	returnMsg = eOK;
        /* Get the DISCREPANCY-TYPE, is it in dollars or percent*/ 
        sprintf (buffer, "  select rtnum1 checked"
		         "    from poldat "
		         "   where polcod = '%s' "
		         "     and polvar = '%s' "
		         "     and polval = '%s' ",
		         POLCOD_CYCLE_COUNTING,
		         POLVAR_CYCCNT_INVENTORY_ADJ_OPTIONS,
		         POLVAL_CYCCNT_DISCREPANCY_TYPE);

        ret_status = sqlExecStr(buffer, &res);
 	
	misTrc(T_FLOW, "tpg.  ret_status for Discrepancy type: %l",ret_status);
        
	if (eOK != ret_status)
        {
		sqlFreeResults(res);
		return(srvSetupReturn(ret_status, ""));
        }

	row = sqlGetRow(res);
	chkinvadjopt = sqlGetLong(res, row, "checked");
	sqlFreeResults(res);

	misTrc(T_FLOW, "tpg. Getting the limit value"); 
        
	/* Get the limit (percent or dollars) */
        sprintf (buffer, "  select rtnum1 limit "
		         "   from poldat "
		         "   where polcod = '%s' "
		         "     and polvar = '%s' "
		         "     and polval = '%s' ",
		         POLCOD_CYCLE_COUNTING,
		         POLVAR_CYCCNT_INVENTORY_ADJ_OPTIONS,
		         POLVAL_CYCCNT_DISCREPANCY_LIMIT);

        ret_status = sqlExecStr(buffer, &res);
        if (eOK != ret_status)
        {
		sqlFreeResults(res);
		return(srvSetupReturn(ret_status, ""));
        }

	row = sqlGetRow(res);
	InvAdjLim = sqlGetLong(res, row, "limit");
	sqlFreeResults(res);

	/* Get the SHOW-DOLLAR-AMT Values */
/*        sprintf (buffer, "  select rtnum1 showdlrs "
		         "   from poldat "
		         "   where polcod = '%s' "
		         "     and polvar = '%s' "
		         "     and polval = '%s' ",
		         POLCOD_CYCLE_COUNTING,
		         POLVAR_CYCCNT_INVENTORY_ADJ_OPTIONS,
		         POLVAL_CYCCNT_SHOW_DOLLAR_AMT);

        ret_status = sqlExecStr(buffer, &res);
        if (eOK != ret_status)
        {
		sqlFreeResults(res);
		return(srvSetupReturn(ret_status, ""));
        }

	row = sqlGetRow(res);
	chkshowdlramt = sqlGetLong(res, row, "showdlrs");
	sqlFreeResults(res);
	misTrc(T_FLOW, "tpg.  chkshowdlramt = %d ",chkshowdlramt);
*/
	misTrc(T_FLOW, "tpg.  chkinvadjopt  = %d ",chkinvadjopt);
 	misTrc(T_FLOW, "tpg.  InvAdjLim     = %d ",InvAdjLim);

    }
    else
    {
        returnMsg = eDB_NO_ROWS_AFFECTED;
 	misTrc(T_FLOW, "TG - not initializing, do nothing");
    }
	CurPtr = NULL;

    /* Now setup the return structure and exit */
    CurPtr = srvResultsInit(returnMsg,
                            "InvAdjLim",	 COMTYP_INT, sizeof(long),
                            "chkinvadjopt",	 COMTYP_INT, sizeof(long),
                            NULL);

    if (!init)
       srvResultsAdd(CurPtr,
		     InvAdjLim,
		     chkinvadjopt);
		     
    misTrc(T_FLOW, "TG - we've set the results, now returning");
    return(CurPtr);

}


