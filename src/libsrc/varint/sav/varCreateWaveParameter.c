/*#START************************************************************
 *  McHugh Software International
 *  Copyright 2001
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 * DESCRIPTION:  This function generates the syntax to insert a
 *               record in the var_wavpar_hdr table using the MCS 
 *		 Base Component - Process Table Action.
 *  For MR1557 when a lane that is added the policies and lane and
 *  slot locations are reset (varResetWaveLane)
 *#END*************************************************************/
#include <moca_app.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <applib.h>
#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

LIBEXPORT 
RETURN_STRUCT *varCreateWaveParameter(long *vc_wave_lane_i,
				      long *vc_max_sid_per_wave_i,
				      long *vc_max_cases_per_wave_i,
				      moca_bool_t *vc_fluid_load_flg_i,
				      moca_bool_t *vc_non_wave_flg_i)
{
    RETURN_STRUCT *CurPtr;
    char  tablename[100];
    char  tmpvar[1000];
    char  *columnlist;
    char  *valuelist;
    long  vc_wave_lane;
    long  vc_max_sid_per_wave;
    long  vc_max_cases_per_wave;
    long  ret_status;
    mocaDataRes *res;
    mocaDataRow *row;
    long  fluid_lane = BOOLEAN_FALSE;	/* MR1557 need for varResetWaveLane*/

    CurPtr = NULL;
    if (!vc_wave_lane_i || *vc_wave_lane_i == 0) {
	return (APPMissingArg("vc_wave_lane"));
    }
    vc_wave_lane = *vc_wave_lane_i;
    
    if (!vc_max_sid_per_wave_i || *vc_max_sid_per_wave_i == 0) {
	return (APPMissingArg("vc_max_sid_per_wave"));
    }
    vc_max_sid_per_wave = *vc_max_sid_per_wave_i;
    
    if(!vc_max_cases_per_wave_i || *vc_max_cases_per_wave_i==0) {
	return (APPMissingArg("vc_max_cases_per_wave"));
    }
    vc_max_cases_per_wave = *vc_max_cases_per_wave_i;
    /* Build the insert list */
    
    strcpy(tablename, "var_wavpar_hdr");
    columnlist = valuelist = NULL;
    
    sprintf(tmpvar, "%ld",vc_wave_lane);
    if(appBuildInsertList("vc_wave_lane", tmpvar, 20,
		&columnlist,&valuelist) != eOK) {
       if (columnlist) free(columnlist);
       if (valuelist) free(valuelist);
       return (srvSetupReturn(eNO_MEMORY, ""));
    }
    
    sprintf(tmpvar, "%ld",vc_max_sid_per_wave);
    if(appBuildInsertList("vc_max_sid_per_wave", tmpvar, 20,
		&columnlist,&valuelist) != eOK) {
       if (columnlist) free(columnlist);
       if (valuelist) free(valuelist);
       return (srvSetupReturn(eNO_MEMORY, ""));
    }

    sprintf(tmpvar, "%ld", *vc_max_cases_per_wave_i);
    if(appBuildInsertList("vc_max_cases_per_wave", tmpvar, 20,
		&columnlist,&valuelist) != eOK) {
        if (columnlist) free(columnlist);
        if (valuelist) free(valuelist);
        return (srvSetupReturn(eNO_MEMORY, ""));
    }
    
    if (!vc_fluid_load_flg_i || *vc_fluid_load_flg_i == 0) {
	fluid_lane=BOOLEAN_FALSE; 	/* must know if fluid for locmst*/
    }
    else {
    	fluid_lane=*vc_fluid_load_flg_i;
    }
    sprintf(tmpvar, "%ld", 
	vc_fluid_load_flg_i ? *vc_fluid_load_flg_i : BOOLEAN_FALSE);
    if(appBuildInsertList("vc_fluid_load_flg", tmpvar, FLAG_LEN,
		&columnlist,&valuelist) != eOK) {
       if (columnlist) free(columnlist);
       if (valuelist) free(valuelist);
       return (srvSetupReturn(eNO_MEMORY, ""));
    }

    sprintf(tmpvar, "%ld", 
	vc_non_wave_flg_i ? *vc_non_wave_flg_i : BOOLEAN_FALSE);
    if(appBuildInsertList("vc_non_wave_flg", tmpvar, FLAG_LEN,
		&columnlist,&valuelist) != eOK) {
       if (columnlist) free(columnlist);
       if (valuelist) free(valuelist);
       return (srvSetupReturn(eNO_MEMORY, ""));
    }

    if (appBuildInsertList("mod_usr_id", 
		osGetVar(LESENV_USR_ID) ? osGetVar(LESENV_USR_ID):"",
		0, &columnlist, &valuelist) != eOK ) {
       if (columnlist) free(columnlist);
       if (valuelist) free(valuelist);
       return (srvSetupReturn(eNO_MEMORY, ""));
    }
   
    if (appBuildInsertListDBKW("moddte","sysdate",8,
		&columnlist,&valuelist) != eOK) {
       if (columnlist) free(columnlist);
       if (valuelist) free(valuelist);
       return (srvSetupReturn(eNO_MEMORY, ""));
    }
 
    fluid_lane=fluid_lane;
    if(eOK != varResetWaveLane(vc_wave_lane,fluid_lane) ) {
      misLogInfo(" MR1557dbgCreate: prob resetting lane policies and loc");
    }
    
    /* Publish out the table, columns, and values for insert by */
    CurPtr = srvResultsInit(eOK, "acttyp", COMTYP_CHAR, 1,
		"tblnam", COMTYP_CHAR, strlen(tablename),
		"collst", COMTYP_CHAR, strlen(columnlist),
		"vallst", COMTYP_CHAR, strlen(valuelist), NULL);
    srvResultsAdd(CurPtr, ACTTYP_INSERT,
		  tablename,
		  columnlist,
		  valuelist);
    free(columnlist);
    free(valuelist);
    return (CurPtr);
}
