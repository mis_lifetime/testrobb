static char     rcsid[] = "$Id: oraProduceOracleReport.c,v 1.4 2001/10/05 14:19:05 verdeyen Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  $Source: /cvs/les/sal/src/libsrc/salora/oraProduceOracleReport.c,v $
 *  $Revision: 1.4 $
 *
 *  Application:
 *  Created:
 *  $Author: verdeyen $
 *
 *
 *#END************************************************************************/

#include <moca.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include <mislib.h>
#include <oslib.h>
#include <sqllib.h>
#include <srvlib.h>

#include <applib.h>
#include <salerr.h>
#include <salcolwid.h>
#include <salgendef.h>

#define DIVIDER "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

#ifdef UNIX
#define SQLPLUS_CMD "sqlplus"
#else
#define SQLPLUS_CMD "sqlplus"
#endif

#define RPT_INITIATE 1
#define RPT_SQLPLUS  2
#define RPT_SQR      3

static void     sPrintResultsFp(RETURN_STRUCT * CurPtr, FILE * fp);
static void     sPrintHeaderFp(RETURN_STRUCT * CurPtr, FILE * fp);
static void     sSubVar(char *command, char *sub, char *parm);
static int      sGetCleanCommand(FILE * fp, char *command, long cmdsize);

#define COMMENT_DELIMITER "/*"	/* comment */
#define SQLCOM_DELIMITER "--"
#define START_OF_VARIABLE "#"
#define MAX_LINE 100

LIBEXPORT 
RETURN_STRUCT *varProduceOracleReport(char *ReportName, char *OutputFile, 
		                      char *ReportTypeText,
		                      char *pagesize_i, char *linesize_i, 
				      char *newpage_i,
		       char *parm1, char *parm2, char *parm3, char *parm4,
		       char *parm5, char *parm6, char *parm7, char *parm8,
		       char *parm9, char *parm10, char *parm11, char *parm12,
		       char *parm13, char *parm14, char *parm15, char *parm16)
{
    FILE           *sqlplus;
    FILE           *fp;
    time_t          timep;
    struct tm      *tp;

    RETURN_STRUCT  *CurPtr;
    long            ret_status;

    char            command[2000];
    char            tempstr[100];
    char            TimeString[40];
    char            sqlfile[512];
    char            outfile[512];
    char            reportDir[512];
    char            varbuf[1024];
    int             rpttyp;
    int             outfil;
    char           *user, *pass, *conn;

    if (!ReportName || strlen(ReportName) == 0)
    {
	return APPMissingArg("rptnam");
    }

    if (strchr(ReportName, PATH_SEPARATOR) == NULL)
    {
	sprintf(sqlfile, "%s%c%s",
	        misExpandVars(varbuf, LES_ORACLE_REPORTS, sizeof(varbuf), NULL), 
		PATH_SEPARATOR, ReportName);
	if (strstr(ReportName, ".") == NULL)
	    strcat(sqlfile, ".sql");
    }
    else
    {
	strcpy(sqlfile, ReportName);
    }

    misTrc(T_FLOW, "Producing report for %s", sqlfile);

    rpttyp = RPT_SQLPLUS;
    if (ReportTypeText && strlen(ReportTypeText))
    {
	if (0 == misCiStrcmp(ReportTypeText, REPORT_TYPE_SALCMD))
	    rpttyp = RPT_INITIATE;
	else if (0 == misCiStrcmp(ReportTypeText, REPORT_TYPE_SQR))
	    rpttyp = RPT_SQR;
	else
	    rpttyp = RPT_SQLPLUS;
    }

    /* Now attempt to see if the file is actually there to use... */

    if ((fp = fopen(sqlfile, "r")) == NULL)
    {
	CurPtr = srvResults(eDB_NO_ROWS_AFFECTED, NULL);
	return (CurPtr);
    }
    memset(command, 0, sizeof(command));
    switch (rpttyp)
    {
    case RPT_INITIATE:
	/* Get all of the command string from the file */
	sGetCleanCommand(fp, command, sizeof command);

	/* find &x and substitue the parm value */
	sSubVar(command, "&1", parm1);
	sSubVar(command, "&2", parm2);
	sSubVar(command, "&3", parm3);
	sSubVar(command, "&4", parm4);
	sSubVar(command, "&5", parm5);
	sSubVar(command, "&6", parm6);
	sSubVar(command, "&7", parm7);
	sSubVar(command, "&8", parm8);
	sSubVar(command, "&9", parm9);
	sSubVar(command, "&10", parm10);
	sSubVar(command, "&11", parm11);
	sSubVar(command, "&12", parm12);
	sSubVar(command, "&13", parm13);
	sSubVar(command, "&14", parm14);
	sSubVar(command, "&15", parm15);
	sSubVar(command, "&16", parm16);
	break;

    case RPT_SQLPLUS:
	/* Build the command string....
	   Notice that we go ahead and copy all 
	   of the arguements on to the line...
	   if the sql command doesn't use them, 
	   it will just ignore them... */

	misTrc(T_FLOW, "Report is a sqlPlus report");
	sprintf(command,
		"start '%s' '%s' '%s' '%s' '%s' '%s' '%s' "
		" '%s' '%s' '%s' '%s' '%s' '%s' '%s' '%s' '%s' '%s'\n",
		sqlfile, (parm1 ? parm1 : ""), (parm2 ? parm2 : ""),
		(parm3 ? parm3 : ""), (parm4 ? parm4 : ""),
		(parm5 ? parm5 : ""), (parm6 ? parm6 : ""),
		(parm7 ? parm7 : ""), (parm8 ? parm8 : ""),
		(parm9 ? parm9 : ""), (parm10 ? parm10 : ""),
		(parm11 ? parm11 : ""), (parm12 ? parm12 : ""),
		(parm13 ? parm13 : ""), (parm14 ? parm14 : ""),
		(parm15 ? parm15 : ""), (parm16 ? parm16 : ""));
	break;
    }
    fclose(fp);

    if (!OutputFile || strlen(OutputFile) == 0)
    {
	memset(outfile, 0, sizeof(outfile));

	/* we generate an output name... */
	time(&timep);

	tp = localtime(&timep);

	sprintf(TimeString, "%02d%02d%02d",
		tp->tm_hour,
		tp->tm_min,
		tp->tm_sec);

	sprintf(reportDir, "%s",
		misExpandVars(varbuf, LES_ORACLE_REPORTS, sizeof(varbuf), NULL));
	if (reportDir[strlen(reportDir) - 1] != PATH_SEPARATOR)
	    strcat(reportDir, PATH_SEPARATOR_STR);

	sprintf(outfile, "%s%s_%s.rpt",
		reportDir, ReportName, TimeString);

    }
    else
    {
	memset(outfile, 0, sizeof(outfile));

	/* Fix up the output name (if needed) */
	if (strchr(OutputFile, PATH_SEPARATOR) == NULL)
	{
	    if (misExpandVars(varbuf, LES_ORACLE_REPORTS, sizeof(varbuf), NULL))
		strcpy(outfile, misExpandVars(varbuf, LES_ORACLE_REPORTS, sizeof(varbuf), NULL));
	    strcat(outfile, PATH_SEPARATOR_STR);
	}

	strcat(outfile, OutputFile);

	if (strstr(OutputFile, ".") == NULL)
	    strcat(outfile, ".rpt");

    }
    misTrc(T_FLOW, "Output file is  %s", outfile);

    switch (rpttyp)
    {
    case RPT_INITIATE:
	CurPtr = NULL;

	if (strstr(command, "&outfil"))
	{
	    sSubVar(command, "&outfil", outfile);
	    outfil = TRUE;
	}
	else
	    outfil = FALSE;

	misTrc(T_FLOW, "Command to execute report is %s", command);

	ret_status = srvInitiateCommand(command, &CurPtr);
	if (ret_status != eOK)
	{
	    srvFreeMemory(SRVRET_STRUCT, CurPtr);
	    return (srvResults(ret_status, NULL));
	}
	/* If we got a result set we want to write it to a file. */

	if (outfil == FALSE)
	{
	    if ((fp = fopen(outfile, "w")) == NULL)
	    {
		/* Can't Open file. The command executed OK so we */
		/* return an eOK but a blank output file. */
		srvFreeMemory(SRVRET_STRUCT, CurPtr);
		memset(outfile, 0, sizeof(outfile));
                CurPtr = srvResultsInit(eOK, 
                                       "filename", COMTYP_CHAR, strlen(outfile),
                                       "srvcmd", COMTYP_CHAR, strlen(command),
                                       NULL);

		return (CurPtr);
	    }
	    /* First write the command that was executed. */
	    fprintf(fp, "%s\n\n", command);

	    /* Next write the result set header to the file. */
	    sPrintHeaderFp(CurPtr, fp);

	    /* Finally, write the result set itself to the file. */
	    sPrintResultsFp(CurPtr, fp);
	    fclose(fp);
	}
	srvFreeMemory(SRVRET_STRUCT, CurPtr);

        CurPtr = srvResultsInit(eOK,
                                "filename", COMTYP_CHAR, strlen(outfile),
                                "srvcmd", COMTYP_CHAR, strlen(command),
                                NULL);
	return (CurPtr);

	break;

    case RPT_SQLPLUS:
	user = osGetReg(REGSEC_SERVER, REGKEY_SERVER_DBUSER);
	pass = osGetReg(REGSEC_SERVER, REGKEY_SERVER_DBPASS);
	conn = osGetReg(REGSEC_SERVER, REGKEY_SERVER_DBCONN);

	/* Now open the connection to SQL*Plus  */
	sprintf(tempstr, SQLPLUS_CMD " -s %s/%s%s%s",
		user ? user : "", pass ? pass : "", conn ? "@" : "", conn ? conn : "");

	misTrc(T_FLOW, "Connecting to sqlPlus via: %s", tempstr);
	sqlplus = (FILE *) osPopen(tempstr, "w");
	if (sqlplus == NULL)
	    return (srvResults(eERROR, NULL));

	/* Now...we've got an open sqlplus process... */
	/*             just send the commands to it... */

	fprintf(sqlplus, "set feedback off\nset wrap on\nset tab off\n");
	fprintf(sqlplus, "set termout off\nset verify off\n");
	fprintf(sqlplus, "alter session set nls_date_format = '%s';\n",
		"YYYYMMDDHH24MISS");

	/* Setup page format information */
	if (pagesize_i && misTrimLen(pagesize_i, 1))
	    fprintf(sqlplus, "set pagesize %s\n", pagesize_i);
	if (linesize_i && misTrimLen(linesize_i, 1))
	    fprintf(sqlplus, "set linesize %s\n", linesize_i);
	if (newpage_i && misTrimLen(newpage_i, 1))
	    fprintf(sqlplus, "set newpage %s\n", newpage_i);

	/* Finally, let's setup run_date and run_time */
	fprintf(sqlplus, "column run_date new_value print_date noprint\n");
	fprintf(sqlplus, "column run_time new_value print_time noprint\n");
	fprintf(sqlplus,
	    "select to_char(sysdate, 'MM-DD-YYYY') run_date from dual;\n");
	fprintf(sqlplus,
		"select to_char(sysdate, 'HH24:MI') run_time from dual;\n");

	/* Set the output file and start the command */
	fprintf(sqlplus, "spool %s\n", outfile);
	fprintf(sqlplus, "%s\n", command);

	fprintf(sqlplus, "exit\n");

	osPclose(sqlplus);

        CurPtr = srvResultsInit(eOK, 
                                "filename", COMTYP_CHAR, strlen(outfile),
                                NULL);
        srvResultsAdd(CurPtr,
		      outfile);
 
	return (CurPtr);
	break;

    case RPT_SQR:
	user = osGetReg(REGSEC_SERVER, REGKEY_SERVER_DBUSER);
	pass = osGetReg(REGSEC_SERVER, REGKEY_SERVER_DBPASS);
	conn = osGetReg(REGSEC_SERVER, REGKEY_SERVER_DBCONN);

	/* Now open the connection to SQL*Plus  */
	sprintf(command, "%s%csqr%csqr %s %s/%s%s%s -F%s -XB -XI",
		misExpandVars(varbuf, LES_ORACLE_REPORTS, sizeof(varbuf), NULL),
		PATH_SEPARATOR, PATH_SEPARATOR,
		sqlfile,
		user ? user : "", pass ? pass : "", conn ? "@" : "", conn ? conn : "",
		outfile);

	strcat(command, " \"");
	strcat(command, parm1);
	strcat(command, "\" \"");
	strcat(command, parm2);
	strcat(command, "\" \"");
	strcat(command, parm3);
	strcat(command, "\" \"");
	strcat(command, parm4);
	strcat(command, "\" \"");
	strcat(command, parm5);
	strcat(command, "\" \"");
	strcat(command, parm6);
	strcat(command, "\" \"");
	strcat(command, parm7);
	strcat(command, "\" \"");
	strcat(command, parm8);
	strcat(command, "\" \"");
	strcat(command, parm9);
	strcat(command, "\" \"");
	strcat(command, parm10);
	strcat(command, "\" \"");
	strcat(command, parm11);
	strcat(command, "\" \"");
	strcat(command, parm12);
	strcat(command, "\" \"");
	strcat(command, parm13);
	strcat(command, "\" \"");
	strcat(command, parm14);
	strcat(command, "\" \"");
	strcat(command, parm15);
	strcat(command, "\" \"");
	strcat(command, parm16);
	strcat(command, "\"");

	system(command);

        CurPtr = srvResultsInit(eOK, 
                                "filename", COMTYP_CHAR, strlen(outfile),
                                NULL);
        srvResultsAdd(CurPtr,
		      outfile);

	return (CurPtr);
    }

    /* Avoid compiler warning by returning something */
    return (srvResults(eOK, NULL));
}

static void     sPrintResultsFp(RETURN_STRUCT * CurPtr, FILE * fp)
{
    mocaDataRes     *Ifc;
    mocaDataRow    *row;
    long            i;
    char            hdr_string[2000];
    char            temp[2000];
    char            temp_str[2000];

    Ifc = srvGetResults(CurPtr);

    for (row = sqlGetRow(Ifc); row; row = sqlGetNextRow(row))
    {

	memset(hdr_string, 0, sizeof(hdr_string));
	for (i = 0; i < sqlGetNumColumns(Ifc); i++)
	{
	    switch (sqlGetDataTypeByPos(Ifc, i))
	    {
	    case COMTYP_CHAR:
                if (!sqlIsNullByPos(Ifc, row, i))
                    sprintf(temp_str, "%-*.*s  ",
                            sqlGetActualColumnLenByPos(Ifc, i),
                            sqlGetActualColumnLenByPos(Ifc, i),
                            sqlGetStringByPos(Ifc, row, i));
                else
                    sprintf(temp_str, "%-*.*s  ",
                            sqlGetActualColumnLenByPos(Ifc, i),
                            sqlGetActualColumnLenByPos(Ifc, i),
                            "(null)");

                strcat(hdr_string, temp_str);
                break;

	    case COMTYP_INT:
                if (!sqlIsNullByPos(Ifc, row, i))
                    sprintf(temp, "%ld", sqlGetLongByPos(Ifc, row, i));
		else
		    sprintf(temp, "(null)");

                sprintf(temp_str, 
                        "%-*.*s  ",
                        sqlGetActualColumnLenByPos(Ifc, i),
                        sqlGetActualColumnLenByPos(Ifc, i),
                        temp);
                strcat(hdr_string, temp_str);
                break;

	    case COMTYP_FLOAT:
                if (!sqlIsNullByPos(Ifc, row, i))
                    sprintf(temp, "%f", sqlGetFloatByPos(Ifc, row, i));
		else
		    sprintf(temp, "(null)");

		sprintf(temp_str, 
                        "%-*.*s  ",
                        sqlGetActualColumnLenByPos(Ifc, i),
                        sqlGetActualColumnLenByPos(Ifc, i),
			temp);
		strcat(hdr_string, temp_str);
		break;
	    }
	}
	fprintf(fp, "%s\n", hdr_string);
    }
    return;
}


static void     sSubVar(char *command, char *sub, char *parm)
{

    char           *varloc;
    char            suffix[2000];

    if ((varloc = strstr(command, sub)) != 0)
    {
	strcpy(suffix, varloc + strlen(sub));
	if (parm && strlen(parm))
	    strcpy(varloc, parm);
	else
	    strcpy(varloc, "\0");
	strcat(command, suffix);
    }

}

static void     sPrintHeaderFp(RETURN_STRUCT * CurPtr, FILE * fp)
{

    mocaDataRes     *res;
    char            tempstr[4000];
    char            hdr_string[4000];
    char            divider[4000];
    long            i;

    res = srvGetResults(CurPtr);

    memset(hdr_string, 0, sizeof(hdr_string));
    for (i = 0; i < sqlGetNumColumns(res); i++)
    {
        sprintf(tempstr,
                "%-*.*s  ", 
                sqlGetActualColumnLenByPos(res, i),
                sqlGetActualColumnLenByPos(res, i),
                sqlGetColumnName(res, i));
	strcat(hdr_string, tempstr);
    }

    /* This just sticks in a divider between the header */
    /* and the data (makes it look nicer). */

    memset(divider, 0, sizeof(divider));
    for (i = 0; i < sqlGetNumColumns(res); i++)
    {
	sprintf(tempstr,
                "%.*s  ", 
                sqlGetActualColumnLenByPos(res, i), 
                (char *) DIVIDER);
	strcat(divider, tempstr);
    }
    fprintf(fp, "%s\n", hdr_string);
    fprintf(fp, "%s\n", divider);
}

/*
 * Parse out comments of the form '/','*' . . . '*','/' or 
 * the sequence "--" through to the end of the line.  This function
 * also has the side-effect of removing all newlines from the
 * string, coming out with one line.
 */
static int      sGetCleanCommand(FILE * fp, char *command, long cmdsize)
{
    int             in_quote, in_comment, in_line_comment;
    char           *c, *p;
    char            line[100];

    in_quote = in_comment = in_line_comment = 0;
    c = command;

    while (fgets(line, sizeof line - 1, fp) != NULL)
    {
	for (p = line; *p; p++)
	{
	    /*
	     * First, check for a quote, single or double.  If we've got a
	     * quoted string, we needn't do comment parsing
	     */
	    if (!in_comment && !in_line_comment &&
		(*p == '\'' || *p == '"'))
	    {
		if (in_quote == *p)
		    in_quote = 0;
		else if (!in_quote)
		    in_quote = *p;
		*c++ = *p;
	    }
	    else if (!in_quote)
	    {
		/*
		 * No quotes, so we need to check for comments.
		 * First, if we're in a comment, see if it's done, yet.
		 * Note that comments are not scanned for syntax, except
		 * to check for the end of the comment.
		 */
		if (in_line_comment)
		{
		    if (*p == '\n')
			in_line_comment = 0;
		}
		else if (in_comment)
		{
		    if (*p == '*' && *(p + 1) == '/')
		    {
			in_comment = 0;

			/* Skip over the second comment delimiter char */
			p++;
		    }
		}
		/*
		 * OK, we're not currently in a comment...should we be?
		 */
		else if (*p == '-' && *(p + 1) == '-')
		{
		    in_line_comment = 1;
		}
		else if (*p == '/' && *(p + 1) == '*')
		{
		    in_comment = 1;

		    /* Skip over the second comment delimiter char */
		    p++;
		}
		else
		{
		    /*
		     * OK, no comments, let's clean up whitespace.  Note that
		     * for C purposes, whitespace includes newline, carriage
		     * return, tab, space and form feed.
		     */
		    if (isspace((unsigned char) *p))
			*c++ = ' ';
		    else
			*c++ = *p;
		}
	    }
	    else
	    {
		/*
		 * Within a quoted string, don't even check for
		 * whitespace, since they said they want a string
		 * literal.
		 */
		*c++ = *p;
	    }
	}
	if ((c - command) >= cmdsize)
	{
	    c = command + (cmdsize - 1);
	    break;
	}
    }
    *c = '\0';
    return (c - command);
}
