
#define	PRCMOD_REGISTER_ONLY	"r"
#define	PRCMOD_ALLOCATOR	"A"
#define	PRCMOD_ALLOCATE_ONLY	"a"
#define	PRCMOD_RELEASER		"R"

#define FILNAM_REGISTER_ONLY    "registrar"
#define FILNAM_ALLOCATOR        "allocator"
#define FILNAM_ALLOCATE_ONLY    "alloconly"
#define FILNAM_RELEASER         "releaser"

#define	MAX_ORA_INSTRING	250	/* Is really 254 */
#define	MAX_REL_RULES		3000	/* Maximum release rules saved */

typedef struct _BatchLookAhead
{
    char batcod[BATCOD_LEN+1];
    long maxbat;
    long batcnt;
    
    struct _BatchLookAhead *next;
} BATCH_LOOK_AHEAD;

typedef struct
{
    long            times_through;
    time_t          startd;
    time_t          lstrel;

    mocaDataRes    *aloc_res;
    mocaDataRes    *ordd_res;

    BATCH_LOOK_AHEAD *BatchLookAhead;

} DATA_STRUCT;

typedef struct
{
    char            wrktyp[WRKTYP_LEN + 1];
    char            srcare[ARECOD_LEN + 1];
    char            dstare[ARECOD_LEN + 1];
    char            lodlvl[LODLVL_LEN + 1];
    char            rtstr1[RTSTR1_LEN + 1];
    char            rtstr2[RTSTR2_LEN + 1];
    char            rtstr3[RTSTR1_LEN + 1];
    char            rtstr4[RTSTR2_LEN + 1];
    long            rtnum1;
    long            rtnum2;
    long            rtnum3;
    long            rtnum4;
    char            finare[ARECOD_LEN + 1];
} RELEASE_STRUCT;

typedef struct
{
    char            stsfil[RTSTR1_LEN + 1];
    long            minlbl;
    long            maxlbl;
    long            maxsec;
    long            small_lane;
    double	    palvol;
    char            devcod[DEVCOD_LEN + 1];
    char            relgrp[RTSTR1_LEN + 1];
    char            srvcmd[RTSTR1_LEN + 1];
    char            srvcmd_by[RTSTR2_LEN + 1];
    char            ord_by[RTSTR1_LEN + RTSTR1_LEN + 1];
    char            chkmov_flag[FLAG_LEN + 1];
    long	    PolXDockInstalled;
    long	    rrlflgDefault;
    long            optimizeManyFtp;
    long            reloadLocs;
    RELEASE_STRUCT  rel[MAX_REL_RULES];
    long            move_path_start;
    long            rel_size;
} RULES_STRUCT;

long pckrel_ProcessXDock(char *group,
			 char *ship_id,
			 char *schbat,
			 char *wkonum,
			 char *wkorev, 
			 char *client_id,
			 char *wrkref);

long pckrel_GetConfig(DATA_STRUCT **data_o,
		      RULES_STRUCT **rules_o,
		      long increment_counter_only);

long pckrel_ElapsedMs();
void pckrel_CloseTrc();
void pckrel_WriteTrc(char *format,...);
void pckrel_OpenTrc(RULES_STRUCT *rules,
		    DATA_STRUCT *data,
		    char *prcmod_parm,
		    long commit_as_we_go);

void pckrel_FreeSharedPickResults(mocaDataRes **sharedRes);
void pckrel_InvalidateSharedPickResults();
void pckrel_SetSharedPickResults(mocaDataRes *results);
mocaDataRes *pckrel_GetSharedPickResults();

long pckrel_PrcReleaseGroup(int *process_relgrp,
                            char *ship_id,
                            char *schbat,
                            char *wkonum,
                            char *wkorev,
                            char *client_id,
			    char *save_relgrp,
			    char *wrkref,
                            moca_bool_t skip_rplchk);
