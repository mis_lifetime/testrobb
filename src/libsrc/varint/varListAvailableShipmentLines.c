#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <srvconst.h>
#include <dcserr.h>
#include <common.h>
#include "intlib.h"

#define DEF_WORK_ORDER_TYPE "I"
#define DEF_WORK_ORDER_TEMPLATE_TYPE "T"

static mocaDataRes	*shColumns = NULL;
static mocaDataRes	*slColumns = NULL;
static mocaDataRes	*ohColumns = NULL;
static mocaDataRes	*olColumns = NULL;
static mocaDataRes  *stpColumns = NULL;
static mocaDataRes  *cstColumns = NULL;

static long sInitialize()
{
    long ret_status;
    char buffer[1000];

    if (!shColumns)
    {
	sprintf(buffer, 
		" select * "
		"   from shipment "
		"  where 1 = 2 ");

	ret_status = sqlExecStr(buffer, &shColumns);
	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(shColumns);
	    shColumns = NULL;
	    return (ret_status);
	}
        misFlagCachedMemory((OSFPTR) sqlFreeResults, shColumns);
    }
    
    if (!slColumns)
    {
	sprintf(buffer, 
		" select * "
		"   from shipment_line "
		"  where 1 = 2 " );

	ret_status = sqlExecStr(buffer, &slColumns);
	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(slColumns);
            sqlFreeResults(shColumns);
	    slColumns = NULL;
	    return (ret_status);
	}
        misFlagCachedMemory((OSFPTR) sqlFreeResults, slColumns);
    }

    if (!ohColumns)
    {
	sprintf(buffer, 
		" select * "
		"   from ord "
		"  where 1 = 2 ");

	ret_status = sqlExecStr(buffer, &ohColumns);
	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(shColumns);
            sqlFreeResults(slColumns);
            sqlFreeResults(ohColumns);
	    ohColumns = NULL;
	    return (ret_status);
	}
        misFlagCachedMemory((OSFPTR) sqlFreeResults, ohColumns);
    }
    
    if (!olColumns)
    {
	sprintf(buffer, 
		" select * "
		"   from ord_line "
		"  where 1 = 2 " );

	ret_status = sqlExecStr(buffer, &olColumns);
	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
            sqlFreeResults(shColumns);
            sqlFreeResults(slColumns);
            sqlFreeResults(ohColumns);
	    sqlFreeResults(olColumns);
	    olColumns = NULL;
	    return (ret_status);
	}
        misFlagCachedMemory((OSFPTR) sqlFreeResults, olColumns);
    }

    if (!stpColumns)
    {
        sprintf(buffer,
                " select * "
                "   from stop "
                "  where 1 = 2 " );

        ret_status = sqlExecStr(buffer, &stpColumns);
        if (ret_status != eDB_NO_ROWS_AFFECTED)
        {
            sqlFreeResults(shColumns);
            sqlFreeResults(slColumns);
            sqlFreeResults(ohColumns);
            sqlFreeResults(olColumns);
            sqlFreeResults(stpColumns);
            stpColumns = NULL;
            return (ret_status);
        }
        misFlagCachedMemory((OSFPTR) sqlFreeResults, stpColumns);
    }

    if (!cstColumns)
    {
        sprintf(buffer,
                " select * "
                "   from cstmst "
                "  where 1 = 2 " );

        ret_status = sqlExecStr(buffer, &cstColumns);
        if (ret_status != eDB_NO_ROWS_AFFECTED)
        {
            sqlFreeResults(shColumns);
            sqlFreeResults(slColumns);
            sqlFreeResults(ohColumns);
            sqlFreeResults(olColumns);
            sqlFreeResults(stpColumns);
            sqlFreeResults(cstColumns);
            cstColumns = NULL;
            return (ret_status);
        }
        misFlagCachedMemory((OSFPTR) sqlFreeResults, cstColumns);
    }

    return (eOK);
} /* End intialize */

static void sFormatWhere(char *buffer, 
			 char *table,
			 char *argname, 
			 int  oper, 
			 void *argdata, 
			 char argtype,
			 char dbType)
{
    char temp1[200];
    char temp2[200];

    memset (temp1, 0, sizeof(temp1));
    memset (temp2, 0, sizeof(temp2));

    if (appDataToString(argdata, argtype, temp1) != eOK)
	return;

    /* Convert to date if we need to */
    if (dbType == COMTYP_DATTIM)
    {
        sprintf(temp2, 
	        "to_date('%s')",
		temp1);
        strcpy(temp1, temp2);
	memset(temp2, 0, sizeof(temp2));
    }

    sprintf(buffer, " and %s.%s ", table, argname);
    switch (oper)
    {
        case OPR_NOTNULL:
	    strcpy(temp2, "is not null ");
	    break;
        case OPR_ISNULL:
	    strcpy(temp2, "is null ");
	    break;
        case OPR_EQ:
            sprintf(temp2, argtype == COMTYP_STRING && 
		    dbType != COMTYP_DATTIM ? " = '%s' " : " = %s ", temp1);
	    break;
        case OPR_NE:
            sprintf(temp2, argtype == COMTYP_STRING && 
		    dbType != COMTYP_DATTIM ? " != '%s' " : " != %s ", temp1);
	    break;
        case OPR_LT:
            sprintf(temp2, argtype == COMTYP_STRING && 
		    dbType != COMTYP_DATTIM ? " < '%s' " : " < %s ", temp1);
	    break;
        case OPR_LE:
            sprintf(temp2, argtype == COMTYP_STRING && 
		    dbType != COMTYP_DATTIM ? " <= '%s' " : " <= %s ", temp1);
	    break;
        case OPR_GT:
            sprintf(temp2, argtype == COMTYP_STRING && 
		    dbType != COMTYP_DATTIM ? " > '%s' " : " > %s ", temp1);
	    break;
        case OPR_GE:
            sprintf(temp2, argtype == COMTYP_STRING && 
		    dbType != COMTYP_DATTIM ? " >= '%s' " : " >= %s ", temp1);
	    break;
        case OPR_LIKE:
	    sprintf(temp2, " like '%s%%' ", temp1);
	    break;
    }
    strcat(buffer, temp2);
} /* Format Where */

static void sSetExclusions(RETURN_STRUCT *CurPtr)
{
    char buffer[500];
    char tmpbuf[100];
    long ret_status;
    mocaDataRes *res, *dres;
    mocaDataRow *row;
    mocaDataRow *drow;
    char colnam[COLNAM_LEN+1];
    char client_id[CLIENT_ID_LEN+1];
    char *val;
    char *tmpptr;
    int header;

    dres = srvGetResults(CurPtr);
		
    /* First check to see if this line is waiting for a replenishment */
#if 0
    for (drow = sqlGetRow(dres);
	 drow; drow = sqlGetNextRow(drow))
    {
        /* TODO 
	 * Keep track of previous row.
	 * If exclude (pckqty <= rplqty) then...
	 * Pass res, row, prev_row into new function to remove current row.
	 * Then set current row to prev_row so get next works.
	 */
	if (sqlGetLong(dres, drow, "pckqty") <= sqlGetLong(dres,drow, "rplqty"))
	    *(char *)sqlGetValueByPos(dres, drow, 0) = EXCLUDED_BY_REPLEN;

	if (*(char *)sqlGetValueByPos(dres, drow, 0) == ' ')
	{
            /* 
             * Exclude this shipment line if there is a pckwrk record
             * for it.
             */
	    sprintf(buffer,
		    "select 'x'"
		    " from dual "
		    "where exists ( "
		    " select 'x' "
		    "   from pckwrk p"
		    "  where p.ship_line_id = '%s' ) ",
		    sqlGetString(dres, drow, "ship_line_id"));

	    ret_status = sqlExecStr(buffer, &res);
	    if (ret_status != eDB_NO_ROWS_AFFECTED)
	    {
		if (ret_status != eOK)
		{
		    if (res) sqlFreeResults(res);
		    return;
		}

		*(char *)sqlGetValueByPos(dres, drow, 0) = EXCLUDED_BY_ALLOC;

		sqlFreeResults(res);
		res = NULL;
	    }
	}
	if (*(char *)sqlGetValueByPos(dres, drow, 0) == ' ')
	{
            /* 
             * Exclude this shipment line if there is a xdkwrk record
             * for it.
             */
	    sprintf(buffer,
		    "select 'x'"
		    " from dual "
		    "where exists ( "
		    " select 'x' "
		    "   from xdkwrk x"
		    "  where x.ship_line_id = '%s' ) ",
		    sqlGetString(dres, drow, "ship_line_id"));
	    
	    ret_status = sqlExecStr(buffer, &res);
	    if (ret_status != eDB_NO_ROWS_AFFECTED)
	    {
		if (ret_status != eOK)
		{
		    if (res) sqlFreeResults(res);
		    return;
		}

		*(char *)sqlGetValueByPos(dres, drow, 0) = EXCLUDED_BY_XDCK;

		sqlFreeResults(res);
		res = NULL;
	    }
	}
    }
#endif

    sprintf(buffer, 
	    "select lower(colnam) colnam, colval, client_id "
	    " from pckexc "
	    " where untdte >= sysdate or untdte is null "
	    " order by colnam ");
    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
        return;

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        header = 0;
	memset(colnam, 0, sizeof(colnam));
	misTrimcpy(colnam, sqlGetValue(res, row, "colnam"), COLNAM_LEN);
	    
	memset(client_id, 0, sizeof(client_id));
	if (!sqlIsNull(res, row, "client_id"))
	    strncpy(client_id, 
		    sqlGetString(res, row, "client_id"), CLIENT_ID_LEN);
	
	/* Let's see if our exception is a header or detail exception */
	header = sqlFindColumn(dres, colnam);

	val = sqlGetString(res,row,"colval");
	if (!val)
	    return;
        
	if (header > 0)
	{
	    /* if it's a header, we should have the info in our select */

	    if (!sqlIsNull(dres, dres->Data,colnam))
	    {    
	        for (drow = sqlGetRow(dres);
		     drow; drow = sqlGetNextRow(drow))
	        {
		    if (*(sqlGetStringByPos(dres, drow, 0)) != ' ')
		        continue;

		    tmpptr = sqlGetString(dres, drow, colnam);

		    if ((tmpptr) &&
		        (appCmpStringWithWilds(tmpptr, val) == eOK))
		    {
			if (strlen(client_id))
			{
			    tmpptr = sqlGetString(dres, drow, "client_id");
				    
			    /* TODO
			     * if row/value matches pattern,
			     * then set first column (PCKEXC) value to EXCLUDED
			     * Should use new MOCA sqllib function to set
			     * value, when it is available
			     */
			    if (appCmpStringWithWilds(tmpptr, client_id) == eOK)
				*(sqlGetStringByPos(dres, drow, 0)) = EXCLUDED;
			} 
			else
			{
			    /* TODO */
			    *(sqlGetStringByPos(dres, drow, 0)) = EXCLUDED;
			}
		    }
		}
	    }
	}
	else
	{
	    /* It's a detail exclusion, we'll need to look it up
	     *
	     * If we're excluding by detailed exception, the
	     * assumption we use is that a client_id specified
	     * in the exclusion is associated with the prt_client_id...
	     * this may be a problem...and if it is, we probably
	     * need to call out the two specific client fields
	     * for exclusions...for now, we go with the assumption 
	     * */
		
	    for (drow = sqlGetRow(dres); drow; drow = sqlGetNextRow(drow))
	    {
		if (*(sqlGetStringByPos(dres, drow, 0)) != ' ')
		    continue;

		sprintf(buffer, 
		        "select %s "
			" from shipment, shipment_line, ord_line, ord "
			" where ship_line_id   = '%s' "
			"   and shipment_line.ship_id = shipment.ship_id "
			"   and shipment_line.client_id = ord.client_id  "
			"   and shipment_line.ordnum = ord.ordnum        "
			"   and shipment_line.client_id = ord_line.client_id "
			"   and shipment_line.ordnum = ord_line.ordnum   "
			"   and shipment_line.ordlin = ord_line.ordlin   "
			"   and shipment_line.ordsln = ord_line.ordsln   ",
			colnam, 
			sqlGetString(dres,drow,"ship_line_id"));
		        
	        if (strchr(val, '%')) /* This means it's a like */
	            sprintf(tmpbuf, " and %s like '%s' ", colnam, val);
	        else
                    sprintf(tmpbuf, " and %s = '%s' ", colnam, val);
	        strcat(buffer, tmpbuf);
	    
	        if (strlen(client_id))
	        {
	            if (strchr(client_id, '%')) sprintf(tmpbuf, 
                         " and prt_client_id like '%s' ", client_id);
	            else
		        sprintf(tmpbuf, " and prt_client_id = '%s' ", client_id);

	            strcat(buffer, tmpbuf);
	        }

  	        ret_status = sqlExecStr(buffer, NULL);
	        if (ret_status == eOK)
	        {
		    /* TODO */
	            *(sqlGetStringByPos(dres, drow, 0)) = EXCLUDED_LINE;
  	        }
            }
	}
    }
    sqlFreeResults(res);

    /* Now look for Lines in the result set that are married to excluded
     * lines.  If this line is married to an excluded line, exclude it also.
     * If the married line is not in the set, we don't see it, so tough luck.
     */
    for (drow = sqlGetRow(dres); drow; drow = sqlGetNextRow(drow))
    {
	/* don't bother if this line is already excluded */
	if (*(sqlGetStringByPos(dres, drow, 0)) != ' ')
	    continue;

	/* don't bother if it isn't married */
	if (sqlIsNull(dres, drow, "shpwth"))
	    continue;

	/* OK, this line is married, but not yet excluded. 
	 * Are any of the lines in the married set excluded?
	 * */
	for (row = sqlGetRow(dres); row; row = sqlGetNextRow(row))
	{
	    if (strncmp(sqlGetString(dres, drow, "shpwth"),
	                sqlGetString(dres, drow, "shpwth"),
			SHPWTH_LEN) == 0)
	    {
		if (*(sqlGetStringByPos(dres, row, 0)) != ' ')
		{
		    /* YES - we are married to an excluded line.
		     * Mark this line as excluded.
		     */
		    *(sqlGetStringByPos(dres, row, 0)) = EXCLUDED_BY_MARRIAGE;
		    break;
		}
	    }
	}
    }
} /* end sFormatExclusions */

/* Function for replacing. */
static void replace(char *strIn, char * oldstr, char * newstr)
{
	int sl = (int)strlen(strIn);
    int ol = (int)strlen(oldstr);
    int nl = (int)strlen(newstr);
    int pl, tl;

	char *p = strstr(strIn, oldstr);
	while(p){
		pl = (int)strlen(p);
		tl = sl - pl;
		memset(strIn+tl, 0x0, ol);
		memmove(strIn+tl+nl, &strIn[tl+ol], pl);
		memcpy(strIn+tl, newstr, nl);
		strIn += tl + nl;
		sl = (int)strlen(strIn);
		p = strstr(strIn, oldstr);
	}
}

/* Function for supporting "||" operation in C file */
static char *sSupprtOrOperation(char *argData)
{
    char argname[100];
    char tmpstr[1000];
    char table[2];
    char *retbuffer = NULL;
    char p[1000];
    int startcopy = 0;
    int i, j = 0;

    memset(tmpstr,0,sizeof(tmpstr));
    memset(table,0,sizeof(table));
    memset(argname,0,sizeof(argname));
    memset(p,0,sizeof(p));

    strncpy(p,argData, strlen(argData));
    
    /* Get the argument name from argData string first.
     * Because at first the argument name is "where".
     * We should get the true field name.
     * 
     * Do a for operation, if find the number or letter,
     * record it, or skip it.
     * If find a non-number or non-letter again, stop it.
     * Then we get the field name
     */
    for (i= 0; i < (int)strlen(p) ; i++ )
    {
        if ((p[i] >= 'a' && p[i] <= 'z') ||
            (p[i] == '_') ||
            (p[i] > 0 && p[i] < 9))
        {
            argname[j] = p[i];
            startcopy = 1;
            j++;
        }
        else
        {
            if (startcopy == 1)
            {
                break;
            }
        }
    }

    /* Validate which table is this argument in.
     * Then rebuild the field name, using table name
     * as a prefix. I.E. "ship_id" should be "h.ship_id".
     * Then replace the "ship_id" with "h.ship_id", and
     * build a new sql buffer.
     * Return it.
     */
    if (argname != NULL)
    {
        if (sqlFindColumn(shColumns, argname) != -1)
            strcpy(table,"h");
        else if (sqlFindColumn(slColumns, argname) != -1)
            strcpy(table,"d");
        else if (sqlFindColumn(ohColumns, argname) != -1)
            strcpy(table,"o");
        else if (sqlFindColumn(olColumns, argname) != -1)
            strcpy(table,"ol");
        else if (sqlFindColumn(stpColumns, argname) != -1)
            strcpy(table,"h");
        else if (sqlFindColumn(cstColumns, argname) != -1)
            strcpy(table,"c");
    }
    
    sprintf(tmpstr, "%s.%s", table,argname);
    replace(p,argname,tmpstr);

    sprintf(tmpstr, "and %s", p);

    retbuffer = tmpstr;

    return retbuffer;
}

LIBEXPORT 
RETURN_STRUCT *varListAvailableShipmentLines(void)
{
    long	  ret_status;
    char	  buffer[10000];
    char	  where_list[10000];
    char	  table_list[200];
    char	  field_list[5000];
    char          *arg_ptr;
    RETURN_STRUCT *CurPtr;

    /* For srvEnumerateArgs */
    char	 argname[ARGNAM_LEN + 1];
    int	         oper;
    void	 *argdata;
    char	 argtype;
    SRV_ARGSLIST *CTX;

    memset(table_list, 0, sizeof(table_list));
    memset(where_list, 0, sizeof(where_list));
    memset(field_list, 0, sizeof(field_list));
    
    if (!shColumns || !slColumns || !ohColumns || 
	!olColumns || !stpColumns || !cstColumns)
    {
	ret_status = sInitialize();
	if (ret_status != eOK)
	    return (srvResults(ret_status, NULL));
    }

    /* We have some required fields to pass back */
    /* TODO - should order header fields be returned? */
    strcpy(field_list, 
	   " d.*, ol.prtnum, ol.prt_client_id, ol.lotnum, "
	   " ol.orgcod, ol.revlvl, ol.xdkflg, ol.dstare, ol.dstloc, o.stcust ");
	     
    /* Now we spin through our where clause... */
    CTX = NULL;
    while(eOK == srvEnumerateArgList(&CTX, argname, &oper, &argdata, &argtype))
    {
        buffer[0] = '\0';
        /* PR 39078
         * When user input ||, like "ship_id=SHP001 || ship_id=SHP002" in GUI
         * the MCS will convert it to 
         * "where = ( ship_id=SHP001 OR ship_id=SHP002 )".
         * Local syntax call support it ,but not C files.
         * We need to support the || operation in C files.
         * So I added a function sSupprtOrOperation.
         * It will analyze the special sentence and change it,
         * let it be a normal SQL sentence.
         * Then it will work well.
         */

        if (!misCiStrcmp(argname, "where"))
        {
            strcat(where_list,sSupprtOrOperation(argdata));
        }

	/* Do we have a route code? if so, we need to add the rtemst table*/
	if (!misCiStrcmp(argname, "rtecod"))
	{
	    strcpy(table_list, "rtemst, adrmst, ");
	    sprintf(buffer, 
		    " and rtemst.rtecod = '%s' "
		    " and rtemst.strgnc = adrmst.rgncod "
		    " and o.stcust = adrmst.adr_id", 
		    argdata);
        }
	else if (sqlFindColumn(shColumns, argname) != -1)
	{
	    /* We've found our argument in the shipment table */
	    sFormatWhere(buffer, "h", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(shColumns, 
					     sqlFindColumn(shColumns, 
	    	                   			   argname)));
            if (strlen(buffer))
        	strcat(where_list, buffer);
        }
	else if (sqlFindColumn(slColumns, argname) != -1)
	{
	    /* We've found our argument in the shipment_line table */
	    sFormatWhere(buffer, "d", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(slColumns, 
					     sqlFindColumn(slColumns,
            						   argname)));
            if (strlen(buffer))
        	strcat(where_list, buffer);
	}
	else if (sqlFindColumn(ohColumns, argname) != -1)
	{
	    /* We've found our argument in the ord table */
	    sFormatWhere(buffer, "o", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(ohColumns, 
					     sqlFindColumn(ohColumns, 
	    						   argname)));
            if (strlen(buffer))
        	strcat(where_list, buffer);
	}
	else if (sqlFindColumn(olColumns, argname) != -1)
	{
	    /* We've found our argument in the ord_line table */
	    sFormatWhere(buffer, "ol", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(olColumns, 
					     sqlFindColumn(olColumns,
                                 			   argname)));
            if (strlen(buffer))
        	strcat(where_list, buffer);
	}
        else if (sqlFindColumn(stpColumns, argname) != -1)
        {
            /* We've found our argument in the stop table */
            sFormatWhere(buffer, "h", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(stpColumns,
                                             sqlFindColumn(stpColumns,
                                                             argname)));
            if (strlen(buffer))
        	strcat(where_list, buffer);
        }
        else if (sqlFindColumn(cstColumns, argname) != -1)
        {
            /* We've found our argument in the cstmst table */
            sFormatWhere(buffer, "c", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(cstColumns,
                                             sqlFindColumn(cstColumns,
                                                           argname)));
            if (strlen(buffer))
        	strcat(where_list, buffer);
        }
	else
	{
            /* See if the column is either a from date
             * or a to date
             */
            if (strncmp(argname, FROM_DATE_PREFIX, FROM_DATE_PREFIX_LEN) == 0)
            {
                /* change the argname */
                arg_ptr = argname;
                strcpy(argname, arg_ptr + FROM_DATE_PREFIX_LEN);
                oper = OPR_GE;
            }
            else if (strncmp(argname, TO_DATE_PREFIX, TO_DATE_PREFIX_LEN) == 0)
            {
                arg_ptr = argname;
                strcpy(argname, arg_ptr + TO_DATE_PREFIX_LEN);
                oper = OPR_LE;
            }
            else
            {
                /* Column is not in either of the tables.
                 * Can not use it for selection criteria
                 */
                oper = -1;
            }
     
            if (oper > 0)
            {
                if (sqlFindColumn(shColumns, argname) != -1)
                {
                    /* We've found our argument in the shipment table */
                    sFormatWhere(buffer, "h", argname, oper, argdata, argtype,
                                 sqlGetDataTypeByPos(shColumns,
                                                     sqlFindColumn(shColumns,
                                                                   argname)));
                    if (strlen(buffer))
                        strcat(where_list, buffer);
                }
                else if (sqlFindColumn(slColumns, argname) != -1)
                {
                    /* We've found our argument in the shipment_line table */
                    sFormatWhere(buffer, "d", argname, oper, argdata, argtype,
                                 sqlGetDataTypeByPos(slColumns,
                                                     sqlFindColumn(slColumns,
                                                                   argname)));
                    if (strlen(buffer))
                        strcat(where_list, buffer);
                }
                else if (sqlFindColumn(ohColumns, argname) != -1)
                {
                    /* We've found our argument in the ord table */
                    sFormatWhere(buffer, "o", argname, oper, argdata, argtype,
                                 sqlGetDataTypeByPos(ohColumns,
                                                     sqlFindColumn(ohColumns,
                                                                   argname)));
                    if (strlen(buffer))
                        strcat(where_list, buffer);
                }
                else if (sqlFindColumn(olColumns, argname) != -1)
                {
                    /* We've found our argument in the ord_line table */
                    sFormatWhere(buffer, "ol", argname, oper, argdata, argtype,
                                 sqlGetDataTypeByPos(olColumns,
                                                     sqlFindColumn(olColumns,
                                                                   argname)));
                    if (strlen(buffer))
                        strcat(where_list, buffer);
                }
                else if (sqlFindColumn(stpColumns, argname) != -1)
                {
                    /* We've found our argument in the stop table */
                    sFormatWhere(buffer, "h", argname, oper, argdata, argtype,
                                 sqlGetDataTypeByPos(stpColumns,
                                                     sqlFindColumn(stpColumns,
                                                                   argname)));
                    if (strlen(buffer))
                        strcat(where_list, buffer);
                }
                else if (sqlFindColumn(cstColumns, argname) != -1)
                {
                    /* We've found our argument in the customer table */
                    sFormatWhere(buffer, "c", argname, oper, argdata, argtype,
                                 sqlGetDataTypeByPos(cstColumns,
                                                     sqlFindColumn(cstColumns,
                                                                   argname)));
                    if (strlen(buffer))
                        strcat(where_list, buffer);
                }
            }
        }
    }
        
    srvFreeArgList(CTX);
    
    /* now let's build our table list */
    sprintf(buffer, 
	    "cstmst c, ftpmst, prtmst, ord_line ol, ord o, "
	    " shipment_line d, ship_struct_view h ");
    strcat(table_list, buffer);
    
    /* AND now the where clause */
    /*
     * Notes...
     * We need to look for a shpsts of Loading, because with fluid loading,
     * once inventory is on a trailer, the shipment goes to loading, even
     * though there may be a lot of work left to do (like allocate the rest
     * of the stops or something...
     */

    sprintf(buffer, 
	     "      ftpmst.ftpcod = prtmst.ftpcod "
	     "  and prtmst.prtnum = ol.prtnum "
	     "  and prtmst.prt_client_id = ol.prt_client_id "
             "  and d.linsts in ('%s', '%s') "
	     "  and d.pckqty    > 0 "
	     "  and d.ship_id   = h.ship_id "
	     "  and d.client_id = o.client_id"
	     "  and d.ordnum    = o.ordnum "
	     "  and o.wave_flg  = '%ld' "
	     "  and d.client_id = ol.client_id"
	     "  and d.ordnum    = ol.ordnum "
	     "  and d.ordlin    = ol.ordlin "
	     "  and d.ordsln    = ol.ordsln "
             "  and ol.non_alc_flg = %ld "             
	     "  and o.client_id = c.client_id "
	     "  and o.vc_cstnum = c.cstnum "
             "  and h.super_ship_flg = '%ld' "
             "  and h.super_ship_id is null "
	     "  and h.shpsts in ('%s', '%s', '%s')" 
             "  %s "
             "  and not exists "
             "    (select 1 "
             "       from rplwrk r "
             "      where d.ship_line_id = r.ship_line_id) "
             "  and not exists "
             "    (select 'x' "
             "       from xdkwrk x "
             "      where x.ship_line_id = d.ship_line_id) ",
	     LINSTS_PENDING, LINSTS_INPROCESS,
	     /* exclude waveable orders */
	     BOOLEAN_FALSE,
             /* exclude non-allocatable lines */
             BOOLEAN_FALSE,
             /* exclude super shipments, must be allcoated via wave screen. */
             BOOLEAN_FALSE,
	     /* exclude "not ready" and "TMS planning" orders */
	     SHPSTS_READY, SHPSTS_IN_PROCESS, SHPSTS_LOADING, 
	     where_list);
    strcpy(where_list, buffer);

    sprintf(buffer, 
	     "[ select ' ' pckexc, %s, "
	     " h.ship_id || d.client_id || d.ordnum matchcode "
	     " from %s "
	     " where %s "
	     " order by d.ship_id, d.ship_line_id ]",
	     field_list, 
	     table_list,
	     where_list);
    
    CurPtr = NULL;
    ret_status = srvInitiateCommand(buffer, &CurPtr);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
	 srvFreeMemory(SRVRET_STRUCT, CurPtr);
	 return (srvSetupReturn(ret_status, ""));
    }
    
    if (ret_status ==eOK)
	sSetExclusions(CurPtr);
    
    return (CurPtr);
}
