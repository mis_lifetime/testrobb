/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999 McHugh Software International, Inc.
 *  Shelton, Connecticut,  U.S.A.
 *  All rights reserved.
 *
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>

#include <applib.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>

typedef struct tm_row_struct {
  char ship_line_id[SHIP_LINE_ID_LEN + 1];
  long pckqty;
  long untcas;
  long untpak;
  moca_bool_t case_splflg;
  moca_bool_t pack_stdflg;

  }  TM_ROW_STRUCT;

#define TM_DEF_MAX_WEIGHT 40000
#define TM_DEF_MAX_DELTA_WEIGHT 1000

static moca_bool_t tm_split_marcod_policy;
static double tm_def_max_delta_weight = -1.0;

void var_initialize_wt(void);
int var_SplitShipmentByWeight(char *ship_id, double max_weight, double shpwgt);
int var_SplitShpwthByWeight(char *shpwth, 
			   char *ship_id, 
			   char *new_ship_id, 
			   double max_weight, 
			   double *shpwthwgt,
			   double *shpwgt,
			   double *new_shpwgt,
			   long numrows,
			   TM_ROW_STRUCT *row_array,
			   mocaDataRes *res);

int var_SplitLineByWeight(char *ship_line_id, 
			 char *ship_id, 
			 char *new_ship_id, 
			 double max_weight, 
			 double *linwgt,
			 double *shpwgt,
			 double *new_shpwgt,
			 long *pckqty,
			 long untcas,
			 long untpak,
			 moca_bool_t case_splflg,
			 moca_bool_t pack_stdflg);

long var_CreateNewLine_wt(mocaDataRes *res, 
                     mocaDataRow *row, 
		     char *new_ship_id, 
		     long qty_fraction);

long var_RemoveShipmentLine_wt(char *ship_line_id);

/* Consoldiate and split shipment lines based on weight */
LIBEXPORT 
RETURN_STRUCT *varConsolidateShipmentLinesByWeight(char *cons_batch_i, 
						  double *max_weight_i)
{
    int ret_status;
    char ship_id[SHIP_ID_LEN + 1];
    char cons_batch[CONS_BATCH_LEN + 1];
    char sqlBuffer[1000];
    double shpwgt;
    double max_weight;
    mocaDataRes *res;
    mocaDataRow *row;

    memset(ship_id, 0, sizeof(ship_id));
    memset(cons_batch, 0, sizeof(cons_batch));

    if (!cons_batch_i && !misTrimLen(cons_batch_i, CONS_BATCH_LEN))
        return (APPMissingArg("cons_batch"));

    var_initialize_wt();

    if (max_weight_i && (*max_weight_i > 0))
        max_weight = *max_weight_i;
    else
    {
       /* Start rp, Feb 18th. changed this to not use a tm function */

       sprintf(sqlBuffer,  "select rtflt1 from poldat where polcod = 'ORDER-CONSOLIDATION' and "                
                           "polvar = 'DEFAULT' and polval = 'MAX-WEIGHT'");

       ret_status = sqlExecStr(sqlBuffer, &res);
       if (ret_status == eOK)
       {
    	   row = sqlGetRow(res);
	   max_weight = sqlGetLong(res, row, "rtflt1");
       }
       else			
       {
	    max_weight = TM_DEF_MAX_WEIGHT;
       }

       /* End rp, Feb 18th */
    }

    /* assume each shipment will fit in truck */
    misTrimcpy(cons_batch, cons_batch_i, CONS_BATCH_LEN);
    sprintf(sqlBuffer,
            "select ship_id, sum(p.grswgt * sl.pckqty / p.untcas ) shpwgt "
            "  from shipment_line sl, ord_line ol, prtmst p               "
            " where sl.cons_batch = '%s'                                  "
            "   and sl.client_id = ol.client_id                           "
            "   and sl.ordnum = ol.ordnum                                 "
            "   and sl.ordlin = ol.ordlin                                 "
            "   and sl.ordsln = ol.ordsln                                 "
            "   and ol.prt_client_id = p.prt_client_id                    "
            "   and ol.prtnum = p.prtnum                                  "
            "group by ship_id                                             ",
            cons_batch);

    ret_status = sqlExecStr(sqlBuffer, &res);
    if (ret_status != eOK)
    {
	sqlFreeResults(res);
        return (srvResults(ret_status, NULL));
    }

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        /* only process those shipments that don't fit */

	shpwgt = sqlGetFloat(res, row, "shpwgt");
	if (max_weight < shpwgt)
	{
	    /* this shipment exceeds the max, have to split it up. */

	    misTrimcpy(ship_id, sqlGetString(res, row, "ship_id"), SHIP_ID_LEN);
	    ret_status = var_SplitShipmentByWeight(ship_id, max_weight, shpwgt);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(res);
	        return (srvResults(ret_status, NULL));
	    }
	}
    }
    sqlFreeResults(res);
    return (srvResults(eOK, NULL));
}

void var_initialize_wt(void)
{
    int ret_status;
    char sqlBuffer[1000];
    mocaDataRes *res;
    mocaDataRow *row;

    if (tm_split_marcod_policy == BOOLEAN_NOTSET)
    {
        /* Start rp, Feb 18th. changed this to not use a tm function */

        sprintf(sqlBuffer,  "select rtflt1 from poldat where polcod = 'ORDER-CONSOLIDATION' and "                
                            "polvar = 'DEFAULT' and polval = 'DELTA-WEIGHT'");

        ret_status = sqlExecStr(sqlBuffer, &res);
        if (ret_status == eOK)
        {
    	    row = sqlGetRow(res);
	    tm_def_max_delta_weight = sqlGetLong(res, row, "rtflt1");
        }
        else			
        {
	    /* No policy either, use default */
	    tm_def_max_delta_weight = TM_DEF_MAX_DELTA_WEIGHT;
        }
        /* End rp, Feb 18th */
    }
    if (tm_def_max_delta_weight < 0)
    {
        /* Start rp, Feb 18th. changed this to not use a tm function */

        sprintf(sqlBuffer,  "select rtflt1 from poldat where polcod = 'ORDER-CONSOLIDATION' and "                
                            "polvar = 'DEFAULT' and polval = 'DELTA-WEIGHT'");

        ret_status = sqlExecStr(sqlBuffer, &res);
        if (ret_status == eOK)
        {
    	    row = sqlGetRow(res);
	    tm_def_max_delta_weight = sqlGetLong(res, row, "rtflt1");
        }
        else			
        {
	    /* No policy either, use default */
	    tm_def_max_delta_weight = TM_DEF_MAX_DELTA_WEIGHT;
        }

        /* End rp, Feb 18th */
    }
}
int var_SplitShipmentByWeight(char *ship_id, 
                             double max_weight,
			     double shpwgt)
{
    int ret_status;
    long numrows, i;
    long pckqty;
    long old_pckqty;
    long untcas;
    long untpak;
    double linwgt;
    double new_shpwgt;
    double shpwthwgt;
    double old_shpwthwgt;
#define SHPWTH_LEN 10
    char shpwth[SHPWTH_LEN + 1];
    char new_ship_id[SHIP_ID_LEN + 1];
    char ship_line_id[SHIP_LINE_ID_LEN + 1];
    char sqlBuffer[2000];
    moca_bool_t case_splflg;
    moca_bool_t pack_stdflg;
    mocaDataRes *res;
    mocaDataRow *row;
    mocaDataRes *linres;
    mocaDataRow *linrow;
    TM_ROW_STRUCT *row_array;

    ret_status = appNextNum("ship_id", new_ship_id);
    if (ret_status != eOK)
	return ret_status;
    new_shpwgt = 0;

    /* First, try the lines that are not constrained to 
     * "Ship with another line"
     * Then, try the constrained lines...
     */

    /* Should we use CONS_BATCH to restrict our search?
     * This shipment should be made entirely of 1 cons_batch
     * Therefore, shouldn't have to use cons_batch, ship_id is sufficient
     */
    sprintf(sqlBuffer,
	    "select sl.ship_line_id, sl.pckqty,                  "
	    "       ol.stdflg, ol.splflg,                        "
	    "       p.untcas, p.untpak,                          "
	    "       sum(p.grswgt * sl.pckqty / p.untcas ) linwgt "
	    "  from shipment_line sl, ord_line ol, prtmst p      "
	    " where sl.ship_id = '%s'                            "
	    "   and sl.client_id = ol.client_id                  "
	    "   and sl.ordnum = ol.ordnum                        "
	    "   and sl.ordlin = ol.ordlin                        "
	    "   and sl.ordsln = ol.ordsln                        "
	    "   and ol.prt_client_id = p.prt_client_id           "
	    "   and ol.prtnum = p.prtnum                         "
	    "   and sl.shpwth is NULL                            "
	    "group by sl.ship_line_id, sl.pckqty, ol.stdflg,     "
	    "         ol.splflg, p.untcas, p.untpak              "
	    "order by linwgt desc                                ",
	    ship_id);

    ret_status = sqlExecStr(sqlBuffer, &res);
    if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
    {
	sqlFreeResults(res);
        return (ret_status);
    }

    for (row = sqlGetRow(res);
         row && (max_weight < shpwgt);
         row = sqlGetNextRow(row))
    {
	misTrimcpy(ship_line_id, 
		   sqlGetString(res, row, "ship_line_id"), 
		   SHIP_LINE_ID_LEN);
        linwgt = sqlGetFloat(res, row, "linwgt");
	pckqty = sqlGetLong(res, row, "pckqty");
	old_pckqty = pckqty;

	case_splflg = sqlGetBoolean(res, row, "splflg");
	pack_stdflg = sqlGetBoolean(res, row, "stdflg");
	untcas = MAX(sqlGetLong(res, row, "untcas"), 1);
	untpak = MAX(sqlGetLong(res, row, "untpak"), 1);

	while (pckqty && (max_weight < shpwgt))
	{
	    /* This line is greater than a single truck 
	     * Try to split the line into multiple shipments
	     */
	    ret_status = var_SplitLineByWeight(ship_line_id, 
					      ship_id, 
					      new_ship_id, 
					      max_weight, 
					      &linwgt,
					      &shpwgt,
					      &new_shpwgt,
					      &pckqty,
					      untcas,
					      untpak,
					      case_splflg,
					      pack_stdflg);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(res);
		return (ret_status);
	    }
	}

	if (pckqty == 0)
	{
	    /* The whole quantity of this line was moved to new shipments.
	     */
	    ret_status = var_RemoveShipmentLine_wt(ship_line_id);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(res);
	        return (ret_status);
	    }
	}
	else if (old_pckqty != pckqty)
	{
	    /* if pckqty changed, 
	     * then we have to update old shipment line 
	     */
	    sprintf(sqlBuffer,
		"update shipment_line set pckqty = '%ld' "
		"where ship_line_id = '%s'",
		pckqty,
		ship_line_id);

	    ret_status = sqlExecStr(sqlBuffer, NULL);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(res);
	        return (ret_status);
	    }
	}
    }
    sqlFreeResults(res);

    if (max_weight < shpwgt)
    {
	/* didn't split off enough yet.
	 * Now, we have to try more complicated shpwth splitting
	 */
	sprintf(sqlBuffer,
		"select sl.shpwth,                                   "
		"       sum(p.grswgt * sl.pckqty / p.untcas ) shpwthwgt "
		"  from shipment_line sl, ord_line ol, prtmst p      "
		" where sl.ship_id = '%s'                            "
		"   and sl.client_id = ol.client_id                  "
		"   and sl.ordnum = ol.ordnum                        "
		"   and sl.ordlin = ol.ordlin                        "
		"   and sl.ordsln = ol.ordsln                        "
		"   and ol.prt_client_id = p.prt_client_id           "
		"   and ol.prtnum = p.prtnum                         "
		"   and sl.shpwth is not NULL                        "
		"group by shpwth                                     "
                /* Change the order by clause to shpwthwgt. Else gives 904 error */
		"order by shpwthwgt desc                                ",
		ship_id);

	ret_status = sqlExecStr(sqlBuffer, &res);
	if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
	{
	    sqlFreeResults(res);
	    return (ret_status);
	}

	for (row = sqlGetRow(res);
	     row && (max_weight < shpwgt);
	     row = sqlGetNextRow(row))
	{
	    misTrimcpy(shpwth, sqlGetString(res, row, "shpwth"), SHPWTH_LEN);
	    shpwthwgt = sqlGetFloat(res, row, "shpwthwgt");
	    old_shpwthwgt = shpwthwgt;

	    sprintf(sqlBuffer,
		    " select distinct sl.*, "
		    "        ol.splflg, ol.stdflg, p.untcas, p.untpak "
		    "   from shipment_line sl, ord_line ol, prtmst p "
		    "  where sl.shpwth = '%s'"
		    "    and sl.client_id = ol.client_id "
		    "    and sl.ordnum = ol.ordnum "
		    "    and sl.ordlin = ol.ordlin "
		    "    and sl.ordsln = ol.ordsln "
		    "    and ol.prt_client_id = p.prt_client_id "
		    "    and ol.prtnum = p.prtnum " ,
		    shpwth);

	    ret_status = sqlExecStr(sqlBuffer, &linres);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(linres);
		sqlFreeResults(res);
		return (ret_status);
	    }

            /* Changed res to linres, rp 02/22/01 */
	    numrows = sqlGetNumRows(linres);
            /* End rp */

	    row_array = calloc(numrows, sizeof(TM_ROW_STRUCT));

	    for (linrow = sqlGetRow(linres), i = 0;
		 linrow && (i < numrows);
		 linrow = sqlGetNextRow(linrow), i++)
	    {
	        misTrimcpy(row_array[i].ship_line_id,
		           sqlGetString(linres, linrow, "ship_line_id"),
			   SHIP_LINE_ID_LEN);
	        row_array[i].pckqty = sqlGetLong(linres, linrow, "pckqty");
		row_array[i].case_splflg = sqlGetBoolean(res, row, "splflg");
		row_array[i].pack_stdflg = sqlGetBoolean(res, row, "stdflg");
		row_array[i].untcas = MAX(sqlGetLong(res, row, "untcas"), 1);
		row_array[i].untpak = MAX(sqlGetLong(res, row, "untpak"), 1);

	    }
	    sqlFreeResults(linres);

	    /* This line is greater than a single truck 
	     * Try to split the line into multiple shipments
	     */
	    while ((shpwthwgt > 0) && (max_weight < shpwgt))
	    {
		ret_status = var_SplitShpwthByWeight(shpwth, 
						    ship_id, 
						    new_ship_id, 
						    max_weight, 
						    &shpwthwgt,
						    &shpwgt,
						    &new_shpwgt,
						    numrows,
						    row_array,
						    linres);
		if (ret_status != eOK)
		{
		    sqlFreeResults(res);
		    return (ret_status);
		}
	    }

	    if (shpwthwgt == 0)
	    {
		/* The whole quantity of these lines were 
		 * moved to new shipments.
		 * Remove the old lines.
		 */
	        for (i = 0; i < numrows; i++)
		{
		  ret_status = var_RemoveShipmentLine_wt(row_array[i].ship_line_id);
		  if (ret_status != eOK)
		  {
			sqlFreeResults(res);
			return (ret_status);
		  }
		}
	    }
	    else if (old_shpwthwgt != shpwthwgt)
	    {
		/* if weight changed, then we have to update 
		 * the pckqty of the old shipment lines
		 */
	        for (i = 0; i < numrows; i++)
		{
		    sprintf(sqlBuffer,
			"update shipment_line set pckqty = '%ld' "
			"where ship_line_id = '%s'",
			row_array[i].pckqty,
			row_array[i].ship_line_id);

		    ret_status = sqlExecStr(sqlBuffer, NULL);
		    if (ret_status != eOK)
		    {
			sqlFreeResults(res);
			return (ret_status);
		    }
		}
 }
	}
	sqlFreeResults(res);
    }
    return eOK;
}

int var_SplitShpwthByWeight(char *shpwth, 
			   char *ship_id, 
			   char *new_ship_id, 
			   double max_weight, 
			   double *shpwthwgt,
			   double *shpwgt,
			   double *new_shpwgt,
			   long numrows,
			   TM_ROW_STRUCT *row_array,
			   mocaDataRes *res)
{
    int ret_status;
    long i;
    long qty_fraction;
    double wgt_fraction;
    double wgt_avail;
    double wgt_ratio;
    mocaDataRow *row;

    wgt_avail = max_weight - (*new_shpwgt);

    if (wgt_avail >= (*shpwthwgt))
    {
	/* all lines in shpwth group fit in new shipment,
	 * just move them over to new shipment
	 */
	for (i = 0; i < numrows; i++)
	{
	    ret_status = var_ChangeShipmentOnShipmentLine(new_ship_id,
					     row_array[i].ship_line_id);
	    if (ret_status != eOK)
	        return (ret_status);

	    row_array[i].pckqty = 0;
	}

	(*new_shpwgt) += (*shpwthwgt);
	(*shpwgt) -= (*shpwthwgt);
	(*shpwthwgt) = 0;
    }
    else
    {
        /* all lines in shpwth group don't fit in new shipment together.
	 * Try to do proportional splitting of group.
	 *
         * Check if enough of the group of lines 
	 * will fit to make the split worthwhile.
	 * Otherwise, just start a new shipment 
	 */

        if (wgt_avail < tm_def_max_delta_weight)
	{
	    ret_status = appNextNum("ship_id", new_ship_id);
	    if (ret_status != eOK)
		return ret_status;

	    *new_shpwgt = 0;
	    wgt_avail = max_weight;
	}

	/* We may have just changed the weight available.
	 * Check again if we can put the whole line in without changes
	 */
	if (wgt_avail >= (*shpwthwgt))
	{
	    /* all lines in shpwth group fit in new shipment,
	     * just move them over to new shipment
	     */
	    for (i = 0; i < numrows; i++)
	    {
		ret_status = var_ChangeShipmentOnShipmentLine(new_ship_id,
						 row_array[i].ship_line_id);
		if (ret_status != eOK)
		    return (ret_status);

		row_array[i].pckqty = 0;
	    }

	    (*new_shpwgt) += (*shpwthwgt);
	    (*shpwgt) -= (*shpwthwgt);
	    (*shpwthwgt) = 0;
	}
	else
	{
            if (tm_split_marcod_policy == BOOLEAN_FALSE)
	    {
		/* TODO */
		/* Check proportional splitting policy
		 * Do I need to check this for individual lines as well?
		 */
		return eINT_INVALID_SHIPMENT;
	    }
	    wgt_ratio = wgt_avail / (*shpwthwgt);
	    for (i = 0; i < numrows; i++)
	    {
		/* part of the line will fit, 
		 * evaluate constraints to see how much
		 *
		 * Can we split a case?
		 * Can we split a package ?
		 */

		qty_fraction = (long) floor(((double) row_array[i].pckqty) 
		                            * wgt_ratio);

		if (row_array[i].case_splflg == BOOLEAN_FALSE)
		{
		    /* Can not split cases */
		    qty_fraction = (long) floor(qty_fraction / 
			                        row_array[i].untcas) * 
		                          row_array[i].untcas;
		}
		else if (row_array[i].pack_stdflg == BOOLEAN_TRUE)
		{
		    /* Must use standard packages */
		    qty_fraction = (long) floor(qty_fraction / 
			                        row_array[i].untpak) * 
		                          row_array[i].untpak;
		}
		wgt_ratio = MIN(wgt_ratio, qty_fraction/row_array[i].pckqty);
	    }

	    if (wgt_ratio > 0)
	    {
	        /* iterate through row and array at the same time */
	        for (i = 0, row = sqlGetRow(res); 
		     row && i < numrows; 
		     row = sqlGetNextRow(row), i++)
		{
		    /* part of the group will fit, 
		     * even after constraint checks 
		     * Generate new pckqty by using ratio 
		     *   and pick constraints
		     * Make a new line out of each line
		     */

		    qty_fraction = (long) ceil(row_array[i].pckqty * 
			                       wgt_ratio);

		    if (row_array[i].case_splflg == BOOLEAN_FALSE)
		    {
		        qty_fraction = (long) ceil(qty_fraction /
			                           row_array[i].untcas) * 
			                      row_array[i].untcas;
		    }
		    else if (row_array[i].pack_stdflg == BOOLEAN_TRUE)
		    {
		        qty_fraction = (long) ceil(qty_fraction /
			                           row_array[i].untpak) * 
			                      row_array[i].untpak;
		    }

		    ret_status = var_CreateNewLine_wt(res, row, 
			                          new_ship_id, 
						  qty_fraction);
		    if (ret_status != eOK)
			return(ret_status);

		    wgt_fraction = qty_fraction * ((*shpwthwgt) / 
			                           (row_array[i].pckqty));

		    *new_shpwgt += wgt_fraction;
		    *shpwthwgt -= wgt_fraction;
		    *shpwgt -= wgt_fraction;
		    row_array[i].pckqty -= qty_fraction;
		}
	    }
	    else if (*new_shpwgt == 0)
	    {
		/* no fraction of the line will fit, 
		 * even though this is a new shipment.
		 * Therefore, bomb out.
		 */
		return eINT_INVALID_SHIPMENT;
	    }
	    else
	    {
		ret_status = appNextNum("ship_id", new_ship_id);
		if (ret_status != eOK)
		    return ret_status;

		*new_shpwgt = 0;
	    }
	} /* end if proportional splitting */
    } /* end if shpwth group doesn't fit in pre-existing shipment */

    return eOK;
}

int var_SplitLineByWeight(char *ship_line_id, 
			 char *ship_id, 
			 char *new_ship_id, 
			 double max_weight, 
			 double *linwgt,
			 double *shpwgt,
			 double *new_shpwgt,
			 long *pckqty,
			 long untcas,
			 long untpak,
			 moca_bool_t case_splflg,
			 moca_bool_t pack_stdflg)
{
    int ret_status;
    long qty_fraction;
    char sqlBuffer[1000];
    double wgt_fraction;
    double wgt_avail;
    double wgt_ratio;
    mocaDataRes *res;
    mocaDataRow *row;

    wgt_avail = max_weight - (*new_shpwgt);

    if (wgt_avail >= (*linwgt))
    {
        /* whole line fits, move whole quantity. */
        qty_fraction = (*pckqty);
    }
    else
    {
        /* Check if enough of the line will fit 
	 * to make the split worthwhile.
	 * Otherwise, just start a new shipment 
	 */
        if (wgt_avail < tm_def_max_delta_weight)
	{
	    ret_status = appNextNum("ship_id", new_ship_id);
	    if (ret_status != eOK)
		return ret_status;

	    *new_shpwgt = 0;
	    wgt_avail = max_weight;
	}

	/* We may have just changed the weight available.
	 * Check again if we can put the whole line in without changes
	 */
	if (wgt_avail >= (*linwgt))
	{
	    /* whole line fits, move whole quantity. */
	    qty_fraction = (*pckqty);
	}
	else
	{
	    wgt_ratio = wgt_avail / (*linwgt);
	    qty_fraction = (long) floor(((double) *pckqty) * wgt_ratio);

	    if (qty_fraction)
	    {
		/* part of the line will fit, 
		 * evaluate constraints to see how much
		 *
		 * Can we split a case?
		 * Can we split a package ?
		 */

		if (case_splflg == BOOLEAN_FALSE)
		{
		    /* Can not split cases */
		    qty_fraction = (long) floor(qty_fraction / untcas) * 
		                          untcas;
		}
		else if (pack_stdflg == BOOLEAN_TRUE)
		{
		    /* Must use standard packages */
		    qty_fraction = (long) floor(qty_fraction / untpak) * 
		                          untpak;
		}
	    }
	}
    }

    if (qty_fraction)
    {
        /* part of the line will fit, even after constraint checks 
	 * Make a new line out of this line
	 */

        sprintf(sqlBuffer, 
	        "select * from shipment_line where ship_line_id = '%s'",
		ship_line_id);

	ret_status = sqlExecStr(sqlBuffer, &res);
	if (ret_status != eOK)
	{
	    sqlFreeResults(res);
	    return (ret_status);
	}

	row = sqlGetRow(res);

        ret_status = var_CreateNewLine_wt(res, row, new_ship_id, qty_fraction);
	if (ret_status != eOK)
	{
	    sqlFreeResults(res);
	    return(ret_status);
	}

	/* These two statements are equivalent.
	 * both linwgt/pckqty and grswgt/untcas equate to unit weight.
	 * Since we already have linwgt and pckqty we'll use that statement.
	 * Although grswgt is easily available in main select for linwgt.
	 * I have removed grswgt from query and decl for now.
	 *
	 * wgt_fraction = qty_fraction * (grswgt / untcas);
	 */
        wgt_fraction = qty_fraction * ((*linwgt) / (*pckqty));

        *new_shpwgt += wgt_fraction;
	*linwgt -= wgt_fraction;
        *shpwgt -= wgt_fraction;
        *pckqty -= qty_fraction;

	sqlFreeResults(res);
    }
    else if (*new_shpwgt == 0)
    {
        /* no fraction of the line will fit, 
	 * even though this is a new shipment.
	 * Therefore, bomb out.
	 */
        return eINT_INVALID_SHIPMENT;
    }
    else
    {
	ret_status = appNextNum("ship_id", new_ship_id);
	if (ret_status != eOK)
	    return ret_status;

	*new_shpwgt = 0;
    }

    return eOK;
}

long var_CreateNewLine_wt(mocaDataRes *res, 
                      mocaDataRow *row, 
		      char *new_ship_id, 
		      long qty_fraction)
{
    long ret_status;
    char new_ship_line_id[SHIP_LINE_ID_LEN + 1];
    char new_values[100];

    ret_status = appNextNum("ship_line_id", new_ship_line_id);
    if (ret_status != eOK)
	return (ret_status);

    sprintf(new_values, 
	    "'%s','%s',%ld", 
	    new_ship_line_id,
	    new_ship_id, 
	    qty_fraction);

    ret_status = appGenerateCommandWithRes("create shipment line",
					   res, row, 
					   "ship_line_id, ship_id, pckqty",
					   new_values, 
					   NULL);
    return(ret_status);
}

long var_RemoveShipmentLine_wt(char *ship_line_id)
{

   RETURN_STRUCT *CurPtr = NULL; /* rp Feb27 new for MR1810 */
   char buffer[2000];		 /* rp Feb27 new for MR1810 */
   int ret_status;	         /* rp Feb27 new for MR1810 */
/* rp Feb27 orig is removed    char sqlBuffer[100]; */

/* rp Feb27 below is orig and we need fix for MR1810
    sprintf(sqlBuffer, 
	    "remove shipment line where ship_line_id = '%s'",
	    ship_line_id);

    return (srvInitiateCommand(sqlBuffer, NULL));
   rp Feb27 above is orig and we need fix for MR1810*/
/* rp Feb27 below is replacement to fix MR1810 from tmRemoveShipmentLine.c */
    /*delete instructions */
    sprintf (buffer, "list shipment line instructions "
                     " where ship_line_id = '%s' | "
                     " remove special handling instructions ",
                     ship_line_id );

    ret_status = srvInitiateInline(buffer, &CurPtr);
    if (eOK != ret_status && eDB_NO_ROWS_AFFECTED != ret_status)
        return CurPtr;
    srvFreeMemory(SRVRET_STRUCT, CurPtr);

    /* delete the rows */
    sprintf(buffer,
            "delete shipment_line where ship_line_id = '%s' ",
            ship_line_id);
    ret_status = sqlExecStr(buffer, NULL);
    return (ret_status);
/* rp Feb27 above is replacement to fix MR1810 */

}
