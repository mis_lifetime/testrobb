static char *rcsid = "$Id: varCreateTopoffReplenishment.c,v 1.2 2002/05/08 22:26:22 prod Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include <trnlib.h>

/* #include "intlib.h" 	kjh */
/* #include "intRplLib.h"  kjh */

/* srh added structure def from intRplLib.h for policy */
typedef struct _policyInfo {
    long is_installed;
    long max_created;
    long max_active;
    long topoffByRplcfg;
    long log_topoff_rpl_process;
    long log_qtr_rpl_process;
} POLICY_INFO;

/* 
 * There are some key pieces of info that we care about:
 *   1.  minunt/maxunt/pctflg/maxlocs
 *   2.  replenishments can be configured at an area level or at a location lvl
 *
 *  If configured at an area level:
 *         minunt:  min # of units in the AREA
 *         maxunt:  max # of units to replenish to the AREA (topoff only)
 *  If configured at a location level:
 *         minunt:  min # of units in the location
 *         maxunt:  max # of units to replenish to the location 
 *
 *  If pctflg is set, then quantities ALWAYS apply to a location 
 *  regardless of how the configuration was specified:
 *         minunt:  min % of location used before replenishment occurs
 *         maxunt:  max % of a location that may be used for this part
 *
 *
 *  maxlocs:  only useful when considering area level configs - this limits
 *            number of locations to be used by product.  For example, even
 *            if we are under our min threshold for the area, but we currently
 *            have product spread in more locations than what maxlocs 
 *            specifies, then we will postpone a replenishment due to the
 *            maxloc criteria being violated.
 *
 */

static long sint_RplCallGenerateReplenishment(char *stoloc,
				      char *prtnum,
				      char *prt_client_id,
				      char *invsts,
				      long quantity,
				      char *vc_lotnum,	/* kjh */	
				      long vc_untpak, 	/* kjh */
    	    	            	      long match_lotnum_only,	/* kjh */
    	    	            	      long match_untpak_only,	/* kjh */
    	    	            	      long match_none,	/* kjh */
				      RETURN_STRUCT **TmpRes)
{

    char *cptr;
    char buffer[2000];
    char tmpstr[200];
    char distinctBuffer[500];
    mocaDataRes *res, *invres;
    mocaDataRow *row, *invrow;
    
    long ret_status;

    RETURN_STRUCT *CmdRes;

    *TmpRes = NULL;

    /* For grins, we need to make sure we only send stuff to the location
       that can be stored there...the mixing rules at the destination
       can influence what we ask for here...i.e. if we have lot controlled
       part at a location and we don't mix lots, then we'd better be 
       asking for inventory that matches the stuff at the destination */

    sprintf(buffer, 
	    "get inventory mixing rule where stoloc = '%s'", stoloc);
    CmdRes = NULL;

    ret_status = srvInitiateCommand(buffer, &CmdRes);
    if (ret_status != eOK)
    {
	srvFreeMemory(SRVRET_STRUCT, &CmdRes);
	misTrc(T_FLOW, "Error determining mixing rules at stoloc: %s",
	       stoloc);
	return(ret_status);
    }
    res = srvGetResults(CmdRes);
    row = sqlGetRow(res);

    memset(distinctBuffer, 0, sizeof(distinctBuffer));
    if (!sqlIsNull(res, row, "selectlist"))
	strcpy(distinctBuffer, sqlGetString(res, row, "selectlist"));

    if (strlen(distinctBuffer) > 0)
    {
	misTrc(T_FLOW, 
	       "Attempting to determine inventory "
	       "values for allocation request");
	sprintf(buffer,
		"select distinct %s "
		"  from invdtl, invsub, invlod "
		" where invlod.stoloc = '%s' "
		"   and invlod.lodnum = invsub.lodnum "
		"   and invsub.subnum = invdtl.subnum ",
		distinctBuffer, stoloc);
	ret_status = sqlExecStr(buffer, &invres);
	if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(invres);
	    srvFreeMemory(SRVRET_STRUCT, CmdRes);
	    return(ret_status);
	}

	memset(distinctBuffer, 0, sizeof(distinctBuffer));
	if (ret_status == eOK)
	{
	    misTrc(T_FLOW, 
		   "inventory already exists at destination - must request"
		   " specific inventory attributes");

	    invrow = sqlGetRow(invres);
	    /* go through columns and build where clause with values */

	    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	    {
		cptr = sqlGetString(res, row, "column");
		if (strncmp(cptr, "orgcod", 6) == 0 ||
		    strncmp(cptr, "lotnum", 6) == 0 ||
		    strncmp(cptr, "revlvl", 6) == 0)
		{
		    sprintf(tmpstr, "and %s = '%s'",
			    cptr, sqlGetString(invres, invrow, cptr));
		    strcat(distinctBuffer, tmpstr);
		}
		else if (strncmp(cptr, "untpak", 6) == 0 ||
			 strncmp(cptr, "untcas", 6) == 0)
		{
		    sprintf(tmpstr, " and %s = '%d' ",
			    cptr, sqlGetLong(invres, invrow, cptr));
		    strcat(distinctBuffer, tmpstr);
		}
	    }
	}
	else
	{
	    misTrc(T_FLOW, 
		   "No inventory exists at destination - no need"
		   " to request specific attributes");
	}
	sqlFreeResults(invres);
    }

    srvFreeMemory(SRVRET_STRUCT, CmdRes);

    sqlSetSavepoint("before_generate_replenishment");
    if(match_none)
    {	
    	sprintf(buffer,
	    "generate replenishment "
	    " where prtnum = '%s' "
	    "   and prt_client_id = '%s' "
	    "   and invsts = '%s' "
	    "   and srcqty = %d "
	    "   and pcktyp = '%s' "
	    "   and dstloc = '%s' %s ",
	    prtnum, prt_client_id,
	    invsts, quantity, 
	    ALLOCATE_TOPOFF_REPLEN, stoloc, distinctBuffer);
    }
    else if(match_untpak_only)
    {
    	sprintf(buffer,
	    "generate replenishment "
	    " where prtnum = '%s' "
	    "   and prt_client_id = '%s' "
	    "   and invsts = '%s' "
	    "   and srcqty = %d "
	    "   and untpak = %d "	/* kjh */	
	    "   and pcktyp = '%s' "
	    "   and dstloc = '%s' %s ",
	    prtnum, prt_client_id,
	    invsts, quantity, 
 	    vc_untpak,	/* kjh */	
	    ALLOCATE_TOPOFF_REPLEN, stoloc, distinctBuffer);
    }
    else if(match_lotnum_only)
    {
    	sprintf(buffer,
	    "generate replenishment "
	    " where prtnum = '%s' "
	    "   and prt_client_id = '%s' "
	    "   and invsts = '%s' "
	    "   and srcqty = %d "
	    "   and lotnum = '%s' "	/* kjh */
	    "   and pcktyp = '%s' "
	    "   and dstloc = '%s' %s ",
	    prtnum, prt_client_id,
	    invsts, quantity, 
 	    vc_lotnum, 	/* kjh */	
	    ALLOCATE_TOPOFF_REPLEN, stoloc, distinctBuffer);
    }
    else	/* match both */
    {
	sprintf(buffer,
	    "generate replenishment "
	    " where prtnum = '%s' "
	    "   and prt_client_id = '%s' "
	    "   and invsts = '%s' "
	    "   and srcqty = %d "
	    "   and lotnum = '%s' "	/* kjh */
	    "   and untpak = %d "	/* kjh */	
	    "   and pcktyp = '%s' "
	    "   and dstloc = '%s' %s ",
	    prtnum, prt_client_id,
	    invsts, quantity, 
 	    vc_lotnum, vc_untpak,	/* kjh */	
	    ALLOCATE_TOPOFF_REPLEN, stoloc, distinctBuffer);
    }
    CmdRes = NULL;
    ret_status = srvInitiateCommand(buffer, &CmdRes);
    if (ret_status != eOK)
    {
	sqlRollbackToSavepoint("before_generate_replenishment");
	srvFreeMemory(SRVRET_STRUCT, CmdRes);
	return(ret_status);
    }
    *TmpRes = CmdRes;
    return(eOK);
}

typedef struct topoff_req_list 
{
    char stoloc[STOLOC_LEN+1];
    struct topoff_req_list *next;
} TOPOFF_REQ_LIST;
    
static void sFreeTopoffRequirementList(TOPOFF_REQ_LIST **head)
{
    TOPOFF_REQ_LIST *last, *lp;

    lp = last = NULL;
    for (lp = *head; lp; last = lp, lp = lp->next)
    {
	if (last)
	    free(last);
    }
    if (last)
	free(last);
   
    *head = NULL;
    return;
}

static long sFillStruct(TOPOFF_REQ_LIST **head,
			TOPOFF_REQ_LIST **last,
			mocaDataRes *res,
			mocaDataRow *row,
			long *locations_needed)
{
    TOPOFF_REQ_LIST *lp;
    
    long skip_stoloc;

    for (row = sqlGetRow(res); 
	 row && *locations_needed > 0; 
	 row = sqlGetNextRow(row))
    {

	skip_stoloc = 0;

	for (lp = *head; lp; lp = lp->next)
	{
	    if (strncmp(lp->stoloc,
                sqlGetString(res, row, "stoloc"), STOLOC_LEN) == 0)
            {
	    	int_RplLog(0, "Skipping duplicate stoloc: %s",
		           lp->stoloc);
  		skip_stoloc = 1;
	    }
	}

	if (skip_stoloc)
	    continue;

	lp = (TOPOFF_REQ_LIST *)calloc(1, sizeof(TOPOFF_REQ_LIST));
	if (lp == NULL)
	{
	    misTrc(T_FLOW, "Error - failed to allocate memory - aborting");
	    return(eNO_MEMORY);
	}

	strncpy(lp->stoloc, 
		sqlGetString(res, row, "stoloc"), STOLOC_LEN);
	
	(*locations_needed)--;

	if (*head == NULL)
	{
	    *head = lp;
	    *last = lp;
	}
	else
	{
	    (*last)->next = lp;
	    (*last) = lp;
	}
    }
    return(eOK);
}

static TOPOFF_REQ_LIST *sGetTopoffRequirementList(mocaDataRes *rplres,
						  mocaDataRow *rplrow)
{

    POLICY_INFO *policyInfo;
    mocaDataRes *res;
    mocaDataRow *row; 
    long ret_status;
    long locations_needed;
    char buffer[2000];

    TOPOFF_REQ_LIST *head, *last, *lp;

    head = last = lp = NULL;

    policyInfo = int_RplGetPolicyInfo(POLVAR_REPLENISHMENT_TOPOFF);

    misTrc(T_FLOW, 
	   "Building a list of locations to topoff");

    /* OK...first things first... if we already have a location, no
       need to go through any work to get another...just set it and
       return */
    if (!sqlIsNull(rplres, rplrow, "stoloc"))
    {
	int_RplLog(0, 
	           "Location %s specified by config(or invsum) - processing",
	           sqlGetString(rplres, rplrow, "stoloc"));

	lp = (TOPOFF_REQ_LIST *)calloc(1, sizeof(TOPOFF_REQ_LIST));
	strncpy(lp->stoloc, sqlGetString(rplres, rplrow, "stoloc"), STOLOC_LEN);
	return(lp);
    }

    /* if we're still left here and we are not by rplcfg, then we have to
       bail...  */
    if (!policyInfo->topoffByRplcfg)
    {
	misTrc(T_FLOW,
	       "Bailing out - got non-rplcfg replenishment that didn't have "
	       "a stoloc - what to do?  Skipping...");
	return(NULL);
    }

    locations_needed = sqlGetLong(rplres, rplrow, "maxloc");

    int_RplLog(0, "Found a rplcfg for the area, but no specific stoloc "
               "was defined, will attempt to find %d stolocs to replenish in "
               "this area.", locations_needed);

    int_RplLog(0, "1st - list locations in the area that already "
               "contain the part, %s.", 
               sqlGetString(rplres, rplrow, "prtnum"));

    sprintf(buffer,
	    "select ivs.stoloc "
	    "  from locmst lm, invsum ivs "
	    " where ivs.prtnum = '%s' "
	    "   and ivs.prt_client_id = '%s'"
	    "   and ivs.invsts = '%s'"
	    "   and ivs.arecod = '%s'"
	    "   and ivs.stoloc = lm.stoloc"
	    "   and lm.useflg = %d "
	    "   and lm.repflg = %d "
	    "   and lm.pckflg = %d "
	    "   and lm.locsts != '%s'",
	    sqlGetString(rplres, rplrow, "prtnum"),
	    sqlGetString(rplres, rplrow, "prt_client_id"),
	    sqlGetString(rplres, rplrow, "invsts"),
	    sqlGetString(rplres, rplrow, "arecod"),
	    BOOLEAN_TRUE, BOOLEAN_TRUE, BOOLEAN_TRUE,
	    LOCSTS_INV_ERROR);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status == eOK)
    {
        /* We're only going to list the locations in the trace/log file */
        /* if they actually have LOG-FILE-ENABLED, in which case it */
        /* should help with debugging. */

        if (policyInfo->log_topoff_rpl_process)
        {
            for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
            {
                int_RplLog(0,"%s",sqlGetString(res, row, "stoloc"));
            }
        }
        
	if (sFillStruct(&head, &last, 
			res, sqlGetRow(res), &locations_needed) != eOK)
	{
	    sFreeTopoffRequirementList(&head);
	    return(NULL);
	}
    }
    else
        int_RplLog(0, "No locations found containing this part.");

    sqlFreeResults(res);
	
    if (locations_needed == 0)
    {
       int_RplLog(0, "\nNow we'll try to replenish the first %d location(s) "
                  "from this list with %s.\n",
                  sqlGetLong(rplres, rplrow, "maxloc"),
                  sqlGetString(rplres, rplrow, "prtnum"));
	return(head);
    }

    int_RplLog(0, "2nd - list locations in the area that are already "
               "defined in a rplcfg for this part, %s.",
               sqlGetString(rplres, rplrow, "prtnum"));


    sprintf(buffer,
	    "select rpl.stoloc "
	    "  from locmst lm, rplcfg rpl "
	    " where rpl.prtnum = '%s' "
	    "   and rpl.prt_client_id = '%s'"
	    "   and rpl.invsts = '%s'"
	    "   and rpl.arecod = '%s'"
	    "   and rpl.stoloc is not null"
	    "   and rpl.stoloc = lm.stoloc"

	    "   and lm.useflg = %d "
	    "   and lm.repflg = %d "
	    "   and lm.pckflg = %d "
	    "   and lm.locsts != '%s'",
	    sqlGetString(rplres, rplrow, "prtnum"),
	    sqlGetString(rplres, rplrow, "prt_client_id"),
	    sqlGetString(rplres, rplrow, "invsts"),
	    sqlGetString(rplres, rplrow, "arecod"),
	    BOOLEAN_TRUE, BOOLEAN_TRUE, BOOLEAN_TRUE,
	    LOCSTS_INV_ERROR);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status == eOK)
    {
        if (policyInfo->log_topoff_rpl_process)
        {
            for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
            {
                int_RplLog(0,"%s",sqlGetString(res, row, "stoloc"));
            }
        }

	if (sFillStruct(&head, &last, 
			res, sqlGetRow(res), &locations_needed) != eOK)
	{
	    sFreeTopoffRequirementList(&head);
	    return(NULL);
	}
    }
    else
        int_RplLog(0, "No specific locations defined in rplcfgs for "
                   " this area and this part.");
       
    sqlFreeResults(res);
	
    if (locations_needed == 0)
    {
       int_RplLog(0, "\nNow we'll try to replenish the first %d location(s) "
                  "from this list with %s.\n",
                  sqlGetLong(rplres, rplrow, "maxloc"),
                  sqlGetString(rplres, rplrow, "prtnum"));
	return(head);
    }
    
    int_RplLog(0, "3rd - list locations in the area that are "
               "assigned to this part, %s.",
               sqlGetString(rplres, rplrow, "prtnum"));
 
    sprintf(buffer,
	    "select lm.stoloc "
	    "  from locmst lm, poldat pd "
	    " where pd.polcod = '%s' "
	    "   and pd.polvar = 'prtnum' "
	    "   and pd.polval = '%s%s%s' "
	    "   and lm.stoloc between rtstr1 and nvl(rtstr2, rtstr1) "
	    "   and lm.arecod = '%s'"
	    "   and lm.useflg = %d "
	    "   and lm.repflg = %d "
	    "   and lm.pckflg = %d "
	    "   and lm.locsts != '%s'",
	    POLCOD_STORE_ASGLOC,
	    sqlGetString(rplres, rplrow, "prt_client_id"),
	    CONCAT_CHAR, sqlGetString(rplres, rplrow, "prtnum"),
	    sqlGetString(rplres, rplrow, "arecod"),
	    BOOLEAN_TRUE, BOOLEAN_TRUE, BOOLEAN_TRUE,
	    LOCSTS_INV_ERROR);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status == eOK)
    {
        if (policyInfo->log_topoff_rpl_process)
        {
            for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
            {
                int_RplLog(0,"%s",sqlGetString(res, row, "stoloc"));
            }
        }
	if (sFillStruct(&head, &last, 
			res, sqlGetRow(res), &locations_needed) != eOK)
	{
	    sFreeTopoffRequirementList(&head);
	    return(NULL);
	}
    }
    else
        int_RplLog(0, "No locations in the area are assigned to this part.");

    sqlFreeResults(res);
	
    if (locations_needed == 0)
    {
       int_RplLog(0, "\nNow we'll try to replenish the first %d location(s) "
                  "from this list with %s.\n",
                  sqlGetLong(rplres, rplrow, "maxloc"),
                  sqlGetString(rplres, rplrow, "prtnum"));
	return(head);
    }

    int_RplLog(0, "4th - list empty locations in the %s area.",
               sqlGetString(rplres, rplrow, "arecod"));
 
    sprintf(buffer,
	    "select lm.stoloc "
	    "  from locmst lm "
	    " where lm.arecod = '%s' "
 	    "   and lm.useflg = %d "
	    "   and lm.repflg = %d "
	    "   and lm.pckflg = %d "
	    "   and lm.locsts = '%s'"
	    "   and not exists "
	    "     (select 1 from invsum where stoloc = lm.stoloc) ",
	    sqlGetString(rplres, rplrow, "arecod"),
	    BOOLEAN_TRUE, BOOLEAN_TRUE, BOOLEAN_TRUE,
	    LOCSTS_EMPTY);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status == eOK)
    {
        if (policyInfo->log_topoff_rpl_process)
        {
            for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
            {
                if (!sqlIsNull(res, row, "stoloc"))
                    int_RplLog(0,"%s",sqlGetString(res, row, "stoloc"));
            }
        }
 
	if (sFillStruct(&head, &last, 
			res, sqlGetRow(res), &locations_needed) != eOK)
	{
	    sFreeTopoffRequirementList(&head);
	    return(NULL);
	}
    }
    else
        int_RplLog(0, "No empty locations in the area.");

    sqlFreeResults(res);

    int_RplLog(0, "\nNow we'll try to replenish the first %d location(s) "
               "from this list with %s.\n",
               sqlGetLong(rplres, rplrow, "maxloc"),
               sqlGetString(rplres, rplrow, "prtnum"));
 	
    return(head);
}

static long sProcessTopoffs(char *arecod,
			    char *stoloc,
			    char *prtnum,
			    char *prt_client_id,
			    RETURN_STRUCT **RetRes)
{
    mocaDataRes *res, *mlsres;
    mocaDataRow *row, *mlsrow;
    RETURN_STRUCT *CmdRes, *TmpRes, *MlsRes;

    long ret_status, errnum;
    char buffer[2000];
    char whereclause[1000];
    char tmpstr[500];
    long qty;
    long rplcnt;

    POLICY_INFO *policyInfo;

    /* LHCSTART - Steve Hanchar - April 22, 2002 */

    long match_lotnum_only  = BOOLEAN_FALSE;
    long match_untpak_only  = BOOLEAN_FALSE;
    long match_none	    = BOOLEAN_FALSE;

    char vc_lotnum	    [LOTNUM_LEN+1];
    long vc_untpak	    = 0;

    /* LHCEND */

    *RetRes = NULL;
    memset(whereclause, 0, sizeof(whereclause));
    if (prtnum && strlen(prtnum))
    {
	sprintf(whereclause,
		" prtnum = '%s' and prt_client_id = '%s'",
		prtnum, prt_client_id);
    }

    if (stoloc && strlen(stoloc))
    {
	if (strlen(whereclause))
	    strcat(whereclause, " and ");
	sprintf(tmpstr,
		" stoloc = '%s' ", stoloc);
	strcat(whereclause, tmpstr);
    }

    if (arecod && strlen(arecod))
    {
	if (strlen(whereclause))
	    strcat(whereclause, " and ");

	sprintf(tmpstr,
		" arecod = '%s' ", arecod);
	strcat(whereclause, tmpstr);
    }

    policyInfo = int_RplGetPolicyInfo(POLVAR_REPLENISHMENT_TOPOFF);

    if (policyInfo->topoffByRplcfg == 0)
    {
	/* Let's go grab all the locations that have product to top-off */
	sprintf(buffer,
		"list possible topoff locations by invsum" 
		" where %s ",
		whereclause);

         int_RplLog(0, "Attempting to find topoff locations by invsum where"
                   " %s...", whereclause);
    }
    else
    {
	sprintf(buffer,
		"list possible topoff locations by rplcfg"
		" where %s",
		whereclause);
        int_RplLog(0, "Attempting to topoff rplcfgs where"
                   " %s...", whereclause);
    }
    
    CmdRes = NULL;
    ret_status = srvInitiateInline(buffer, &CmdRes);
    if (ret_status != eOK)
    {
	srvFreeMemory(SRVRET_STRUCT, CmdRes);
	if (ret_status == eDB_NO_ROWS_AFFECTED)
        {
            if (policyInfo->topoffByRplcfg == 0)
            {
                int_RplLog(0, "Unable to find any locations in "
                           "this area that need to be topped off.\n");
            }
            else
                int_RplLog(0, "Unable to find any rplcfgs defined for "
                           "this area (and part/client/stoloc, if specified) "
                           "that need to be topped off.\n");

	    return (eINT_NO_LOCS_TO_TOPOFF);
        }
        else
	    return(ret_status);
    }
    res = srvGetResults(CmdRes);


    /* 
     * For every location that we pulled back, let's go attempt to
     * topoff - we cheat a little here...the "sNeedsReplenishment"
     * was really written for triggered replenishments...but...if
     * we treat a topoff like a triggered replenishment where the
     * min/max is 100% of the location, then I think we are golden.
     */
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	long maxpct;
	long rpl_success;

	TOPOFF_REQ_LIST *tor_p, *torList;

	if (policyInfo->topoffByRplcfg && 
	    sqlGetBoolean(res, row, "pctflg") == BOOLEAN_TRUE)
	    maxpct = sqlGetLong(res, row, "maxunt");
	else
	    maxpct = 100;

	/* In the event that we didn't get a stoloc, the 
	   "GetTopoffReq..." routine will generate some locations
	   for us to use...either way, we call it to keep the 
	   flow manageable.... */

	torList = sGetTopoffRequirementList(res, row);

	rpl_success = 0;
	for (tor_p = torList; tor_p; tor_p = tor_p->next)
	{
	    if (int_RplNeedsReplenishment(tor_p->stoloc,
					  arecod,
					  sqlGetString(res, row, "loccod"),
					  sqlGetString(res, row, "pckcod"),
					  1, /* cfg at loc level */
					  0, /* max loc */
					  100, /* min pct */
					  maxpct, /* max pct */
					  1,   /* pctflg */
					  sqlGetString(res, row, "prtnum"),
					  sqlGetString(res, row, "prt_client_id"),
					  sqlGetString(res, row, "invsts"),
					  &qty))
	    {
		if (policyInfo->max_active > 0)
		{
		    int_RplGetActiveRplCount(&rplcnt);
		
		    if (rplcnt >= policyInfo->max_active)
		    {
			sFreeTopoffRequirementList(&torList);
			srvFreeMemory(SRVRET_STRUCT, CmdRes);
                        int_RplLog(0, "Max number of allowed replenishments is "
                                   "%d, and there are currently %d "
                                   "outstanding replenishments - exiting!/n",
                                   policyInfo->max_active,
                                   rplcnt); 
			return(eOK);
		    }
		}

		/* LHCSTART - Steve Hanchar - April 22, 2002 */
		/* we need to replenish....go for it... but first... */
		/* retrieve any custom configuration values */

		match_lotnum_only = BOOLEAN_FALSE;
		match_untpak_only = BOOLEAN_FALSE;
		match_none = BOOLEAN_FALSE;

		memset (vc_lotnum, 0, sizeof (vc_lotnum));
		vc_untpak = 0;

		if (!sqlIsNull (res, row, "vc_lotnum"))
		{
		    misTrimcpy (vc_lotnum, sqlGetString (res, row, "vc_lotnum"), LOTNUM_LEN);
		}
		if (!sqlIsNull (res, row, "vc_untpak"))
		{
		    vc_untpak = sqlGetLong (res, row, "vc_untpak");
		}

		if (strlen (vc_lotnum) && vc_untpak > 0)
		{
		    /* Do nothing */
		}
		else if (strlen (vc_lotnum))
		{
		    match_lotnum_only = BOOLEAN_TRUE;
		}
		else if (vc_untpak > 0)
		{
		    match_untpak_only = BOOLEAN_TRUE;
		}
		else
		{
		    match_none = BOOLEAN_TRUE;
		}

		/* LHCEND */

		/* we need to replenish....go for it... */

                int_RplLog(0, "Attempting to replenish %s with %ld of "
                           "prtnum: %s, prt_client_id: %s ...", 
                           tor_p->stoloc,
                           qty,
                           sqlGetString(res, row, "prtnum"),
                           sqlGetString(res, row, "prt_client_id")); 

		TmpRes = NULL;
		ret_status =
		    sint_RplCallGenerateReplenishment(tor_p->stoloc, 
						     sqlGetString(res, 
								  row, 
								  "prtnum"),
						     sqlGetString(res, 
								  row, 
								  "prt_client_id"),
						     sqlGetString(res, 
								  row, 
								  "invsts"),
						     qty,
						     vc_lotnum,	/* srh */	
						     vc_untpak, 	/* srh */
						     match_lotnum_only,	/* srh */
						     match_untpak_only,	/* srh */
						     match_none,	/* srh */
						     &TmpRes); 
		if (ret_status != eOK)
		{
                    errnum = ret_status;
 
		    misTrc(T_FLOW, 
			   "Call to replenish inventory failed: %d",
			   ret_status);

                    sprintf(buffer,
                            "get mls text "
                            " where locale_id = '%s' "
                            "   and mls_id = 'err%ld'",
                            osGetVar (LESENV_LOCALE_ID) ? 
                                      osGetVar (LESENV_LOCALE_ID) : "",
                            ret_status);

                    MlsRes = NULL;
 
                    ret_status = srvInitiateCommand (buffer, &MlsRes);
 
                    if (ret_status == eOK)
                    {
                        mlsres = srvGetResults(MlsRes);
                        mlsrow = sqlGetRow(mlsres);
                        int_RplLog(0, "Call to replenish inventory failed: " 
                                   "%s\n",
                                   sqlGetString(mlsres, mlsrow, "mls_text"));
                    }
                    else
                    {
                        int_RplLog(0, "Call to replenish inventory failed: "
                                   "%ld.\n",
                                   errnum);
                   }
                    if (MlsRes) srvFreeMemory (SRVRET_STRUCT, MlsRes);
 
		    srvFreeMemory(SRVRET_STRUCT, TmpRes);
		}
		else
		{
		    /* LH - 05/08/02 - Alex Arifin
		     * Add sqlCommit() to alleviate database lock problem.
		     */
  		    sqlCommit();		   
 
		    rpl_success++;

                    int_RplLog(0, "Call to replenish %s succeeded.\n",
                                   tor_p->stoloc);
                      
		    if (*RetRes == NULL)
			*RetRes = TmpRes;
		    else
		    {
			if (TmpRes)
			    srvCombineResults(RetRes, &TmpRes);
		    }
		}
    
	    }
            else
            {
                int_RplLog(0, "%s does not need replenished.\n", tor_p->stoloc);
            }
	}
	sFreeTopoffRequirementList(&torList);
	if (rpl_success && policyInfo->topoffByRplcfg)
	{
	    sprintf(buffer,
		    "update rplcfg "
		    "   set cmpflg = %d, "
		    "       rpldte = sysdate "
		    " where rplnum = '%s' ",
		    BOOLEAN_TRUE,
		    sqlGetString(res, row, "rplnum"));

	    ret_status = sqlExecStr(buffer, NULL);
	    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
	    {
		srvFreeMemory(SRVRET_STRUCT, CmdRes);
		return(ret_status);
	    }
            int_RplLog(0, "Successfully replenished %ld location(s) for "
                       "rplcfg: %s.\n", 
                       rpl_success, 
                       sqlGetString(res, row, "rplnum"));
	}

    }
    srvFreeMemory(SRVRET_STRUCT, CmdRes);

    return(eOK);
}


LIBEXPORT 
RETURN_STRUCT *varCreateTopoffReplenishment(char *arecod_i,
					    char *stoloc_i,
					    char *prtnum_i,
					    char *prt_client_id_i,
                                            char *trace_suffix_i)
{
    POLICY_INFO *policyInfo;
    RETURN_STRUCT *AccumRes, *CmdRes;

    long ret_status;
    long old_status = -1;
    long cnt;
    
    char arecod[ARECOD_LEN+1];
    char stoloc[STOLOC_LEN+1];
    char prtnum[PRTNUM_LEN+1];
    char prt_client_id[CLIENT_ID_LEN+1];
    char trace_suffix[2000];
    char tmpstr[2000];
    char buffer[2000];

    mocaDataRes *res;
    mocaDataRow *row;

    memset(arecod, 0, sizeof(arecod));
    memset(stoloc, 0, sizeof(stoloc));
    memset(prtnum, 0, sizeof(prtnum));
    memset(prt_client_id, 0, sizeof(prt_client_id));
    memset(trace_suffix, 0, sizeof(trace_suffix));
    memset(tmpstr, 0, sizeof(tmpstr));

    if (arecod_i && misTrimLen(arecod_i, ARECOD_LEN))
	misTrimcpy(arecod, arecod_i, ARECOD_LEN);

    if (prtnum_i && misTrimLen(prtnum_i, PRTNUM_LEN))
    {
	misTrimcpy(prtnum, prtnum_i, PRTNUM_LEN);

	/*
	 * If we got a part number, then we better get a client
	 * id in a 3pl environment.
	 */

	ret_status = appGetClient(prt_client_id_i, prt_client_id);
	if (ret_status != eOK)
	    return (srvResults(ret_status, NULL));
    }

    if (stoloc_i && misTrimLen(stoloc_i, STOLOC_LEN))
	misTrimcpy(stoloc, stoloc_i, STOLOC_LEN);

    policyInfo = int_RplGetPolicyInfo(POLVAR_REPLENISHMENT_TOPOFF);

    /* Are we installed? */
    if (!policyInfo->is_installed)
    {
	misTrc(T_FLOW,
	       "Exiting from Top Off Replenishment - policies "
	       "indicate module not installed ");
	return(srvResults(eOK, NULL));
    }

    /* Check the maximum active allowed */
    if (policyInfo->max_active > 0)
    {
	int_RplGetActiveRplCount(&cnt);
	if (cnt > policyInfo->max_active)
	{
	    misTrc(T_FLOW, 
		   "Maximum number of replenishments (%d) "
		   "already outstanding - exiting", 
		   policyInfo->max_active);
	    return(srvResults(eOK, NULL));
	}
    }

    if (policyInfo->topoffByRplcfg)
    {
	sprintf(buffer,
		"update rplcfg set cmpflg = 0 where cmpflg=1");
	sqlExecStr(buffer, NULL);
    }

    if (policyInfo->log_topoff_rpl_process)
    {
        sprintf(trace_suffix, "-Topoff.Sts");

        if (trace_suffix_i)
        {
            sprintf(tmpstr, trace_suffix_i);
            strcat(trace_suffix, tmpstr);
        }

        misTrc(T_FLOW, "Opening trace file with suffix: %s", trace_suffix);

        int_OpenRplTraceFile(policyInfo->log_topoff_rpl_process, trace_suffix);

        /* int_RplLog will write to both the trace file and the log file */
        /* if the LOG-FILE-ENABLED policy is set for TOPOFF. If it is not */
        /* set, int_RplLog will just write to a trace file. */

        int_RplLog(0,
             "\nGenerating topoff replenishment for:\n"
             "   Arecod        %s\n"
             "   Stoloc        %s\n"
             "   Part          %s\n"
             "   Part Client   %s\n",
             (arecod_i ? arecod_i : ""),
             (stoloc_i ? stoloc_i : ""),
             (prtnum_i ? prtnum_i : ""),
             (prt_client_id_i ? prt_client_id_i : ""));
    }

    AccumRes = CmdRes = NULL;

    if (strlen(arecod) || strlen(stoloc))
    {
	CmdRes = NULL;
	ret_status = sProcessTopoffs(arecod,
				     stoloc,
				     prtnum, 
				     prt_client_id, 
				     &CmdRes);
	
	if (ret_status == eOK)
	{
	    if (CmdRes)
            {
                int_CloseRplTraceFile();
		return(CmdRes);
            }
	    else
            {
                int_CloseRplTraceFile();
		return(srvResults(ret_status, NULL));
            }
	}
	else
        {
            if (CmdRes)
                srvFreeMemory(SRVRET_STRUCT, CmdRes);
            int_CloseRplTraceFile();
	    return(srvResults(ret_status, NULL));
        }
	
    }
    else 
    {
	/* If we were not told what to go replenish, then we either
	   must derive it from the existing rplcfg's (if that is how
	   we do topoffs) or we look at area codes as specified by the
	   policies */
	if (policyInfo->topoffByRplcfg) 
	{
            int_RplLog(0, "We weren't given a specific arecod or stoloc"
                       " so we'll make a list of all arecods defined in"
                       " rplcfgs and check each one to see if a rplcfg"
                       " is defined for our part in that area.\n");
 
	    sprintf(buffer,
		    "select distinct arecod "
		    "  from rplcfg ");
	}
	else
	{
	    sprintf(buffer,
		    "select rtstr1 arecod "
		    "  from poldat "
		    " where polcod = 'REPLENISHMENT' "
		    "   and polvar = 'TOP-OFF' "
		    "   and polval = 'AREA-CODES' "
		    " order by srtseq ");
	}
	
	ret_status = sqlExecStr(buffer, &res);
	if (ret_status != eOK)
	{
            int_RplLog(0, "Unable to generate a list of areas to "
                       "replenish - exiting!\n");
            int_CloseRplTraceFile();
	    sqlFreeResults(res);
	    return(srvResults(ret_status, NULL));
	}
	for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	{
	    CmdRes = NULL;
	    ret_status = sProcessTopoffs(sqlGetString(res, row, "arecod"), 
					 NULL,
					 prtnum, 
					 prt_client_id, 
					 &CmdRes);
	    if (ret_status == eOK)
	    {
		if (CmdRes)
		{
		    if (AccumRes)
		    {
			srvCombineResults(&AccumRes, &CmdRes);
			CmdRes = NULL;
		    }
		    else
		    {
			AccumRes = CmdRes;
			CmdRes = NULL;
		    }
		}
                old_status = ret_status;
	    }
	    else
	    {
                if (eINT_NO_LOCS_TO_TOPOFF != ret_status)
                {
		    if (AccumRes) 
		        srvFreeMemory(SRVRET_STRUCT, AccumRes);
                    int_RplLog(0, "Error processing topoff: %d./n"
                               "Exiting!", ret_status);
                    int_CloseRplTraceFile();
		    return(srvResults(ret_status, NULL));
                }
                /* 
                 * If even one came back eOK, we'll return OK.
                 * If all come back NO_LOCS_TO_TOPOFF, we'll return
                 * that.
                 */
                misTrc(T_FLOW, 
                       "No replenishments in area %s",
                       sqlGetString(res, row, "arecod"));

                if (eOK != old_status)
                    old_status = ret_status;
	    }
	}
	sqlFreeResults(res);
    }
    int_CloseRplTraceFile();
    if (CmdRes)
        srvFreeMemory(SRVRET_STRUCT, CmdRes);
    if (AccumRes == NULL)
        return(srvResults((old_status == -1) ? eOK : old_status, NULL));
    else
	return(AccumRes);
} 


