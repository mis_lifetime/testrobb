/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 2002 
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <srvconst.h>
#include <dcserr.h>
#include <common.h>

LIBEXPORT 
RETURN_STRUCT *varGenerateAuditCounts(char *stoloc_i, 
			     char *cntbat_i)
{

    long ret_status;
    char cntbat_string[200];
    char stoloc_string[200];
    char buffer[2000];

    mocaDataRes *res;
    mocaDataRow *row;

    RETURN_STRUCT *CurPtr;
    
    memset(stoloc_string, 0, sizeof(stoloc_string));
    memset(cntbat_string, 0, sizeof(cntbat_string));

    if (stoloc_i && misTrimLen(stoloc_i, STOLOC_LEN))
	sprintf(stoloc_string, " and cntwrk.stoloc = '%s'", stoloc_i);
    if (cntbat_i && misTrimLen(cntbat_i, CNTBAT_LEN))
	sprintf(cntbat_string, " and cntwrk.cntbat = '%s'", cntbat_i);
    
    sprintf(buffer,
	    "select distinct cntwrk.stoloc, cntwrk.cntbat "
	    "  from cntwrk "
	    " where cntqty != untqty "
	    "   and cntsts = '%s' ",
	    CNTSTS_COMPLETE);
    if (strlen(stoloc_string))
	strcat(buffer, stoloc_string);
    if (strlen(cntbat_string))
	strcat(buffer, cntbat_string);

    strcat(buffer, " order by cntwrk.stoloc");

    ret_status = sqlExecStr(buffer, &res);
    if (eDB_NO_ROWS_AFFECTED != ret_status && eOK != ret_status)
    {
	sqlFreeResults(res);
	return(srvResults(ret_status, NULL));
    }
    else if (eDB_NO_ROWS_AFFECTED == ret_status)
    {
	sqlFreeResults(res);
	return(srvResults(eOK, NULL));
    }
    
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        /* Checking to see if audit already exists for this location. */
        /* If it does exist, we'll just get out of here. */

        sprintf(buffer,
                " select 'x' "
                "   from cntwrk "
                " where stoloc = '%s' "
                "   and cnttyp = '%s' "
                "   and cntsts != '%s' ",
                sqlGetString(res, row, "stoloc"),
                CNTTYP_AUDIT,
                CNTSTS_COMPLETE);

        ret_status = sqlExecStr(buffer, NULL);
        if (eOK == ret_status)
        {
            misTrc(T_FLOW, "Open audit already exists for %s, getting out.",
                   sqlGetString(res, row, "stoloc"));
            sqlFreeResults(res);
            return(srvResults(ret_status, NULL));
        }

	sprintf(buffer,
		"insert "
		"  into cntwrk "
		"       (cntbat, stoloc, cntsts, cnttyp, prtflg) "
		"values ('%s', '%s', '%s', '%s', '%d')",
		(char *)sqlGetValue(res, row, "cntbat"),
		(char *)sqlGetValue(res, row, "stoloc"),
		CNTSTS_GENERATED,
		CNTTYP_AUDIT,
		BOOLEAN_FALSE);

	ret_status = sqlExecStr(buffer, NULL);
	if (eOK != ret_status)
	{
	    sqlFreeResults(res);
	    return(srvResults(ret_status, NULL));
	}
        /* Added a section to set the cipflg in all cases when an audit is generated */
        /* MR 4739, Raman P */
        sprintf (buffer,
                 "update locmst "
                 "set cipflg = '%d' "
                 "where stoloc = '%s' ",
                 BOOLEAN_TRUE,
		(char *)sqlGetValue(res, row, "stoloc") );
	
        ret_status = sqlExecStr(buffer, NULL);
	if (eOK != ret_status)
	{
	    sqlFreeResults(res);
	    return(srvResults(ret_status, NULL));
	}
        /* End MR 4739 */
    }

    misTrc(T_FLOW, "Building result set for generated audits");

    CurPtr = NULL;
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	if (!CurPtr)
	{
	    CurPtr = srvResultsInit(eOK,
		                    "cntbat",  COMTYP_CHAR, CNTBAT_LEN,
			            "stoloc",  COMTYP_CHAR, STOLOC_LEN,
			            NULL);
	}    

        srvResultsAdd(CurPtr,
	              sqlGetValue(res, row, "cntbat"),
	              sqlGetValue(res, row, "stoloc"));

    }
    sqlFreeResults(res);
    return(CurPtr);
}
