/* #REPORTNAME=PROD- Cross Reference Data Detail Report */
/* #HELPTEXT= This report lists information */
/* #HELPTEXT= about a specific item. */
/* #HELPTEXT= Enter Item Number. */

/* #VARNAM=Item , #REQFLG=Y */

column part heading 'Item #'
column part format a30
column des heading 'Description'
column des format a30
column ndc heading 'NDC Code' 
column ndc format a20
column upc heading 'UPC #' 
column upc format a20
column ip heading 'Inner Pack'
column ip format 9999999999
column untcas heading 'Case Pack'
column untcas format 9999999999
column untpal heading 'Units/Pallet' 
column untpal format 9999999999
column carpal heading 'Cartons/Pallet' 
column carpal format 9999999999

set linesize 160

ttitle left  print_time -
       center 'PROD-Cross Reference Data Detail Report' -
       right print_date skip 2 -

select a.prtnum part, b.lngdsc des, a.ndccod ndc, a.upccod upc,
case when a.pakuom ='IP' then untpak
else null
end ip,
a.untcas untcas, a.untpal untpal, (a.untpal/a.untcas) carpal
from prtmst a, prtdsc b
where a.prtnum||'|----' = b.colval
and b.colnam = 'prtnum|prt_client_id'
and b.locale_id='US_ENGLISH'
and a.prtnum='&&1'
/
