/* #REPORTNAME=User What's on a Purchase Order */
/* #VARNAM=cponum , #REQFLG=Y */
/* #HELPTEXT= Requested by Order Management */

set pagesize 50000
set linesize 160


ttitle left print_time -
center 'What is on a PO' -
right print_date skip 2 -


column stname heading 'Customer'
column stname format A30
column prtnum heading 'Part Number'
column prtnum format  A20
column cstprt heading 'Customer Part'
column cstprt format A20
column cponum heading 'PO #'
column cponum format A30
column total heading 'Total Qty'
column total format 99999

select ord.btcust stname,
ord_line.prtnum, ord_line.cstprt, ord.cponum,
       sum(ord_line.ordqty) total 
    FROM adrmst, ord_line, ord, prtmst 
       WHERE ord.cponum = upper('&1')
        AND ord.st_adr_id              = adrmst.adr_id
        AND ord.client_id               = '----'
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord.client_id               = prtmst.prt_client_id
        AND ord_line.prtnum             = prtmst.prtnum
  group by ord.btcust, 
 ord_line.prtnum,
  ord_line.cstprt, ord.cponum;
