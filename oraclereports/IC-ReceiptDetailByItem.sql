/* #REPORTNAME=IC-Receipt Detail By Item */
/* #VARNAM=prtnum , #REQFLG=Y */
/* #HELPTEXT= This report lists truck number, date */
/* #HELPTEXT= received, date closed, expected qty, identified qty, */
/* #HELPTEXT= received qty, and variance between the expected and received */
/* #HELPTEXT= qtys for a given part number */

ttitle center 'IC-Receipt Detail By Item' -

set linesize 300
set pagesize 200

alter session set nls_date_format ='DD-MON-YYYY HH24:MI:SS';

column prtnum heading 'Part Number'
column prtnum format a30
column trknum heading 'File #'
column trknum format a20
column arrdte heading 'Date Received'
column arrdte format a24
column clsdte heading 'Date Closed'
column clsdte format a24
column exp heading 'Qty Expected'
column exp format 999,999,999,999
column idq heading 'Qty Identified'
column idq format 999,999,999,999
column rcv heading 'Receive Qty'
column rcv format 999,999,999,999
column exvrcv heading 'Expected vs. Received Variance'
column exvrcv format 999,999,999,999
column excrcvcst heading 'Expected vs. Received Variancs $'
column exvrcvcst format 999,999,999,999.99

select a.prtnum, a.trknum, d.arrdte, b.clsdte,
sum(a.expqty) exp, sum(a.idnqty) idq, sum(a.rcvqty) rcv, sum((a.rcvqty-a.expqty)) exvrcv,  
sum(((a.rcvqty-a.expqty)*c.untcst)) excrcvcst
from rcvlin a, rcvtrk b, trlr d, prtmst c
where a.prtnum=c.prtnum
and a.prtnum='&&1'
and a.trknum=b.trknum
and a.client_id='----'
and c.prt_client_id='----'
and b.trlr_id=d.trlr_id
group by a.prtnum, a.trknum, b.clsdte, d.arrdte
order by d.arrdte;
