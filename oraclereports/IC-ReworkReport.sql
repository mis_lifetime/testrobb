
set linesize 300
set pagesize 50000

alter session set nls_date_format = 'dd-mon-yyyy';

/* #REPORTNAME=IC Rework Report */
/* #HELPTEXT= This report lists all requested */
/* #HELPTEXT= information regarding rework inventory */
/* #HELPTEXT= Requested by Inventory Control - October 2002 */
/* #GROUPS=NOONE */


ttitle  left print_date     print_time center 'IC Rework Report' skip  -
btitle  center 'Page: ' format 999 sql.pno

column stoloc heading 'Loc.'
column stoloc format a20
column prtnum heading 'Item'
column prtnum format a12
column rescod heading 'Reason Code'
column rescod format a110
column invsts heading 'Status'
column invsts format a20
column cost heading 'Cost'
column cost format 999999.99
column untqty heading 'Qty'
column untqty format 9999999
column pndqty heading 'Pending Order Qty'
column pndqty format 9999999
column lngdsc heading 'Description'
column lngdsc format a30
column lstdte heading 'Date'
column lstdte format a12
column untpak heading 'Unit Pack'
column untpak format 999999
column untcas heading 'Case Pack'
column untcas format 999999



select
  invsum.prtnum,
  invsum.stoloc,decode(a.coding||'-'||a.reason,'-',nvl(dlytrn.reacod,'********************'),
  a.coding||'-'||a.reason) rescod,
  substr(prtdsc.lngdsc,1,30) lngdsc,
  decode(invsum.invsts,'W','Rework','C','Customer Issue','O','Vendor Issue','B','Robbinsville Issue',
  'T','Westbury Issue','L','Miscellaneous Issue') invsts,
  rtrim(trunc(invsum.newdte)) lstdte,
  sum(invdtl.untqty) untqty,nvl(pnd.ordqty,0) pndqty,
  prtmst.untpak, prtmst.untcas, sum(invdtl.untqty) * prtmst.untcst cost
  from
  (select prtnum,count(distinct ordnum) ordqty from ord_line
  where not exists(select 'x' from shipment_line where shipment_line.ordnum =
  ord_line.ordnum) and invsts_prg = 'A'
  group by prtnum) pnd,
  usr_reason a,prtmst,prtdsc,invsum,invlod,invsub,invdtl,dlytrn
  where invsum.stoloc = a.stoloc(+)
  and invsum.prtnum = a.prtnum(+)
  and invsum.prtnum = prtmst.prtnum
  and invsum.prt_client_id = prtmst.prt_client_id
  and prtmst.prtnum = pnd.prtnum(+)
  and prtmst.prtnum|| '|----' = prtdsc.colval
  and invsum.stoloc = invlod.stoloc
  and invlod.lodnum = invsub.lodnum
  and invsub.subnum = invdtl.subnum
  and invdtl.prtnum = invsum.prtnum
  and invdtl.prt_client_id = invsum.prt_client_id
  and invdtl.subnum = dlytrn.subnum(+)
  and invdtl.dtlnum = dlytrn.dtlnum(+)
  and invdtl.invsts = dlytrn.TOINVS(+)
  and invdtl.prtnum = dlytrn.prtnum(+)
  and trunc(dlytrn.trndte(+)) between add_months(sysdate,-5) and trunc(sysdate)
  and invsum.invsts in ('W','C','O','B','T','L')
  and prtdsc.colnam='prtnum|prt_client_id'
  and prtdsc.locale_id ='US_ENGLISH'
group by
invsum.prtnum, invsum.stoloc,decode(a.coding||'-'||a.reason,'-',nvl(dlytrn.reacod,'********************'),
a.coding||'-'||a.reason),
invsum.invsts, prtmst.untcst, prtmst.untpak, prtmst.untcas, prtdsc.lngdsc,
invsum.newdte,pnd.ordqty
/ 
