/* #REPORTNAME =IC-Open Dummy Adjustment Report */
/* #HELPTEXT= Enter date in DD-MON-YYY*/
/* #HELPTEXT= This report provides information */
/* #HELPTEXT= on dummy adjustments that have not */
/* #HELPTEXT= been balanaced out in a specific date range. */
/* #HELPTEXT= Can be run for a specific part, or for all. */
/* #HELPTEXT= Also, ClearDate can be entered to include */
/* #HELPTEXT= to use to enter the date that it was last */
/* #HELPTEXT= confirmed that all dummy adjustments */
/* #HELPTEXT= were balanced out. */
/* #HELPTEXT= Requested by IC */
/* #VARNAM=From-Date , #REQFLG=Y */
/* #VARNAM=To-Date , #REQFLG=Y */
/* #VARNAM=ItemNumber , #REQFLG=N */
/* #VARNAM=ClearDate , #REQFLG=N */

alter session set nls_date_format ='DD-MON-YYYY';

set linesize 300
set pagesize 1000

column trndte heading 'Date'
column trndte format date
column prtnum heading 'Part Number'
column prtnum format a40
column trnqty heading 'Qty.'
column trnqty format 999,999,999
column untcst heading 'Unit Cost'
column untcst format 999,999,999.99
column usr_id heading 'User ID'
column usr_id format a20
column tostol heading 'To Location'
column tostol format a20
column frstol heading 'From Location'
column frstol format a20


select a.trndte, a.prtnum, a.trnqty, b.untcst,
a.usr_id, a.tostol, a.frstol
from dlytrn a, prtmst b
where a.prtnum=b.prtnum
and a.prt_client_id='----'
and a.prt_client_id=b.prt_client_id
and a.actcod='ADJ'
and a.reacod='DUMMY'
and a.orgcod='----'
and a.revlvl='----'
and a.trndte between '&1' and '&2'
and ('&3' is null or a.prtnum='&3')
and a.prtnum in (select c.prtnum
from dlytrn c
where c.actcod='ADJ'
and c.reacod='DUMMY'
and c.prt_client_id='----'
and c.orgcod='----'
and c.revlvl='----'
and ('&3' is null or c.prtnum='&3')
and ('&4' is null or c.trndte>='&4')
group by c.prtnum
having sum(trnqty)!=0);
