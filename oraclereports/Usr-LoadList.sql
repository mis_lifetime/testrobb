set heading on;
set linesize 200;
set pagesize 65;

/* #REPORTNAME=User Load List Summary Report */
/* #VARNAM=doc_num , #REQFLG=N */
/* #VARNAM=ship_id , #REQFLG=N */
/* #HELPTEXT= The Load List Report lists, by */
/* #HELPTEXT= pallet id and number of cartons , the*/
/* #HELPTEXT= pallets that are on a given waybill */
/* #HELPTEXT= or shipment. A waybill(doc_num), ship id, or */
/* #HELPTEXT= order number is optional input. */ 
/* #HELPTEXT= Requested by Shipping. */

ttitle left print_time -
                center 'User Load List Summary Report' -
                right print_date skip 1 - 
        
btitle skip1 left 'Loaded By   : ' skip 2 -
                left 'Date Loaded : ' skip 2 -
                left 'Carrier     : ' skip 2 -
                left 'Truck #     : ' skip 2 -
                left 'Door #     : ' skip 2 -
                left 'Load Start Time :' skip 2 -
                center 'Page:' format 999 sql.pno 

column pallet heading 'Pallet Identifier'
column pallet format a23
column stoloc heading 'Location' 
column stoloc format a15
column ship_id heading 'Shipment' 
column ship_id format a11
column NUMCTN heading '# Ctns'
column NUMCTN format 99999999 
column ctncnt heading 'Ctns Count'
column ctncnt format a11
column init heading 'Initial'
column init format a11
column rtname heading 'Customer' 
column rtname format a26
column doc_num heading 'BOL'
column doc_num format a18
column username heading 'User Name'
column username format a15

select distinct
       il.lodnum                     pallet,
        rtrim(il.stoloc)             stoloc,
        rtrim(sh.ship_id)            ship_id,
       count(distinct ib.subnum)     NUMCTN, '__________' ctncnt, '__________' init,
        '     '||substr(adrmst.adrnam,1,20)       rtname,
        rtrim(sh.doc_num)            doc_num,
        substr(cm.mod_usr_id,1,15)||'            ' username
  from  adrmst,
        locmst lm,
        invlod il,
        invsub ib,
        invdtl id, car_move cm, stop st, shipment_line sl,
        shipment sh
 where  nvl(sh.doc_num,' ') like '&1%'  and
        sh.ship_id  like '&2%'           and
        sh.ship_id = sl.ship_id          and
        sl.ship_line_id = id.ship_line_id and
        id.subnum = ib.subnum          and
        ib.lodnum = il.lodnum          and
        lm.stoloc = il.stoloc          and
        sh.stop_id = st.stop_id(+)        and
        st.car_move_id = cm.car_move_id(+)     and
        sh.rt_adr_id = adrmst.adr_id
 group
    by il.lodnum,
       rtrim(sh.ship_id),
       rtrim(sh.doc_num),
       substr(adrmst.adrnam,1,20),
       rtrim(il.stoloc), cm.mod_usr_id
union
select 'Total Loads: '||to_char(count(distinct
       il.lodnum))                     pallet,
        null stoloc,
        'Total Ctns:'  ship_id,
       count(distinct ib.subnum)     NUMCTN,
null ctncnt, null init,
        null rtname,
        null doc_num,
        null username
  from  invlod il,
        invsub ib,
        invdtl id,
        shipment_line sl,
        shipment sh
 where  nvl(sh.doc_num,' ') like '&1%'  and
        sh.ship_id  like '&2%'           and
        sh.ship_id = sl.ship_id          and
        sl.ship_line_id = id.ship_line_id and
        id.subnum = ib.subnum          and
        ib.lodnum = il.lodnum
order  by 2;
