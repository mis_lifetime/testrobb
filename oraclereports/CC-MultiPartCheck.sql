/* #REPORTNAME=CC-Multi Part Check */
/* #HELPTEXT = Requested by Inventory Control */
/* #HELPTEXT = Checks the multi part cycle counts. */
/* #HELPTEXT = Enter the date in DD-MON-YYYY form. */
/* #VARNAM=Start_Date, #REQFLG=Y */
/* #VARNAM=End_Date, #REQFLG=Y */ 

column batch heading 'Count Batch'
column batch format A15
column cntdte heading 'Count Date'
column cntdte format A20
column prt heading 'Item'
column prt format A14
column qty heading 'Quantity'
column qty format 999,999,999
column cost heading 'Unit Cost'
column cost format 999,999.999
column loc heading 'Location'
column loc format A15

alter session set nls_date_format ='dd-mon-yyyy';
set linesize 200

select distinct a.adj_ref1 batch, a.trndte cntdte, a.prtnum prt,
a.trnqty qty, b.untcst cost, a.adj_ref2 loc
from dlytrn a, prtmst b
where a.prtnum=b.prtnum
and a.adj_ref1 is not null
and a.trndte between '&1' and '&2'
/
