/* #REPORTNAME=ENG Shipment by Month Report */
/* #HELPTEXT= Ths report generates shipments by Month. */
/* #HELPTEXT= Requested by Engineering.*/
/* #HELPTEXT= Please enter date in  DD-Mon-YYYY format .*/

/* #VARNAM=loddte , #REQFLG=Y */
/* #VARNAM=loddte1 , #REQFLG=Y */

ttitle left  print_time -
       center 'ENG Shipment By Month: ' -
       right print_date skip -
       center ' &1 '  to  ' &2 ' skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno 

column month heading 'Month' 
column month format A12
column prtnum heading 'Item' 
column prtnum format A12
column shpqty heading 'Qty Shipped'
column shpqty format 999999
column total heading 'Total'
column total format 999999.99

set pagesize 500
set linesize 200

alter session set nls_date_format ='dd-mon-yyyy';

select
       substr(trlr.dispatch_dte,4,3) month,
       ord_line.prtnum,
       sum(sd.shpqty) shpqty, sum(sd.shpqty) * prtmst.untcst total
 from  prtmst, ord, ord_line, shipment sh, shipment_line sd, stop, car_move, trlr
        where prtmst.prtnum= ord_line.prtnum
                    and ord_line.ordnum = ord.ordnum
                    and ord_line.client_id = ord.client_id
                    and ord_line.client_id= sd.client_id
                    and ord_line.ordnum = sd.ordnum
                    and ord_line.ordlin= sd.ordlin
                    and ord_line.ordsln = sd.ordsln
                    and ord.client_id = sd.client_id
                    and ord.rt_adr_id = sh.rt_adr_id
                    and sd.ship_id = sh.ship_id
                    and sh.stop_id = stop.stop_id
                    and stop.car_move_id = car_move.car_move_id
                    and car_move.trlr_id  = trlr.trlr_id
                    and prtmst.prt_client_id = '----'
                    and sh.shpsts = 'C'
                    AND ord.ordtyp                   != 'W'
        AND trunc(trlr.dispatch_dte) between '&1' and '&2'
group by  substr(trlr.dispatch_dte,4,3), ord_line.prtnum, prtmst.untcst
union
select
       substr(trlr.dispatch_dte,4,3) month,
       ord_line.prtnum,
       sum(sd.shpqty)  shpqty, sum(sd.shpqty) * prtmst.untcst total
from prtmst@arch, ord@arch, ord_line@arch, shipment@arch sh, shipment_line@arch sd, stop@arch, car_move@arch, trlr@arch
where prtmst.prtnum= ord_line.prtnum
                    and ord_line.ordnum = ord.ordnum
                    and ord_line.client_id = ord.client_id
                    and ord_line.client_id= sd.client_id
                    and ord_line.ordnum = sd.ordnum
                    and ord_line.ordlin= sd.ordlin
                    and ord_line.ordsln = sd.ordsln
                    and ord.client_id = sd.client_id
                    and ord.rt_adr_id = sh.rt_adr_id
                    and sd.ship_id = sh.ship_id
                    and sh.stop_id = stop.stop_id
                    and stop.car_move_id = car_move.car_move_id
                    and car_move.trlr_id  = trlr.trlr_id
                    and prtmst.prt_client_id = '----'
                    and sh.shpsts = 'C'
                    AND ord.ordtyp                   != 'W'
AND trunc(trlr.dispatch_dte) between '&1' and '&2'
group by  substr(trlr.dispatch_dte,4,3), ord_line.prtnum, prtmst.untcst;
