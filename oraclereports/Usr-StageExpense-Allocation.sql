/* #REPORTNAME=User Stage Expense-Allocation Report */
/* #HELPTEXT= Requested by Senior Management */


 ttitle left '&1' print_time center 'User Stage Expense-Allocation Report' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Item '
column prtnum format a12
column lngdsc heading 'Description'
column lngdsc format a30
column vc_itmtyp heading 'Type     '
column vc_itmtyp format a5
column stoloc heading 'Stg Location     '
column stoloc format a20
column arecod heading 'Storage Area    '
column arecod format a18
column avail heading 'Available Qty In Location'
column avail format 999999
column vc_prdcod heading 'WMS Class'
column vc_prdcod format a12
column pc heading 'LHC Class   '
column pc format a10
column description heading 'LHC Class Description      '
column description format a30
column dcs5 heading 'System'
column dcs5 format a6


set pagesize 40000
set linesize 200


select a.prtnum, substr(b.lngdsc,1,30) lngdsc, c.vc_itmtyp, 
a.stoloc, sum(a.untqty-a.comqty) avail, a.arecod, c.vc_prdcod, 
d.pc, d.description, 'DCS5' dcs5
from invsum a, prtdsc b, prtmst c, prodclass d, locmst e
where a.prtnum = c.prtnum
and a.stoloc = e.stoloc
and a.arecod = e.arecod
and a.prt_client_id ='----'
and c.prtnum||'|----' = b.colval
and b.colnam ='prtnum|prt_client_id'
and b.locale_id ='US_ENGLISH'
and c.vc_prdcod = d.pc
group by a.prtnum, substr(b.lngdsc,1,30), c.vc_itmtyp, a.stoloc, a.arecod, c.vc_prdcod, d.pc, d.description
/
