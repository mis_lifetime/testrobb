/* #REPORTNAME=IC Received  By Date Report */
/* #HELPTEXT = Requested by Engineering */
/* #VARNAM=trndte , #REQFLG=Y */

set pagesize 50000
set linesize 160


ttitle left print_time -
center 'Receive By Date &1 ' -
right print_date skip 2 -


column prtnum heading 'Item'
column prtnum format A15
column pallet heading 'Pallet Id'
column pallet format  A20
column untqty heading 'Qty'
column untqty format 999999
column rftmod heading 'RFT   '
column rftmod format A30
column trndte heading 'Date     '
column trndte format A24
column usr_id 'User     '
column usr_id format a30
column tostol heading 'To Location'
column tostol format a20

alter session set nls_date_format ='dd-mon-yyyy HH24:MI:SS';

select dlytrn.prtnum,
dlytrn.lodnum,
dlytrn.trnqty,
dlytrn.usr_id,
rftmst.vehtyp,
rftmst.rftmod,
dlytrn.tostol,
dlytrn.trndte
    FROM dlytrn, rftmst 
       WHERE trunc(dlytrn.trndte) = '&1'
        AND  dlytrn.devcod              = rftmst.devcod
        AND dlytrn.tostol between 'REC01' and 'REC30'
  order by dlytrn.devcod, dlytrn.usr_id
/
