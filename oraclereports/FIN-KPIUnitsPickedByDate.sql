/* #REPORTNAME=FIN-KPI CFPP Units Picked Report */
/* #VARNAM=trndte , #REQFLG=Y */
/* #VARNAM=End-Date, #REQFLG=Y */
/* #HELPTEXT= This report lists all requested */
/* #HELPTEXT= information regarding CFPP Units picked */
/* #HELPTEXT= Enter Date in format DD-MON-YYYY */
/* #HELPTEXT= Requested by The Controller */
/* #GROUPS=NOONE */

/* The below script obtains information from both production and */
/* archive simultaneously by using a union. The date parameters should be input in */
/* dd-mon-yyyy format.                                                    */
/* For Piece Picks, the script looks                                      */
/* for records without a subucc.                                          */

set pagesize 500
set linesize 150 


ttitle left print_time -
center 'FIN-KPI CFPP Units Picked From &&1 To &&2 ' -
right print_date skip 2 -


column uccdte heading 'Date'
column uccdte format A25
column total heading 'CFPP Units Picked'
column total format  9999999 

alter session set nls_date_format ='DD-MON-YYYY';

select substr(a.pckdte,4) uccdte, sum(a.appqty) total
      from pckwrk a,invdtl b
      where a.wrkref = b.wrkref
      and a.prtnum = b.prtnum 
      and a.wrktyp ='P'
      and a.ship_line_id = b.ship_line_id
      and a.prt_client_id = '----'
      and b.prt_client_id = '----'
      and a.appqty  > 0
      and a.subucc is  null    
      and trunc(a.pckdte) between '&&1' and '&&2'
      group by substr(a.pckdte, 4)
union
select substr(a.pckdte,4) uccdte, sum(a.appqty) total
      from pckwrk@arch a,invdtl@arch b
      where a.wrkref = b.wrkref
      and a.prtnum = b.prtnum
      and a.wrktyp = 'P'
      and a.ship_line_id = b.ship_line_id
      and a.prt_client_id = '----'
      and b.prt_client_id = '----'
      and a.appqty  > 0
      and a.subucc is  null
      and not exists(select 'x' from pckwrk where wrkref = a.wrkref)
      and trunc(a.pckdte) between '&&1' and '&&2'
 group by substr(a.pckdte,4)
/
