/* #REPORTNAME= FIN-Archived Fill Rate Top 15 Customers Report */
/* #HELPTEXT= This report accepts a month parameter only.  */
/* #HELPTEXT= It will list the fill rate for each order  */
/* #HELPTEXT= entered into DCS during that month for the */
/* #HELPTEXT= Top 15 customers in YTD $'s shipped. */
/* #HELPTEXT= Enter date format as MON-YYYY. */
/* #HELPTEXT= Requested by the Controller */
/* #VARNAM=Month  ,   #REQFLG=Y */


/* Author: Al Driver */
/* Report was created off of the Usr-FillRateAll Report. */
/* The only difference from this report and Fill Rate All */
/* is that the fill rates are returned for the top 15 customers */
/* in YTD shipment dollars. If one of the top 15 does not show in */
/* a given month, it would mean no orders were placed by that customer */
/* in that month, even though their YTD shipment dollars puts them in the top 15. */
  
ttitle left  print_time -
		center 'FIN - Archived Fill Rate Top 15 Customers Report' -
		right print_date skip 1 -
		center 'For: ' ' &&1'  skip 2

btitle center 'Page: ' format 999 sql.pno 

column subsid noprint 
column ordqty heading 'Order|Qty'
column ordqty format 9999990
column shpqty heading 'Shipped|Qty'
column shpqty format 9999990
column fillrate heading 'Fill|Rate'
column fillrate format 9990.0
column ordnum heading 'Order|Number' trunc
column ordnum format a12
column cponum heading 'PO Number' trunc
column cponum format a14
column adrnam heading 'Customer Name' trunc
column adrnam format a20
column loddte heading 'Act Ship|Date'
column loddte format a11
column cpodte heading 'Cancel|Date'
column cpodte format a11
column entdte heading 'Req Ship|Date'
column entdte format a11
column candayslate heading 'Cancel|Days Late'
column candayslate format 9990
column reqdayslate heading 'Req|Days Late'
column reqdayslate format 9990

set linesize 400
set pagesize 20000

alter session set nls_date_format ='DD-MON-YYYY';

SELECT adrnam adrnam,
         ord.cponum cponum,
        ord.ordnum ordnum, to_char(ord_line.early_shpdte) entdte, to_char(ord_line.late_shpdte) cpodte,
  to_char(trlr.dispatch_dte,'DD-MON-YY') loddte,
        decode( sign(nvl(trunc(dispatch_dte),trunc(sysdate))-trunc(ord_line.early_shpdte)),-1,0,
                nvl(trunc(dispatch_dte),trunc(sysdate))-trunc(ord_line.early_shpdte)) reqdayslate,
        decode( sign(nvl(trunc(dispatch_dte),trunc(sysdate))-decode(trunc(ord_line.late_shpdte),to_date('01-JAN-1900','DD-MON-YYYY'),
 trunc(ord_line.early_shpdte),
 trunc(ord_line.late_shpdte))),-1,0,
                nvl(trunc(dispatch_dte),trunc(sysdate))-decode(trunc(ord_line.late_shpdte),to_date('01-JAN-1900','DD-MON-YYYY'),
trunc(ord_line.early_shpdte),
trunc(ord_line.late_shpdte))) candayslate,
       usrCalcOrdQtyForOrder( ord.ordnum,NULL ) ordqty,
       usrCalcShpQtyForOrder(ord.ordnum,NULL,NULL) shpqty,
       usrCalcShpQtyForOrder(ord.ordnum,NULL,NULL)/usrCalcOrdQtyForOrder( ord.ordnum,NULL )*100 fillrate
FROM adrmst, ord, ord_line, shipment_line, shipment,stop ,car_move, trlr
 WHERE adrmst.host_ext_id  = ord.btcust
   and adrmst.client_id  = ord.client_id
   and ord.ordnum = ord_line.ordnum
   and ord.client_id = ord_line.client_id
   and ord.client_id = shipment_line.client_id
   and ord.rt_adr_id  = shipment.rt_adr_id
   and ord_line.ordnum = shipment_line.ordnum
   and ord_line.ordlin = shipment_line.ordlin
   and ord_line.ordsln = shipment_line.ordsln
   and ord_line.client_id = shipment_line.client_id
   and shipment_line.ship_id = shipment.ship_id
   and shipment.stop_id = stop.stop_id
   and stop.car_move_id  = car_move.car_move_id
   and car_move.trlr_id  = trlr.trlr_id
   and ord.client_id ||''='----'              /* Get top 15 customers */
   and ord.btcust in (select x.btcust from
   (select adr.adrnam, ord.btcust, sum(nvl(ord_line.vc_cust_untcst ,0) * i.untqty) dlramt
    from invdtl i, shipment_line sd, shipment sh, ord, ord_line,
    adrmst adr
    where i.ship_line_id            =  sd.ship_line_id
    AND ord.ordnum                  = ord_line.ordnum
    AND ord.client_id               = ord_line.client_id
    AND ord_line.client_id          = sd.client_id
    AND ord_line.ordnum             = sd.ordnum
    AND ord_line.ordlin             = sd.ordlin
    AND ord_line.ordsln             = sd.ordsln
    AND sd.ship_id                  = sh.ship_id
    AND ord.btcust                  = adr.host_ext_id
    AND ord.client_id               = adr.client_id
    AND adr.adrtyp = 'CST'
    AND sh.shpsts = 'C'
    AND ord.ordtyp != 'W'
    AND trunc(sh.loddte) between trunc(to_date(replace(sysdate,substr(sysdate,1,6),'01-JAN')))
    AND trunc(sysdate)
    group by adr.adrnam,ord.btcust
    order by dlramt desc) x
    where rownum <= 15)   
   and shipment_line.linsts ||''= 'C'
   and adrmst.adrtyp = 'CST'
   and trunc(ord_line.early_shpdte) between to_date('01-'||substr('&&1',1,8),'DD-MON-YYYY')
   and LAST_DAY(to_date('01-'||substr('&&1',1,8),'DD-MON-YYYY'))
group by adrmst.adrnam,
              ord.cponum,
              ord.ordnum, to_char(ord_line.early_shpdte), to_char(ord_line.late_shpdte)
            ,to_char(trlr.dispatch_dte,'DD-MON-YY'),
        decode( sign(nvl(trunc(dispatch_dte),trunc(sysdate))-trunc(ord_line.early_shpdte)),-1,0,
                nvl(trunc(dispatch_dte),trunc(sysdate))-trunc(ord_line.early_shpdte)),
        decode( sign(nvl(trunc(dispatch_dte),trunc(sysdate))-decode(trunc(ord_line.late_shpdte),to_date('01-JAN-1900','DD-MON-YYYY')
,
 trunc(ord_line.early_shpdte),
 trunc(ord_line.late_shpdte))),-1,0,
                nvl(trunc(dispatch_dte),trunc(sysdate))-decode(trunc(ord_line.late_shpdte),to_date('01-JAN-1900','DD-MON-YYYY'),
trunc(ord_line.early_shpdte),
trunc(ord_line.late_shpdte)))
/
