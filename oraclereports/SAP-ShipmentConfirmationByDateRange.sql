/* #REPORTNAME=SAP- Shipment Confirmation By Date Range */
/* #HELPTEXT = SAP Shipment Confirmation */
/* #HELPTEXT = by Date Range */

set feedback off;
alter session set nls_date_format ='yyyymmdd';
set linesize 300
set pagesize 5000

select 'ROB' whse_id, rtrim(ord.btcust), trunc(tr.dispatch_dte) dispatched , ord_line.ordlin, rtrim(ord.cponum) cponum, rtrim(ord.ordnum), rtrim(sh.ship_id) ship_id,
rtrim(ord.stcust) stcust,
rtrim(ord.rtcust) rtcust, ord_line.prtnum, sd.shpqty,  (nvl(ord_line.vc_cust_untcst ,0) * sd.shpqty) dlramt
from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop, adrmst lookup1,
trlr tr,car_move cr
where i.ship_line_id            =  sd.ship_line_id
AND ord.st_adr_id               = lookup1.adr_id
AND ord.client_id               = lookup1.client_id
AND ord.ordnum                  = ord_line.ordnum
AND ord.client_id               = ord_line.client_id
AND ord_line.client_id          = sd.client_id
AND ord_line.ordnum             = sd.ordnum
AND ord_line.ordlin             = sd.ordlin
AND ord_line.ordsln             = sd.ordsln
AND sd.ship_id                  = sh.ship_id
AND sh.stop_id                  = stop.stop_id
AND stop.car_move_id            = cr.car_move_id
AND cr.trlr_id                  = tr.trlr_id
AND sh.shpsts                   = 'C'
AND ord.ordtyp                  != 'W'
AND trunc(tr.dispatch_dte) =  trunc(sysdate) - 1
group
by ord.btcust, trunc(tr.dispatch_dte),
ord.cponum,
 ord.ordnum,sh.ship_id, ord_line.ordlin, ord.stcust, ord.rtcust, ord_line.prtnum, sd.shpqty, vc_cust_untcst
/
