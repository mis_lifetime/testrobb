/* #REPORTNAME=User Print Preticketing Labels Report */
/* #REPORTTYPE=DCSCMD */
/* #HELPTEXT= Requested by Order Management. */
/* #VARNAM=cstnum , #REQFLG=y */
/* #VARNAM=prtnum , #REQFLG=y */
/* #VARNAM=Qty    , #REQFLG=y */

select distinct vc_pretck, cstnum from cstprq where cstnum ='&1' | get label format where format = @usr_pretck and devcod = @@devcod and prtnum ='&2' and cstnum = @cstnum|produce label where  quantity = '&3' and netcod = '1'  and prtadr = 'E'
