/* #REPORTNAME=User Shipment by Date Range Report for PFZ */
/* #HELPTEXT= The Shipped Orders Summary by Date */
/* #HELPTEXT= yeilds a summary of shipped orders.*/
/* #HELPTEXT= It requires a date range entered in the*/
/* #HELPTEXT= form DD-Mon-YYYY.*/

/* #VARNAM=loddte , #REQFLG=Y */
/* #VARNAM=loddte1 , #REQFLG=Y */

ttitle left  print_time -
       center 'User Shipment By Date Range for PFZ: ' -
       right print_date skip -
       center ' &1 '  to  ' &2 ' skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno 

column ordnum heading 'Order #' just center
column ordnum format A15
column cponum heading 'PO Number'
column cponum format A30
column ship_id heading 'Ship ID'
column ship_id format A10
column shpdte heading 'Ship Date'
column shpdte format A12
column stname heading 'Customer Name'
column stname format A30
column cityst heading 'Destination'
column cityst format A30
column prtnum heading 'Item #'
column prtnum format A20
column prtdsc heading 'Item Description'
column prtdsc format A30
column upccod heading 'UPC #'
column upccod format A20
column shpqty heading 'Quantity'
column shpqty format 999999



set pagesize 2000
set linesize 500


select rtrim(ltrim(sd.ordnum)) ordnum,
       rtrim(ltrim(ord.cponum)) cponum,
       rtrim(ltrim(sh.ship_id)) ship_id, 
       to_char(tr.dispatch_dte, 'DD-MON-YYYY') shpdte,
       rtrim(adrmst.adrnam) stname,
       rtrim(adrmst.adrcty)||', '||adrmst.adrstc cityst,
       substr(prd.lngdsc,1,30) prtdsc,
       rtrim(ltrim(pr.prtnum)) prtnum,
       rtrim(ltrim(ord_line.shpqty)) shpqty,
       rtrim(ltrim(pr.upccod)) upccod
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line,stop,
        trlr tr,car_move cr, prtmst pr, prtdsc prd, adrmst
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.st_adr_id               = adrmst.adr_id
        AND ord.client_id               = adrmst.client_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND ord_line.prtnum             = pr.prtnum
        AND pr.prt_client_id            = '----'
        AND prd.colnam (+)              = 'prtnum|prt_client_id'
        AND prd.locale_id (+)           = 'US_ENGLISH'
        AND prd.colval (+)              = pr.prtnum||'|----'
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                 != 'W'
        AND tr.dispatch_dte between to_date('&&1','DD-MON-YYYY') and
                      to_date('&&2','DD-MON-YYYY')+.99999
        AND ord.btcust in ('H2707100','L4537500','113014')
group
    by tr.dispatch_dte,
       sd.ordnum,
       sd.ordlin,
       ord.cponum,
       sh.ship_id,
       ord.cponum,
       prd.lngdsc,
       pr.prtnum,
       ord_line.shpqty,
       pr.upccod,
       adrmst.adrnam,
       adrmst.adrcty,
       adrmst.adrstc
order
    by tr.dispatch_dte, sh.ship_id, sd.ordnum;
