/* #REPORTNAME=ENG Total Quantity Per Area Report */
/* #HELPTEXT= Ths report generates counts of items per area. */
/* #HELPTEXT= Requested by Engineering.*/

ttitle left  print_time -
       center 'ENG Total Quantity Per Area : ' -
       right print_date skip -
btitle skip 1 center 'Page: ' format 999 sql.pno 

column counts heading 'Item Counts' 
column counts format 999999999
column arecod heading 'Area' 
column arecod format a12

select count(distinct(prtnum)) counts, arecod from invsum
where arecod in ('GWALL','CFPP','TOWERFCC','TOWERFCP','TOWERREP','RACKSPCK')
group by arecod;
