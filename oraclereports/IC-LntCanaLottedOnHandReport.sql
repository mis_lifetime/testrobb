/* #REPORTNAME=IC LNT Cana Lotted On-Hand Report */
/* #HELPTEXT= This report displays the item, lot code and  available quantity */
/* #HELPTEXT= on select items with CANA and NOLOT lot codes*/
/* #HELPTEXT= Requested by Inventory Control */


 ttitle left  print_time center 'IC LNT Cana Lotted On-Hand Report' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Item'
column prtnum format a12
column lotnum heading 'Lot Code'
column lotnum format a8
column untqty heading 'On Hand '
column untqty format 999999
column pndqty format 999999

set pagesize 300
set linesize 200


select a.prtnum, a.lotnum, nvl(sum(b.untqty),0) untqty
from intcanalot a, invdtl b
where a.prtnum = b.prtnum
and a.lotnum = b.lotnum
group by a.lotnum, a.prtnum
union
select a.prtnum, a.lotnum, 0 untqty        
from intcanalot a 
where not exists(select 'x' from invdtl b
where a.prtnum = b.prtnum
and a.lotnum = b.lotnum)
group by a.lotnum, a.prtnum
/
