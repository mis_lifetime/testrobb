
set linesize 300
set pagesize 50000

alter session set nls_date_format = 'dd-mon-yyyy';

/* #REPORTNAME=User Problem Locations Report */
/* #HELPTEXT= This report lists all pertinent inventory information */
/* #HELPTEXT= that is stuck in locations entitled PROB01 */
/* #HELPTEXT= and PROBLEM */
/* #HELPTEXT= Requested by Senior Management */

/* #GROUPS=NOONE */

/* Requested by Rick Geiger - February 2002 */

ttitle  left print_date     print_time skip  2 - 
left 'Problem Location' skip 2 -
btitle  center 'Page: ' format 999 sql.pno

column stoloc heading 'Loc.'
column stoloc format a20
column prtnum heading 'Item'
column prtnum format a11
column lodnum heading 'Pallet'
column lodnum format a20
column invsts heading 'Status'
column invsts format a6
column adddte heading 'Date' 
column adddte format a12
column untqty heading 'Qty' 
column untqty format 9999999



select distinct  
rtrim(locmst.stoloc) stoloc , 
rtrim(invdtl.prtnum) prtnum, 
rtrim(invlod.lodnum) lodnum, rtrim(invdtl.invsts) invsts ,
rtrim(trunc(invdtl.adddte)) adddte,  sum(invdtl.untqty) untqty 
from
locmst, invlod, invsub, invdtl   
where
locmst.stoloc in ('PROB01', 'PROBLEM')
and  locmst.stoloc = invlod.stoloc
and trunc(invdtl.adddte) < trunc(sysdate) - 1 
and  invlod.lodnum = invsub.lodnum
and  invsub.subnum = invdtl.subnum
and invdtl.subnum not like 'CTN%'
group by
locmst.stoloc,invdtl.prtnum,
invlod.lodnum,
invdtl.invsts,invdtl.adddte


/
