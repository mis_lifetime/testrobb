/* #REPORTNAME=User Locations Without Subucc Report */
/* #HELPTEXT= This report provides the lodnum and location for  */
/* #HELPTEXT= any loads without a subucc and valid work reference, */
/* #HELPTEXT= where the location is on the CONVEYOR. */
/* #HELPTEXT= Requested by Order Management - January 4, 2005 */

/* Author: Al Driver */
/* This report is to provide the visibility of loads */
/* without subuccs that are sitting systematically on  */
/* the conveyor. */

ttitle left print_time -
       center 'User Locations Without Subucc Report' -
       right print_date skip 1 -
btitle center 'Page: ' format 999 sql.pno

column lodnum heading 'Pallet ID'
column arecod format  a20
column stoloc heading 'Location'
column stoloc format  a20

set pagesize 50
set linesize 100

select distinct invlod.lodnum,invlod.stoloc 
from invsub,invlod,invdtl
where invdtl.subnum = invsub.subnum
and invsub.lodnum = invlod.lodnum
and invsub.subucc is null
and (invdtl.wrkref is null or not exists (select 'x' from pckwrk where pckwrk.wrkref = invdtl.wrkref))
and stoloc = 'CONVEYOR'
/
