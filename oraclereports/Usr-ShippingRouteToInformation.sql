/* #REPORTNAME=User Shipping Route To Information */
/* #HELPTEXT= User should enter Route To Customer to  */
/* #HELPTEXT= obtain information */
/* #HELPTEXT= Requested by Shipping */

/* #VARNAM=Route-To-Customer, #REQFLG=Y */


/* Author: Denise Miller */

/* This report was created to assist shipping with gathering information */
/* on large shipments */

column cponum heading 'PO Number'
column cponum format a20
column ship_id heading 'Ship ID'
column ship_id format a14
column btcust heading 'Bill To Cust#'
column btcust format a20
column rtcust heading 'Route To Cust#'
column rtcust format a20
column adrnam heading 'Route to Customer'
column adrnam format a30
column shpsts heading 'Status'
column shpsts format a20
column NUMCTN heading '# Ctns'
column NUMCTN format 999999
column weight heading 'Weight'
column weight format 999999.99
column cube heading 'Cube'
column cube format 999999.9999
column early_shpdte heading 'Early Ship Date'
column early_shpdte format A15
column Cancel_Date heading 'Cancel Date'
column Cancel_Date format A11
column vc_extdte heading 'Extension Date'
column vc_extdte format A15

alter session set nls_date_format ='MM/DD/YYYY';

set linesize 500
set pagesize 5000

select a.cponum, a.ship_id, a.btcust, a.rtcust, substr(b.adrnam,1,30) adrnam, c.lngdsc shpsts, sum(a.carton_count) NUMCTN, sum(a.weight) weight,
   sum(a.volume) cube, a.early_shpdte, a.late_shpdte Cancel_date, a.vc_extdte
from (SELECT ord.btcust,
       ord.cponum,
       shipment_line.ship_id, 
       shipment.shpsts, ord.rtcust, ord_line.early_shpdte, 
       ord_line.late_shpdte, ord_line.vc_extdte,
       decode(pckwrk.lodlvl,'S',1,0) carton_count,
       decode(pckwrk.lodlvl,'S',((pckwrk.pckqty/prtmst.untcas)*prtmst.netwgt),(floor(pckwrk.pckqty/prtmst.untcas)*prtmst.netwgt+mod(
pckwrk.pckqty,prtmst.untcas)*prtmst.vc_grswgt_each)) weight,
       decode(pckwrk.lodlvl, 'S', decode(pckwrk.wrktyp, 'K', 
pckwrk.pckqty*ctnmst.ctnlen*ctnmst.ctnwid*ctnmst.ctnhgt/pckwrk.untcas/1728, 
pckwrk.pckqty*ftpmst.caslen*ftpmst.caswid*ftpmst.cashgt/pckwrk.untcas/1728), 0) volume  
FROM pckwrk,  
     prtmst,  
     ord_line,  
     ord, 
     prtmst prtmst_temp, 
     shipment,
     shipment_line,
     ctnmst,
     ftpmst
WHERE prtmst.prt_client_id (+) = pckwrk.prt_client_id
  and prtmst.prtnum (+)= pckwrk.prtnum 
  and pckwrk.ftpcod = ftpmst.ftpcod
  and ctnmst.ctncod (+)= pckwrk.ctncod
  and pckwrk.ship_line_id = shipment_line.ship_line_id 
  and prtmst_temp.prtnum = ord_line.prtnum 
  and prtmst_temp.prt_client_id = ord_line.prt_client_id
  and ord_line.ordnum = shipment_line.ordnum 
  and ord_line.client_id = shipment_line.client_id
  and ord_line.ordlin = shipment_line.ordlin 
  and ord_line.ordsln = shipment_line.ordsln 
  and ord_line.client_id = ord.client_id
  and ord_line.ordnum = ord.ordnum 
  and shipment.ship_id = shipment_line.ship_id
  and ord.rtcust= '&1'
ORDER BY shipment_line.ship_id) a, adrmst b, dscmst c
where a.btcust=b.host_ext_id
and c.colnam='shpsts'
and c.locale_id='US_ENGLISH'
and a.shpsts=c.colval
group by cponum, ship_id, btcust, rtcust, lngdsc, early_shpdte, late_shpdte, vc_extdte, adrnam
/
