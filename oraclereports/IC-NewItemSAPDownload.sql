/* #REPORTNAME=IC New Item SAP Download */
/* #HELPTEXT= This report will list the Item SAP Download */
/* #HELPTEXT= by Date                               */
/* #HELPTEXT= Requested by Inventory Control             */

/* #VARNAM=Begin-Date , #REQFLG=Y */
/* #VARNAM=End-Date , #REQFLG=Y */



 ttitle left '&1' print_time center 'IC New Item SAP Download Report ' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Item'
column prtnum format a12
column lngdsc heading 'Description'
column lngdsc format a30
column upccod heading 'UPC Code'
column upccod format a20
column ndccod heading 'NDC Code'
column ndccod format a20
column ftpcod heading 'Footprint'
column ftpcod format a30
column vc_prdcod heading 'Brand'
column vc_prdcod format a5
column untpak heading 'Unit/Pack'
column untpak format 9999999999
column untcas heading 'Unit/Case'
column untcas format 9999999999
column untcst heading 'Unit Cost'
column untcst format 9999999999.99
column vc_itmgrp heading 'Item Group'
column vc_itmgrp format a10
column vc_itmtyp heading 'Item Type'
column vc_itmtyp format a10
column moddte heading 'Date Came Down'
column moddte format a15

alter session set nls_Date_format ='dd-mon-yyyy';
set linesize 200

select a.prtnum,substr(b.lngdsc,1,30) lngdsc, a.upccod, a.ndccod, a.ftpcod, a.vc_prdcod, a.untpak,
a.untcas, a.untcst, a.vc_itmgrp, a.vc_itmtyp, a.moddte
from prtmst a, prtdsc b
where trunc(a.moddte)  between '&&1' and '&&2'
and a.prtnum||'|----' =b.colval
and a.mod_usr_id ='slIFDG_0'
and b.colnam ='prtnum|prt_client_id'
and b.locale_id ='US_ENGLISH'
/
