/* #REPORTNAME=Usr-Last Activity Report */
/* #HELPTEXT= This report lists the last activity   */
/* #HELPTEXT= for all items in our system. */
/* #HELPTEXT= Enter Dates in format DD-MON-YYYY. */
/* #HELPTEXT= Requested by Senior Management */

/* #VARNAM=created_dte , #REQFLG=N */
/* #VARNAM=last_used_dte , #REQFLG=N */
/* #VARNAM=qty_on_hand , #REQFLG=N */

/* Author: Al Driver */
/* Report lists last activity for any items in */
/* inventory. If no parameters are input, the report */
/* will return information on all parts.  */
/* For any parameters input by the user, the report will search */
/* for that value and anything lower than or older. */
/* The usr_shp_hdr table is used to record information */
/* on when a part was either last shipped or used in production. */

ttitle left  print_time -
       center 'Last Activity Report' -
       right print_date skip 2 -

btitle center 'Page: ' sql.pno



column prtnum heading 'Item#'
column prtnum format  a14
column lngdsc heading 'Description'
column lngdsc format  a30
column typcod heading 'ItmTyp'
column typcod format  a7
column itmgrp heading 'ItmGrp'
column itmgrp format  a10
column prdcod heading 'ItmCls'
column prdcod format a10
column dis heading 'Disc?'
column dis format a5
column untcst heading 'Cost'
column untcst format 999999.99
column invsts heading 'ItemSts'
column invsts format a7 
column parm_on_hand heading 'OnHandQty'
column parm_on_hand format  9999999
column arecod heading 'StorageArea'
column arecod format a11
column parm_created_dte heading 'CreatedDte'
column parm_created_dte format a12
column lstrcv heading 'LastRecdDte'
column lstrcv format a12
column parm_last_usedte heading 'LastUseDate'
column parm_last_usedte format a12

set linesize 400
set pagesize 5000 

alter session set nls_date_format ='DD-MON-YYYY';

select a.prtnum,substr(b.lngdsc,1,30) lngdsc,vc_itmtyp typcod,a.vc_itmgrp itmgrp,a.vc_prdcod prdcod,
decode(substr(a.vc_prdcod,2,1),'X','Y','N') dis,
a.untcst ,c.invsts, nvl(sum(c.untqty -c.comqty),0) parm_on_hand ,c.arecod,trunc(a.moddte) parm_created_dte
,trunc(a.lstrcv) lstrcv,usrGetLastActivityDate(trunc(d.lstshp),
trunc(d.lstprd)) parm_last_usedte from
    prtmst a,prtdsc b,invsum c,usr_shp_hdr d
    where a.prtnum = d.prtnum(+)
    and d.prtnum = c.prtnum(+)
    and  a.prtnum|| '|----' = b.colval
    and b.colnam='prtnum|prt_client_id'
    and b.locale_id ='US_ENGLISH'
    and c.invsts is not null
and decode('&&1',null,decode(trunc(a.moddte),null,trunc(sysdate),trunc(a.moddte)),trunc(a.moddte))
 <= decode('&&1',null,trunc(sysdate),'&&1')
having nvl(usrGetLastActivityDate(trunc(d.lstshp),trunc(d.lstprd)),decode('&&2',null,trunc(sysdate),null))
<= decode('&&2',null,trunc(sysdate),'&&2')
and nvl(sum(c.untqty -c.comqty),decode('&&3',null,0,null)) 
<= nvl('&&3',100000000)
group by a.prtnum,b.lngdsc,vc_itmtyp,a.vc_itmgrp,a.vc_prdcod,decode(substr(a.vc_prdcod,2,1),'X','Y','N'),a.untcst
  ,c.invsts,c.arecod,trunc(a.moddte),trunc(a.lstrcv),trunc(d.lstshp),trunc(d.lstprd)
union
select a.prtnum, substr(b.lngdsc,1,30) lndgsc, c.vc_itmtyp,c.vc_itmgrp,c.vc_prdcod,
decode(substr(c.vc_prdcod,2,1),'X','Y','N') dis, c.untcst, a.invsts, nvl(sum(a.untqty-a.comqty),0) quantity, a.arecod, 
trunc(c.moddte) parm_created_dte,trunc(c.lstrcv) lstrcv, trunc(a.new_expire_dte) parm_last_usedte
from invsum a, prtdsc b, prtmst c
     where a.prtnum in (select prtnum from invdtl where wrkref is null and ship_line_id is null
     and lstdte is null)
     and a.prtnum not in (select prtnum from usr_shp_hdr)
     and a.prtnum|| '|----' = b.colval
     and b.colnam='prtnum|prt_client_id'
     and a.prtnum=c.prtnum
     and b.locale_id ='US_ENGLISH'
     and a.invsts is not null
     and decode('&&1',null,decode(trunc(c.moddte),null,trunc(sysdate),trunc(c.moddte)),trunc(c.moddte))
      <= decode('&&1',null,trunc(sysdate),'&&1')
having nvl(sum(a.untqty -a.comqty),decode('&&3',null,0,null)) 
<= nvl('&&3',100000000)
group by a.prtnum, b.lngdsc, c.vc_itmtyp, c.vc_itmgrp, c.vc_prdcod, c.untcst, 
a.invsts, a.arecod, trunc(c.moddte), trunc(c.lstrcv), trunc(a.new_expire_dte)
/
