/* #REPORTNAME =IC Empty Locations In Area Code Change Report */

column stoloc heading 'Location'
column stoloc format A15
column locsts heading 'Status'
column locsts format A5
column srcare heading 'Source Area'
column srcare format A15
column dstare heading 'Destination Area'
column dstare format A16

set pagesize 1000;
set linesize 200;



select
   locmst.stoloc, decode(locmst.locsts,'E','Empty') locsts, 
   var_loc_chg.srcare, var_loc_chg.dstare 
from locmst, var_loc_chg 
where locmst.stoloc = var_loc_chg.stoloc
   and locmst.locsts='E' and locmst.useflg = 1 and 
   locmst.curqvl = 0 and locmst.pndqvl = 0
and not exists(select 'x' from invsum 
where var_loc_chg.stoloc = invsum.stoloc)
order by locmst.stoloc
/
