/* #REPORTNAME=IC Ti Hi Report */
/* #HELPTEXT= Requested by Inventory Control. */


ttitle left print_time center 'IC Ti Hi Report' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Part Number'
column prtnum format a12
column caslvl heading 'Cases/Level'
column caslvl format 99999
column caspal heading 'Cas/Pal'
column caspal format 999999
column tier heading '# of Levels'
column tier format 9999999


select a.prtnum, b.caslvl, (a.untpal/a.untcas) caspal,
((a.untpal/a.untcas)/b.caslvl) tier  from prtmst a, ftpmst b
where a.prtnum=b.ftpcod
and a.prt_client_id ='----'
and a.untcas != 0
/
