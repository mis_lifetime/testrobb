/* #REPORTNAME=User Items Shipped By Date Range Report */
/* #HELPTEXT= This report provides all items and unit */
/* #HELPTEXT= quantities shipped for desired date range. */
/* #HELPTEXT= Requires user to enter date in the format DD-MON-YYYY. */
/* #HELPTEXT= Requested by Shipping */

/* #VARNAM=Item , #REQFLG=Y */
/* #VARNAM=from , #REQFLG=Y */
/* #VARNAM=to , #REQFLG=Y */

ttitle left  print_time -
       center 'User Items Shipped By Date Range Report: ' -
       right print_date skip 2 -

btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Item #'
column prtnum format A30
column ship_id heading 'Ship ID'
column ship_id format A10
column shpqty heading 'Ship Qty'
column shpqty format 999999999 
column shpdte heading 'Ship Date'
column shpdte format A11

alter session set nls_date_format = 'DD-MON-YYYY';

set linesize 150 
set pagesize 10000

break on report 
compute sum label 'Report Total' of shpqty on report 

select ord_line.prtnum,sh.ship_id, sum(sd.shpqty) shpqty,trunc(tr.dispatch_dte) shpdte
 from  shipment_line sd, shipment sh, ord, ord_line, stop,
        car_move cr,trlr tr
        WHERE ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts     ||'' = 'C'
        AND ord.ordtyp                   != 'W'
        AND ord_line.prtnum = '&&1'
        AND trunc(tr.dispatch_dte) between '&&2' and '&&3'
group by prtnum,sh.ship_id,trunc(tr.dispatch_dte)
union
select ord_line.prtnum,sh.ship_id, sum(sd.shpqty) shpqty,trunc(tr.dispatch_dte) shpdte
 from  shipment_line@arch sd, shipment@arch sh, ord@arch, ord_line@arch, stop@arch,
        car_move@arch cr,trlr@arch tr
        WHERE ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts     ||'' = 'C'
        AND ord.ordtyp                   != 'W'
        AND ord_line.prtnum ='&&1'
        AND trunc(tr.dispatch_dte) between '&&2' and '&&3'
group by prtnum,sh.ship_id,trunc(tr.dispatch_dte)
order by 4
/
