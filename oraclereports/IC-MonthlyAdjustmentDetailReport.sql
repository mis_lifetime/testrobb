/* #REPORTNAME=IC Monthly Adjustment Detail Report */
/* #VARNAM=From , #REQFLG=Y */
/* #VARNAM=To , #REQFLG=Y */
/* #HELPTEXT= Please enter date in dd-mon-yyyy format */
/* #HELPTEXT = Requested by Inventory Control */
/* #HELPTEXT = July , 2003 */

-- * #GROUPS=NOONE */

ttitle left - 
       center 'IC Monthly Adjustment Detail Report ' -
       right h_print_date skip 1 -
       center 'from &&1  to &&2' skip 2

column prtnum heading 'Item'
column prtnum format a20
column lngdsc heading 'Reason'
column lngdsc format a50
column reacod heading 'Type'
column reacod format a30
column actcod heading 'Activity Code'
column actcod format a15
column adjqty heading 'Adj Quantity'
column adjqty format 999,999,990
column fr_arecod heading 'From Area'
column fr_arecod format a25
column frstol heading 'From Location'
column frstol format a25
column to_arecod heading 'To Area'
column to_arecod format a25
column tostol heading 'To Location '
column tostol format a25
column usr_id heading 'User Id'
column usr_id format a15
column dtlnum heading 'Detail Number'
column dtlnum format a20
column trndte heading 'Tran Date'
column trndte format a25
column adjamt heading 'Adj Amount'
column adjamt format 999,999,990.00

alter session set nls_date_format ='dd-mon-yyyy HH:MI:SS AM';

set linesize 300
set pagesize 10000

select a.prtnum, a.lngdsc, 
case when a.reacod in ('Audited Loc-Errored','Audited Loc-Mis','Audited Loc-Multi Pa','Audited Loc-Rack',
'AUDIT','Aisle Sweep','Bulk-1 Overage','Bulk-1 Shortage','MISPCK','SUBS','CONCEAL',
'INB OVERAGE','INB SHORT','Prod-Misidentified','Shipping-Lost Inv','Outbound Short Shipm',
'LostInShipping-Pal','LostInShipping-Pro','LostInShipping-CFPP','LostInShipping-Ca',
'LostInShipping-Other','Tower Clean Up','TowerRackQuiz','Conveyor-System Issu') then 'Operational Adjustment'
when a.reacod in ('AUTHDISP','DAMAGE','OBSOLETE','PROB01','GIVEAWAY') then 'Authorized Disposal'
when a.reacod in ('Cust Return-Replace','CUSTRTRN','Cust Return-Supplies') then 'Customer Returns/Replacements'
when a.reacod in ('EMPLOYEE THEFT','EMPORD','CANC','PID','DUMMY','PTKT','ORDER MGMT-MODIFY CP',
'ERROR','Prod-Reverse BOM','Receiving-Overage','Receiving-Shortage','Supplies-CFPP',
'Supplies-PROD','XFERWEST','DATAERROR','VEND RTN','Samples','TRANS','Intercompany Transfe') then 'Non Operational Adjustment'
when a.reacod in ('CYCLE1','CYCLE2','CYCLE3','CYCLE4','CYCLE5','CYCLE6','CYCLE7') then 'Cycle Count Adjustments'
else ' '
end reacod, a.adjqty, b.arecod fr_arecod, a.frstol, c.arecod to_arecod, 
a.tostol, a.usr_id, a.dtlnum, a.trndte, a.adjamt
from usr_monthly_adjs a, locmst b, locmst c
where
a.trndte between  to_date('&1'||' 12:00:00 AM')  and to_date('&2'||' 11:59:59 PM')
and a.frstol=b.stoloc
and a.tostol=c.stoloc
order by 10; 
