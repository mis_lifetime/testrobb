/* #REPORTNAME=9CE Payroll Data */
/* #VARNAM=Department, #REQFLG=N */
/* #VARNAM=Start-Pay-Date,  #REQFLG=N */
/* #VARNAM=End-Pay-Date, REQFLG=N */
/* #VARNAM=Password, REQFLG=Y */

/* #HELPTEXT= This report lists all requested employee information */
/* #HELPTEXT= The date should be entered in a dd-mon-yyyy format */

ttitle left print_time -
center '9CE Payroll Data' -
right print_date skip 2


column dept heading 'Department'
column dept format a12
column jobtitle heading 'Job Code'
column jobtitle format A15
column period heading 'Pay Date'
column period format a12
column reghrs heading 'Reg. Hrs'
column reghrs format 9,999.99
column regearn heading 'Reg. Earnings'
column regearn format  999,999.99
column othrs heading 'O.T. Hrs'
column othrs format 9,999.99
column otearn heading 'O.T. Earnings'
column otearn format 999,999.99
column grossearn heading 'Gross Earnings'
column grossearn format 999,999.99
column grosspay heading 'Gross Pay'
column grosspay format 999,999.99


set linesize 500
set pagesize 300

alter session set nls_date_format ='dd-mon-yyyy';


select dept, period, usr_hrdata.filenum, usr_hrdata.empname, jobcode, sum(reghrs) reghrs, sum(regearn) regearn, sum(othrs) othrs,
sum(bereavehrs) bereavehrs, sum(holhrs) holhrs, sum(jurydutyhrs) jurydutyhrs,sum(medhrs) medhrs,
sum(medreimbhrs) medreimbhrs,sum(retrohrs) retrohrs,sum(sickhrs) sickhrs,sum(sunhrs) sunhrs,
sum(vachrs) vachrs,sum(autohrs) autohrs,sum(reimbearn) reimbearn,sum(bonearn) bonearn,sum(bereavearn) bereavearn,
sum(holearn) holearn,sum(jurydutyearn) jurydutyearn,sum(medearn) medearn,sum(medreimbearn) medreimbearn,
sum(retroearn) retroearn,sum(sickearn) sickearn,sum(sunearn) sunearn,sum(vacaearn) vacaearn,sum(autoearn) autoearn,
sum(differencialpay) differencialpay,
sum(otearn) otearn, sum(grossearn) grossearn,sum(grosspay) grosspay
from usr_hrdata, usr_hrhdr
where usr_hrdata.filenum = usr_hrhdr.filenum
and usr_hrdata.empname = usr_hrhdr.empname
and dept =upper('&1')
and trunc(period) between  '&2' and '&3'
and upper('&4') ='PATTYCAKES'
group by dept, period, usr_hrdata.filenum,usr_hrdata.empname,jobcode
union
select dept, period, usr_hrdata.filenum,usr_hrdata.empname, jobcode, sum(reghrs) reghrs, sum(regearn) regearn, sum(othrs) othrs,
sum(bereavehrs) bereavehrs, sum(holhrs) holhrs, sum(jurydutyhrs) jurydutyhrs,sum(medhrs) medhrs,
sum(medreimbhrs) medreimbhrs,sum(retrohrs) retrohrs,sum(sickhrs) sickhrs,sum(sunhrs) sunhrs,
sum(vachrs) vachrs,sum(autohrs) autohrs,sum(reimbearn) reimbearn,sum(bonearn) bonearn,sum(bereavearn) bereavearn,
sum(holearn) holearn,sum(jurydutyearn) jurydutyearn,sum(medearn) medearn,sum(medreimbearn) medreimbearn,
sum(retroearn) retroearn,sum(sickearn) sickearn,sum(sunearn) sunearn,sum(vacaearn) vacaearn,sum(autoearn) autoearn,
sum(differencialpay) differencialpay,
sum(otearn) otearn, sum(grossearn) grossearn,sum(grosspay) grosspay
from usr_hrdata, usr_hrhdr
where usr_hrdata.filenum = usr_hrhdr.filenum
and usr_hrdata.empname = usr_hrhdr.empname
and upper('&1') is null
and trunc(period) between  '&2' and '&3'
and upper('&4') ='PATTYCAKES'
group by dept, period,usr_hrdata.filenum,usr_hrdata.empname,jobcode
/
