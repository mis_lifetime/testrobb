
set linesize 450
set pagesize 50000

alter session set nls_date_format = 'DD-MON-YYYY';

/* #REPORTNAME=IC Oversized Items Report */
/* #HELPTEXT= This report lists all requested */
/* #HELPTEXT= information regarding oversized inventory */
/* #HELPTEXT= Requested by Inventory Control - August 2003 */
/* #GROUPS=NOONE */

/* Author: Al Driver */
/* Report simply retrieves requested info for oversized items */
/* Oversized items defined as items with caslen or caswid > 47 */
/* found in footprint master */

ttitle  left print_date     print_time center 'IC Oversized Items Report' ski  -
btitle  center 'Page: ' format 999 sql.pno

column stoloc heading 'Loc.'
column stoloc format a20
column prtnum heading 'Item'
column prtnum format a11
column prtfam heading 'Part Family'
column prtfam format  a12
column invsts heading 'Status'
column invsts format a6
column untqty heading 'Qty' 
column untqty format 9999999
column lngdsc heading 'Description'
column lngdsc format a30
column caslen heading 'Case Length'
column caslen format 9999.99 
column caswid heading 'Case Width'
column caswid format 9999.99 
column cashgt heading 'Case Height'
column cashgt format 9999.99 
column pallet heading 'Pallet ID'
column pallet format a20

select a.prtnum,substr(b.lngdsc,1,30) lngdsc,c.stoloc stoloc,
c.untqty untqty,e.lodnum pallet,c.invsts invsts,a.prtfam prtfam,
d.caslen caslen,d.caswid caswid,d.cashgt cashgt
from prtmst a,prtdsc b, invsum c,ftpmst d,invlod e
where a.prtnum = c.prtnum
and a.prtnum||'|----' = b.colval
and b.colnam='prtnum|prt_client_id'
and b.locale_id ='US_ENGLISH'
and a.prtnum = d.ftpcod
and a.prt_client_id = c.prt_client_id
and a.prt_client_id = '----'
and c.stoloc = e.stoloc
and (d.caslen > 47 or d.caswid > 47)
order by a.prtnum
/
