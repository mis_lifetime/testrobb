/* #REPORTNAME= QC - Items With Same NDC Code Report */
/* #HELPTEXT= This report lists all items */
/* #HELPTEXT= with the same NDC Code in DCS. */

column prtnum heading 'Item#'
column prtnum format a30
column ndccod heading 'NDC Code'
column ndccod format a20
column upccod heading 'UPC Code'
column upccod format a20
column onhqty heading 'On Hand Qty.'
column onhqty format 99999999


set linesize 200
set pagesize 500

select a.prtnum, a.ndccod, a.upccod, sum((b.untqty+b.pndqty)-b.comqty) onhqty
from prtmst a, invsum b, aremst c
where a.ndccod in (select ndccod
from prtmst
group by ndccod
having count(prtnum)>1)
and a.prtnum=b.prtnum(+)
and b.arecod=c.arecod
and c.fwiflg=1
group by a.prtnum, a.ndccod, a.upccod
union
select distinct a.prtnum, a.ndccod, a.upccod, 0 onhqty
from prtmst a
where a.ndccod in (select ndccod
from prtmst
group by ndccod
having count(prtnum)>1)
and prtnum not in (select a.prtnum
from prtmst a, invsum b, aremst c
where a.ndccod in (select ndccod
from prtmst
group by ndccod
having count(prtnum)>1)
and a.prtnum=b.prtnum(+)
and b.arecod=c.arecod
and c.fwiflg=1)
order by ndccod
/
