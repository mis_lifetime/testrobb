/* #REPORTNAME=User Label Check Out */
/* #HELPTEXT= This report provides all information pertaining  */
/* #HELPTEXT= to the Label Checkout. */

/* #VARNAM=ShipId , #REQFLG=Y */
/* #VARNAM=SubUcc  , #REQFLG=Y */
 
ttitle left  print_time -
       center 'User Label Check Out Report: ' -
       right print_date skip 2 -

btitle skip 1 center 'Page: ' format 999 sql.pno

column ship_id heading 'Ship ID'
column ship_id format A15
column subucc heading 'Subucc'
column subucc format A30 
column srcloc heading 'Location'
column srcloc format A15
column usr_id heading 'User'
column usr_id format A15
column ackdte heading'Date/Time'
column ackdte format A20

alter session set nls_Date_Format ='dd-mon-yyyy HH24:MI:SS';

set linesize 300 
set pagesize 1000

select pckwrk.ship_id, pckwrk.srcloc, pckwrk.subucc, var_lblseq.usr_id, var_lblseq.ackdte
 from  pckwrk, var_lblseq
        WHERE pckwrk.wrkref = var_lblseq.wrkref and var_lblseq.usr_id is not null
        AND pckwrk.ship_id ='&&1' and '&&2' is null
union
select pckwrk.ship_id, pckwrk.srcloc, pckwrk.subucc, var_lblseq.usr_id, var_lblseq.ackdte                   
 from  pckwrk, var_lblseq
        WHERE pckwrk.wrkref = var_lblseq.wrkref and var_lblseq.usr_id is not null 
        AND pckwrk.subucc ='&&2' and '&&1' is null
order by 1
/
