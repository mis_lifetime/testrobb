/* #REPORTNAME=IC Pkg by Location Report */
/* #HELPTEXT= This report lists all PKG item types and their locations.*/
/* #HELPTEXT= Requested by Inventory Control.*/

 ttitle left '&1' print_time center 'IC Pkg by Location Report ' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno

column arecod heading 'Area'
column arecod format a10
column stoloc heading 'Location'
column stoloc format a10
column prtnum heading 'Item'
column prtnum format a12
column lngdsc heading 'Description'
column lngdsc format a30
column vc_itmtyp heading 'Item Type'
column vc_itmtyp format a9
column untcst heading 'Unit Cost'
column untcst format 999999
column untqty heading 'On Hand'
column untqty format 999999
column comqty heading 'Committed'
column comqty format 999999
column invsts heading 'Status'
column invsts format 'a2'

set linesize 200
set pagesize 9000


select invsum.arecod,
invsum.stoloc,
invsum.prtnum,
substr(prtdsc.lngdsc,1,30) lngdsc,
prtmst.vc_itmtyp,
prtmst.untcst,
nvl(invsum.untqty,0) untqty,
invsum.comqty,
invsum.invsts
from
invsum, locmst, prtmst, prtdsc
where invsum.arecod = locmst.arecod
and invsum.stoloc = locmst.stoloc
and invsum.prtnum = prtmst.prtnum
and prtmst.prtnum||'|----' = prtdsc.colval
and prtdsc.colnam = 'prtnum|prt_client_id'
and prtdsc.locale_id ='US_ENGLISH'
and prtmst.prt_client_id ='----'
and prtmst.vc_itmtyp = 'PKG'
order by invsum.stoloc
/
