/* #REPORTNAME=IC KitchenAid Returns By Date Report */
/* #VARNAM=From , #REQFLG=Y */
/* #VARNAM=To , #REQFLG=Y */
/* #HELPTEXT= Please enter date in dd-mon-yyyy format */
/* #HELPTEXT= This report will only display information */
/* #HELPTEXT= with Customer Return reason code  */

/* #HELPTEXT = Requested by Inventory Control */

-- * #GROUPS=NOONE */

ttitle left - 
       center 'IC KitchenAid Returns By Date Report ' -
       right h_print_date skip 1 -
       center 'from &1  to &2' skip 2


column prtnum heading 'Part Number'
column prtnum format a20
column lngdsc heading 'Description'
column lngdsc format a30
column reacod heading 'Reason'
column reacod format a20
column adjqty heading 'Adj Qty'
column adjqty format 999,990
column adjamt heading 'Return Cost'
column adjamt format 99,999,990.00
column trndte heading 'Tran Date'
column trndte format a15
column untcst heading 'Item Cost'
column untcst format 999,999
column vc_prdcod heading 'Prod Class'
column vc_prdcod format a6

alter session set nls_date_format ='dd-mon-yyyy';

select 
       a.prtnum, 
       substr(b.lngdsc,1,30) lngdsc,
       c.reacod,
       sum(c.trnqty) adjqty, trunc(c.trndte) trndte,
       sum(c.trnqty*nvl(a.untcst,0)) adjamt
  from prtdsc b, prtmst a, dlytrn c
where a.prtnum=c.prtnum
and substr(a.vc_prdcod,1,1) = 'K'
and c.reacod ='RTRN'
and a.prtnum||'|----' = b.colval
 and b.locale_id ='US_ENGLISH'
   and b.colnam = 'prtnum|prt_client_id'
   and c.actcod = 'ADJ' and c.prtnum != 'LABOR'
   and trunc(c.trndte) 
       between 
       '&1' and '&2'
 group
    by c.trndte, c.reacod, a.prtnum,
       b.lngdsc
having sum(c.trnqty) != 0
union
select
       a.prtnum,
       substr(b.lngdsc,1,30) lngdsc,
       c.reacod,
       sum(c.trnqty) adjqty, trunc(c.trndte) trndte,
       sum(c.trnqty*nvl(a.untcst,0)) adjamt
  from prtdsc@arch b, prtmst@arch a, dlytrn@arch c
where a.prtnum=c.prtnum
and substr(a.vc_prdcod,1,1) = 'K'
and c.reacod ='RTRN'
and a.prtnum||'|----' = b.colval
 and b.locale_id ='US_ENGLISH'
   and b.colnam = 'prtnum|prt_client_id'
   and c.actcod = 'ADJ' and c.prtnum != 'LABOR'
   and trunc(c.trndte)
       between
       '&1' and '&2'
 group
    by c.trndte, c.reacod, a.prtnum,
       b.lngdsc
having sum(c.trnqty) != 0
/
