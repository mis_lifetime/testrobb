/* #REPORTNAME = Usr-Cancelled Orders Report */
/* #HELPTEXT= This report lists the orders of $5000.00 or more */
/* #HELPTEXT= that have been cancelled within the date range given. */
/* #HELPTEXT= Enter date format as DD-MON-YYYY.*/
/* #HELPTEXT= Requested by Senior Management- Shipping */
/* #VARNAM=Begin-Date , #REQFLG=Y */
/* #VARNAM=End-Date , #REQFLG=Y */

/* Author: Al Driver */
/* Orders with a shpsts = 'B' are cancelled orders */

ttitle left  print_time -
       center 'User Cancelled Orders Report ' -
       right print_date skip -
       center '&&1 '  to  '&&2 ' skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column ordnum heading 'Order#'
column ordnum format A12 
column shpsts heading 'Status'
column shpsts format A10 
column cpodte heading 'Canceled Date'
column cpodte format A13
column amount heading 'Order Amount'
column amount format 9999999.99

alter session set nls_date_format ='DD-MON-YYYY';
 
set linesize 250
set pagesize 5000 

select distinct a.ordnum ordnum,decode(d.shpsts,'B','Cancelled','Unknown!') shpsts,trunc(a.cpodte) cpodte,
sum(b.ordqty * vc_cust_untcst) amount
from
ord a,ord_line b,shipment_line c,shipment d
where a.ordnum = b.ordnum
and a.client_id = b.client_id
and a.client_id = c.client_id
and a.rt_adr_id  = d.rt_adr_id
and b.ordnum = c.ordnum
and b.ordsln = c.ordsln
and b.ordlin = c.ordlin
and b.client_id = c.client_id
and c.ship_id = d.ship_id
and a.ordtyp = 'C'
and d.shpsts ||''= 'B'
and trunc(a.cpodte) between '&&1' and '&&2'
having sum(b.ordqty * vc_cust_untcst) >= 5000
group by a.ordnum,d.shpsts,trunc(a.cpodte)
order by trunc(a.cpodte)
/
