/* #REPORTNAME=User End Of Shift RF Report */
/* #HELPTEXT= This report provides the ship id, lodnum and location for  */
/* #HELPTEXT= any loads where the location is an RF gun (tel..) */
/* #HELPTEXT= Requested by Shipping - January 6, 2005 */

/* Author: Al Driver */
/* This report is to provide the visibility of inventory */
/* on the RF guns at the end of the day  */

ttitle left print_time -
       center 'User End Of Shift RF Report' -
       right print_date skip 1 -
btitle center 'Page: ' format 999 sql.pno

column lodnum heading 'Pallet ID'
column arecod format  a20
column stoloc heading 'Location'
column stoloc format  a20
column ship_id heading 'SHIP ID'
column ship_id format a10

set pagesize 50
set linesize 100

select distinct ship_id, invlod.lodnum,invlod.stoloc 
from shipment_line, invsub,invlod,invdtl
where shipment_line.ship_line_id = invdtl.ship_line_id 
and invdtl.subnum = invsub.subnum
and invsub.lodnum = invlod.lodnum
and stoloc like 'TEL%'
/
