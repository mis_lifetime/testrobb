/* #REPORTNAME=Usr- Deferred and Count In Progress Locations */
/* #HELPTEXT= This report lists the locations */
/* #HELPTEXT= that are in a deferred status or */
/* #HELPTEXT= currently have a count in progress */

column stoloc heading 'Location'
column stoloc format a20
column type heading 'Type'
column type format a20
column prtnum heading 'Part Number'
column prtnum format a20

set pagesize 300
set linesize 100

select a.stoloc, 'Deferred' type, nvl(b.prtnum, ' ') prtnum
from cntwrk a, invsum b
where a.cntsts='D'
and a.stoloc=b.stoloc(+)
union
select a.stoloc, 'In Progress' type, nvl(b.prtnum, ' ') prtnum
from locmst a, invsum b
where a.cipflg=1
and a.stoloc=b.stoloc(+);
