/* #REPORTNAME=User What's on a Label*/
/* #REPORTTYPE=DCSCMD */
/* #HELPTEXT= Displays What is on a Label*/ 
/* #HELPTEXT= Requested by Order Management */
/* #VARNAM=ctnnum , #REQFLG=Y */ 

/* The decodes are to try and get the correct value in the event of a split */

select pw.ctnnum, pw.srcloc, pw.prtnum, decode(pw.pckqty, pw.appqty, nvl(d.untqty, pw.appqty), pw.appqty) appqty,
       decode(pw.pckqty, pw.appqty, nvl(d.untqty, pw.pckqty), pw.pckqty) pckqty, pw.ordnum, pw.ship_id
from invdtl d, locmst l, pckwrk pw
where pw.ctnnum ='&1' and  l.stoloc=pw.srcloc
and d.wrkref (+)= pw.wrkref and d.subnum (+)= pw.ctnnum order by trvseq

/
