/* #REPORTNAME=ENG Palletizing by Item Report */
/* #HELPTEXT= Ths report generates palletizing locations by Item. */
/* #HELPTEXT= Requested by Engineering.*/
/* #HELPTEXT= Please enter date in  DD-Mon-YYYY  and time format .*/

/* #VARNAM=From , #REQFLG=Y */
/* #VARNAM=To , #REQFLG=Y */

ttitle left  print_time -
       center 'ENG Palletizing by Item: ' -
       right print_date skip -
       center ' &1 '  to  ' &2 ' skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno 

column prtnum heading 'Item' 
column prtnum format A12
column trnqty heading 'Qty' 
column trnqty format 999999
column frstol heading 'Location'
column frstol format A15
column untcst heading 'Unit Cost'
column untcst format 999.999
column total heading 'Total'
column total format 999999.99
column trndte heading 'Date'
column trndte format a20


set pagesize 500
set linesize 200

alter session set nls_date_format ='dd-mon-yyyy HH24:MI:SS';



select dlytrn.prtnum, sum(dlytrn.trnqty) trnqty,
dlytrn.frstol, dlytrn.trndte, prtmst.untcst, sum(dlytrn.trnqty) * prtmst.untcst total
from dlytrn, prtmst
where dlytrn.trndte between '&1' and '&2'
and dlytrn.frstol  in ('LANE001','LANE002','LANE003','LANE004','LANE005','LANE006',
'LANE007','LANE008','LANE009','LANE010','LANE011','LANE012','LANE013','LANE014','LANE015',
'LANE016','LANE017','LANE018','LANE019','LANE020','LANE021','LANE022','LANE023','LANE024',
'LANE025','LANE026','LANE027','LANE028','LANE029','LANE030')
and dlytrn.prtnum = prtmst.prtnum
and prtmst.prt_client_id ='----'
group by dlytrn.frstol, dlytrn.prtnum, prtmst.untcst, dlytrn.trndte;
