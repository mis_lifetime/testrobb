/* #REPORTNAME=ENG Expected Receipts By Date Report */
/* #HELPTEXT = Requested by Engineering */
/* #VARNAM=From , #REQFLG=Y */
/* #VARNAM=To ,   #REQFLG=Y */  

ttitle left  print_time -
       center 'ENG Expected Receipts By Date Report' -
       right print_date skip 1 -
btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Part Number'
column trknum heading 'PO #'
column trknum format a10 trunc
column expqty heading 'Exp|Qty'
column expqty format 9999999999
column expdte heading 'Expected Date'
column expdte format a15

alter session set nls_date_format ='dd-mon-yyyy';

SELECT rcvlin.prtnum,
       rcvlin.trknum,
       sum(rcvlin.expqty) ExpQty,
       rcvtrk.expdte
  FROM rcvlin, rcvtrk, prtmst, trlr
 WHERE trunc(rcvtrk.expdte) between  '&1' and '&2'
   AND rcvlin.trknum  = rcvtrk.trknum
   AND rcvtrk.trlr_id = trlr.trlr_id
   AND rcvlin.prtnum = prtmst.prtnum
   AND prtmst.prt_client_id = '----'
   and trlr.arrdte is null and rcvtrk.clsdte is null
 GROUP
    BY 
       rcvtrk.expdte, 
       rcvlin.trknum,
       rcvlin.prtnum
/
