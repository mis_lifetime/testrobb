/* #REPORTNAME=PI Variance Report By Location */
/* #VARNAM=Dollar-Threshold #REQFLG=N */
/* #VARNAM=Percent-Threshold #REQFLG=N */
/* #VARNAM=Order-By-Item #REQFLG=N */
/* #HELPTEXT= The Variance Report By Location lists */
/* #HELPTEXT= the differences in inventory quantities */
/* #HELPTEXT= by location, where the difference exceeds */
/* #HELPTEXT= the specified dollar or percentage Threshold */


/* 
** This report is intimately tied to the usrGetVarianceByLocationRecord.fnc PL/SQL function.
** Any change to that function will most likely require a change to this report
*/


set linesize 150
set pagesize 10000

ttitle left  print_time - 
       center 'Variance Report By Location' - 
       right print_date skip 2 - 
       left ' Dollar Threshold:  &1 ' skip 1 -
       left 'Percent Threshold:  &2 ' skip 1 -

btitle skip center 'Page: ' format 999 sql.pno skip 1

column record heading '                                                                       On-Hand     On-Hand|                                                               Case     Before      After                 Difference                Average|Location   Item #     Item Description               Lot       Pack     Phys.       Phys.      (Units)    (Percent)   (Dollars)   Cost/Unit'

/*
** Need to use 'distinct' here since the function returns NULL for records that do not match the criteria
** and that would result in multiple blank lines on the report
**
** If the user wants to order by Item, order by the Item # part of the the record string
*/

select distinct usrGetVarianceByLocationRecord
('phys_inv_snapshot', pi.stoloc, pi.prtnum,
pi.lotnum, pi.untpak, pi.untcas, sum(pi.untqty), '&1', '&2') record
from phys_inv_snapshot pi
where pi.gentyp = 'INITIAL'
group by pi.stoloc, pi.prtnum, pi.lotnum, pi.untpak, pi.untcas
union
select distinct usrGetVarianceByLocationRecord
('invsum', il.stoloc, id.prtnum, id.lotnum, id.untpak, id.untcas, sum(id.untqty), '&1', '&2') record
from invlod il, invdtl id, invsub iv, locmst, aremst
where aremst.fwiflg = 1
  and aremst.cntflg = 1
and id.subnum = iv.subnum
and iv.lodnum = il.lodnum 
and not exists(select 1 from phys_inv_snapshot up where il.stoloc = up.stoloc
and id.prtnum = up.prtnum
and id.lotnum = up.lotnum
and id.untcas = up.untcas
and up.gentyp = 'INITIAL'
and id.untpak = up.untpak)
and il.stoloc = locmst.stoloc
and locmst.arecod = aremst.arecod
group by il.stoloc, id.prtnum, id.lotnum, id.untpak, id.untcas

/* select distinct  usrGetVarianceByLocationRecord
('empties', cnthst.stoloc, decode(cnthst.prtnum,null,'EMPTY',cnthst.prtnum),
decode(cnthst.inval1,null,'EMPTY',cnthst.inval1),  0, 0, 0, '&1', '&2') record
from cnthst
where untqty = 0 and cntqty = 0
and not exists(select 'x' from cnthst a
              where cnthst.stoloc = a.stoloc
              and untqty > 0)
group by cnthst.stoloc, cnthst.prtnum, cnthst.inval1 */
/
