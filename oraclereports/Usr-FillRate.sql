/* #REPORTNAME=User Fill Rate */
/* #HELPTEXT= Returns order number, PO number, and order and*/
/* #HELPTEXT= ship quantities for the specific customer*/
/* #HELPTEXT= It requires a date range entered in the*/
/* #HELPTEXT= form DD-Mon-YYYY.*/
/* #HELPTEXT= Requested by Shipping */

/* #VARNAM=Customer , #REQFLG=Y */
/* #VARNAM=From , #REQFLG=Y */
/* #VARNAM=To , #REQFLG=Y */

column ordnum heading 'Order #'
column ordnum format a20
column cponum heading 'PO #'
column cponum format a30
column ordqty heading 'Order Qty.'
column ordqty format 999,999,999
column shpqty heading 'Ship Qty.'
column shpqty format 999,999,999
column fillrate heading 'Fill Rate'
column fillrate format 999.99

alter session set nls_date_format ='dd-mon-yyyy';
set pagesize 1000
set linesize 300

select a.ordnum, b.cponum, sum(a.ordqty) ordqty , sum(a.shpqty) shpqty,
round(((sum(a.shpqty)/sum(a.ordqty))*100),2) fillrate
from ord_line a, ord b
where a.ordnum=b.ordnum
and a.client_id='----'
and b.ordnum in (select distinct a.ordnum 
from ord a, shipment_line b, shipment c, stop d, car_move e, trlr f
where a.btcust='&1'
and a.ordnum=b.ordnum
and b.ship_id=c.ship_id
and c.stop_id=d.stop_id
and d.car_move_id=e.car_move_id
and e.trlr_id=f.trlr_id
and f.dispatch_dte between '&2' and '&3')
group by a.ordnum, b.cponum;
