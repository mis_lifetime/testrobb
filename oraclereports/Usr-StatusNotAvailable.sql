/* #REPORTNAME=User Inventory Not Available Status Report */
/* #HELPTEXT= This report displays inventory status information */
/* #HELPTEXT= where the status is not available */
/* #HELPTEXT = Requested by Senior Management */

ttitle left  print_time -
       center 'User Inventory Status Not Available Report' -
       right print_date skip 1 -
btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Part Number'
column prtnum format a15
column lngdsc heading 'Description'
column lngdsc format a30 
column invsts heading 'Inv. Status'
column invsts format a30
column untqty heading 'Qty'
column untqty format 999999
column comqty heading 'Committed'
column comqty format 999999
column pckable heading 'Pickable'
column pckable format a8
column prdcod heading 'Product Code'
column prdcod format a4
column itmtyp heading 'Item Type'
column itmtyp format a4
column untcst heading 'Unit Cost'
column untcst format 999,999,999.99
column extcst heading 'Extended Cost'
column extcst format 999,999,999.99

set pagesize 10000
set linesize 200


select a.prtnum, substr(b.lngdsc,1,30) lngdsc, substr(e.lngdsc,1,30) invsts, decode(d.pckflg,0,'N',1,'Y') pckable, 
c.vc_prdcod prdcod, c.vc_itmtyp itmtyp, sum(a.untqty) untqty,
sum(a.comqty) comqty, c.untcst, sum(a.untqty*c.untcst) extcst
from invsum a, prtdsc b, prtmst c, locmst d, dscmst e
where
a.invsts != 'A'
and a.prtnum = c.prtnum
and a.prt_client_id = c.prt_client_id
and a.arecod = d.arecod
and a.stoloc = d.stoloc
and a.prtnum||'|----' = b.colval
and b.colnam ='prtnum|prt_client_id'
and a.invsts=e.colval
and e.colnam='invsts'
and e.locale_id ='US_ENGLISH'
and b.locale_id ='US_ENGLISH'
group by a.prtnum, substr(e.lngdsc,1,30),substr(b.lngdsc,1,30), c.vc_prdcod, c.vc_itmtyp, d.pckflg, c.untcst
/
