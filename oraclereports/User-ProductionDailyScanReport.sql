/* #REPORTNAME=User Production Daily Scan Report */
/* #HELPTEXT= This report provides details on workorders   */
/* #HELPTEXT= closed out during a given date range. */
/* #HELPTEXT= User is required to use date format DD-MON-YYYY */
/* #HELPTEXT= Requested by Finance - January 11, 2005 */
/* #VARNAM=Begin-Date , #REQFLG=Y */
/* #VARNAM=End-Date , #REQFLG=Y */

/* Author: Al Driver */
/* Report provides detailed breakdown of production */
/* units produced. Should coincide with KPI report  */
/* Function usrGetWorkOrdType will provide the */
/* bucket that the workorder falls into (Rework, BOM, XBOM) */

ttitle left print_time -
       center 'User Production Daily Scan Report' -
       right print_date skip 1 -
btitle center 'Page: ' format 999 sql.pno

column wkonum heading 'Work Order'
column wkonum format  a20
column prdqty heading 'Production Qty'
column prdqty format  9999999 
column clsdte heading 'Closed Date'
column clsdte format a12
column prcloc heading 'Processing Location'
column prcloc format a20
column type heading 'Type'
column type format a10

alter session set nls_date_format = 'DD-MON-YYYY';

set pagesize 200
set linesize 200

break on report 

compute sum label 'Total' of prdqty on report 

select distinct wkohdr.wkonum,prdqty,trunc(clsdte) clsdte,prcloc,usrGetWorkOrdType(wkohdr.wkonum) type
from wkohdr,wkodtl,prtmst
where wkohdr.wkonum = wkodtl.wkonum
and wkohdr.wkorev = wkodtl.wkorev
and wkohdr.client_id = wkodtl.client_id
and wkohdr.prtnum = prtmst.prtnum
and wkosts = 'C'
and trunc(clsdte) between '&&1' and '&&2' 
union
select distinct wkohdr.wkonum,prdqty,trunc(clsdte) clsdte,prcloc,usrGetWorkOrdTypeArch(wkohdr.wkonum) type
from wkohdr@arch,wkodtl@arch,prtmst
where wkohdr.wkonum = wkodtl.wkonum
and wkohdr.wkorev = wkodtl.wkorev
and wkohdr.client_id = wkodtl.client_id
and wkohdr.prtnum = prtmst.prtnum
and wkosts = 'C'
and trunc(clsdte) between '&&1' and '&&2'
order by 3 
/
