/* #REPORTNAME=User Linens N' Things UPS Tracking Report */
/* #VARNAM=traknm , #REQFLG=Y */ 
/* #VARNAM=ship_id, #REQFLG=Y */
/* #VARNAM=doc_num, #REQFLG=Y */
/* #HELPTEXT= Report requires user to input a valid tracking # , SID */
/* #HELPTEXT= or waybill #. The report will display the SID, item,  */
/* #HELPTEXT= tracking # and or waybill# and */
/* #HELPTEXT= quantity for the parameters submitted. */

ttitle left  print_time -
		center 'User Linens N'' Things UPS Tracking Report' -
		right print_date skip 2 

column ship_id heading 'Shipment ID'
column ship_id format a12
column prtnum heading 'Item #'
column prtnum format a30
column traknm heading 'Tracking / Waybill #'
column traknm format a30 
column untqty heading 'Quantity' 
column untqty format 999999 

set linesize 100
set pagesize 100

select manfst.ship_id ship_id,invdtl.prtnum prtnum ,traknm ,sum(invdtl.untqty) untqty
from manfst,invsub,invdtl
where (manfst.traknm = '&&1' or manfst.ship_id = '&&2')
and manfst.subnum = invdtl.subnum
and invdtl.subnum = invsub.subnum
group by manfst.ship_id,invdtl.prtnum,traknm
union
select manfst.ship_id ship_id,invdtl.prtnum prtnum ,traknm ,sum(invdtl.untqty) untqty
from manfst@arch,invsub@arch,invdtl@arch
where (manfst.traknm = '&&1' or manfst.ship_id = '&&2')
and manfst.subnum = invdtl.subnum
and invdtl.subnum = invsub.subnum
group by manfst.ship_id,invdtl.prtnum,traknm
union
select sh.ship_id ship_id,invdtl.prtnum prtnum ,sh.doc_num ,sum(invdtl.untqty) untqty
from shipment_line sl,shipment sh,invsub,invdtl
where (sh.doc_num = '&&3' or sh.ship_id = '&&2')
and sh.ship_id = sl.ship_id
and sl.ship_line_id = invdtl.ship_line_id
and invdtl.subnum = invsub.subnum
group by sh.ship_id,invdtl.prtnum,doc_num
union
select sh.ship_id ship_id,invdtl.prtnum prtnum ,sh.doc_num ,sum(invdtl.untqty) untqty
from shipment_line@arch sl, shipment@arch sh,invsub@arch,invdtl@arch
where (sh.doc_num = '&&3' or sh.ship_id = '&&2')
and sh.ship_id = sl.ship_id
and sl.ship_line_id = invdtl.ship_line_id
and invdtl.subnum = invsub.subnum
group by sh.ship_id,invdtl.prtnum,doc_num
/
