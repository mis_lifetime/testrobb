/* #REPORTNAME=User Work Order Production Report */
/* #HELPTEXT= This report lists all requested */
/* #HELPTEXT= work order information  */
/* #HELPTEXT= Requested by Production */
/* #GROUPS=NOONE */



ttitle left print_time -
center 'Work Order Production Sheet ' -
right print_date skip 2 -

set pagesize 500
set linesize 440

column partnum heading 'Item #'
column prtnum format A30
column wkonum heading 'Work Order'
column wkonum format A24
column released heading 'Date Released'
column released format  999999
column prcloc  heading 'Lane'
column prcloc  format A6
column floor heading 'Loc.'
column floor format A5
column invsts heading 'Status'
column invsts format a6
column components heading 'Comp in Stock'
column components format  999999
column pickcount heading 'Pick Count Verified     '
column pickcount format 999999
column labor heading 'Labor'
column labor format 999999
column picked  heading '% Pcd'
column picked  format 9999
column verify heading 'Verified'
column verify format 999999
column countsign heading 'Pick Count Sign Off'
column countsign format 999999
column started heading 'Production Started'
column started format 999999
column time heading 'Time Started'
column time format 999999
column expected heading 'Expected Hrs To Complete'
column expected format 999999
column qtycom heading 'Qty Completed'
column qtycom format 999999
column prodcom heading 'Production Completed'
column prodcom format 999999
column unexpected heading 'Unexpected Residuals'
column unexpected format 999999
column signoff heading 'Signoff'
column signoff format 999999
column lotnum heading 'Lot'
column lotnum format A5
column entdte heading 'Date Entered'
column entdte format A15
column wkoqty heading 'Wko Qty'
column wkoqty format 999999
column prdqty heading 'Prod Qty'
column prdqty format 999999
column comments heading 'Comments'
column comments format 999999


alter session set nls_date_format ='dd-mon-yyyy';
select rtrim(wkohdr.prtnum) partnum, rtrim(wkohdr.wkonum) wkonum,
rtrim(wkohdr.entdte) entdte,
rtrim(wkohdr.prcloc) prcloc, '    ' floor,
(sum(wkodtl.tot_dlvqty)/sum(wkodtl.linqty) * 100)  picked,
sum(pckwrk.pckqty-pckwrk.appqty) verify,
'       ' labor,
rtrim(wkohdr.lotnum) lotnum,
wkohdr.wkoqty,
wkohdr.prdqty,
'                        ' comments
from wkohdr, wkodtl, pckwrk
where wkosts = 'I' and wkohdr.client_id = wkodtl.client_id
and wkohdr.wkonum = wkodtl.wkonum 
and wkodtl.wkonum = pckwrk.wkonum 
and wkohdr.wkorev = wkodtl.wkorev
group by
wkohdr.prtnum,
wkohdr.wkonum,
wkohdr.prcloc,
wkohdr.invsts,
wkohdr.lotnum,
wkohdr.entdte,
wkohdr.wkoqty,
wkohdr.prdqty
order by
wkohdr.prtnum;
