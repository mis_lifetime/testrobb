/* #REPORTNAME = ENG Daily Transactions By Date */
/* #VARNAM=trndte , #REQFLG=Y */
/* #HELPTEXT= This report lists all requested */
/* #HELPTEXT= information Daily Transactions by Date entered. */
/* #HELPTEXT= Requested by Engineering - October 2002 */


ttitle left  print_time -
       center 'ENG Daily Transactions By Date' -
       right print_date skip 1 -
       center 'Item: ' &1 skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

alter session set nls_date_format='dd-mon-yyyy';

column prtnum heading 'Part Number'
column prtnum format a12
column trndte heading 'Trn. Date'
column trndte format a12
column lodnum heading 'Load #'
column lodnum format a20
column oprcod heading 'Operation Code'
column oprcod format a15
column actcod heading 'Activity Code'
column actcod format a15
column movref heading 'Move Ref.'
column movref format a12
column trnqty heading 'Tran. Qty'
column trnqty format 999999
column frstol heading 'From Location'
column frstol format a20
column tostol heading 'To Location'
column tostol format a20
column usr_id heading 'User'
column usr_id format a10
column devcod heading 'Device Code'
column devcod format a10

set pagesize 5000;
set linesize 600;



SELECT
prtnum,
trndte,
lodnum,
oprcod,
actcod,
movref,
trnqty,
frstol,
tostol,
usr_id,
devcod
from dlytrn
where trunc(trndte) ='&1'
and actcod in ('CASPCK','GENMOV','GENRPL','MOV-INV','MOVSHIP','PALPCK','PCEPCK','PUT_DRCT')
/

