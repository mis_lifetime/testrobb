/* #REPORTNAME =IC Update Empty Locations In Area Code Change Report */

/******************************************************************************************/
/* This will update locations that were identified by the IC Empty Locations In Area Code */
/* Report                                                                                 */
/*****************************************************************************************/

column stoloc format A15
column locsts heading 'Status'
column locsts format A5
column srcare heading 'Source Area'
column srcare format A15
column dstare heading 'Destination Area'
column dstare format A16

set pagesize 1000;
set linesize 200;


update locmst 
set locmst.arecod=(select var_loc_chg.dstare from var_loc_chg 
where locmst.stoloc = var_loc_chg.stoloc and locmst.locsts ='E'
and locmst.curqvl = 0 and locmst.pndqvl = 0 and 
locmst.useflg = 1)
where exists(select 'x' from var_loc_chg b
where locmst.stoloc = b.stoloc
and locmst.locsts ='E' and locmst.curqvl = 0 and locmst.pndqvl = 0 and 
locmst.useflg = 1);
commit;

delete from var_loc_chg 
where var_loc_chg.stoloc =(select locmst.stoloc
from locmst where var_loc_chg.stoloc = locmst.stoloc
and locmst.locsts ='E' and locmst.curqvl = 0 and
locmst.pndqvl = 0 and locmst.useflg = 1);
commit;
/
