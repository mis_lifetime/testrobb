/* #REPORTNAME=User Total Forecasting Report */
/* #HELPTEXT= This information is combined with data from */
/* #HELPTEXT= Demand Solutions to provide forecasting for */
/* #HELPTEXT= Finished Goods and All items. To obtain the */
/* #HELPTEXT= forecast for finished goods, user must enter */
/* #HELPTEXT= FGFORECAST.  For the total forecast enter */
/* #HELPTEXT= TOTALFORECAST.  Requested by Inventory Control */
/* #VARNAM=in_type , #REQFLG=Y */
 
ttitle left print_time center 'User Total Forecasting Report' -
       right print_date skip 2 
btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Item'
column prtnum format a20
column primary heading 'Primary #'
column primary format a20
column lngdsc heading 'Description'
column lngdsc format a30
column untcst heading 'Unit Cost'
column untcst format 999999.999
column ftpcod heading 'Footprint'
column ftpcod format a30
column available heading 'DCS5 on Hand- Availabe-NOLOT'
column available format 999999
column availableo heading 'DCS5 on Hand- LOTTED and Non-Available'
column availableo format 999999
column vc_prdcod heading 'Prod|Class'
column vc_prdcod format a5
column vc_itmgrp heading 'Category'
column vc_itmgrp format a12
column division heading 'Division'
column division format a16
column brand heading 'Brand'
column brand format a16
column vc_itmcls heading 'BOM'
column vc_itmcls format a8
column vc_itmtyp heading 'Type'
column vc_itmtyp format a8
column reopnt heading 'Min'
column reopnt format 999999
column fullpallet heading 'Full Pallet Ship'
column fullpallet format a16 
column nonconvey heading 'Non Conv'
column nonconvey format a8 
column pcpick heading 'Pc. Pick'
column pcpick format a8
column towerpal heading 'Tower By Pallet'
column towerpal format a15 
column towercase heading 'Tower By Case'
column towercase format a13 
column fc heading 'FC As Blotted'
column fc format a13 
column altfc heading 'Alt # fc'
column altfc format a8 
column ttlfc heading 'Ttl fc incl alts'
column ttlfc format a16 
column prtfam heading 'Part Family'
column prtfam format a11
column total heading 'Total'
column total format 9999999
column january  heading 'January'
column january format 999999
column february heading 'February '
column february format 999999
column march heading 'March'
column march format 999999
column april heading 'April'
column april format 999999
column may heading 'May'
column may format 999999
column june heading 'June'
column june format 999999
column july heading 'July'
column july format 999999
column august heading 'August'
column august format 999999
column september heading 'September'
column september format 999999
column october heading 'October'
column october format 999999
column november heading 'November'
column november format 999999
column december heading 'December'
column december format 999999
column untpal heading 'Unt/Pallet'
column untpal format 999,999,999
column untcas heading 'Units/Carton'
column untcas format 999,999,999
column needed heading 'Pallets|Needed'
column needed format 999,999,999

set verify off
set linesize 700 
set pagesize 20000


select c.prtnum, d.altnum primary, nvl(substr(e.lngdsc,1,30), ' ') lngdsc, nvl(c.untcst,0) untcst, c.ftpcod, 
usrGetAvailQtyF(c.prtnum) available, usrGetAvailQtyO(c.prtnum) availableo,
rpad(c.vc_prdcod,5,' ') vc_prdcod, c.vc_itmcls, c.vc_itmgrp, nvl(f.division, ' ') division, nvl(f.brand, ' ' ) brand,
c.vc_itmtyp, nvl(c.reopnt,0) reopnt, ' 'fullpallet,' 'nonconvey,' 'pcpick,
' 'towerpal,' 'towercase,
' 'fc,' 'altfc,' 'ttlfc,
rpad(c.prtfam,11,' ') prtfam,
nvl(a.total,0) total,  nvl(a.january,0) january,
nvl(a.february,0) february,  nvl(a.march,0) march,  nvl(a.april,0) april,
nvl(a.may,0) may,  nvl(a.june,0) june, nvl(a.july,0) july,  nvl(a.august,0) august,
nvl(a.september,0) september,  nvl(a.october,0)
october,  nvl(a.november,0) november,
nvl(a.december,0) december, c.untcas,
c.untpal,nvl(a.total/decode(c.untpal,0,1,c.untpal),0) needed
from usr_totalforecast a, prtmst c, var_alt_prtnum d, prtdsc e, usr_classdtl f
where
c.prtnum=a.prtnum(+)
and c.prtnum = d.prtnum(+)
and c.vc_prdcod = f.vc_prdcod (+)
and c.prtnum||'|----' = e.colval
and e.colnam = 'prtnum|prt_client_id'
and e.locale_id='US_ENGLISH'
and upper('&&1') = 'FGFORECAST'
group by c.prtnum,  d.altnum, c.ftpcod, e.lngdsc, c.vc_prdcod, c.vc_itmcls,
c.vc_itmgrp, f.division, f.brand, c.vc_itmtyp, c.reopnt, c.prtfam,
a.total, a.january, a.february, a.march,
a.april, a.may, a.june, a.july, a.august,
a.september, a.october, a.november,
a.december, c.untcas, c.untpal, c.untcst;

select c.prtnum, d.altnum primary, nvl(substr(e.lngdsc,1,30), ' ')lngdsc, nvl(c.untcst,0) untcst, c.ftpcod, 
usrGetAvailQtyF(c.prtnum) available, usrGetAvailQtyO(c.prtnum) availableo,
rpad(c.vc_prdcod,5,' ') vc_prdcod, c.vc_itmcls, c.vc_itmgrp, nvl(f.division, ' ') division, nvl(f.brand, ' ' ) brand,
c.vc_itmtyp, nvl(c.reopnt,0) reopnt, ' 'fullpallet,' 'nonconvey,' 'pcpick,
' 'towerpal,' 'towercase,
' 'fc,' 'altfc,' 'ttlfc,
rpad(c.prtfam,11,' ') prtfam,
nvl(a.total,0) total,  nvl(a.january,0) january,
nvl(a.february,0) february,  nvl(a.march,0) march,  nvl(a.april,0) april,
nvl(a.may,0) may,  nvl(a.june,0) june, nvl(a.july,0) july,  nvl(a.august,0) august,
nvl(a.september,0) september,  nvl(a.october,0)
october,  nvl(a.november,0) november,
nvl(a.december,0) december, c.untcas,
c.untpal, nvl(a.total/decode(c.untpal,0,1,c.untpal),0) needed
from usr_totalforecast a, prtmst c, var_alt_prtnum d, prtdsc e, usr_classdtl f
where
c.prtnum=a.prtnum(+)
and c.prtnum = d.prtnum(+)
and c.vc_prdcod = f.vc_prdcod(+)
and c.prtnum||'|----' = e.colval
and e.colnam = 'prtnum|prt_client_id'
and e.locale_id='US_ENGLISH'
and upper('&&1') = 'TOTALFORECAST'
group by c.prtnum,  d.altnum, c.ftpcod, e.lngdsc, c.vc_prdcod, c.vc_itmcls,
c.vc_itmgrp, f.division, f.brand, c.vc_itmtyp, c.reopnt, c.prtfam,
a.total, a.january, a.february, a.march,
a.april, a.may, a.june, a.july, a.august,
a.september, a.october, a.november,
a.december, c.untcas, c.untpal, c.untcst
/
