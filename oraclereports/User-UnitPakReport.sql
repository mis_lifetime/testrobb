/* #REPORTNAME=User-Unit Pack Report */
/* #HELPTEXT= This report lists all items where the */
/* #HELPTEXT= unit pack in replen config is different  */
/* #HELPTEXT= from the unit pack found in part maintenance. */
/* #HELPTEXT= Requested by Mike Gouveia -Inventory Operations */

 ttitle left  print_time center 'User- Unit Pack Report ' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Part Number'
column prtnum format A30 
column stoloc heading 'Location'
column stoloc format A20 
column vc_untpak heading 'Replen Config Unit Pack'
column vc_untpak format 999999
column untpak heading 'Part Maint Unit Pack'
column untpak format 999999

set linesize 200 
set pagesize 100

select a.prtnum,stoloc,a.vc_untpak,b.untpak
           from rplcfg a, prtmst b
           where a.prtnum = b.prtnum
           and a.vc_untpak <>  b.untpak
/
