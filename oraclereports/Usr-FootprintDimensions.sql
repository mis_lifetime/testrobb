/* #REPORTNAME=User Footprint Dimensions Report */
/* #HELPTEXT = This report lists items and their dimensions  */
/* #HELPTEXT = Requested by Senior Management */


ttitle left print_time center 'User Footprint Dimensions Report' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Item'
column prtnum format a12
column lngdsc heading 'Description'
column lngdsc format a38
column caslen heading 'Master Length'
column caslen format 999999
column caswid heading 'Master Width'
column caswid format 999999
column cashgt heading 'Master Height'
column cashgt format 999999



select a.prtnum, c.lngdsc, b.caslen, b.caswid, b.cashgt
from prtmst a, ftpmst b, prtdsc c
where a.prtnum = b.ftpcod
and a.prt_client_id = '----'
and a.prtnum||'|----' = c.colval
and c.colnam = 'prtnum|prt_client_id'
and c.locale_id ='US_ENGLISH'
order by a.prtnum
/
