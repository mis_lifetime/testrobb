/* #REPORTNAME=User BOM120 */
/* #HELPTEXT= This report lists all  */
/* #HELPTEXT= BOM information */
/* #HELPTEXT= Requested by Production */
/* #GROUPS=NOONE */

set pagesize 10000
set linesize 200 


ttitle left print_time -
center 'BOM120 ' -
right print_date skip 2 -


column bomnum heading 'BOM'
column domnum format A30
column lngdsc heading 'Description'
column lngdsc format  A30
column prtnum heading 'Prtnum'
column prtnum format A20
column cnsqty heading 'Consumed   '
column cnsqty format 999999.99

alter session set nls_date_format ='dd-mon-yyyy HH24:MI:SS';


select bomdtl.bomnum, bomdtl.prtnum, substr(prtdsc.lngdsc,1,30) lngdsc,
       bomdtl.cnsqty
       from bomdtl, prtdsc
       where bomdtl.bomnum ||'|----' = prtdsc.colval(+)
       and bomdtl.client_id = '----'
       and prtdsc.locale_id ='US_ENGLISH'
       group by bomdtl.bomnum, prtdsc.lngdsc, bomdtl.prtnum, bomdtl.cnsqty
/
