/* #REPORTNAME=ENG Inventory Status Report */
/* #HELPTEXT= This report generates # of items, pieces and locations */
/* #HELPTEXT= on R, W, H  or D status */ 
/* #HELPTEXT= Requested by Engineering.*/

ttitle left  print_time -
       center 'ENG Inventory Status Report : ' -
       right print_date skip -
btitle skip 1 center 'Page: ' format 999 sql.pno 

column prtnum heading '# of Items' 
column prtnum format 999,999,999
column pieces heading '# of Pieces' 
column pieces format 9,999,999,999
column locations heading '# of Locations'
column locations format 999,999,999
column status heading 'Status'
column status format a6

select count(distinct(a.prtnum)) prtnum, sum(a.untqty-a.comqty) pieces, 
count(distinct(a.stoloc)) locations, a.invsts status
from invsum a, invdtl b, invsub c, invlod d
where a.invsts in ('R','W','D','F','O','C','B','L','T')
and a.stoloc = d.stoloc
and d.lodnum = c.lodnum
and c.subnum = b.subnum
group by a.invsts
having sum(a.untqty) != 0
/
