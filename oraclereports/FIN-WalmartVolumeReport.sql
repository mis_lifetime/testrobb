/* #REPORTNAME=FIN-Walmart Volume Report */
/* #VARNAM=begin_date , #REQFLG=Y */
/* #VARNAM=end_date , #REQFLG=Y */
/* #HELPTEXT= Report provides total received and shipped */
/* #HELPTEXT= items, units, dollars and volume for all Walmart */
/* #HELPTEXT= items for the selected date range. YTD and In-stock */
/* #HELPTEXT= data also provided. */
/* #HELPTEXT= Date must be entered in format DD-MON-YYYY */
/* #HELPTEXT= Requested by Inventory Control */


/* Author: Al Driver */
/* This report pulls all summary information from the usr_wmvolume */
/* table. This table is updated each morning, with the receiving and  */
/* shipping information for walmart items. This data is designated with */
/* either a type 'RCV' or 'SHP'. The in-stock data is re-written each */
/* day, and is designated with a type 'STK'. */
/* A package (FINWalVol.pkg) was created to hold the functions */
/* used to calculate the YTD totals for each category. */

ttitle left print_time center 'FIN-Walmart Volume Report from &&1 to &&2' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column title heading 'Category'
column title format a20
column items heading 'Total Unique Items'
column items format 999999999 
column files heading 'Total Items'
column files format 999999999
column units heading 'Total Units' 
column units format 999999999
column cartons heading 'Total Cartons'
column cartons format 999999999
column pallets heading 'Total Pallets'
column pallets format 99999999
column dollars heading 'Total $s'
column dollars format 999999999 
column qube heading 'Total Volume'
column qube format 999999999
column rack heading 'Total Rack Locations'
column rack format a20
column tower heading 'Total Tower Locations'
column tower format a20
column others heading 'Total Other Locations'
column others format a20
column item_ytd heading 'Total Unique Items YTD'
column item_ytd format a20
column file_ytd heading 'Total Items YTD'
column file_ytd format a20
column unit_ytd heading 'Total Units YTD'
column unit_ytd format a20
column cartons_ytd heading 'Total Cartons YTD'
column cartons_ytd format a20
column pallet_ytd heading 'Total Pallets YTD'
column pallet_ytd format a20
column dollar_ytd heading 'Total Dollars YTD'
column dollar_ytd format a20
column qube_ytd heading 'Total Volume YTD'
column qube_ytd format a20

alter session set nls_date_format ='DD-MON-YYYY';

set linesize 400


select 'Receiving Totals' title, nvl(count(distinct prtnum),0) items, nvl(count(prtnum),0) files, nvl(sum(units),0) units ,
nvl(sum(cartons),0) cartons, nvl(sum(lodnums),0) pallets, nvl(sum(dollars),0) dollars,
nvl(sum(qube),0) qube,NULL rack,NULL tower,NULL others,
to_char(FINWalmVol.WalmItemsYTD(type),'999999999') item_ytd,to_char(FINWalmVol.WalmFilesYTD(type),'999999999') file_ytd,
to_char(FINWalmVol.WalmUnitsYTD(type),'999999999') unit_ytd,to_char(FINWalmVol.WalmCartonsYTD(type),'999999999') cartons_ytd,
to_char(FINWalmVol.WalmPalletsYTD(type),'999999999') pallet_ytd,
to_char(FINWalmVol.WalmDollarsYTD(type),'999999999') dollar_ytd
,to_char(FINWalmVol.WalmVolumeYTD(type),'999999999') qube_ytd
   from usr_wmvolume
   where dte between '&&1' and '&&2'
   and type = 'RCV'
group by type
   union
select 'Shipping Totals' title, nvl(count(distinct prtnum),0) items, nvl(count(prtnum),0)  files, nvl(sum(units),0) units, 
nvl(sum(cartons),0) cartons, nvl(sum(lodnums),0) pallets, nvl(sum(dollars),0) dollars,
nvl(sum(qube),0) qube ,NULL rack,NULL tower,NULL others,
    to_char(FINWalmVol.WalmItemsYTD(type),'999999999') item_ytd,to_char(FINWalmVol.WalmFilesYTD(type),'999999999') file_ytd, 
  to_char(FINWalmVol.WalmUnitsYTD(type),'999999999') unit_ytd,to_char(FINWalmVol.WalmCartonsYTD(type),'999999999') cartons_ytd,
to_char(FINWalmVol.WalmPalletsYTD(type),'999999999') pallet_ytd,
to_char(FINWalmVol.WalmDollarsYTD(type),'999999999') dollar_ytd,
    to_char(FINWalmVol.WalmVolumeYTD(type),'999999999') qube_ytd
    from usr_wmvolume
    where dte between '&&1' and '&&2' 
    and type = 'SHP'
group by type
    union
select 'Total In-Stock' title, nvl(count(distinct prtnum),0) items, 0 files,  nvl(sum(units),0) units ,
nvl(sum(cartons),0) cartons, 
nvl(sum(lodnums),0) pallets, nvl(sum(dollars),0) dollars ,nvl(sum(qube),0) qube,
to_char(FINWalmVol.WalmRacks(type)) rack, to_char(FINWalmVol.WalmTowers(type)) tower, 
to_char(FINWalmVol.WalmOthers(type)) others,
NULL,NULL,NULL,NULL,NULL,NULL,NULL
   from usr_wmvolume
   where type = 'STK'
group by type
/

