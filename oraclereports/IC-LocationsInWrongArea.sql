
/* #REPORTNAME=IC All Incorrect Pallet Report */
/* #HELPTEXT= This report lists the counts for areas and part family */
/* #HELPTEXT= for RACKCOMP, RACKHIGH, RACKMED, RACKNC, RACKSPCK AND RACKSRES.*/
/* #HELPTEXT= Requested by Senior Management */


ttitle left print_time center 'IC All Incorrect Pallet Report' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column arecod heading 'Area'
column arecod format a10
column prtfam heading 'Part Family'
column prtfam format a12


select arecod, 
prtfam,
count(*)
from locmst lm,
invlod il, invsub isb, invdtl id, prtmst pm
where pm.prtnum=id.prtnum
and id.subnum=isb.subnum
and isb.lodnum=il.lodnum
and il.stoloc=lm.stoloc
and lm.arecod in ('RACKCOMP','RACKHIGH','RACKMED','RACKNC','RACKSPCK','RACKSRES')
group by arecod, prtfam
/
