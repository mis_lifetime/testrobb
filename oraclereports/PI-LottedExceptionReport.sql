/* #REPORTNAME=PI Lot Exception Report */
* #HELPTEXT= This report displays the item, lot code, available quantity, */
/* #HELPTEXT= committed quantity and pending quantity, */
/* #HELPTEXT= for lot codes other than                */
/* #HELPTEXT= NOLOT, ASCC, BBBB, CANA, CORN, FARB, */
/* #HELPTEXT= KROG, MACY, MBLK, MERV, ROSS, TUES, WALM, WILS, FOLEY,  */
/* #HELPTEXT= GREY, HMBS, KOHL, LINT, SEAR, TARG, WINN, JCPE.  */
/* #HELPTEXT= Requested by Inventory Control */


 ttitle left '&1' print_time center 'PI Lot Exception Report' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Item'
column prtnum format a12
column avlqty heading 'Available'
column avlqty format 999999
column comqty heading 'Comm. Qty'
column comqty format 999999
column pndqty heading 'Pending'
column pndqty format 999999
column stoloc heading 'Location'
column stoloc format a20


set pagesize 30000
set linesize 200

select 
       invdtl.prtnum,
       invdtl.lotnum, invlod.stoloc,
       sum(invdtl.untqty-invsum.comqty) avlqty,
       sum(invsum.comqty) comqty,
       sum(invsum.pndqty) pndqty
 from invsum, invdtl, invsub, invlod, aremst 
       where invdtl.lotnum not in ('NOLOT','ASCC','BBBB','CANA','CORN','FARB','KROG','MACY','MBLK','MERV','ROSS',
       'TUES','WALM','WILS','FOLEY','GREY','HMBS','KOHL','LINT','SEAR','TARG','WINN','JCPE')
        and invsum.prtnum = invdtl.prtnum
        and invsum.stoloc = invlod.stoloc
        and invsum.arecod = aremst.arecod
        and invsum.prt_client_id ='----'
        and invdtl.subnum = invsub.subnum
        and invsub.lodnum = invlod.lodnum
group by invdtl.prtnum, invdtl.lotnum, invlod.stoloc
/
