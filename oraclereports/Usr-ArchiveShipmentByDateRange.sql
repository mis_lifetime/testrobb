/* #REPORTNAME=User Archived Shipments by Date Range Report */
/* #HELPTEXT= Archived Shipped Orders Summary by Date */
/* #HELPTEXT= It requires a date range entered in the*/
/* #HELPTEXT= form DD-Mon-YYYY. Requested by Shipping. */

/* #VARNAM=loddte , #REQFLG=Y */
/* #VARNAM=loddt1 , #REQFLG=Y */

ttitle left  print_time -
       center 'User Archived Shipments  By Date Range: ' -
       right print_date skip -
       center ' &1 '  to  ' &2 ' skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno 

column ordnum heading 'Order No.' just center
column ordnum format A10 
column clsdte heading 'Close Date' just center
column clsdte format A12
column stname heading 'Customer Name'just center
column stname format A30
column cityst heading 'Destination' just center
column cityst format A25
column cponum heading 'PO Number' just center
column cponum format A20
column carcod heading 'Shipped VIA' just center
column carcod format A10 
column pronum heading 'PRO Number' just center
column pronum format A20
column usr_totctn heading 'CNT' just center
column usr_totctn format 9999990 
column waybil heading 'WayBill' just center
column waybil format a25
column dlramt heading 'Amt Shipped' just center
column dlramt format 999999.00

set pagesize 5000
set linesize 200


select rtrim(ltrim(sd.ordnum)) ordnum,
       rtrim(ltrim(sh.doc_num)) waybil,
       to_char(sh.loddte, 'DD-MON-YYYY') clsdte,
       rtrim(lookup1.adrnam) stname,
       rtrim(lookup1.adrcty)||', '||lookup1.adrstc cityst,
       rtrim(ltrim(ord.cponum)) cponum,
       rtrim(ltrim(sh.carcod)) carcod,
       rtrim(ltrim(sh.track_num)) pronum,
       count(distinct(i.subnum)) usr_totctn,
       sum(nvl(ord_line.vc_cust_untcst ,0) * i.untqty) dlramt
 from invdtl@arch  i, shipment_line@arch sd, shipment@arch sh,
 ord@arch ord, ord_line@arch ord_line, stop@arch stop, adrmst@arch  lookup1
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.st_adr_id               = lookup1.adr_id
        AND ord.client_id               = lookup1.client_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND sh.loddte between to_date('&&1','DD-MON-YYYY') and
                      to_date('&&2','DD-MON-YYYY')+.99999
group
    by sd.ordnum,
       sh.loddte,
       lookup1.adrnam,
       lookup1.adrcty,
       lookup1.adrstc,
       ord.cponum,
       sh.carcod,
       sh.track_num,
       sh.doc_num
order
    by sd.ordnum,
       lookup1.adrnam;
