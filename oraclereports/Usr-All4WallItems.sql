/* #REPORTNAME=Usr All 4 Wall Items */
/* #HELPTEXT= This report lists item number,  */
/* #HELPTEXT= location, area, quantitiy, status and added date */
/* #HELPTEXT= for all invenotry in a 4 Wall Area */
/* #HELPTEXT= for the entered item number. */

/* #VARNAM=prtnum , #REQFLG=Y */

ttitle left print_time center 'All 4 Wall Items' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno

column part heading 'Item #'
column part format a30
column loc heading 'Location'
column loc format a20
column area heading 'Area'
column area format a10
column qty heading 'Quantity'
column qty format 999999
column status heading 'Status'
column status format a10
column dte heading 'Date'
column dte format a10

alter session set nls_date_format ='MM/DD/YYYY';

set linesize 200
set pagesize 100

select a.prtnum part, a.stoloc loc, a.arecod area, sum(a.untqty) qty, a.invsts status, 
trunc(b.lstdte) dte
from invsum a, invdtl b, aremst c
where a.prtnum=b.prtnum
and a.prt_client_id=b.prt_client_id
and a.arecod=c.arecod
and c.fwiflg=1
and a.prtnum='&&1'
group by trunc(b.lstdte) ,a.prtnum, a.stoloc, a.arecod, a.invsts
/
