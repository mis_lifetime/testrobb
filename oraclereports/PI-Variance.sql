/* #REPORTNAME=PI Variance Report */
/* #VARNAM=Dollar-Threshold #REQFLG=N */
/* #VARNAM=Percent-Threshold #REQFLG=N */
/* #HELPTEXT= The Variance Report lists */
/* #HELPTEXT= the differences in inventory quantities */
/* #HELPTEXT= before and after the physical inventory operation. */

/* 
** This report is intimately tied to the UsrGetVarianceRecord.fnc PL/SQL function.
** Any change to that function will most likely result in a change to this report
*/



set linesize 132
set pagesize 9000

ttitle left  print_time - 
       center 'Variance Report' - 
       right print_date skip 2 - 
       left ' Dollar Threshold:  &1 ' skip 1 -
       left 'Percent Threshold:  &2 ' skip 2


btitle skip center 'Page: ' format 999 sql.pno skip 1

column record heading '                                          On-Hand     On-Hand|                                           Before      After                 Difference                Average|Item #    Item Description                  Phys.       Phys.      (Units)    (Percent)   (Dollars)   Cost/Unit'
column record format A112 trunc

select distinct UsrGetVarianceRecord
('phys_inv_snapshot', phys_inv_snapshot.prtnum, sum(phys_inv_snapshot.untqty), '&1', '&2') record
from phys_inv_snapshot
where phys_inv_snapshot.gentyp ='INITIAL' 
group by phys_inv_snapshot.prtnum
union 
select distinct UsrGetVarianceRecord ('invsum', id.prtnum, sum (id.untqty), '&1', '&2') record
  from invlod il, invsub ib, invdtl id, locmst l, aremst a
  where a.fwiflg = 1
    and a.cntflg = 1
    and a.arecod = l.arecod
    and l.stoloc = il.stoloc
    and il.lodnum = ib.lodnum
    and ib.subnum = id.subnum
    and not exists (select 1 from phys_inv_snapshot pi where pi.gentyp||'' = 'INITIAL' and pi.prtnum = id.prtnum)
 group by id.prtnum
/* union
select distinct  usrGetVarianceRecord
('empties', decode(cntwrk.prtnum,null,'EMPTY',cntwrk.prtnum),
0, '&1', '&2') record
from cntwrk
where untqty = 0 and cntqty = 0 and cntsts ='C'
and not exists(select 'x' from cntwrk a
              where cntwrk.prtnum = a.prtnum
              and untqty > 0)
group by cntwrk.prtnum */
/
