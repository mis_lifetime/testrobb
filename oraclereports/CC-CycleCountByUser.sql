set linesize 200
set pagesize 500

alter session set nls_date_format = 'dd-mon-yyyy';

/* #REPORTNAME=CC Cycle Count By User Report */
/* #HELPTEXT= This report lists all completed counts by Date and User */ 
/* #HELPTEXT= Please enter in DD-MON-YYYY date format */
/* #HELPTEXT= Requested by Inventory Control  - March 2003 */
/* #VARNAM=From , #REQFLG=Y */
/* #VARNAM=To , #REQFLG=Y */
/* #GROUPS=NOONE */


ttitle  left print_date     print_time center 'CC Cycle Count By User Report' skip  -
btitle  center 'Page: ' format 999 sql.pno

column adddte heading 'Date '
column adddte format a12
column cnt_usr_id heading 'User'
column cnt_usr_id  format a11
column untqty heading 'Qty' 
column count heading 'Completed Count'
column cntqty format 9999999
column area heading 'Area'
column area format a10

select trunc(a.cntdte) adddte, a.cnt_usr_id, count(distinct a.stoloc) count, b.arecod area
from usr_cnthst a,locmst b
where a.stoloc = b.stoloc(+) 
and a.cnttyp = 'C'
and a.cntmod = 'C' and exists (select 1 from usr_cnthst u where u.cntbat  =
a.cntbat and (u.cnttyp = 'F' and u.untqty >=0)  
and u.stoloc = a.stoloc)
and trunc(a.cntdte) between '&&1' and '&&2'
group by trunc(a.cntdte),a.cnt_usr_id, b.arecod
union
select trunc(a.cntdte) adddte, a.cnt_usr_id, count(distinct a.stoloc) count, b.arecod area
from usr_cnthst a,locmst b
where a.stoloc = b.stoloc(+) 
and a.cnttyp = 'E'
and a.cntmod = 'C' and exists (select 1 from usr_cnthst u where u.cntbat  =
a.cntbat and (u.cnttyp = 'B')  
and u.stoloc = a.stoloc)
and not exists (select 1 from usr_cnthst u where u.cntbat  =
a.cntbat and (u.cnttyp = 'F' and u.untqty >=0)  
and u.stoloc = a.stoloc)
and trunc(a.cntdte) between '&&1' and '&&2'
group by trunc(a.cntdte),a.cnt_usr_id, b.arecod;
