/* #REPORTNAME=ENG Trucks Closed By Date Report */
/* #VARNAM=Start-Date , #REQFLG=Y */
/* #VARNAM=End-Date , #REQFLG=Y */
/* #HELPTEXT= This report displays a listing of trucks by closed date. */
/* #HELPTEXT= Date should be entered in dd-mon-yyyy format. */
/* #HELPTEXT = Requested by Engineering */

ttitle left  print_time -
       center 'ENG Trucks Closed By Date Report' -
       right print_date skip 1 -
btitle skip 1 center 'Page: ' format 999 sql.pno

alter session set nls_date_format ='dd-mon-yyyy';

column trknum heading 'Truck #'
column trknum format a15
column trksts heading 'Status'
column trksts format a10
column clsdte heading 'Date Closed'
column clsdte format a14
alter session set nls_date_format ='dd-mon-yyyy';

SELECT rcvtrk.trknum,
       trlr.trlr_stat, 
       rcvtrk.clsdte
  FROM rcvlin, rcvtrk,trlr
 WHERE 
   rcvtrk.trknum = rcvlin.trknum
   and rcvtrk.trlr_id = trlr.trlr_id
   AND trlr.trlr_stat = 'C'
   AND trunc(rcvtrk.clsdte)  between '&1' and '&2'
union
SELECT rcvtrk.trknum,
       rcvtrk.trksts,
       rcvtrk.clsdte
  FROM rcvlin@arch, rcvtrk@arch
 WHERE 
   rcvtrk.trknum = rcvlin.trknum
   AND rcvtrk.trksts = 'C'
   AND trunc(rcvtrk.clsdte)  between '&1' and '&2'
/
