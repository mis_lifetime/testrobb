/* #REPORTNAME=ENG Daily Ticketing Report */
/* #VARNAM=trndte , #REQFLG=Y */
/* #VARNAM=trndte1 , #REQFLG=Y */
/* #HELPTEXT= This report lists all requested */
/* #HELPTEXT= information regarding Ticketing. */
/* #HELPTEXT= Requested by Engineering */
/* #GROUPS=NOONE */

set pagesize 50000
set linesize 160


ttitle left print_time -
center 'ENG Daily Ticketing Report Between &1 and &2 ' -
right print_date skip 2 -


column location heading 'Location'
column location format a30
column trnqty heading 'Qty'
column trnqty format 9999999999

alter session set nls_date_format ='dd-mon-yyyy HH24:MI:SS';


 select
  dlytrn.tostol location, sum(dlytrn.trnqty) trnqty
 FROM dlytrn
 WHERE dlytrn.trndte between  '&1' and '&2'
 AND  dlytrn.tostol             like 'TICKET%'
 group by dlytrn.tostol
/
