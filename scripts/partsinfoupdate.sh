#!/usr/bin/ksh
#
# This script will run the extra parts info update every day WB 6/6/11. 
#


#. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/scripts


sql << //
truncate table usr_partsinfo;
exit
//
/opt/mchugh/prod/moca/bin/mload -H -c /opt/mchugh/prod/les/db/data/load/partsinfo.ctl -D /opt/mchugh/prod/les/db/data/load/partsinfo -d partsinfo.csv

exit 0

