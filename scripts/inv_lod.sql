SQL> clear
SQL> select * from invlod where lodnum = '00000000000004684114';

LODNUM                         STOLOC                   LODWGT     PRMFLG
------------------------------ -------------------- ---------- ----------
    UNKFLG     MVLFLG ADDDTE    LSTMOV    LSTDTE    LSTCOD
---------- ---------- --------- --------- --------- ----------
LST_USR_ID                               LODUCC               UCCDTE
---------------------------------------- -------------------- ---------
PALPOS
--------------------
00000000000004684114           LANE031                                  1
         0          0 04-NOV-03
SLEVT_0



SQL> update invlod set stoloc = 'DR291' where lodnum = '00000000000004684114';

1 row updated.

SQL> comit;
SP2-0042: unknown command "comit" - rest of line ignored.
SQL> commit;

Commit complete.

SQL> select appqty, pckqty, cmbcod from invlod where ship_id = 'SID0306333';
select appqty, pckqty, cmbcod from invlod where ship_id = 'SID0306333'
                                                *
ERROR at line 1:
ORA-00904: invalid column name


SQL> select appqty, pckqty, cmbcod from shipment where ship_id = 'SID0306333';
select appqty, pckqty, cmbcod from shipment where ship_id = 'SID0306333'
                       *
ERROR at line 1:
ORA-00904: invalid column name


SQL> desc invlod;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 LODNUM                                    NOT NULL VARCHAR2(30)
 STOLOC                                    NOT NULL VARCHAR2(20)
 LODWGT                                             NUMBER(19,4)
 PRMFLG                                             NUMBER(1)
 UNKFLG                                             NUMBER(1)
 MVLFLG                                             NUMBER(1)
 ADDDTE                                    NOT NULL DATE
 LSTMOV                                             DATE
 LSTDTE                                             DATE
 LSTCOD                                             VARCHAR2(10)
 LST_USR_ID                                         VARCHAR2(40)
 LODUCC                                             VARCHAR2(20)
 UCCDTE                                             DATE
 PALPOS                                             VARCHAR2(20)

SQL> desc shipment;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 SHIP_ID                                   NOT NULL VARCHAR2(30)
 HOST_CLIENT_ID                                     VARCHAR2(10)
 HOST_EXT_ID                                        VARCHAR2(40)
 SHPSTS                                             VARCHAR2(1)
 STOP_ID                                            VARCHAR2(10)
 RT_ADR_ID                                 NOT NULL VARCHAR2(20)
 CARGRP                                             VARCHAR2(10)
 CARCOD                                             VARCHAR2(10)
 SRVLVL                                             VARCHAR2(10)
 SDDFLG                                             NUMBER(1)
 DOC_NUM                                            VARCHAR2(20)
 TRACK_NUM                                          VARCHAR2(20)
 FRTCHG                                             VARCHAR2(1)
 FRTCOD                                             VARCHAR2(6)
 FRTRTE                                             NUMBER(19,4)
 ADDDTE                                    NOT NULL DATE
 ALCDTE                                             DATE
 STGDTE                                             DATE
 LODDTE                                             DATE
 ENTDTE                                    NOT NULL DATE
 EARLY_SHPDTE                                       DATE
 LATE_SHPDTE                                        DATE
 EARLY_DLVDTE                                       DATE
 LATE_DLVDTE                                        DATE
 DSTARE                                             VARCHAR2(10)
 DSTLOC                                             VARCHAR2(20)
 RRLFLG                                             NUMBER(1)
 WAVE_SET                                           VARCHAR2(10)
 ARCDTE                                             DATE
 ARC_SRC                                            VARCHAR2(30)
 MODDTE                                             DATE
 MOD_USR_ID                                         VARCHAR2(40)
 VC_CALL_FLG                                        NUMBER(1)
 VC_PLAN_SHPDTE                                     DATE
 VC_WAVE_FLG                                        NUMBER(1)
 LST_PRTDTE                                         DATE
 TMS_GROUP_ID                                       VARCHAR2(10)
 SUPER_SHIP_FLG                                     NUMBER(1)
 SUPER_SHIP_ID                                      VARCHAR2(30)
 IN_TRANSIT_FLG                                     NUMBER(1)
 LBL_PRTDTE                                         DATE

SQL> select appqty, pckqty, cmbcod from pckwrk where ship_id = 'SID0306333';

    APPQTY     PCKQTY CMBCOD
---------- ---------- ----------
         2          2 CMB4673466
         2          2 CMB4714749

SQL> select * from pckmov where cmbcod in('CMB4673466', 'CMB4714749');

CMBCOD         SEQNUM ARECOD     STOLOC
---------- ---------- ---------- --------------------
RESCOD                                                 ARRQTY     PRCQTY
-------------------------------------------------- ---------- ----------
CMB4714749          2 FSTG       FSTG01
XXXX                                                        2          0

CMB4673466          2 FSTG       FSTG01
XXXX                                                        2          0


SQL> update pckmov set arecod = 'SSTG', stoloc = 'DR291' where cmbcod in('CMB4673466', 'CMB4714749');

2 rows updated.
:wq

