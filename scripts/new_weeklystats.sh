#!/usr/bin/ksh
#
# This script will run the Report at 5:30am using cron.
#


run_sql() {
    sql << //
    set trimspool on
    set pagesize 50000
    set linesize 300
    spool $2
    select to_char(sysdate, 'MM/DD/YYYY HH24:MI:SS') tstmp from dual;
    @ $1
    select to_char(sysdate, 'MM/DD/YYYY HH24:MI:SS') tstmp from dual;
    spool off
    exit
//
}

. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/oraclereports

run_sql Usr-OpenOrdsDetailReport.sql OpenSalesOrdersDetails.out &
run_sql Usr-OpenOrdsShipment.sql  OpenOrdersShipment.out &
run_sql Usr-PalletCountUpdate.sql PalletCountUpdate.out &
run_sql Usr-WeeklyStatsUpdate.sql WeeklyStatsUpdate.out &

wait

sql << //
set trimspool on
set verify off
column sysdte new_value 1 noprint
select decode(to_char(sysdate-1,'D'),1,to_char(sysdate-3,'DD-MON-YYYY'),
to_char(sysdate-1,'DD-MON-YYYY')) sysdte from dual;
spool /opt/mchugh/prod/les/oraclereports/DailyFlash.out
@FIN-DailyFlash.sql
spool off
exit
//
exit 0
