/*
 * $Id: clean_invalid.sql,v 1.1.1.1 2001/09/18 23:05:50 lh51sh Exp $
 * $Log: clean_invalid.sql,v $
 * Revision 1.1.1.1  2001/09/18 23:05:50  lh51sh
 * Initial Import
 *
# Revision 1.9  1996/08/09  18:41:02  dmp_sw
# freeze for V4_10
#
# Revision 1.8  1996/07/12  14:36:34  dmp_sw
# Converted script to create recomp.sql in current working directory
#
# Revision 1.7  1996/05/03  13:52:23  dmp_bg
# Set the echo to off and the feedback to a large number to get proper output.
#
# Revision 1.6  1996/04/19  14:12:30  dmp_sw
# freeze for V4_09
#
# Revision 1.5  1996/04/10  21:31:39  phx_bg
# Changed to compile only package bodies if the package body was invalid.
# Previously it would have compiled the entire package, header includede
# which could have invalidated other packages.
#
# Revision 1.4  1996/02/01  14:39:12  phx_sw
# freeze for V4_08
#
# Revision 1.3  1995/11/09  20:30:43  phx_sw
# freeze for V4_07
#
# Revision 1.2  1995/10/09  15:11:26  phx_dp
# freeze for V4_06
#
# Revision 1.1  1995/09/21  15:30:26  phx_bg
# Initial revision
#
# Revision 1.2  1995/09/18  14:05:44  phx_bg
# *** empty log message ***
#
# Revision 1.1  1995/09/18  14:05:19  phx_bg
# Initial revision
#
 *
 * clean_inv.sql - This is designed to run from the alter.sh script and
 *                 to find the INVALID objects, then recompile them.
 *
 * usage         - @clean <OBJECT_TYPE> ; use ALL for all object types
 *
 */

set pages 0
set verify off
set head off
set feedback 20000

spool recomp.sql

select 'ALTER ' || decode (object_type, 'PACKAGE BODY', 'PACKAGE', object_type) || ' ' || object_name || ' COMPILE'|| decode (object_type, 'PACKAGE BODY', ' BODY', null) ||';'
from user_objects
where status = 'INVALID'
and object_type = decode(upper('&1'), 'ALL', object_type, '&1')
order by object_type, object_name
/
spool off

@recomp.sql
! rm recomp.sql
