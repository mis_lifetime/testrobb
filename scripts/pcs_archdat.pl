#!/usr/local/bin/perl
#
# Dustin Radtke
# This file should include all database purges and archives
# A cron task or at job should be created to call this script
# once a day.
#
$| = 1;                   # Flush output

$cmd=<<EOF;                                
set trace where activate = 1 and trcfil = 'archive_purge_data.trc'
/
[update shipment set vc_plan_shpdte = trunc(sysdate-35) where vc_plan_shpdte is not null and shpsts = 'R' and vc_plan_shpdte < trunc(sysdate-35)]
/
archive production data where arc_nam = 'RECEIVE-TRUCK'
/
archive production data where arc_nam = 'WORK-ORDERS'
/
archive production data where arc_nam = 'WORK-HISTORY'
/
archive production data where arc_nam = 'DAILY-TRAN'
/
archive production data where arc_nam = 'ADJUSTMENT-HISTORY'
/
archive production data where arc_nam = 'DISPATCHED-TRAILERS'
/
archive production data where arc_nam = 'CANCELED-SHIPMENTS'
/
archive production data where arc_nam = 'NOT-TIED-TEMP-ADDRESS'
/
archive production data where arc_nam = 'DELETED-ORDERS'
/
remove seamles event where dayold = "35" and evt_id = 'VC_MNT-HEARTBEAT' and evt_stat_cd = 'SC'
/
remove seamles event where dayold = "35" and evt_id = 'VC_HEARTBEAT OUT' and evt_stat_cd = 'SC'
/
remove seamles event where dayold = "35" and evt_id = 'VC_MNT-DIVERT_CONFIRM' and evt_stat_cd = 'SC'
/
remove seamles event where dayold = "35" and evt_id = 'VC_MNT-SHIPSCAN' and evt_stat_cd = 'SC'
/
remove seamles event where dayold = "35" and evt_id = 'VC_MNT-SHIPSCAN1 and evt_stat_cd = 'SC'
/
sl_purge evt_data where older_than_days = 35 and evt_stat_cd_list = 'EE, SE, EN'
/
sl_purge dwnld where older_than_days = 35 and dwnld_stat_cd_list = 'EERR, EPROC'
/
sl_purge msg_log where older_than_days = 35
/
[truncate table srvusg]
/
EOF

$time = localtime;
print "$time \n $cmd";

open(MSQL,"| msql ") || die;
print MSQL $cmd;
close(MSQL);

exit(0);
