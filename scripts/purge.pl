#!/usr/local/bin/perl

########
# Notes
########

#
# Platform-independence and Compressors
#
# We want this script to be as "platform-independent" as possible.
# Earlier versions were tied to Win32 (actually, NT4's version of
# DOS) filesystem and date standards.  In this version, we use 
# Perl intrinsic functions for filesystem operations.
#
# File Path Formats
#
# Intrinsic Perl functions like to deal with pathnames containing the
# UNIX-style /.  However, we want our users to be able to enter the
# DOS-style \ if they are running under DOS.  We convert the paths
# automatically after we read them from the argument list. 
#
# Filesystem File Patterns
#
# We want the user to be able to enter filepaths and have pattern
# matching work as expected for the operating system.  For example,
# the user might want to enter "*xml*.log".  However, that's not
# a valid perl regular expression.  For perl, the expression would
# be ".*xml.*\.log" or something similar to that.  We assume that the
# user is entering a filesystem pattern and convert to a perl pattern.
# 


################
# Package setup
################

use DirHandle;
use strict 'vars';
use Getopt::Long;
use Env;


#################
# Constant setup
#################

my $set       = 1;
my $unset     = 0;
my $unset_num = -1;
my $blank     = "";

my $seconds_per_day = (60*60*24);


########################
# Variable declarations
########################

my $return       = $unset_num;
my $purge_dir    = $blank;
my $file_pattern = $blank;
my $purge_days   = $unset_num;
my $help         = $unset;
my $test         = $unset;
my $dir_deleted  = 0;
my $dh           = new DirHandle;

my $orig_file_pattern;

my $date;
my $time;

my $file;
my %FILES;


################################
# Handle command-line arguments
################################

$return = GetOptions("p=n"  => \$purge_days,
                     "d=s"  => \$purge_dir,
                     "f=s"  => \$file_pattern,
                     "t"    => \$test,
                     "help" => \$help);

if($return == 0)
{ 
   usage(); 
   exit 1;
}

if($help == $set)
{ 
   usage(); 
   exit 0;
}

if($purge_days == $unset_num)
{
   print "You have to enter the number of days to purge from.\n";
   exit 1;
}
if($purge_days < 0)
{
   print "The number of days has to be a positive number.\n";
   exit 1;
}
my $purge_time = time - ($seconds_per_day * $purge_days);

if($purge_dir eq $blank)
{
   print "You have to enter a directory to purge from.\n";
   exit 1;
}
else
{
   # Change \ to /.
   $purge_dir =~ s|\\|\/|g;
} 

if($file_pattern eq $blank)
{
   print "You have to enter the file pattern.\n";
   exit 1;
}
else
{
   # Convert from filesystem pattern to perl pattern.
   # This turns "*xml*.log" into ".*xml.*\.log".
   # Also, we can now search for xml?.log, etc.
   $orig_file_pattern = $file_pattern;
   $file_pattern =~ s|\.|\\.|g;
   $file_pattern =~ s|\*|\.\*|g;  
   $file_pattern =~ s|\?|.|g;
}


#####################
# Get date and time.
#####################

my ($sec,$min,$hour,$mday,$mon,$year,$yday,$isdst) = localtime(time);

if($mon+1 < 10){ $mon  = "0".($mon+1);}
if($mday  < 10){ $mday = "0".$mday;   }
if($hour  < 10){ $hour = "0".$hour;   }
if($min   < 10){ $min  = "0".$min;    }

$date = (1900+$year).$mon.$mday;
$time = $hour.$min;


###########################
# Print a starting message
###########################

print "\nStarting the purge process for dir ($purge_dir)...\n";
print "Date/time is ($date/$time).\n";
print "Files older than $purge_days days will be purged.\n";
print "Files matching pattern ($orig_file_pattern) will be purged.\n";


#################################
# Build a list of files to purge
#################################

if(!opendir($dh, "$purge_dir"))
{ 
   print "Unable to open directory $purge_dir\n";
   exit 1;
}

foreach $file (sort readdir($dh))
{
   my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,
       $size,$atime,$mtime,$ctime,$blksize,$blocs) = stat("$purge_dir/$file");

   # We want to make sure that the file pattern matches the 
   # WHOLE name, not just part of the name.
   if(-f "$purge_dir/$file" and $file =~ /^$file_pattern$/ and $mtime <= $purge_time)
   {           
      $FILES{$file} = "$file";
   }
}

closedir($dh);


##############################
# Purge the files in the list
##############################

foreach $file (keys %FILES)
{
   print "\n    *** Purging file ($file)...";
   if($test ne $set)
   {
      if(unlink("$purge_dir/$file"))
      {
         $dir_deleted++;
      }
      else
      {
         print "failed delete.\n";
         next;
      }
   }
   print "done";
} 


#######################
# Print ending message
#######################

if($dir_deleted > 0)
{
   print "\n\nCompleted.  A total of ($dir_deleted) files were purged.\n";
}
else
{
   print "\n\nCompleted.  No files were purged.\n";
}

exit 0;


###################
# Usage subroutine
###################

sub usage()
{
   print "\nUsage: \tpurge.pl -d purge-dir -f file-ptn -p purge-days [-t] [--help]\n";
   print "\n\t-d purge-dir  \t- Directory to purge files from\n";
   print "\t-f file-ptn    \t- File pattern specifying files to purge\n";
   print "\t-p purge-days  \t- Purge files older than a certain number of days\n";
   print "\t-t             \t- Test ONLY (show what would happen) \n";
   print "\t--help         \t- Show this message\n";
   print "\nPurges files matching the file-ptn in the purge-dir which are\n";
   print "older than purge-days old.  The purge-dir, file-ptn and purge-days\n";
   print "paramaters are required.\n";
   print "\nKeep in mind that you'll have to quote file patterns, i.e. -f \"*.log\"\n";
}

