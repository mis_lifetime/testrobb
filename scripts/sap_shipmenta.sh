#!/usr/bin/ksh

. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/oraclereports

#
#  This script will create the shipment report 
#  needed for Dave Fitzgerald and LiZhang.
#  

datevar=`date  +%m%d%Y`

sql << //
set trimspool on
set linesize 120
set pagesize 1000


spool /opt/mchugh/prod/les/files/hostout/RobbinsvilleShipment$datevar.txt

@SAP-ShipmentConfirmationByDateRange.sql

spool off
exit
//
exit 0

