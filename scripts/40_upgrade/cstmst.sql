insert into cstmst
(
CLIENT_ID,
CSTNUM,
ADR_ID,
BCKFLG,
PARFLG,
CARFLG,
SPLFLG,
STDFLG,
SHPLBL,
SHIPBY,
CARGRP,
MANFID,
DEPTNO,
ORDINV,
GRP_NAM,
VC_ASNFLG,
VC_PNPFLG,
VC_PPNFLG,
VC_PTKSTK,
VC_DVLFLG,
VC_PPLFLG,
VC_DTEOND,
VC_FLRLOD_FLG,
VC_UPCPREFIX, 
VC_SNDUPC_FLG,
VC_SND_GLOB_LOCNUM_FLG, 
VC_MAXSHPVOL
)
select
'----',
rtrim (CSTNUM),
NULL,
decode (BCKFLG, 'Y', 1, 0),
decode (PARFLG, 'Y', 1, 0),
decode (CARFLG, 'Y', 1, 0),  
decode (SPLFLG, 'Y', 1, 0),  
decode (STDFLG, 'Y', 1, 0),  
rtrim (SHPLBL),
decode(rtrim (SHIPBY), 'ffcust', 'rtcust',
                       'ffcusT', 'rtcust',
                                  rtrim(SHIPBY)),
-- there is no WAYBBY in 5.1 rtrim (WAYBBY),
rtrim (CARGRP),
rtrim (MANFID),
rtrim (DEPTNO),
NULL,
NULL,
decode (USR_ASNFLG,'Y', 1, 0),
decode (USR_PNPFLG,'Y', 1, 0),
decode (USR_PPNFLG,'Y', 1, 0),
rtrim (USR_PTKSTK),
decode (USR_DVLFLG,'Y', 1, 0),
NULL,
rtrim (USR_DTEOND),
NULL, -- VC_FLRLOD_FLG
NULL, -- VC_UPCPREFIX, (for walmart)
NULL, -- VC_SNDUPC_FLG, (for walmart)
NULL, -- VC_SND_GLOB_LOCNUM_FLG, (for walmart)
NULL  -- VC_MAXSHPVOL     (for ASC) 
from cstreq_40
/
