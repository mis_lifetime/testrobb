insert into carmtx (
MTXCOL,
MTXVAL,
CARGRP,
SEQNUM,
CARCOD,
SRVLVL,
NUMPCS,
ESTPAK,
ESTCAS,
ESTPAL,
MAXGIR,
CUBVOL,
NETWGT,
GRSWGT,
MOD_USR_ID,
MODDTE
)
select
decode(rtrim (MTXCOL), 'ffcust', 'rtcust', 
                       'FFCUST', 'rtcust', 
                                  rtrim(MTXCOL)),
rtrim (MTXVAL),
rtrim (CARGRP),
SEQNUM,
rtrim (CARCOD),
rtrim (SRVLVL),
NUMPCS,
ESTPAK,
ESTCAS,
ESTPAL,
MAXGIR,
CUBVOL,
NETWGT,
GRSWGT,
rtrim (MODEMP),
MODDTE
from carmtx_40
/
