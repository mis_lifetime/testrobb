insert into ctnmst 
(
CTNCOD,
CTNLEN,    
CTNWID,    
CTNHGT,    
CTNWGT,    
CTNWLM,    
CTNFPC,    
MODDTE,    
MOD_USR_ID
)
select
rtrim (CTNCOD),
CTNLEN,
CTNWID,
CTNHGT,
CTNWGT,
CTNWLM,
CTNFPC,
MODDTE,
rtrim (MODEMP)
from ctnmst_40
