/*The following script updates the usr_weeklystats table.*/
/*The script will run on a daily basis and update with various categories */
/*Looks at production and the archive tables */ 



/*Insert Cartons Shipped - Code S*/


insert into usr_weeklystats(counts,trndte,status)
select count(distinct(invdtl.subnum)) counts,
trunc(shipment.loddte) trndte,'S' status
   from shipment, prtmst,
        pckwrk,
        invdtl
  where shipment.ship_id = pckwrk.ship_id
    and pckwrk.wrkref = invdtl.wrkref
    and invdtl.prtnum = prtmst.prtnum
    and prtmst.prt_client_id ='----'
    and shipment.shpsts = 'C'
    and trunc(shipment.loddte) = trunc(sysdate) - 1  
  group by trunc(shipment.loddte)
union
select count(distinct(invdtl.subnum)) counts,
trunc(shipment.loddte) trndte,'S' status
   from shipment@arch, prtmst@arch,
        pckwrk@arch,
        invdtl@arch
  where shipment.ship_id = pckwrk.ship_id
    and pckwrk.wrkref = invdtl.wrkref
    and invdtl.prtnum = prtmst.prtnum
    and prtmst.prt_client_id ='----'
    and shipment.shpsts = 'C'
    and trunc(shipment.loddte) = trunc(sysdate) - 1    
  group by trunc(shipment.loddte);
commit;


/*Insert Orders Shipped Complete - Code SC */


insert into usr_weeklystats(counts,trndte,status)
select count(distinct(d.ordnum)) counts,trunc(c.loddte) trndte,'SC'  status
  from ord_line a,shipment_line b,shipment c, ord d
       where a.ordnum = b.ordnum
      and a.ordlin = b.ordlin
      and a.client_id = b.client_id
      and a.ordsln = b.ordsln
      and b.ship_id = c.ship_id
      and a.ordnum = d.ordnum
      and a.client_id = d.client_id
     and a.client_id = '----'
    and d.ordtyp = 'C'
    and c.shpsts = 'C'
     and trunc(c.loddte) = trunc(sysdate) - 1  
and exists(select '1' from ord_line where ordnum = a.ordnum
having sum(shpqty) = sum(ordqty))
   group by trunc(c.loddte)
union
select count(distinct(d.ordnum)) counts,trunc(c.loddte) trndte,'SC'  status
  from ord_line@arch a,shipment_line@arch b,shipment@arch c, ord@arch d
       where a.ordnum = b.ordnum
      and a.ordlin = b.ordlin
      and a.client_id = b.client_id
      and a.ordsln = b.ordsln
      and b.ship_id = c.ship_id
      and a.ordnum = d.ordnum
      and a.client_id = d.client_id
     and a.client_id = '----'
    and d.ordtyp = 'C'
    and c.shpsts = 'C'
     and trunc(c.loddte) = trunc(sysdate) - 1  
and exists(select '1' from ord_line where ordnum = a.ordnum
having sum(shpqty) = sum(ordqty))
   group by trunc(c.loddte)
order by 2;
commit;


/* Insert Total Orders Shipped - Code OS */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct(d.ordnum)) counts,trunc(c.loddte) trndte,'OS'  status
  from ord_line a,shipment_line b,shipment c, ord d
   where a.ordnum = b.ordnum
  and a.ordlin = b.ordlin
  and a.client_id = b.client_id
  and a.ordsln = b.ordsln
  and b.ship_id = c.ship_id
  and a.ordnum = d.ordnum
  and a.client_id = d.client_id
  and a.client_id = '----'
  and c.shpsts = 'C'
  and d.ordtyp = 'C'
  and trunc(c.loddte) = trunc(sysdate) - 1  
group by trunc(c.loddte)
union
 select count(distinct(d.ordnum)) counts,trunc(c.loddte) trndte,'OS'  status
  from ord_line@arch a,shipment_line@arch b,shipment@arch c, ord@arch d
   where a.ordnum = b.ordnum
  and a.ordlin = b.ordlin
  and a.client_id = b.client_id
  and a.ordsln = b.ordsln
  and b.ship_id = c.ship_id
  and a.ordnum = d.ordnum
  and a.client_id = d.client_id
  and a.client_id = '----'
  and c.shpsts = 'C'
  and d.ordtyp = 'C'
  and trunc(c.loddte) = trunc(sysdate) - 1  
group by trunc(c.loddte)
order by 2;
commit;


/*Get Cartons Received from International Suppliers - Code CRI */


insert into usr_weeklystats(counts,trndte,status)
select count(distinct(invsub.subnum)) counts ,trunc(rcvtrk.arrdte) trndte,
'CRI' status
from invsub,invdtl,rcvinv,rcvlin,rcvtrk,
supmst,adrmst,prtmst
where invsub.subnum = invdtl.subnum
and invdtl.prtnum = rcvlin.prtnum
and invdtl.rcvkey = rcvlin.rcvkey
and rcvlin.trknum = rcvinv.trknum
and rcvlin.client_id = rcvinv.client_id
and rcvlin.supnum = rcvinv.supnum
and rcvlin.invnum = rcvinv.invnum
and rcvlin.trknum = rcvtrk.trknum
and rcvinv.client_id = supmst.client_id
and rcvinv.supnum = supmst.supnum
and supmst.adr_id = adrmst.adr_id
and supmst.client_id = adrmst.client_id
and invdtl.prtnum = prtmst.prtnum
and invdtl.prt_client_id = prtmst.prt_client_id
and supmst.supnum in ('2205','2680','2681','3056',
'3117','3150','3250','4630','5505','5528','6409','7887',
'8595','8675','CY02','CY05')
and rcvinv.client_id = '----'
and adrmst.adrtyp = 'SUP'
and trunc(rcvtrk.arrdte) = trunc(sysdate) - 1  
group by trunc(rcvtrk.arrdte)
union
select count(distinct(invsub.subnum)) counts ,trunc(rcvtrk.arrdte) trndte,
'CRI' status
from invsub@arch,invdtl@arch,rcvinv@arch,rcvlin@arch,rcvtrk@arch,
supmst@arch,adrmst@arch,prtmst@arch
where invsub.subnum = invdtl.subnum
and invdtl.prtnum = rcvlin.prtnum
and invdtl.rcvkey = rcvlin.rcvkey
and rcvlin.trknum = rcvinv.trknum
and rcvlin.client_id = rcvinv.client_id
and rcvlin.supnum = rcvinv.supnum
and rcvlin.invnum = rcvinv.invnum
and rcvlin.trknum = rcvtrk.trknum
and rcvinv.client_id = supmst.client_id
and rcvinv.supnum = supmst.supnum
and supmst.adr_id = adrmst.adr_id
and supmst.client_id = adrmst.client_id
and invdtl.prtnum = prtmst.prtnum
and invdtl.prt_client_id = prtmst.prt_client_id
and supmst.supnum in ('2205','2680','2681','3056',
'3117','3150','3250','4630','5505','5528','6409','7887',
'8595','8675','CY02','CY05')
and rcvinv.client_id = '----'
and adrmst.adrtyp = 'SUP'
and trunc(rcvtrk.arrdte) = trunc(sysdate) - 1  
group by trunc(rcvtrk.arrdte)
order by 2;
commit;



/*insert OrdersShippedComplete On Time- Code OTC */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct(d.ordnum)) counts,trunc(c.loddte) trndte,'OTC'  status
  from ord_line a,shipment_line b,shipment c, ord d
       where a.ordnum = b.ordnum
      and a.ordlin = b.ordlin
      and a.client_id = b.client_id
      and a.ordsln = b.ordsln
      and b.ship_id = c.ship_id
      and a.ordnum = d.ordnum
      and a.client_id = d.client_id
     and a.client_id = '----'
    and d.ordtyp = 'C'
    and c.shpsts = 'C'
     and trunc(c.loddte) = trunc(sysdate) - 1  
and trunc(a.early_shpdte)= trunc(c.loddte)
and exists(select '1' from ord_line where ordnum = a.ordnum
having sum(shpqty) = sum(ordqty))
   group by trunc(c.loddte)
union
select count(distinct(d.ordnum)) counts,trunc(c.loddte) trndte,'OTC'  status
  from ord_line@arch a,shipment_line@arch b,shipment@arch c, ord@arch d
       where a.ordnum = b.ordnum
      and a.ordlin = b.ordlin
      and a.client_id = b.client_id
      and a.ordsln = b.ordsln
      and b.ship_id = c.ship_id
      and a.ordnum = d.ordnum
      and a.client_id = d.client_id
     and a.client_id = '----'
    and d.ordtyp = 'C'
    and c.shpsts = 'C'
     and trunc(c.loddte) = trunc(sysdate) - 1 
  and trunc(a.early_shpdte)= trunc(c.loddte)
and exists(select '1' from ord_line where ordnum = a.ordnum
having sum(shpqty) = sum(ordqty))
   group by trunc(c.loddte)
order by 2;
commit;


/*Insert OrdersShippedNotComplete On Time- Code OTN */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct(d.ordnum)) counts,trunc(c.loddte) trndte,'OTN'  status
  from ord_line a,shipment_line b,shipment c, ord d
       where a.ordnum = b.ordnum
      and a.ordlin = b.ordlin
      and a.client_id = b.client_id
      and a.ordsln = b.ordsln
      and b.ship_id = c.ship_id
      and a.ordnum = d.ordnum
      and a.client_id = d.client_id
     and a.client_id = '----'
    and d.ordtyp = 'C'
    and c.shpsts = 'C'
     and trunc(c.loddte) = trunc(sysdate) - 1 
  and trunc(a.early_shpdte)= trunc(c.loddte)
and exists(select '1' from ord_line where ordnum = a.ordnum
having sum(shpqty) != sum(ordqty))
   group by trunc(c.loddte)
union
select count(distinct(d.ordnum)) counts,trunc(c.loddte) trndte,'OTN'  status
  from ord_line@arch a,shipment_line@arch b,shipment@arch c, ord@arch d
       where a.ordnum = b.ordnum
      and a.ordlin = b.ordlin
      and a.client_id = b.client_id
      and a.ordsln = b.ordsln
      and b.ship_id = c.ship_id
      and a.ordnum = d.ordnum
      and a.client_id = d.client_id
     and a.client_id = '----'
    and d.ordtyp = 'C'
    and c.shpsts = 'C'
     and trunc(c.loddte) = trunc(sysdate) - 1 
  and trunc(a.early_shpdte)= trunc(c.loddte)
and exists(select '1' from ord_line where ordnum = a.ordnum
having sum(shpqty) != sum(ordqty))
   group by trunc(c.loddte)
order by 2;
commit;

