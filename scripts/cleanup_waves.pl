#!/usr/local/bin/perl
# Raman Parthasarathy 04/24/02 
# Call cleanup_waves to fix any hanging waves that must be Complete but still have a 
# status of released

my $cmd = "";
$LOGFILE = "$ENV{LESDIR}/log/cleanup.log";

$cmd=<<EOF;                                #Command to execute 
[select distinct pckbat.schbat, pckbat.batsts from pckbat
where pckbat.wave_prc_flg = 1
and pckbat.batsts in ( 'REL','SCH','PLAN','ALOC')
and pckbat.cmpdte is null
and not exists ( select 1 from shipment s, shipment_line sl
where sl.schbat = pckbat.schbat
and sl.ship_id = s.ship_id
and s.shpsts not in ('B', 'C' ) )
order by pckbat.schbat]
|
[update pckbat set batsts = 'CMPL', cmpdte = sysdate, mod_usr_id = 'CLEANUP', moddte = sysdate where schbat = \@schbat ]
|
[select schbat, batsts, cmpdte, moddte, mod_usr_id from pckbat where schbat = \@schbat]
/
EOF

open(SQL,"| msql > $LOGFILE.$$.1 2>&1") || die ("Failed to open log file");
print SQL $cmd;
close(SQL);

exit(0);
