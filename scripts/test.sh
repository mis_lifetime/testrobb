#!/usr/bin/ksh
#
# This script will run the Forecasting Report at 6AM using cron.
#


. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/scripts


/opt/mchugh/prod/moca/bin/mload -H -c /opt/mchugh/prod/les/db/data/load/parts.ctl -D /opt/mchugh/prod/les/db/data/load/parts -d parts.csv
