#!/usr/local/bin/perl
# This script is used to generate the reconciliation in the 5.1x system
# Raman Parthasarathy, Dt: 01/28/02

use IO::Handle;

if ($^O =~ /win32/i)
{                       # NT
    $par_pthsep = "\\";
}
else
{                       # Unix
    $par_pthsep = "\/";
}

# Added a comment to discourage users from being logged into the system when this is running
# This is set to run at night. However, if Lifetime decides to run 3rd shift, then this
# has to be run at a period when there is no activity in the system.

#Make a change to remove any existing REC files in the hostout directory
my $hostout_dir=$ENV{LESDIR}.$par_pthsep."files".$par_pthsep."hostout";
my $cmd = "";
$LOGFILE = "$ENV{LESDIR}/log/invrec.log";

$status = open(FILE, "> $LOGFILE.$$.1") || die "Unable to open log file. exiting...";
printf (FILE "Successfully opened log file...");

eval {
   chdir ($hostout_dir) || die "Failed to chdir to $hostout_dir directory";
   if (-e <REC*>)
   {
      unlink <REC*>;
      printf (FILE "Successfully removed REC files...");
   }
};

#Check for Errors
if ($@) {
   printf (FILE "unable to remove REC files. Continuing.... ");
}

close (FILE);
   
$cmd=<<EOF;                                #Command to execute 
sl_log event where evt_id = 'INVENTORY_RECONCILIATION' and sys_id = 'DCS'
/
EOF

open(SQL,"| msql >> $LOGFILE.$$.1 2>&1") || die ("Failed to open log file");
print SQL $cmd;
close(SQL);

exit(0);
