#!/opt/perl5/bin/perl
################################################################################
#
# $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/scripts/monarch_rflink.pl,v $
# $Revision: 1.2 $
# $Author: devsh $
#
# Description: RF data file link script.
#
# $McHugh_Copyright-Start$
#
# Copyright (c) 1999
# McHugh Software International
# Waukesha, Wisconsin
#
# This software is furnished under a corporate license for use on a
# single computer system and can be copied (with inclusion of the
# above copyright) only for use on such a system.
# International.
#
# The information in this document is subject to change without notice
# and should not be construed as a commitment by McHugh Software
#
# McHugh Software International assumes no responsibility for the use of
# the software described in this document on equipment which has not been
# supplied or approved by McHugh Software International.
#
# $McHugh_Copyright-End$
#
################################################################################
#
# Include needed sub-components
#
require "getopts.pl";
#
################################################################################
#
# Initialize internal parameters
#
if ($^O =~ /win32/i)
{			    # NT
    $par_pthsep = "\\";
    $par_userid = $ENV{USERNAME};
}
else
{			    # Unix
    $par_pthsep = "\/";
    $par_userid = $ENV{LOGNAME};
}
#
$par_dcsdir = $ENV{DCSDIR} || die "DCSDIR environment variable not defined\nStopped";
#
$par_lesdir = $ENV{LESDIR} || die "LESDIR environment variable not defined\nStopped";
#
$par_dcs_rf = $par_dcsdir.$par_pthsep."src".$par_pthsep."rfsrc";
#
$par_les_rf = $par_lesdir.$par_pthsep."src".$par_pthsep."monarch_rfsrc";
#
################################################################################
#
# Initialize internal variables
#
$arg_hndflg = "0";
#
$arg_vehflg = "0";
#
$arg_dbgflg = "0";
#
$arg_filpfx = "monarch_";
#
$arg_filsfx = "";
#
###############################################################################
#
# Define print help routine
#
sub print_help()
{
    printf ("\n");
    printf ("Link (concatonate) RF terminal form object files into single data file\n");
    printf ("for use by the DCS RF terminal driver (dcsrdt)\n");
    printf ("\n");
    printf ("Usage:  monarch_rflink (-h|-v) [-d] [-p<pfx>] [-s<sfx>]\n");
    printf ("\n");
    printf ("    -h) Handheld mode - create handheld terminal data file\n");
    printf ("\n");
    printf ("    -v) Vehicle mode - create vehicle terminal data file\n");
    printf ("\n");
    printf ("    -d) Debug mode - display debug messages\n");
    printf ("\n");
    printf ("    -p) Terminal driver file prefix - used to specify custom driver files\n");
    printf ("        (example: '-p tst' will make 'tst_handheld.dat' or 'tst_vehicle.dat')\n");
    printf ("        (default is '$arg_filpfx')\n");
    printf ("\n");
    printf ("    -s) Terminal driver file suffix - used to specify custom driver files\n");
    printf ("        (example: '-s tst' will make 'handheld_tst.dat' or 'vehicle_tst.dat')\n");
    printf ("        (default is '$arg_filsfx')\n");
    printf ("\n");
}
#
################################################################################
#
# Define load form files routine
#
sub load_form_files
{
    my ($dirnam, $dirlvl) = @_;
    #
    my (@fillst) = "";
    my ($filnam) = "";
    #
    my ($filspc) = "";
    my (@filcon) = "";
    #
    printf ("Processing directory ($dirnam) at level ($dirlvl)\n") if ($arg_dbgflg);
    #
    # Get a file listing of all 'object' files for current directory
    #
    opendir (DIRNAM, $dirnam) || die "Could not open directory ($dirnam)\nStopped";
    @fillst = grep /^.*\.$dat_objtyp/i, readdir (DIRNAM);
    close (DIRNAM);
    #
    # Do for each 'object' file found
    #
    foreach $filnam (@fillst)
    {
	printf ("  Processing file ($filnam)\n") if ($arg_dbgflg);
	#
	# If current directory level is 'DCS' or 'LES', then check for local custom file
	#
	if (($dirlvl eq "DCS" or $dirlvl eq "LES") and (-f $filnam))
	{
	    printf ("  ++Skipping file ($filnam), local version exists\n") if ($arg_dbgflg);
	    next;
	}
	#
	# If current directory level is 'DCS', then check for LES custom file
	#
	if (($dirlvl eq "DCS") and (-f $par_les_rf.$par_pthsep.$filnam))
	{
	    printf ("  ++Skipping file ($filnam), les version exists\n") if ($arg_dbgflg);
	    next;
	}
	#
	# Copy 'object' file contents into data file
	#
	$filspc = $dirnam.$par_pthsep.$filnam;
	#
	printf ("  Copying file ($filspc)\n") if ($arg_dbgflg);
	#
	open (OBJFIL, $filspc) || die "Could not open form object file ($filspc)\nStopped";
	@filcon = <OBJFIL>;
	close (OBJFIL);
	#
	print DATFIL @filcon;
	#
	printf (".") if (!$arg_dbgflg);
    }
}
#
################################################################################
#
# Input argument checking/processing
#
# Get command line arguments
#
$status = &Getopts ('hvdp:s:');
#
# If help mode enabled, then print help screen & exit
#
if (!$status)
{
    &print_help ();
    exit (0);
}
#
# Load arguments into local variables
#
$arg_hndflg = "1"        if ($opt_h);
$arg_vehflg = "1"        if ($opt_v);
$arg_dbgflg = "1"        if ($opt_d);
$arg_filpfx = $opt_p."_" if ($opt_p);
$arg_filsfx = "_".$opt_s if ($opt_s);
#
if ($arg_dbgflg)
{
    printf ("\nArgument List\n");
    printf ("HndFlg ($arg_hndflg)\n");
    printf ("VehFlg ($arg_vehflg)\n");
    printf ("DbgFlg ($arg_dbgflg)\n");
    printf ("FilPfx ($arg_filpfx)\n");
    printf ("FilSfx ($arg_filsfx)\n");
}
printf ("\n");
#
# Ensure all required arguments are defined
#
if (!$arg_hndflg && !$arg_vehflg)
{
    printf ("Terminal data file type not defined (-h or -v)\n");
    &print_help;
    exit (2);
}
#
if ($arg_hndflg && $arg_vehflg)
{
    printf ("Terminal data file type ambiguously defined (-h *or* -v)\n");
    &print_help;
    exit (2);
}
#
# Initialize argument dependent variables
#
if ($arg_hndflg)
{
    $dat_filnam = "${arg_filpfx}handheld${arg_filsfx}.dat";
    $dat_objtyp = "obh"
}
#
if ($arg_vehflg)
{
    $dat_filnam = "${arg_filpfx}vehicle${arg_filsfx}.dat";
    $dat_objtyp = "obv"
}
#
################################################################################
#
# Verify we're the only one linking right now & set lock file to prevent
# anyone else from linking (bad things happen if two links are in progress).
#
$lck_filnam = "monarch_rflink.lck";
#
printf ("Checking for lock file ($lck_filnam)\n") if ($arg_dbgflg);
#
if (-e $lck_filnam)
{
    printf ("Lock file ($lck_filnam) found, displaying contents\n\n") if ($arg_dbgflg);
    #
    open (LCKFIL, $lck_filnam) || die "Could not open lock file ($lck_filnam)\nStopped";
    $text = <LCKFIL>;
    close (LCKFIL);
    printf ("$text\n");
    exit (-1);
}
#
open   (LCKFIL, ">$lck_filnam") || die "Could not create lock file ($lck_filnam)\nStopped";
printf (LCKFIL "monarch_rflink is locked by $par_userid\n");
close  (LCKFIL);
#
################################################################################
#
# Open new terminal data file & write standard header information to it
#
printf ("Creating $dat_filnam\n");
#
open (DATFIL, ">".$dat_filnam) || die "Could not create data file ($dat_filnam)\nStopped";
#
$hdr_filnam = $par_dcs_rf.$par_pthsep."standard.prj";
open (HDRFIL, $hdr_filnam) || die "Could not open header file ($hdr_filnam)\nStopped";
@text = <HDRFIL>;
close (HDRFIL);
print DATFIL @text;
#
################################################################################
#
# Load terminal form 'object' files into data file in the order shown below
#   1) DCS form files
#   2) LES form files
#   3) Local directory form files
#
#&load_form_files ($par_dcs_rf, "DCS");
#
&load_form_files ($par_les_rf, "LES");
#
&load_form_files (".", "LCL");
#
printf ("\n");
#
################################################################################
#
# Write standard trailer information into new data file & close file
#
print DATFIL "E\n";
close  (DATFIL);
#
################################################################################
#
# Common exit point for command file.  Perform any required clean-up activities
#
unlink ($lck_filnam) || die "Could not delete lock file ($lck_filnam)\nStopped";
#
exit (0);
#
################################################################################
