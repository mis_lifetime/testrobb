update var_shpord 
set (stname, stadd1,
     stadd2, stadd3,
     stcity, ststcd, stposc) = (select adrnam, 
                                      adrln1,  
                                      adrln2, adrln3,
                                      adrcty, 
                                      adrstc, 
                                      adrpsz 
                                  from adrmst, ord
                                 where var_shpord.ordnum = ord.ordnum
                                   and ord.st_adr_id = adrmst.adr_id ) 
where exists (select adr_id 
                from adrmst, ord 
               where var_shpord.ordnum = ord.ordnum
                 and ord.st_adr_id = adrmst.adr_id
                ) 
              
     

