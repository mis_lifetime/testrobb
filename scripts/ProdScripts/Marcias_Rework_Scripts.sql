Marcia will submit the appropriate spreadsheet to work with.  You want to delete any lines with empty spaces, and only use the prtnum, dsc, qty, comments, rescod and seqnum columns.  Use those names as headers and save as a file called production.csv.

Place the file in the /opt/mchugh/prod/les/db/data/load/production  directory.  Truncate the usr_prodrework table and load the information into the table using the production.ctl script. dload production production.csv.  This will load the information into the table, with the exception of the itmcls, available and pending columns.  

After the information from the file is loaded, run this script to insert the itmcls:

update usr_prodrework a
set a.itmcls = (select b.vc_itmcls from prtmst b where 
b.prtnum = a.prtnum)
where exists(select 'x' from prtmst c where c.prtnum =
a.prtnum)

To update the usr_prodrework table with the systems most up-to-date available and pending quantities, run this script:

update usr_prodrework a
set (availqty,pndqty) = (select sum(x.untqty-x.comqty),sum(x.pndqty)
                        from
                        (SELECT distinct invsum.prtnum prtnum,invsum.stoloc,invsum.untqty,invsum.comqty,invsum.pndqty
                                    from prtmst,invsum,invlod,invsub,invdtl
                                    where prtmst.prtnum = invsum.prtnum
                                    and prtmst.prt_client_id = invsum.prt_client_id
                                    and prtmst.prtnum = invdtl.prtnum
                               and prtmst.prt_client_id = invdtl.prt_client_id
                               and invsum.stoloc = invlod.stoloc
                              and invsum.prtnum = invdtl.prtnum
                              and invsum.prt_client_id = invdtl.prt_client_id
                              and invlod.lodnum = invsub.lodnum
                              and invsub.subnum = invdtl.subnum) x
where x.prtnum = a.prtnum
group by x.prtnum)
where exists(select 'x' from (SELECT distinct invsum.prtnum prtnum,invsum.stoloc,invsum.untqty,invsum.comqty,invsum.pndqty
                                    from prtmst,invsum,invlod,invsub,invdtl
                                    where prtmst.prtnum = invsum.prtnum
                                    and prtmst.prt_client_id = invsum.prt_client_id
                                    and prtmst.prtnum = invdtl.prtnum
                               and prtmst.prt_client_id = invdtl.prt_client_id
                               and invsum.stoloc = invlod.stoloc
                              and invsum.prtnum = invdtl.prtnum
                              and invsum.prt_client_id = invdtl.prt_client_id
                              and invlod.lodnum = invsub.lodnum
                              and invsub.subnum = invdtl.subnum) y
where y.prtnum = a.prtnum)


You can run this script to compare individual parts in usr_prodrework with what the system has

select sum(x.untqty-x.comqty),sum(x.pndqty)
                           from
                            (SELECT distinct invsum.prtnum prtnum,invsum.stoloc,invsum.untqty,invsum.comqty,invsum.pndqty
                                        from prtmst,invsum,invlod,invsub,invdtl
                                        where prtmst.prtnum = invsum.prtnum
                                        and prtmst.prt_client_id = invsum.prt_client_id
                                        and prtmst.prtnum = invdtl.prtnum
                                   and prtmst.prt_client_id = invdtl.prt_client_id
                                   and invsum.stoloc = invlod.stoloc
                                 and invsum.prtnum = invdtl.prtnum
                                 and invsum.prt_client_id = invdtl.prt_client_id
                                 and invlod.lodnum = invsub.lodnum
                                 and invsub.subnum = invdtl.subnum
                               and invdtl.prtnum= '00309-93') x


Confirm with Marcia, what months of the forecast she wants, update the months in below script accordingly. Run below script and spool results to a file called prodreworks.txt.  The results will probably have less lines than Marcia's original file because this is a summary.  You will notice the same part# listed multiple times in Marica's original spreadsheet due to multiple locations, dates and comments.

select a.prtnum,dsc,sum(a.qty),a.itmcls,comments, rescod,nvl(pndqty,0),nvl(availqty,0),nvl(july,0),nvl(august,0),nvl(september,0),
        nvl(july+august+september,0) total
        from usr_prodrework a, usr_forecasta b
        where a.prtnum = b.prtnum(+)
group by a.prtnum,dsc,itmcls,comments,pndqty,availqty, rescod, july,august,september
