/* #REPORTNAME= User Ship Dock Update  */
/* #HELPTEXT= This script truncates and updates the usr_shipdock table */
/* #HELPTEXT= with information from the Usr-ShippingDock.sql report. */
/* #HELPTEXT= the information from the table is spooled to an .out file. */
/* #HELPTEXT= Requested by Senior Management-Westbury during Physical. */

/* Author: Al Driver */

btitle skip1 center 'Page:' format 999 sql.pno

column lodnum heading 'Pallet'
column lodnum format a25
column stgdte heading 'StgDte'
column stgdte format a13
column stoloc heading 'Location'
column stoloc format a15
column ship_id heading 'Ship ID'
column ship_id format a14
column prtnum heading 'Item'
column prtnum format a12
column stgqty heading 'Qty'
column stgqty format 999999
column cpodte heading 'CnlDte'
column cpodte format a13
column extdte heading 'ExtDte'
column extdte format a13
column cponum heading 'PO #'
column cponum format a18
column untcst heading 'Cost'
column untcst format 999.99
column ordnum heading 'Order #'
column ordnum format a14


--set linesize 200
--set pagesize 5000

alter session set nls_date_format ='DD-MON-YYYY';

truncate table usr_shipdock;

insert into usr_shipdock(cponum,ship_id,lodnum,stoloc,ordnum,untcst,
prtnum,stgqty,stgdte,cpodte)
select rtrim(ord.cponum) cponum,rtrim(sh.ship_id) shipid,
       rtrim(invlod.lodnum) pallet, rtrim(invlod.stoloc) stoloc,
       rtrim(ord.ordnum) ordnum, rtrim(pm.untcst) untcst, rtrim(ord_line.prtnum) prtnum,
       sd.stgqty,
       sh.stgdte stgdte,
       ord.cpodte cpodte
 from
       locmst lm,
       aremst am,
       invlod,
       invsub,
       invdtl,
       shipment_line  sd,
       shipment sh,
       ord,
       ord_line, prtmst pm,
       dscmst, adrmst lookup1
      WHERE invlod.stoloc = lm.stoloc
   AND lm.arecod                   = am.arecod
        AND am.stgflg                   = 1
        AND invlod.lodnum = invsub.lodnum
        AND invsub.subnum = invdtl.subnum
        AND ord.st_adr_id              = lookup1.adr_id
        AND ord.client_id               = lookup1.client_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND ord_line.prtnum             = pm.prtnum
        AND pm.prt_client_id            = '----'
        AND sd.ship_id                  = sh.ship_id
        AND invdtl.ship_line_id         = sd.ship_line_id
        AND dscmst.colnam               = 'shpsts'
        AND dscmst.locale_id            = 'US_ENGLISH'
        AND dscmst.colval               = sh.shpsts
        AND sh.shpsts                   in ('S', 'P','X')
group
    by
       invlod.lodnum, invlod.stoloc, ord.ordnum, ord.cponum, pm.untcst,
       sh.ship_id, ord_line.prtnum, sd.stgqty,
       sh.stgdte ,
       ord.cpodte;

-- spool /opt/mchugh/prod/les/oraclereports/shipdock.out

select * from usr_shipdock;

-- spool off
-- exit
