/**********************************************************************************************************/
/* This script will populate the usr_aretemp table with the contents of the aremst table, before the start*/
/* of Physical.  This should be done when the snapshot and other updates are done.                        */
/**********************************************************************************************************/

truncate table usr_aretemp;

/* Populate table with current data */

insert into usr_aretemp 
(
select * from aremst)
/
commit;
