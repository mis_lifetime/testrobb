#!/opt/perl5/bin/perl
#
# File    : Rf.pl
#
# Purpose : Dcs RF Terminal Driver Interface
#
# Author  : Steve R. Hanchar
#
# Date    : 10-Jan-2000
#
# Usage   : Rf [-t <termid>] [-h] [-v] [-m] [-a <address>] [-p <port>] [-d <devcod>] [-n]
#
###############################################################################
#
# Include needed sub-components
#
require "getopts.pl";
#
###############################################################################
#
# Initialize server host and server port per operating system requirements
#
if ($^O =~ /win32/i)	# Windows
{
    #printf ("Windows environment\n");
    #
    #  Declare registry interface & define registry key roots
    #
    eval "use win32::TieRegistry 0.20";
    #
    #  If MOCA_ENVNAME is defined, then assume server is local and
    #  get server port number from Moca server registry structure.
    #
    if ($ENV{MOCA_ENVNAME})
    {
	#printf ("Local server defined, using local server\n");
	$arg_srvadr = "localhost";
	$key_srvprt = 'LMachine\\Software\\Mchugh\\'.
			($ENV{MOCA_PRODUCT}||'MOCA').'\\'.
			($ENV{MOCA_ENVNAME}).'\\CONMGR\\port';
	eval '$arg_srvprt = $Win32::TieRegistry::Registry->{$key_srvprt};';
    }
    #
    #  Else assume we're running on a client computer and get server
    #  host and port number from Moca client registry structure.
    #
    if (!($ENV{MOCA_ENVNAME} && ($arg_srvprt)))
    {
	#printf ("Local server not defined, using default client server\n");
	$key_srvdef = 'LMachine\\Software\\Mchugh\\'.
			($ENV{MOCA_PRODUCT}||'MOCA').' Servers\\Default Server';
	eval '$arg_srvdef = $Win32::TieRegistry::Registry->{$key_srvdef};';
	#
	$key_srvadr = 'LMachine\\Software\\Mchugh\\'.
			($ENV{MOCA_PRODUCT}||'MOCA').' Servers\\'.
			($arg_srvdef).'\\HostName';
	eval '$arg_srvadr = $Win32::TieRegistry::Registry->{$key_srvadr};';
	#
	$key_srvprt = 'LMachine\\Software\\Mchugh\\'.
			($ENV{MOCA_PRODUCT}||'MOCA').' Servers\\'.
			($arg_srvdef).'\\PortNumber';
	eval '$arg_srvprt = $Win32::TieRegistry::Registry->{$key_srvprt};';
    }
}
else					# Unix
{
    #printf ("Unix environment\n");
    #
    #  For Unix, always assume server is local and all that is needed is to
    #  get the server port number from the moca registry file.
    #
    $arg_srvadr = "localhost";
    open (CMD_RSP, "grep '^port=' \$MOCA_REGISTRY 2>/dev/null | cut -f2 -d'=' |");
    $arg_srvprt = <CMD_RSP>;
    chomp ($arg_srvprt);
    close (CMD_RSP);
    #$arg_srvprt = system ("grep '^port=' \$MOCA_REGISTRY 2>/dev/null | cut -f2 -d'='");
}
#
#printf ("Default host ($arg_srvadr)\n");
#printf ("Default port ($arg_srvprt)\n");
#
###############################################################################
#
# Initialize internal variables
#
$arg_termid = "";
$arg_termid = $ENV{DCS_RF_TERMID} if ($ENV{DCS_RF_TERMID});
#
$arg_rftype = "v";
$arg_rftype = $ENV{DCS_RF_RFTYPE} if ($ENV{DCS_RF_RFTYPE});
#
#$arg_srvadr = "";  # Do not uncomment
$arg_srvadr = $ENV{DCS_RF_srvadr} if ($ENV{DCS_RF_SRVADR});
#
#$arg_srvprt = "";  # Do not uncomment
$arg_srvprt = $ENV{DCS_RF_SRVPRT} if ($ENV{DCS_RF_SRVPRT});
#
$arg_devcod = "TEL";
$arg_devcod = $ENV{DCS_RF_DEVCOD} if ($ENV{DCS_RF_DEVCOD});
#
$arg_nosfmt = "";
#
###############################################################################
#
# Initialize internal parameters
#
if ($^O =~ /win32/i) {		# NT
    $par_pthsep = "\\";
} else {                    # Unix
    $par_pthsep = "\/";
}
#
###############################################################################
#
# Define print help routine
#
sub print_help()
{
    printf (STDOUT "\n");
    printf (STDOUT "Invoke Dcs RF terminal driver with desired settings.  Removes the need\n");
    printf (STDOUT "to constantly create individual configuration files.\n");
    printf (STDOUT "\n");
    printf (STDOUT "Usage:  Rf [-t <termid>] [-h] [-v] [-m] [-a <address>] [-p <port>] [-d <devcod>] [-n]\n");
    printf (STDOUT "\n");
    printf (STDOUT "	-t) RF Terminal Id\n");
    if (!$arg_termid)
    {
	printf (STDOUT "            (example: '01', default set by DCS_RF_TERMID)\n");
    }
    else
    {
	printf (STDOUT "            (default is '$arg_termid', set by DCS_RF_TERMID)\n");
    }
    printf (STDOUT "\n");
    printf (STDOUT "	-h) Invoke RF driver as handheld terminal\n");
    if ($arg_rftype eq "h")
    {
	printf (STDOUT "            (default, set by DCS_RF_RFTYPE)\n");
    }
    printf (STDOUT "\n");
    printf (STDOUT "	-v) Invoke RF driver as vehicle terminal\n");
    if ($arg_rftype eq "v")
    {
	printf (STDOUT "            (default, set by DCS_RF_RFTYPE)\n");
    }
    printf (STDOUT "\n");
    printf (STDOUT "	-m) Invoke RF driver as Monarch terminal\n");
    if ($arg_rftype eq "m")
    {
	printf (STDOUT "            (default, set by DCS_RF_RFTYPE)\n");
    }
    printf (STDOUT "\n");
    printf (STDOUT "	-a) Server host address\n");
    printf (STDOUT "	    (default is '$arg_srvadr', set by DCS_RF_SRVADR)\n");
    printf (STDOUT "\n");
    printf (STDOUT "	-p) Server port number\n");
    printf (STDOUT "	    (default is '$arg_srvprt', set by DCS_RF_SRVPRT)\n");
    printf (STDOUT "\n");
    printf (STDOUT "	-d) Device code prefix\n");
    printf (STDOUT "	    (default is '$arg_devcod', set by DCS_RF_DEVCOD)\n");
    printf (STDOUT "\n");
    printf (STDOUT "	-n) No screen formatting (for use with real RF units)\n");
    printf (STDOUT "\n");
}
#
###############################################################################
#
# Input argument checking/processing
#
#
# Get command line arguments
#
&Getopts ('t:hvma:p:d:n');
#
# Load arguments into local variables
#
$arg_termid = $opt_t if ($opt_t);
$arg_rftype = "h"    if ($opt_h);
$arg_rftype = "m"    if ($opt_m);
$arg_rftype = "v"    if ($opt_v);
$arg_srvadr = $opt_a if ($opt_a);
$arg_srvprt = $opt_p if ($opt_p);
$arg_devcod = $opt_d if ($opt_d);
$arg_nosfmt = "-n"   if ($opt_n);
#
# Prompt user for selected arguments if missing
#
if (!$arg_termid)
{
    printf ("\nRF Terminal Id (nn)? ");
    $arg_termid = <STDIN>;
    chomp ($arg_termid);
}
#
# Ensure all required arguments are defined
#
printf ("\nArgument List\n");
printf ("TermId ($arg_termid)\n");
printf ("RfType ($arg_rftype)\n");
printf ("SrvAdr ($arg_srvadr)\n");
printf ("SrvPrt ($arg_srvprt)\n");
#
if (!$arg_termid)
{
    printf (STDOUT "\nRF Terminal Id not defined\n");
    &print_help;
    exit (2);
}
#
if (!$arg_rftype)
{
    printf (STDOUT "\nRF Terminal Type not defined\n");
    &print_help;
    exit (2);
}
#
if (!$arg_srvadr)
{
    printf (STDOUT "\nServer Host Address not defined\n");
    &print_help;
    exit (2);
}
#
if (!$arg_srvprt)
{
    printf (STDOUT "\nServer Port Number not defined\n");
    &print_help;
    exit (2);
}
#
if (!$arg_devcod)
{
    printf (STDOUT "\nDevice Code Prefix not defined\n");
    &print_help;
    exit (2);
}
#
# Ensure selected arguments are formatted correctly
#
$_ = $arg_termid;
if ((!/^\d\d$/) && (!/^\d\d\d$/))
{
    printf (STDOUT "\nInvalid RF Terminal Id format, proper format is 'nn' or 'nnn', exiting\n");
    exit (2);
}
#
# Initialize dependant variables
#
if ($arg_rftype eq "h")
{
    $var_rfname = "handheld";
    $var_rfsufx = "";
}
elsif ($arg_rftype eq "m")
{
    $var_rfname = "monarch_handheld";
    $var_rfsufx = "";
}
else
{
    $var_rfname = "vehicle";
    $var_rfsufx = "W";
}
#
###############################################################################
#
# Determine working file name and delete any old working files
#
$wrk_filnam = "$ENV{LESDIR}${par_pthsep}log${par_pthsep}RF_$arg_termid$arg_rftype.cfg";
if (-e $wrk_filnam)
{
    printf ("\nDeleting old working file ($wrk_filnam)\n");
    unlink ($wrk_filnam) || die "Could not delete old working file";
}
#
###############################################################################
#
# Write RF driver configuration file
#
open (WRK_FIL, ">$wrk_filnam") || die "Could not create working file";
#
printf (WRK_FIL "TERMID=$arg_termid\n");
printf (WRK_FIL "DEVCOD=$arg_devcod$arg_termid$var_rfsufx\n");
printf (WRK_FIL "VOLUME=1\n");
printf (WRK_FIL "FILELOC=H\n");
printf (WRK_FIL "FILENAME=$var_rfname.dat\n");
#
close (WRK_FIL);
#
###############################################################################
#
# Update SAI_RF_TERMTYPE environment variable
#
if ($arg_rftype eq "h")
{
    $ENV{SAI_RF_TERMTYPE} = "15";
}
elsif ($arg_rftype eq "m")
{
    $ENV{SAI_RF_LINES} = "4";
    $ENV{SAI_RF_COLUMNS} = "20";
    $ENV{SAI_RF_CAPTIONS} = "0";
}
else
{
    $ENV{SAI_RF_TERMTYPE} = "";
}
#
###############################################################################
#
# Invoke Dcs RF Terminal Driver
#
printf ("\n");
system ("dcsrdt -c $wrk_filnam -a $arg_srvadr -p $arg_srvprt -g $arg_nosfmt");
#
###############################################################################
#
# Common exit point for command file.  Perform any required clean-up activities
#
unlink ($wrk_filnam) || die "Could not delete working file";
exit (0);
#
###############################################################################
