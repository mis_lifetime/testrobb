#!/opt/perl5/bin/perl -w

#----------------------------- libraries and use directives --------------------
use Socket;
use strict;
use Getopt::Std;


#----------------------------- forward declarations ----------------------------
sub show_usage;
sub logmsg;
sub myclose;
sub myconnect;
sub myconnect_loop;


#----------------------------- some #defines -----------------------------------
#  All these are decimal values converted to strings of one char length.
my $STX = chr(2);
my $ETX = chr(3);
my $ACK = chr(6);
my $NAK = chr(21); 

# used in the packet header for message sequencing.
my $counter = 1;
my $fn = 'main()';

# ---------------------------- Globals -----------------------------------------


#----------------------------- Main program ------------------------------------
# get the command line arguments: machine-name, port, retry time and verbose level.
my ($host, $port, $retry_time, $debug_level) = getOptions();
my $fd = myconnect_loop($host, $port, $retry_time);
logmsg 1, $fn, "connected socket id is (" . fileno($fd) . ")";

# read message from the stdout and send it to server
while (<STDIN>) {
	chomp;
	# marshall the input data according to the protocol
	my $packet = make_packet($_);
	my $printable = get_printable_packet($packet);
	logmsg 1, $fn, sprintf("sending out  |%s|", $printable);

	# send the protocol packet over the socket
	my $n = undef;
	until (defined($n)) {
		($n = send ($fd, $packet, 0))
			or logmsg 3, $fn, "send: $!";
		unless (defined($n)) {
			logmsg 1, $fn, "Will drop socket and attempt to reconnect...";
			myclose($fd);
			$fd = myconnect_loop($host, $port, $retry_time);
			if (!$fd) {
				die "Major problem in connecting to $host at $port.";
			}
		}
	}

	unless ($n = length($packet)) {
		logmsg 1, $fn, "send: Part of the text could not be send: $!\n";
		next;
	}

	# read ack/nak
	logmsg 3, $fn, "Attempting to read ack/nak";
	my $ack_nak = read_ack_nak();
	if (!defined($ack_nak)) {
		logmsg 1, $fn, "send: Error reading ack/nak!";
		logmsg 1, $fn, "Will drop socket and attempt to reconnect...";
		myclose($fd);
		$fd = myconnect_loop($host, $port, $retry_time);
	}
	else {
		# entire ack was recvd!
		$printable = get_printable_ack_nak($ack_nak);
		logmsg 1, $fn, "Ack/Nak recd |$printable|";
	}
}

close(SOCK);

# ----------------- Definition of local functions --------
# Get the command line arguments.
sub getOptions {
	my %opts = ();
	getopts('a:p:t:v:', \%opts);

	my ($hostname, $portnum, $retry_sec, $dbglvl) = ();

	# get the required arguments
	$hostname = $opts{a} if defined($opts{a});
	$portnum = $opts{p} if defined($opts{p});
	$retry_sec = $opts{t} if defined($opts{t});

	# make sure that we have all the required parameters
	unless($hostname && $portnum && $retry_sec) {
		show_usage();
	}
	
	# now get the optional arguments.
	# by default log as few messages as possible because it clutters up the output screen.
	$dbglvl = 1;
	$dbglvl = $opts{v} if defined $opts{v};
	# valid values for debug level are 1 through 6 only.
	show_usage() unless $dbglvl >=1 && $dbglvl <= 6;

	return ($hostname, $portnum, $retry_sec, $dbglvl);
}

sub show_usage {
	die "Correct usage for the TCP/IP client is as follows:\n"
		. "$0\n"
		. "\t-a <host-address>\n"
		. "\t-p <port>\n"
		. "\t-t <connection-retry-time-in-seconds>\n"
		. "\t-v <verbose?:debug-level-from 1(least) through 6(most)>\n";
}

# level 4, 5 and 6 are not about severity level of message but they control how much
# or how little information gets printed out.  Level 1, 2 and 3 control the severity
# of the messages itself.
sub logmsg {
	my $lvl = shift;
	my $fn = shift;

	if ($lvl <= $debug_level) {
		my ($sec, $min, $hour, $mday, $month, $year) = localtime(time);
		if ($debug_level == 6) {
			# show every thing at this level
			print sprintf("$0 %4d - %02d/%02d/%04d %02d:%02d:%02d - %s - @_\n", $$,
				$mday, $month + 1, $year + 1900, $hour, $min, $sec, $fn);
		}
		elsif ($debug_level == 5) {
			# do not show script-name and line # information at this level
			print sprintf("%02d/%02d/%04d %02d:%02d:%02d - %s - @_\n",
				$mday, $month + 1, $year + 1900, $hour, $min, $sec, $fn);
		}
		elsif ($debug_level == 4) {
			# stop showing function name at this stage
			print sprintf("%02d/%02d/%04d %02d:%02d:%02d - @_\n",
				$mday, $month + 1, $year + 1900, $hour, $min, $sec);
		}
		else {
			# Further skip the date information at this level and below
			printf sprintf("%02d:%02d:%02d - @_\n", $hour, $min, $sec);
		}
	}
}

sub myclose {
	my $fn = "myclose()";
	# close the socket
	my $fd = shift;
	shutdown ($fd, 2)
		or logmsg 1, $fn, "shutdown: ERROR!: $!";
	close ($fd) 
		or logmsg 1, $fn, "close: ERROR!: $!";
}

sub myconnect_loop {
	my $fn = "myconnect_loop()";
	my ($host, $port, $retry_time) = @_;

	my $attempts = 0;
	my $connected = undef;
	local *SOCK;

	logmsg 1, $fn, "Attempting to connect to ($host) at ($port)";
	until ($connected) {
		my $protocol = getprotobyname('tcp');

		socket(SOCK, PF_INET, SOCK_STREAM, $protocol)
			or die "socket: $!";

		my $iaddr = inet_aton($host)
			or die "inet_aton: no host $host: $!";

		my $addr = pack_sockaddr_in($port, $iaddr);

		($connected = connect (SOCK, $addr))
			or logmsg 3, $fn, "connect: $!";
		$attempts++;

		# log a message if it is the first iteration of the loop.
		unless ($connected) {
			if ($attempts == 1) {
				logmsg 1, $fn, "connect failed.  Will continue retrying after every", $retry_time, "seconds.";
			}
			# sleep for a while before retrying.
			sleep($retry_time);
		}
	}
	
	logmsg 1, $fn, $attempts == 1 ? "Connect" : "Reconnect", "to ($host) at ($port) successful.";
	return *SOCK;
}

# --------------------------------- Protocol specific Stuff --------------------
sub make_packet {
	return sprintf("%s%05d%s%s", $STX, $counter++, $_[0], $ETX);
}

sub get_printable_packet {
	my $msg = shift;

	# convert the non-printable ASCII characters to meaningful strings.
	$msg =~ s/$STX/<STX>/;
	$msg =~ s/$ETX/<ETX>/;
	return $msg;
}

sub read_ack_nak {
	my $fn = 'read_ack_nak()';
	my $msg;
	my $expected_len = 8;
	my $n = sysread($fd, $msg, $expected_len, 0);
	if (!defined($n) || $n == 0) {
		# socket was reset.
		logmsg 1, $fn, "Socket was reset.  sysread returned (", defined($n) ? $n : "UNDEF", ")";

		$msg = undef;
	}
	elsif ($n != $expected_len) {
		logmsg 1, $fn, "Received incorrect ACK/NAK length ($n) back. Correct length is ($expected_len)";
	}

	return $msg;
}

sub get_printable_ack_nak {
		$_[0] =~ s/$STX/<STX>/;
		$_[0] =~ s/$ETX/<ETX>/;
		$_[0] =~ s/$ACK/<ACK>/;
		$_[0] =~ s/$NAK/<NAK>/;

		return $_[0];
}

