#!/usr/local/bin/perl
########################################################################
#
# Description: Project fix check utility.
# Usage: check_prjfix.pl <Project fix name> 
#  For example: check_prjfix.pl PF000012
#
########################################################################

$PrjFix = $ARGV[0];
my $PrjFixDirectory = "$ENV{LESDIR}/temp/$PrjFix";
$PrjFixDirectory=~s/\\/\//g;

my $PrjFixScript;
my @line;


# Set the prj fix script and make sure it exists.
$PrjFixScript = "$PrjFixDirectory\/$PrjFix";

if (! -r $PrjFixScript || ! -r $PrjFix)
{
    $PrjFixDirectory = "$ENV{LESDIR}/temp/$PrjFix";
    $PrjFixDirectory=~s/\\/\//g;
    $PrjFixScript = "$PrjFixDirectory\/$PrjFix";
    if (! -r $PrjFixScript)
    {
       print "ERROR: The prj fix script does not exist.\n";
       print "       $!\n";
       print "       Script: $PrjFixScript\n";
       print "       Script: $PrjFix\n";
       exit 1;
   }
}
print "The file differences for the prjfix $PrjFix are:\n\n\n";


# Open the prj fix script.
$status = open(INFILE, "$PrjFixScript");
if ($status == 0)
{
    $status = open(INFILE, "$PrjFix");
    if ($status == 0)
    {
      print "ERROR: Could not open prjfix script.\n";
      print "       $!\n";
      print "       Script: $PrjFixScript\n";
      exit 1;
    }
}


# Cycle through each line in the prj fix script.
while (<INFILE>) {
chomp;

# Skip comment lines.
next if (/^#/);                                 # Comment

# Parse the line and pick out the command.
@line = split(' ');
$command = shift @line;

# Pick out each argument.
if ($command =~ /^add$/i)                       # Add               
{
  $line[1] =~ s/%LESDIR%/$ENV{LESDIR}/ge;
  print "$command instruction\n";
  print "Comparing $line[0].\n";
  print "with $line[1]\/$line[0].\n\n\n";

  system("diff $PrjFixDirectory/$line[0] $line[1]/$line[0]");
}
elsif ($command =~ /^replace$/i)                # Replace
{
  $line[1] =~ s/%LESDIR%/$ENV{LESDIR}/ge;
  print "$command instruction\n";
  print "Comparing $line[0].\n";
  print "with $line[1]\/$line[0].\n\n\n";

  system("diff $PrjFixDirectory/$line[0] $line[1]/$line[0]");

}
}

# Close the prj fix script.
close(INFILE);
