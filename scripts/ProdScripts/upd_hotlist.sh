#!/usr/bin/ksh
#
# This script will call the script to update the usr_hotlist table with hot item information.
#


. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/scripts



#
#  This is the directory location of the sql script.  Typing sql will enable the sql prompt.  
#

#  This is to allow for a prompt to run the script

sql << //
set trimspool on
set pagesize 50000
set linesize 300
spool /opt/mchugh/prod/les/oraclereports/UpdHotList.out
@Usr_IN409FileRefUpdate.sql
spool off
exit
//
exit 0
