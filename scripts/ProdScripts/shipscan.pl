#This is for the sorter simulation

#!perl
use lib "$ENV{MOCADIR}/scripts";
use MocaReg; 
#use strict;
use DBI;

$schbat = $ARGV[0]; 

# Open a file for writing
$out = "pckwrk.dat";
open OUT, ">$out" or die "Cannot open $out for write :$!";

# This will be hardcoded
$command = "SHIPSCAN";

$subucc_charcnt = 0;
$subucc_spacecnt = 0;

$dbuser = MocaReg->get("Server", "dbuser");
$dbpass = MocaReg->get("Server", "dbpass");
$dbconn = MocaReg->get("Server", "dbconn"); 

# Which database are we looking at?
my ($usr, $pwd, $db) = ($dbuser, $dbpass, $dbconn);

# connect to the database
my $db_h = DBI->connect("DBI:Oracle:$db", $usr, $pwd)
	or die "Count not connect to database: " . DBI->errstr;

# Start by finding the objects that span more than $extent_limit # of extent
#my $st_h = $db_h->prepare("select column_name, index_name " .
#                          "  from user_ind_columns" .
#                         " where table_name = upper('$tabnam') " .
#                          " order by index_name, column_position")
#	or die "Could not prepare statement: " . $db_h->errstr;

# Finding a list of pckwrk which has pcksts of 'R', appqty = 0, and subucc is not null
my $st_h = $db_h->prepare("select subucc " .
                          "  from pckwrk " .
                          " where schbat = upper('$schbat') and pcksts = 'R' and appqty = 0 " .
                          "   and subucc is not null " .
                          " order by wrkref")
	or die "Could not prepare statement: " . $db_h->errstr;

$st_h->execute()
	or die "Could not execute statement: " . $st_h->errstr;

my @row = ();
my %segments = ();

#print "\n\n";
#print "Column Name              Index Name\n";
#print '-' x 20 . "\n";
my $previous_segment_type;
my $index = 0;
while (@row = $st_h->fetchrow_array()) {
	my ($subucc, $wrkref) = @row;
        # calculate how many space we need after subucc
        $subucc_charcnt = length($subucc);
        $subucc_spacecnt = 20 - $subucc_charcnt;

        #get the server date and time in an appropriate format 
        #the format is like this 05/31/2001 12:00:00 PM
       ($time_second, $time_minute, $time_hour,        
        $date_day, $date_month, $date_year,
        $day_week, $day_year, $isdst) = localtime(time);
        $time_hour   = sprintf("%02d", $time_hour);
        
        if ($time_hour > 12) {$AMPM = "PM";}
          else {$AMPM = "AM";}
        $time_minute = sprintf("%02d", $time_minute);    
        $time_second = sprintf("%02d", $time_second);
        $time_full = "$time_hour:$time_minute:$time_second $AMPM";
         
        if ($date_year < 70) {$date_year += 2000;}        # add century
          else {$date_year += 1900;}
   
        $workm1 = sprintf("%02d", $date_month + 1);       # force 2 digits
        $workd1 = sprintf("%02d", $date_day);
        $workd2 = sprintf("%03d", $day_year + 1);
        $date_full = "$workm1/$workd1/$date_year";      
   
        $date_time = "$date_full $time_full";

        #write out to the file
        print OUT $command," ",$subucc,' ' x $subucc_spacecnt," ",$date_time,"!","\n";
}

# release the statement handle
$st_h->finish;

$db_h->disconnect;

