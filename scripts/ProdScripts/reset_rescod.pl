#!/usr/local/bin/perl
#
# Paul Wong
# This file is for deallocate shipping lane which has problem to get reset 
# A cron task or at job should be created to call this script
# a few times a day
#
$| = 1;                   # Flush output

$cmd=<<EOF;                                
set trace where activate = 1 and trcfil = 'reset_rescod.trc'
/
[select stoloc, rescod from locmst where arecod = 'SSTG' and  not exists (select distinct rescod from pckmov where pckmov.stoloc = locmst.stoloc and pckmov.rescod = locmst.rescod) and locmst.rescod is not null] | deallocate resource location where stoloc = \@stoloc catch (@?)
/
EOF

$time = localtime;
print "$time \n $cmd";

open(MSQL,"| msql ") || die;
print MSQL $cmd;
close(MSQL);

exit(0);
