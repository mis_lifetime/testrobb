#!/usr/local/bin/perl
#
# Dynamically create the makefile and makefile.nt files for an  
# rdt form rfsrc directory
#
# NOTE: you must have cygwin tools installed and in your path for
# NT/2000 users.
#
# Usage: create_rdt_makefiles.pl (run in the directory you wish to 
#                              create the makefile in)

my @line;

#we will open the makefile.nt
#we will dynamicaly create it based on the rdt forms in it's directory 
chdir ($ENV{LESDIR}."/src/rfsrc"); 
unless (open(OUTFILE, ">makefile.nt"))
{die ("Cannot open input file makefile.nt\n");}
unless (open(OUTFILE2, ">makefile"))
{die ("Cannot open input file makefile\n");}

#we will print all the *.rdt files into a file
#the wierd name is to decrease the chances of writing over someones file
system("ls -1 *.rdt > sourcecode.names.txt");
unless (open(INFILE, "sourcecode.names.txt"))
{die ("Cannot open output file file1\n");}

#print make file header for NT
print OUTFILE ("OBV=");

#print make file header for Unix
print OUTFILE2 ("LESDIR=../..");
print OUTFILE2 ("\n\n");
print OUTFILE2 ("include \$\(LESDIR)/makefiles/StandardHeader.mk\n\n");
print OUTFILE2 ("OBV=");

#print all OBV files into makefile and makefile.nt
while (<INFILE>)
{
@line = split('.rdt\n');
$command = shift @line;
#Do not print the slash on the last line
print OUTFILE ("$command");
print OUTFILE2 ("$command");
if (eof(INFILE))
   {
    print OUTFILE (".obv \n\n");
    print OUTFILE2 (".obv \n\n");
   }
else
   { 
    print OUTFILE (".obv \\\n");
    print OUTFILE2 (".obv \\\n");
   }
}
unless (close(INFILE)) {die ("Cannot close infile")};

#re-open INFILE
unless (open(INFILE, "sourcecode.names.txt"))
{die ("Cannot open output file file1\n");}
#print OBH files for NT 
print OUTFILE ("OBH=");
#print OBH files for Unix
print OUTFILE2 ("OBH=");

#print all OBH files into makefile and makefile.nt
while (<INFILE>)
{
@line = split('.rdt\n');
$command = shift @line;
#Do not print the slash on the last line
print OUTFILE ("$command");
print OUTFILE2 ("$command");
if (eof(INFILE))
   {
    print OUTFILE (".obh \n\n");
    print OUTFILE2 (".obh \n\n");
   }
else
   { 
    print OUTFILE (".obh \\\n");
    print OUTFILE2 (".obh \\\n");
   }
}

#print tail for makefile.nt
print OUTFILE ("all\: vehicle.dat handheld.dat\n\n");
print OUTFILE ("install\: all\n");
print OUTFILE ("        \@copy vehicle.dat \$\(LESDIR\)\\data\n");
print OUTFILE ("        \@copy handheld.dat \$\(LESDIR\)\\data\n\n");
print OUTFILE ("hinstall\: \n\n");
print OUTFILE ("clean\: \n");
print OUTFILE ("        -\@del \$\(OBV\)                                 # DEVONLY (Do not remove)\n");
print OUTFILE ("        -\@del \$\(OBH\)                                 # DEVONLY (Do not remove)\n");
print OUTFILE ("        -\@del \*.dat\n\n");
print OUTFILE ("vehicle.dat \: \$\(OBV\)\n");
print OUTFILE ("        perl \$\(DCSDIR\)\\scripts\\rflink.pl -v\n\n");
print OUTFILE ("handheld.dat \: \$\(OBH\)\n");
print OUTFILE ("        perl \$\(DCSDIR\)\\scripts\\rflink.pl -h\n\n");
print OUTFILE (".SUFFIXES\:  .rdt .obh .obv\n\n");
print OUTFILE (".rdt.obh\:\n");
print OUTFILE ("        perl \$\(DCSDIR\)\\scripts\\rfcompile.pl -f \$< -h\n\n");
print OUTFILE (".rdt.obv\:\n");
print OUTFILE ("        perl \$\(DCSDIR\)\\scripts\\rfcompile.pl -f \$< -v\n\n");

#print tail for makefile
print OUTFILE2 ("all: vehicle.dat handheld.dat\n\n");
print OUTFILE2 ("install\: all\n");
print OUTFILE2 ("	\$\(INSTALL\) \$\(DATMODE\) \$\(LESDIR\)/data vehicle.dat\n");
print OUTFILE2 ("	\$\(INSTALL\) \$\(DATMODE\) \$\(LESDIR\)/data handheld.dat\n\n");
print OUTFILE2 ("hinstall\: \n\n");
print OUTFILE2 ("clean\: \n");
print OUTFILE2 ("	-\@rm -f \*.dat \*.obh \*.obv\n\n");
print OUTFILE2 ("vehicle.dat \: \$\(OBV\)\n");
print OUTFILE2 ("	\$\(DCSDIR\)/scripts/rflink.pl -v\n\n");
print OUTFILE2 ("handheld.dat \: \$\(OBH\)\n");
print OUTFILE2 ("	\$\(DCSDIR\)/scripts/rflink.pl -h\n\n");
print OUTFILE2 (".SUFFIXES\:  .rdt .obh .obv\n\n");
print OUTFILE2 (".rdt.obh\:\n");
print OUTFILE2 ("	\$\(DCSDIR\)/scripts/rfcompile.pl -f \$< -h\n\n");
print OUTFILE2 (".rdt.obv\:\n");
print OUTFILE2 ("	\$\(DCSDIR\)/scripts/rfcompile.pl -f \$< -v\n\n");

unless (close(INFILE)) {die ("Cannot close infile")};
system("rm sourcecode.names.txt");
#system("mv makefile $ENV{LESDIR}/src/rfsrc");
