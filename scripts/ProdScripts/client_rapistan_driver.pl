#!/opt/perl5/bin/perl
#
# File    : client_rapistan_driver.pl
#
# Purpose : Client - Rapistan conveyor driver for DCS v4.0 system (kludge version)
#
# Author  : Steve R. Hanchar
#
# Date    : 08-Oct-2001
#
# Usage   : client_rapistan_driver 
#
###############################################################################
#
# Include needed sub-components
#
use strict;
#
use Getopt::Std;
#
###############################################################################
#
# Initialize internal parameters
#
my $par_pthsep;
#
if ($^O =~ /win32/i)
{			# NT
    $par_pthsep = "\\";
}
else
{			# Unix
    $par_pthsep = "\/";
}
#
my $par_dcsutl = "<dcs command utility name (dsql -s)>";
#
my $par_dcscon = "<host name>:<port number>";
my $par_dcscmd = "get data for conveyor";
#
my $par_cnvcmd = "perl \$LESDIR/scripts/client_rapistan.pl -a <rapistan host> -p <rapistan part>";
#
my $par_wattim = "10";  # Wait time between queries (in seconds)
#
###############################################################################
#
# Initialize internal variables
#
my $arg_vbsflg = "";
#
my $arg_dbgflg = "";
#
###############################################################################
#
# Define run Dcs sql command routine
#
sub run_sql_cmd ()
{
    my ($sqlcon, $sqlcmd) = @_;
    #
    my $sqlerr = "";
    my $sqlflg = "";
    my $sqlrsp = "";
    my $sqlwrk = "";
    #
    printf ("\nSqlCon ($sqlcon)\nSqlCmd ($sqlcmd)\n") if ($arg_dbgflg);
    #
    # Convert connection information into msql utility arguments
    #
    $sqlcon =~ s/([^:]+):([^:]+)/-a $1 -p $2/;
    #
    # Determine working file name and delete any old working files
    #
    $sqlwrk = "$ENV{LESDIR}${par_pthsep}log${par_pthsep}client_rapistan_driver_$$.tmp";
    if (-e $sqlwrk)
    {
	printf ("\nDeleting old working file ($sqlwrk)\n") if ($arg_dbgflg);
	unlink ($sqlwrk) || die "\nFATAL ERROR - Could not delete old working file ($sqlwrk).\nStopped";
    }
    #
    # Run sql command
    #
    open   (SQLCMD, "| $par_dcsutl $sqlcon > $sqlwrk") || die "\nFATAL ERROR - Could not start sql utility.\nStopped";
    printf (SQLCMD "$sqlcmd\n");
    printf (SQLCMD "exit\n");
    close  (SQLCMD); # || die "\nFATAL ERROR - Could not run msql utility.\nStopped";
    #
    # Open sql command results file
    #
    open (SQLRSP, $sqlwrk) || die "\nFATAL ERROR - Could not access msql utility results.\nStopped";
    #
    # Read sql command results (filter out result header & blank lines)
    #
    $sqlerr = "";
    $sqlflg = "";
    $sqlrsp = "";
    #
    while (<SQLRSP>)
    {
 	chomp;
	#
	if (/^\s*$/)
	{
	    next;  # Skip blank lines
	}
	#
	if (/^MSQL>/)
	{
	    next;  # Skip command prompt lines
	}
	#
	if (/Rows Affected\)$/)
	{
	    next;  # Skip rows affected trailer line
	}
	#
	if (/Executing... Error!/)
	{
	    $sqlerr = $_;
	    next;
	}
	if ($sqlerr =~ /Executing... Error!/)
	{
	    $sqlerr = $_;  # Get line after 'Executing... Error!' line
	}
	#
	if ($sqlflg)
	{
	    s/(^.*\S)\s*$/$1 /;  # Trim excess trailing blanks
	    $sqlrsp .= $_."\n";
	}
	#
	if (/^-/)
	{
	    $sqlflg = "1";  # End of results header detected
	}
    }
    #
    # Verify no errors occurred running sql command
    #
    if (!$sqlrsp && $sqlerr)
    {
	if ($sqlerr =~ /Success/i)
	{
	    $sqlerr = "";
	}
	#
	if ($sqlerr =~ /-1403/)
	{
	    $sqlerr = "";
	}
	#
	if ($sqlerr =~ /^Command affected no rows$/i)
	{
	    $sqlerr = "";
	}
	#
	if ($sqlerr =~ /^Result: 512 - ORA-00001/i)  # Continue on this error
	{
	    printf ("SqlErr: Duplicate entry insert... continuing\n") if ($arg_vbsflg);
	    $sqlrsp = $sqlerr;
	    $sqlerr = "";
	}
    }
    #
    if (!$sqlrsp && $sqlerr)
    {
	printf ("\nFATAL ERROR - Error occurred running sql command\n");
	printf ("\nSqlCmd: ($sqlcmd)\n");
	printf ("SqlErr: ($sqlerr)\n");
	die;
    }
    #
    # Close & delete sql command results file
    #
    close  (SQLRSP);
    unlink ($sqlwrk);
    #
    if ($arg_dbgflg)
    {
	if ($sqlrsp =~ /\n/)
	{
	    printf ("SqlRsp:\n$sqlrsp<End of SqlRsp>\n");
	}
	else
	{
	    printf ("SqlRsp: ($sqlrsp)\n\n");
	}
    }
    #
    return $sqlrsp;
}
#
###############################################################################
#
# Wake up periodically and relay any command information from the Dcs server
# to the rapistan conveyor.
#
my $sqlrsp;
#
while (1)
{
    print "Sleeping...\n";
    sleep ($par_wattim);
    print "Processing...\n";
    #
    # Query server for information to transmit to rapistan conveyer
    #
    $sqlrsp = &run_sql_cmd ($par_dcscon, $par_dcscmd);
    #
    # If information was returned
    #
    if ($sqlrsp ne "")
    {
	open (OUTFIL, "| $par_cnvcmd") || die "FATAL ERROR - Could not start conveyor transmission utility\nStopped";
	print (OUTFIL $sqlrsp);
	close (OUTFIL);
    }
}
#
###############################################################################
#
# Common exit point for command file.  Perform any required clean-up activities
#
exit (0);
#
###############################################################################
