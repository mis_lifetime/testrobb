/* Below are the year end scipts that should be used to obtain selected */
/* EOY information. Certain scripts may have to be modified to obtain */
/* archive information. */

-- Receiving Information --

/* The # of pallets recd may be similar to what is found in the usr_pallet_counts */
/* table.  #of cartons recd will be similar to usr_weeklystats table for the sum */
/* of status CRI and CRD and #of Units recd will be similar to usr_weeklystats */
/* table for status URC. The sum of the figures for # of pallets, may be higher */
/* per day and month because certain pallets will contain multiple part#s, in */
/* which case the same pallet id is being counted more than once. */  

select trunc(rcvtrk.clsdte) "Receipt Date", rcvtrk.trknum "Container", rcvlin.prtnum "SKU(item)",
count(distinct b.lodnum) "#Of Pallets", round(rcvlin.rcvqty/decode(untcas,0,1,untcas)) "#Of Cartons",
rcvlin.rcvqty "#Of Units"
    from rcvtrk, rcvinv, rcvlin,prtmst,dlytrn b
    where rcvtrk.trknum = rcvinv.trknum
    and rcvtrk.trknum = rcvlin.trknum
    and rcvlin.trknum=rcvinv.trknum
    and rcvlin.supnum= rcvinv.supnum
    and rcvlin.invnum =rcvinv.invnum
    and rcvlin.client_id = rcvinv.client_id
    and rcvlin.prtnum = prtmst.prtnum
    and rcvtrk.trksts = 'C' and rcvqty !=0
    and rcvtrk.trknum = b.tostol
    and rcvlin.prtnum = b.prtnum
    and b.oprcod = 'UID'
    and rcvtrk.expdte is not null
    and trunc(rcvtrk.clsdte) between '01-DEC-2003' and '31-DEC-2003'
group by trunc(rcvtrk.clsdte), rcvtrk.trknum , rcvlin.prtnum ,
round(rcvlin.rcvqty/decode(untcas,0,1,untcas)) , rcvlin.rcvqty
order by 1


/* This script may have to be used to obtain archive information if the */
/* archive info is still located in the daily trans production table */

select trunc(rcvtrk.clsdte) "Receipt Date", rcvtrk.trknum "Container", 
rcvlin.prtnum "SKU(item)",count(distinct b.lodnum) "#Of Pallets",
round(rcvlin.rcvqty/decode(untcas,0,1,untcas)) "#Of Cartons", rcvlin.rcvqty "#Of Units"
    from rcvtrk@arch, rcvinv@arch, rcvlin@arch ,prtmst@arch ,dlytrn b
    where rcvtrk.trknum = rcvinv.trknum
    and rcvtrk.trknum = rcvlin.trknum
    and rcvlin.trknum=rcvinv.trknum
    and rcvlin.supnum= rcvinv.supnum
    and rcvlin.invnum =rcvinv.invnum
    and rcvlin.client_id = rcvinv.client_id
    and rcvlin.prtnum = prtmst.prtnum
    and rcvtrk.trksts = 'C' and rcvqty !=0
    and rcvtrk.trknum = b.tostol
    and rcvlin.prtnum = b.prtnum
    and b.oprcod = 'UID'
    and rcvtrk.expdte is not null
    and trunc(rcvtrk.clsdte) between '01-JAN-2003' and '28-DEC-2003'
group by trunc(rcvtrk.clsdte), rcvtrk.trknum , rcvlin.prtnum ,
round(rcvlin.rcvqty/decode(untcas,0,1,untcas)) , rcvlin.rcvqty
union
select trunc(rcvtrk.clsdte) "Receipt Date", rcvtrk.trknum "Container", rcvlin.prtnum 
"SKU(item)",count(distinct b.lodnum) "#Of Pallets",
round(rcvlin.rcvqty/decode(untcas,0,1,untcas)) "#Of Cartons", rcvlin.rcvqty "#Of Units"
    from rcvtrk@arch, rcvinv@arch, rcvlin@arch ,prtmst@arch ,dlytrn@arch b
    where rcvtrk.trknum = rcvinv.trknum
    and rcvtrk.trknum = rcvlin.trknum
    and rcvlin.trknum=rcvinv.trknum
    and rcvlin.supnum= rcvinv.supnum
    and rcvlin.invnum =rcvinv.invnum
    and rcvlin.client_id = rcvinv.client_id
    and rcvlin.prtnum = prtmst.prtnum
    and rcvtrk.trksts = 'C' and rcvqty !=0
    and rcvtrk.trknum = b.tostol
    and rcvlin.prtnum = b.prtnum
    and b.oprcod = 'UID'
    and rcvtrk.expdte is not null
    and trunc(rcvtrk.clsdte) between '01-JAN-2003' and '28-DEC-2003'
group by trunc(rcvtrk.clsdte), rcvtrk.trknum , rcvlin.prtnum ,
round(rcvlin.rcvqty/decode(untcas,0,1,untcas)) , rcvlin.rcvqty
order by 1


--Shipping Information --

/* To count pallets, do not use the formula shpqty/untpal, the figures will */
/* be grossly inaccurate. #of Pallets will have certain pallet Ids counted */
/* twice due to multiple items be on the same pallet.  The # of Cartons Shipped */
/* will be similar to the usr_weeklystats table under code S and the # of Units */
/* shipped will be similar to the usr_weeklystats table under code UNS. The number */
/* of cartons shipped may be different for certain days or months because muliple */
/* items can be in the same carton, and the carton (subnum) will be counted more */
/* than once.  The only way this would not happen, is if all cartons shipped were */
/* full case. */

select x.dispatch_dte "Shipping Date", x.btcust "Customer#", x.prtnum "SKU(Item)",x.ordnum 
"Order#", sum(x.pal) "#Of Pallets", sum(x.totcarton) "Of Cartons", sum(x.shpqty) "#Of Units"
from
(select  distinct i.prtnum, btcust,ord_line.ordlin,sh.ship_id, sd.shpqty, ord.ordnum,
         trunc(tr.dispatch_dte) dispatch_dte,
         count(distinct i.subnum) totcarton,count(distinct s.lodnum) pal
         from invdtl@arch i, invsub@arch s,shipment_line@arch sd, shipment@arch sh,
         ord@arch,ord_line@arch, stop@arch, trlr@arch tr,car_move@arch cr
               where i.ship_line_id            =  sd.ship_line_id
               AND i.subnum = s.subnum
               AND ord.ordnum                  = ord_line.ordnum
               AND ord.client_id               = ord_line.client_id
               AND ord_line.client_id          = sd.client_id
               AND ord_line.ordnum             = sd.ordnum
               AND ord_line.ordlin             = sd.ordlin
               AND ord_line.ordsln             = sd.ordsln
               AND sd.ship_id                  = sh.ship_id
               AND sh.stop_id                  = stop.stop_id
               AND stop.car_move_id            = cr.car_move_id
               AND cr.trlr_id                  = tr.trlr_id
               AND sd.shpqty <> 0
               AND sh.shpsts                   = 'C'
               AND ord.ordtyp                   != 'W'
               AND trunc(tr.dispatch_dte) between '01-DEC-2003' and '31-DEC-2003'
   group by i.prtnum, btcust,ord_line.ordlin,sh.ship_id, sd.shpqty,ord.ordnum,    
trunc(tr.dispatch_dte)) x
group by x.dispatch_dte, x.btcust, x.prtnum,x.ordnum
/

/* This script was used to obtain the shipping information including the wave #. */
/* Valid waves needed to be confirmed using the pckbat table.  Because the pckbat@arch */
/* table did not have any records, we created a temporary table usr_pckbat@arch */
/* to hold the information found in the pckbat@prod table and obtained the information */
/* We had a problem connecting to the pckbat@prod table from the archive instance. */

select x.dispatch_dte "Shipping Date", x.btcust "Customer#", x.prtnum "SKU(Item)",x.ordnum
"Order#", x.schbat "Wave #",sum(x.pal) "#Of Pallets", sum(x.totcarton) "Of Cartons", sum(x.shpqty) "#Of Units"
from
(select  distinct i.prtnum, btcust,ord_line.ordlin,sh.ship_id, sd.schbat, sd.shpqty, ord.ordnum,
         trunc(tr.dispatch_dte) dispatch_dte,
         count(distinct i.subnum) totcarton,count(distinct s.lodnum) pal
         from invdtl@arch i, invsub@arch s,shipment_line@arch sd, usr_pckbat@arch p,shipment@arch sh,
         ord@arch,ord_line@arch, stop@arch, trlr@arch tr,car_move@arch cr
               where i.ship_line_id            =  sd.ship_line_id
               AND i.subnum = s.subnum
               AND ord.ordnum                  = ord_line.ordnum
               AND ord.client_id               = ord_line.client_id
               AND ord_line.client_id          = sd.client_id
               AND ord_line.ordnum             = sd.ordnum
               AND ord_line.ordlin             = sd.ordlin
               AND ord_line.ordsln             = sd.ordsln
               AND sd.ship_id                  = sh.ship_id
               AND sd.schbat                   = p.schbat
               AND p.wave_prc_flg =1
               AND sh.stop_id                  = stop.stop_id
               AND stop.car_move_id            = cr.car_move_id
               AND cr.trlr_id                  = tr.trlr_id
               AND sd.shpqty <> 0
               AND sh.shpsts                   = 'C'
               AND ord.ordtyp                   != 'W'
               AND trunc(tr.dispatch_dte) between '01-JAN-2003' and '31-JAN-2003'
   group by i.prtnum, btcust,ord_line.ordlin,sh.ship_id,sd.schbat,sd.shpqty,ord.ordnum,
trunc(tr.dispatch_dte)) x
group by x.dispatch_dte, x.btcust, x.prtnum,x.ordnum,x.schbat
/


-- Production Inbound --

/* The counts the numbers for inbound components.  Items from their */
/* specified locations come in on their own pallet, so the pallet calculation */
/* is not needed.  Plus counting the actual pallet id the items came in on can */
/* only be obtained in daily tran, but a link to that pallet record cannot be */
/* linked. Production Outbound does count the pallet IDs in invlod, but you */
/* will notice that mostly all of the records only have 1 pallet count */

select distinct trunc(clsdte) "Inbound Date" ,wkodtl.prtnum "SKU(Item)",'1' "#Of Pallets",
sum(pckwrk.pckqty)/decode(pckwrk.untcas,0,1,pckwrk.untcas) "# of Cases" ,
sum(pckwrk.pckqty) "# Of Units",srcloc "From Location"
                  from wkohdr, wkodtl,pckwrk
                  where
                  wkohdr.wkonum = wkodtl.wkonum
                  and wkohdr.wkorev = wkodtl.wkorev
                  and wkohdr.client_id = wkodtl.client_id
                  and wkosts = 'C' and tot_dlvqty <> 0
                  and pckwrk.wkonum = wkodtl.wkonum
                  and pckwrk.prtnum = wkodtl.prtnum
                  and trunc(clsdte) = '23-FEB-2004'
group by trunc(clsdte), wkodtl.prtnum, pckwrk.untcas,srcloc

-- Production Outbound --

/* This script counts the numbers for outbound partmaster items. */
/* The actual carton (subnum), pallet (lodnum) and destination location (stoloc) */
/* can be obtained by linking from the workorder tables to rcvlin to invdtl, */
/* invsub and invlod. We may see most records with a pallet count of 1. */
/* To obtain archived information, please test the trknum in rcvlin to make */
/* sure they exist in the rcvlin@arch and that its rcvkey is in invdtl@arch. */
/* For 2003, the trknum and rcvkeys were still in production and we had to use */  
/* the below script. Because this uses a lot of tables, this may have to be */
/* run a month at a time */

select x.clsdte "Outbound Date", x.prtnum "SKU(item)", count(distinct x.lodnum) "#Of Pallets", 
count(distinct x.subnum) "#Of Cases",sum(x.untqty) "#Of Units",x.stoloc "To Location"
from
(select distinct wkohdr.wkonum,wkohdr.prtnum,rcvlin.rcvkey,invsub.lodnum,invdtl.subnum,
invdtl.untqty,invdtl.dtlnum, invlod.stoloc,trunc(clsdte) clsdte
       from wkohdr@arch, wkodtl@arch,rcvlin,invsub@arch,invlod@arch, invdtl@arch
       where wkohdr.wkonum = wkodtl.wkonum
        and wkohdr.wkorev = wkodtl.wkorev
        and wkohdr.client_id = wkodtl.client_id
        and wkosts = 'C'
        and trunc(clsdte) between '01-JAN-2003' and '31-JAN-2003'
        and rcvlin.trknum = wkohdr.wkonum
        and rcvlin.rcvkey = invdtl.rcvkey
        and invdtl.subnum = invsub.subnum
        and invsub.lodnum = invlod.lodnum
        and rcvlin.invnum = '1'
        and rcvlin.client_id = '----'
        and rcvlin.supnum = 'PROD' ) x
group by x.clsdte,x.prtnum,x.stoloc


-- Picking Data --

/* A union of both production and archive should be done for all months. */
/* Some older records were found in production. You may have to run this */
/* 2 or 3 months at a time. */

select trunc(b.lstdte) "PICK DATE",a.ordnum "ORDER#",b.prtnum "SKU(item)",
count(distinct d.lodnum) "#OF PALLETS",count(distinct b.subnum) "#OF CASES",
sum(a.pckqty) "UNTQTY", a.srcloc "FROM LOCATION",d.stoloc "TO LOCATION"
                                from pckwrk a,invsub c,invlod d,invdtl b
                                where a.wrkref = b.wrkref
                                and a.ship_line_id = b.ship_line_id
                                and a.prt_client_id = b.prt_client_id
                                and a.prtnum = b.prtnum
                                and b.subnum = c.subnum
                                and c.lodnum = d.lodnum
                                and a.wrktyp = 'P'
                                and a.appqty  > 0           
                        and trunc(b.lstdte) between '01-DEC-2003' and '31-DEC-2003'
group by trunc(b.lstdte) ,a.ordnum,b.prtnum,
a.srcloc,d.stoloc
union
select trunc(b.lstdte) "PICK DATE",a.ordnum "ORDER#",b.prtnum "SKU(item)",
count(distinct d.lodnum) "#OF PALLETS",count(distinct b.subnum) "#OF CASES",
sum(a.pckqty) "UNTQTY", a.srcloc "FROM LOCATION",d.stoloc "TO LOCATION"
                                from pckwrk@arch a,invsub@arch c,invlod@arch d,invdtl@arch b
                                where a.wrkref = b.wrkref
                                and a.ship_line_id = b.ship_line_id
                                and a.prt_client_id = b.prt_client_id
                                and a.prtnum = b.prtnum
                                and b.subnum = c.subnum
                                and c.lodnum = d.lodnum
                                and a.wrktyp = 'P'
                                and a.appqty  > 0            
                        and trunc(b.lstdte) between '01-DEC-2003' and '31-DEC-2003'
group by trunc(b.lstdte) ,a.ordnum,b.prtnum,
a.srcloc,d.stoloc
/
