use strict;
use DBI;

# Which database are we looking at?
my ($usr, $pwd, $db) = ('TWMT', 'TWMT', 'WMST');
# how many extents in an object are accecptable?
my $extents_limit = 5;

# connect to the database
my $db_h = DBI->connect("DBI:Oracle:$db", $usr, $pwd)
	or die "Count not connect to database: " . DBI->errstr;

# Start by finding the objects that span more than $extent_limit # of extent
my $st_h = $db_h->prepare("select segment_name, segment_type, extents" .
						   " from user_segments" .
						  " where extents > $extents_limit" .
						  " order by segment_type desc, extents desc")
	or die "Could not prepare statement: " . $db_h->errstr;

$st_h->execute()
	or die "Could not execute statement: " . $st_h->errstr;

my @row = ();
my %segments = ();

print "\n\n";
print "Following segments span more than $extents_limit extents.\n";
print '-' x 50 . "\n";
my $previous_segment_type;
my $index = 0;
while (@row = $st_h->fetchrow_array()) {
	my ($segment_name, $segment_type, $extents) = @row;
	# this if-els block would print a blank line between the list of tables and indexes.
	if (!defined $previous_segment_type) {
		$previous_segment_type = $segment_type;
	}
	elsif ($previous_segment_type ne $segment_type) {
		$previous_segment_type = $segment_type;
		$index = 0;
		print "\n";
	}

	$segments{$segment_type}[$index++] = $segment_name;
	printf "%-30s|%6s |%6d\n", $segment_name, $segment_type, $extents;
}

# release the statement handle
$st_h->finish;

# Now call analyze table on each of these objects
print "Proceeding to analyze each of these database objects.\n";
print "This may take several minutes depending on the size of objects.\n";
foreach my $segment_type (keys %segments) {
	foreach my $segment_name (@{$segments{$segment_type}}) {
		print "Analyzing $segment_type $segment_name to compute statistics.\n";

		# prepeare statment
		$st_h = $db_h->prepare("analyze $segment_type $segment_name compute statistics")
			or die "Cannot prepare statement: " . $st_h->errstr;

		# execute the statement
		$st_h->execute() or die "Cannot execute statement: " . $st_h->errstr;
	}
}
# release the statement handle
$st_h->finish;

$db_h->disconnect;

