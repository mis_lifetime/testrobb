#!/usr/local/bin/perl
########################################################################
#
# Description: Project fix check utility.
# Usage: check_prjfix.pl <Project fix name> 
# For example: check_prjfix.pl PF000012
#
########################################################################

# Initialize internal parameters
#
if ($^O =~ /win32/i)
{                       # NT
    $par_pthsep = "\\";
}
else
{                       # Unix
    $par_pthsep = "\/";
}

sub print_help ()
{
   printf ("\n");
   printf ("Check Project fix file differences \n");
   printf ("\n");
   printf ("Usage:  check_prjfix.pl  <prjfix> \n");
   printf ("\n");
   printf ("    <prjfix>) The project fix number \n");
   printf ("\n");
   printf ("\n For Ex. check_prjfix.pl PF000012 \n");
}

$prjfix = $ARGV[0];

# If the argument is not passed, exit out
if (!$prjfix)
{
  print_help();
  exit 1;
}

# Set the correctdirectory based on where the command is running from 
if (!-d $prjfix)
{
   $prjfix_dir = "\.";
}
else
{
   $prjfix_dir = "$prjfix";
}

my $prjfix_script = "$prjfix_dir".$par_pthsep."$prjfix";

if (! -r $prjfix_script)
{
   print_help();
   exit 1;
}

my @line;

print "The file differences for the prjfix $PrjFix are:\n\n\n";

# Open the prj fix script.
$status = open(INFILE, "$prjfix_script");
if ($status == 0)
{
   print "ERROR: Could not open prjfix script.\n";
   print "       $!\n";
   print "       Script: $prjfix_script\n";
   exit 1;
}


# Cycle through each line in the prj fix script.
while (<INFILE>) 
{
  chomp;

  # Skip comment lines.
  next if (/^#/);                                 # Comment

  # Parse the line and pick out the command.
  @line = split(' ');
  $command = shift @line;

  # Pick out each argument.
  if ($command =~ /^add$/i)                       # Add               
  {
    $line[1] =~ s/%LESDIR%/$ENV{LESDIR}/ge;
    print "$command instruction\n";
    print "Comparing $line[0].\n";
    print "with $line[1]\/$line[0].\n\n\n";

    system("diff $prjfix_dir/$line[0] $line[1]/$line[0]");
  }
  elsif ($command =~ /^replace$/i)                # Replace
  {
    $line[1] =~ s/%LESDIR%/$ENV{LESDIR}/ge;
    print "$command instruction\n";
    print "Comparing $line[0].\n";
    print "with $line[1]\/$line[0].\n\n\n";

    system("diff $prjfix_dir/$line[0] $line[1]/$line[0]");

  }
}

# Close the prj fix script.
close(INFILE);
