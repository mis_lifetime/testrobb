/* #REPORTNAME=User End Of Shift RF Report */
/* #HELPTEXT= This report provides the lodnum and location for  */
/* #HELPTEXT= any loads where the location is an RF gun (tel..) */
/* #HELPTEXT= Requested by Shipping - January 6, 2005 */

/* Author: Al Driver */
/* This report is to provide the visibility of inventory */
/* on the RF guns at the end of the day  */

ttitle left print_time -
       center 'User End Of Shift RF Report' -
       right print_date skip 1 -
btitle center 'Page: ' format 999 sql.pno

column lodnum heading 'Pallet ID'
column arecod format  a20
column stoloc heading 'Location'
column stoloc format  a20


select distinct invlod.lodnum,invlod.stoloc 
from invsub,invlod,invdtl
where invdtl.subnum = invsub.subnum
and invsub.lodnum = invlod.lodnum
and stoloc like 'TEL%'
/
