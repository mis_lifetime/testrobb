#!/usr/local/bin/perl
use lib "$ENV{MOCADIR}/scripts";
use MocaReg; 
#use strict;
use DBI;

$tabnam = $ARGV[0]; 

$dbuser = MocaReg->get("Server", "dbuser");
$dbpass = MocaReg->get("Server", "dbpass");
$dbconn = MocaReg->get("Server", "dbconn"); 

# Which database are we looking at?
my ($usr, $pwd, $db) = ($dbuser, $dbpass, $dbconn);

# connect to the database
my $db_h = DBI->connect("DBI:Oracle:$db", $usr, $pwd)
	or die "Count not connect to database: " . DBI->errstr;

# Start by finding the objects that span more than $extent_limit # of extent
my $st_h = $db_h->prepare("select column_name, index_name " .
                          "  from user_ind_columns" .
                          " where table_name = upper('$tabnam') " .
                          " order by index_name, column_position")
	or die "Could not prepare statement: " . $db_h->errstr;

$st_h->execute()
	or die "Could not execute statement: " . $st_h->errstr;

my @row = ();
my %segments = ();

print "\n\n";
print "Column Name              Index Name\n";
print '-' x 35 . "\n";
my $previous_segment_type;
my $index = 0;
while (@row = $st_h->fetchrow_array()) {
	my ($colnam, $tabnam) = @row;
	# this if-els block would print a blank line between the list of tables and indexes.
	printf "%-20s%15s \n", $colnam, $tabnam;
}

# release the statement handle
$st_h->finish;

$db_h->disconnect;

