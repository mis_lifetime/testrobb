set linesize 250
set pagesize 200
set echo on

select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'FORECAST_TEMP1';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'INCANALOT';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_PACKAGINGr';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_AAFES';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_CLASSDTL';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_CNT_ACCURACY';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_CNTACC_CATS';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_CNTHST';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_CSTFORECAST';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_FORECASTA';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_FORECASTA';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_HOTLIST';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_ITEMCOSTS';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_MONTHLY_ADJS';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_OPENORDS';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_SHIPDOCK';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_SHP_HDR';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_TEMP_KHSUMM';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_TEMP_SNAP_4WALL';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_TEMP_SNAP_PEND';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_TEMP_SNAP_SHIP';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_TOTALFORECAST';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_TOTALFORECAST';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_WEEKLYSTATS';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_WMITEMS';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'USR_WORKORDER_SCRAP';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'VAR_ALT_PRTNUM';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'VAR_ALT_PRTNUM';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'VAR_LBLSEQ';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'VAR_LOC_CHG';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'VAR_WAVPAR_DTL';
select constraint_name, constraint_type, table_name, r_owner, r_constraint_name, index_name
from dba_constraints where table_name = 'VAR_WAVPAR_HDR';

select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'INDX_FORECAST_TEMP1';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'INTCANALOT';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'KHSUMM';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'TOSTOL';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_AAFES_PRTNUM';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_AAFES_PRTSEQ';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_CLSDTE_IDX';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_CNT_ACC_IDX';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_CNTACC_CATS_IDX';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_CSTFORECAST_IDX';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_FORECASTA_PRTNUM';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_HOTLIST_PRTNUM';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_ITEMCOSTS_PRTNUM';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_MONTHLY_ADJS_IDX1';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_OPENORDS_CPOORD';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_PACKAGING';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_SHP_HDR_PRT';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_TEMP_SNAP_4WALL_PK';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_TOTALFORECAST_PRTNUM';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_USR_CNTHST_IDX1';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_USR_CNTHST_IDX2';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_USR_CNTHST_IDX3';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_USR_CNTHST_IDX4';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_USR_CNTHST_IDX5';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_WMITEMS_PRTNUM';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'USR_WORKORDER_SCRAP_IDX1';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'VAR_ALT_PRTNUM_PK';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'VAR_LBLSEQ_IDX1';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'VAR_LBLSEQ_IDX2';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'VAR_LOC_CHG_STOLOC_PK';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'VAR_WAVPAR_DTL_PK';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'VAR_WAVPAR_HDR_PK';
select index_name, index_type, table_name, table_type, uniqueness, distinct_keys, parameters
from dba_indexes where index_name = 'WEEKLYSTATS_IDX1';
