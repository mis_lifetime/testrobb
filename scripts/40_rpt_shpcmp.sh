#/bin/ksh

eset prod

sql <<EOF
set pagesize 10000
set linesize 90
spool 40_shipment_completed.rpt
select distinct shphdr.shipid, shpdtl.ordnum, shpdtl.ordlin, somdtl.prtnum, 
       shpdtl.shpqty
from shphdr, shpdtl, somdtl 
where somdtl.ordnum = shpdtl.ordnum
  and somdtl.ordlin = shpdtl.ordlin
  and somdtl.ordsln = shpdtl.ordsln
  and shphdr.shipid = shpdtl.shipid 
  and shphdr.shpseq = shpdtl.shpseq
  and shphdr.subsid = shpdtl.subsid
  and shphdr.ordnum = shpdtl.ordnum
  and shphdr.shpsts = 'C' 
  and shphdr.loddte >= to_date('1/1/2002','MM/DD/YYYY')
  and shpdtl.shpqty > 0
union
select distinct shphdr.shipid, shpdtl.ordnum, shpdtl.ordlin, somdtl.prtnum,
       -(shpdtl.linqty -(shpdtl.pckqty + shpdtl.shpqty)) 
from shphdr, shpdtl, somdtl
where somdtl.ordnum = shpdtl.ordnum
  and somdtl.ordlin = shpdtl.ordlin
  and somdtl.ordsln = shpdtl.ordsln
  and shphdr.shipid = shpdtl.shipid
  and shphdr.shpseq = shpdtl.shpseq
  and shphdr.subsid = shpdtl.subsid
  and shphdr.ordnum = shpdtl.ordnum
  and shphdr.shpsts = 'C'
  and shphdr.loddte >= to_date('1/1/2002','MM/DD/YYYY')
  and somdtl.bckflg = 'N'
  and shpdtl.linqty > shpdtl.shpqty
/
