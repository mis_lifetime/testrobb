#!perl
#
# File    : (dcs_)dunload.pl
#
# Purpose : DCS distributed data unload (friendly interface to mload)
#
# Author  : Steve R. Hanchar
#
# Date    : 15-Nov-2000
#
# Usage   : dunload <topdir> <dirnam> <filnam>
#
###############################################################################
#
# Include needed sub-components
#
require "getopts.pl";
#
###############################################################################
#
# Initialize internal parameters
#
if ($^O =~ /win32/i)
{			# NT
    $par_pthsep = "\\";
}
else
{			# Unix
    $par_pthsep = "\/";
}
#
$par_datdir = $par_pthsep."db".$par_pthsep."data".$par_pthsep."unload";
#
###############################################################################
#
# Initialize internal variables
#
$arg_topdir = "";
#
$arg_dirnam = "";
#
$arg_filnam = "";
#
###############################################################################
#
# Define print help routine
#
sub print_help()
{
    printf ("\n");
    printf ("Distributed data unload (friendly interface to mload)\n");
    printf ("\n");
    printf ("Usage:  dunload <topdir> <dirnam> <filnam>\n");
    printf ("\n");
    printf ("    <topdir>) Top directory name (usually defined by alias)\n");
    printf ("\n");
    printf ("    <dirnam>) Distributed data directory name\n");
    printf ("\n");
    printf ("    <filnam>) Distributed data file name\n");
    printf ("\n");
}
#
###############################################################################
#
# Input argument checking/processing
#
#
# Get command line arguments
#
$arg_topdir = @ARGV[0];
$arg_dirnam = @ARGV[1];
$arg_filnam = @ARGV[2];
#
# Verify required arguments are present
#
if (!$arg_topdir)
{
    printf ("Top directory name is not defined\n");
    print_help ();
    exit (1);
}
#
if (!$arg_dirnam)
{
    printf ("Distributed data directory name is not defined\n");
    print_help ();
    exit (1);
}
#
#if (!$arg_filnam)
#{
#    printf ("Distributed data file name is not defined\n");
#    print_help ();
#    exit (1);
#}
#
printf ("\nArgument List\n");
printf ("TopDir ($arg_topdir)\n");
printf ("DirNam ($arg_dirnam)\n");
printf ("FilNam ($arg_filnam)\n");
printf ("\n");
#
###############################################################################
#
# Move to appropriate data dictionary directory structure
#
chdir ("$arg_topdir$par_datdir");
if (!-d $arg_dirnam)
{
    printf ("Directory name ($arg_dirnam) is not valid\n");
    exit (1);
}
#
###############################################################################
#
# Invoke standard DCS utility to unload distributed data file
#
$cmdtxt = "mload -H -D $arg_dirnam -c $arg_dirnam.ctl";
if ($arg_filnam)
{
    $cmdtxt .= " -d $arg_filnam";
}
#
printf ("\nUnload Command\n$cmdtxt\n");
#
system ($cmdtxt);
#
###############################################################################
#
# Common exit point for command file.  Perform any required clean-up activities
#
exit (0);
#
###############################################################################
