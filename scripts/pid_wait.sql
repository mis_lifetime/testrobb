/*
$Log: pid_wait.sql,v $
Revision 1.1.1.1  2001/09/18 23:05:50  lh51sh
Initial Import

Revision 1.1  2001/06/11  20:22:31  20:22:31  lh51sh
System Test Start

# Revision 1.10  1996/11/27  19:29:43  dmp_sw
# freeze for V4_2
#
# Revision 1.9  1996/08/09  18:41:11  dmp_sw
# freeze for V4_10
#
# Revision 1.8  1996/04/19  14:11:28  dmp_sw
# freeze for V4_09
#
# Revision 1.7  1996/02/01  15:11:57  phx_sw
# freeze for V4_08
#
# Revision 1.6  1996/01/04  20:51:08  phx_sw
# Converted to more common oracle views
#
# Revision 1.5  1995/11/09  20:44:40  phx_sw
# freeze for V4_07
#
# Revision 1.4  1995/10/09  15:25:14  phx_dp
# freeze for V4_06
#
# Revision 1.3  1995/09/07  23:47:55  phx_dp
# freeze for V4.05
#
# Revision 1.2  1995/07/27  18:01:06  phx_sw
# Remember GRANT SELECT ON TABLES OWNERED BY SYS
#
# Revision 1.1  1995/07/27  17:59:05  phx_sw
# Initial revision
#
*/
set pagesize 10000


select s.sid SID, p.pid OPID, s.username, s.process, s.osuser, s.machine, s.terminal, s.program, s.lockwait 
from v$process p, v$session s 
where 
p.addr = s.paddr 
and s.type='USER'
and s.lockwait is not null
/

select s.sid SID, p.pid OPID, s.username, s.process, s.osuser, s.machine, s.terminal, s.program, s.lockwait , l1.request
from v$process p, v$session s, v$lock l1, v$lock l2
where 
p.addr = s.paddr 
and s.type='USER'
and s.sid = l1.sid
and l1.sid!=l2.sid
and l1.type=l2.type
and l1.id1 = l2.id1
and l1.id2 = l2.id2
and l2.type = 'TM'
and l2.sid in (select s3.sid 
from v$session s3 
where 
s3.type='USER'
and s3.lockwait is not null)
intersect
select s.sid SID, p.pid OPID, s.username, s.process, s.osuser, s.machine, s.terminal, s.program, s.lockwait, l1.request
from v$process p, v$session s, v$lock l1, v$lock l2
where 
p.addr = s.paddr 
and s.type='USER'
and s.sid = l1.sid
and l1.sid!=l2.sid
and l1.type=l2.type
and l1.id1 = l2.id1
and l1.id2 = l2.id2
and l2.type = 'TX'
and l2.sid in (select s3.sid 
from v$session s3 
where 
s3.type='USER'
and s3.lockwait is not null)
/
exit;

