#!/usr/bin/ksh
#
# This script will change location for shipment.
#


. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/scripts

echo "Enter Shipment Id: \c" 
read SHIP_ID
echo "Enter Location: \c" 
read STOLOC
echo "Enter Lod Number: \c" 
read LODNUM
echo "Enter Arecod Value: \c" 
read ARECOD
sql << //
set trimspool on
set pagesize 50 
set linesize 100  
spool /opt/mchugh/prod/les/scripts/ChangeLocation.out
select * from invlod where lodnum = '$LODNUM';
update invlod set stoloc = '$STOLOC' where lodnum = '$LODNUM';
commit;
select * from pckmov where cmbcod in (select cmbcod from pckwrk where ship_id = '$SHIP_ID');
update pckmov set arecod = '$ARECOD', stoloc = '$STOLOC' where cmbcod in (select cmbcod from pckwrk where ship_id = '$SHIP_ID');
commit;
spool off
exit
//
exit 0
