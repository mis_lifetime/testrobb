#!/opt/perl5/bin/perl
#
# File    : RfBootstrap.pl
#
# Purpose : RF Terminal Driver Interface Bootstrap Script
#
# Author  : Steve R. Hanchar
#
# Date    : 23-Mar-2000
#
# Usage   : RfBootstrap [-r <remote-host>] [-h] [-i] [-v] [-d]
#
###############################################################################
#
# Installation Instructions:
#
#    1) Copy RfBootstrap_template.pl & RfBootstrap_template.dat to desired
#       location.  Recommended locations are rf terminal account home directory
#       or %LESDIR%\scripts.
#
#    2) Rename RfBootstrap_template.pl to RfBootstrap.pl and
#       rename RfBootstrap_template.dat to RfBootstrap.dat
#
#    3) Edit RfBootstrap.dat to define dcs server, remote hosts (terminal
#       controllers), & rf terminals.  Remember to also define file paths
#       for where the rf terminal driver (dcsrdt) is and where you want
#       temporary working files to be created.
#
#    4) For each rf terminal account, edit the account's profile to change its
#       shell to perl and the shell argument to run RfBootstrap.pl.  If using
#       multiple accounts, remember include the 'remote host' information
#       (example: "RfBootstrap -r intctl01").
#
#    5) Test completed installation by logging in with an rf terminal
#
###############################################################################
#
# Include needed sub-components
#
require "getopts.pl";
#
################################################################################
#
# Initialize internal parameters
#
if ($^O =~ /win32/i)
{			    # NT
    $par_pthsep = "\\";
    $par_inidir = $1 if ($0 =~ /(.+\\).+$/);
    $par_inicmd = $1 if ($0 =~ /(.+)\..+$/);
    $par_inicmd = $1 if ($0 =~ /.+\\([^\.]+).+$/);
}
else
{			    # Unix
    $par_pthsep = "\/";
    $par_inidir = $1 if ($0 =~ /(.+\/).+$/);
    $par_inicmd = $1 if ($0 =~ /.+\/([^\.]+).+$/);
}
#
$par_inifil = "${par_inidir}${par_inicmd}.dat";
if (!-e "$par_inifil" && -e "${par_inidir}RfBootstrap.dat")
{
    $par_inifil = "${par_inidir}RfBootstrap.dat";
}
#
$par_trmsfx_h = "";
#
$par_trmsfx_v = "W";
#
################################################################################
#
# Initialize internal variables
#
$arg_rmthst = "default";
$arg_rmthst = "trm_".$ENV{MOCA_ENVNAME} if ($ENV{MOCA_ENVNAME});
#
$arg_inqflg = "0";
#
$arg_vbsflg = "0";
#
$arg_dbgflg = "0";
#
$arg_drvpfx = "";
#
$arg_drvsfx = "";
#
###############################################################################
#
# Define print help routine
#
sub print_help()
{
    printf ("\n");
    printf ("Rf terminal driver interface bootstrap script.  Will query the rf terminal for\n");
    printf ("its terminal number & connect the rf terminal to the appropriate environment.\n");
    printf ("\n");
    printf ("Usage:  RfBootstrap [-r <remote-host>] [-h] [-i] [-v] [-d]\n");
    printf ("\n");
    printf ("    -r) Remote host - used to determine rf terminal vendor & device code prefix\n");
    printf ("        (default is '$arg_rmthst')\n");
    printf ("\n");
    printf ("    -h) Help mode - display this help screen\n");
    printf ("\n");
    printf ("    -i) Inquiry test mode - display query to terminal and its response\n");
    printf ("\n");
    printf ("    -v) Verbose mode - display running status messages\n");
    printf ("\n");
    printf ("    -d) Debug mode - display debug messages\n");
    printf ("\n");
    printf ("    -p) Terminal driver file prefix - used to specific custom terminal driver files\n");
    printf ("        (example: '-p tst' will use 'tst_handheld.dat' and 'tst_vehicle.dat')\n");
    printf ("        (default is '$arg_drvpfx')\n");
    printf ("\n");
    printf ("    -s) Terminal driver file suffix - used to specific custom terminal driver files\n");
    printf ("        (example: '-p tst' will use 'handheld_tst.dat' and 'vehicle_tst.dat')\n");
    printf ("        (default is '$arg_drvsfx')\n");
    printf ("\n");
}
#
################################################################################
#
# Input argument checking/processing
#
# Get command line arguments
#
$status = &Getopts ('r:hivdp:s:');
#
# If help mode enabled, then print help screen & exit
#
if ($opt_h || !$status)
{
    &print_help ();
    exit (0);
}
#
# Load arguments into local variables
#
$arg_rmthst = $opt_r     if ($opt_r);
$arg_inqflg = "1"        if ($opt_i || $opt_v || $opt_d);
$arg_vbsflg = "1"        if ($opt_v || $opt_d);
$arg_dbgflg = "1"        if ($opt_d);
$arg_drvpfx = $opt_p."_" if ($opt_p);
$arg_drvsfx = "_".$opt_s if ($opt_s);
#
if ($arg_vbsflg)
{
    printf ("\nArgument List\n");
    printf ("RmtHst ($arg_rmthst)\n");
    printf ("InqFlg ($arg_inqflg)\n");
    printf ("VbsFlg ($arg_vbsflg)\n");
    printf ("DbgFlg ($arg_dbgflg)\n");
    printf ("DrvPfx ($arg_drvpfx)\n");
    printf ("DrvSfx ($arg_drvsfx)\n");
}
printf ("\n");
#
# Ensure all required arguments are defined
#
if (!$arg_rmthst)
{
    printf ("Remote Host not defined\n");
    &print_help;
    exit (2);
}
#
################################################################################
#
# Read rf bootstrap initialization file
#
printf ("Initializing...\n");
#
if (! -e $par_inifil)
{
    die "\nFATAL ERROR - Rf bootstrap initialization file ($par_inifil) not present.\nStopped";
}
#
printf ("Reading Rf bootstrap initialization file ($par_inifil)\n") if ($arg_vbsflg);
#
open (INIFIL, $par_inifil) || die "\nFATAL ERROR - Could not open rf bootstrap initialization file ($par_inifil).\nStopped";
#
while (<INIFIL>)
{
    $inilin = $_;
    chomp $inilin;
    #
    # Skip comments & blank lines
    #
    if (($inilin =~ /^#/) || ($inilin =~ /^\s*$/))
    {
	next;
    }
    #
    printf ("Processing line ($inilin)\n") if ($arg_dbgflg);
    #
    # If current line is section header, then extract section header name
    #
    if ($inilin =~ /^\s*\[.*\]/)
    {
	$inisec = $inilin;
	$inisec =~ s/^\s*\[\s*(\S(.*\S)?)\s*\].*$/$1/;
	$inisec =~ tr/A-Z/a-z/;
	#
	printf ("Reading ($inisec) section\n") if ($arg_vbsflg);
	#
	next;
    }
    #
    # If current section is 'default environment', then load default environment information
    #
    if ($inisec eq "default environment")
    {
	($envidn, $envinf) = split '\|', $inilin, 2;
	#
	$envidn =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
	$envinf =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
	#
	printf ("Reading default environment ($envidn) information\n") if ($arg_vbsflg);
	#
	$envary {$envidn} = $envinf;
	#
	$defenv = $envidn;  # Set default environment name
	#
	next;
    }
    #
    # If current section is 'environments', then load environment information
    #
    if ($inisec eq "environments")
    {
	($envidn, $envinf) = split '\|', $inilin, 2;
	#
	$envidn =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
	$envinf =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
	#
	printf ("Reading environment ($envidn) information\n") if ($arg_vbsflg);
	#
	$envary {$envidn} = $envinf;
	#
	next;
    }
    #
    # If current section is 'default paths', then load default path information
    #
    if ($inisec eq "default paths")
    {
	($pthidn, $pthinf) = split '\|', $inilin, 2;
	#
	$pthidn =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
	$pthinf =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
	#
	printf ("Reading default path ($pthidn) information\n") if ($arg_vbsflg);
	#
	$pthary {$pthidn} = $pthinf;
	#
	next;
    }
    #
    # If current section is 'remote hosts', then load remote host information
    #
    if ($inisec eq "remote hosts")
    {
	($rmtidn, $rmtinf) = split '\|', $inilin, 2;
	#
	$rmtidn =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
	$rmtinf =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
	#
	printf ("Reading remote host ($rmtidn) information\n") if ($arg_vbsflg);
	#
	$rmtary {$rmtidn} = $rmtinf;
	#
	next;
    }
    #
    # If current section is 'terminals', then load terminal information
    #
    if ($inisec eq "terminals")
    {
	($rmtidn, $trmidn, $trminf) = split '\|', $inilin, 3;
	#
	$rmtidn =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
	$trmidn =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
	$trminf =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
	#
	printf ("Reading terminal ($rmtidn|$trmidn) information\n") if ($arg_vbsflg);
	#
	$trmary {$rmtidn.'\|'.$trmidn} = $trminf;
	#
	next;
    }
    #
    # If current section is 'vendors', then load vendor information
    #
    if ($inisec eq "vendors")
    {
	($venidn, $veninf) = split '\|', $inilin, 2;
	#
	$venidn =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
	$veninf =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
	#
	printf ("Reading vendor ($venidn) information\n") if ($arg_vbsflg);
	#
	$venary {$venidn} = $veninf;
	#
	next;
    }
    #
    # If we get there, then current section is unrecognized
    #
    die "FATAL ERROR - Unrecognized section ($inisec) encountered.\nStopped";
}
#
close (INIFIL);
#
################################################################################
#
# Retrieve remote host information for current remote host
#
$rmtinf = $rmtary {$arg_rmthst};
if ((!$rmtinf) && (!$opt_r) && ($rmtary {"default"}))
{
    printf ("Resetting default remote host to (default)\n") if ($arg_dbgflg);
    $arg_rmthst = "default";
    $rmtinf = $rmtary {$arg_rmthst};
}
if (!$rmtinf)
{
    printf ("\nFATAL ERROR - Remote host ($arg_rmthst) information not found.\n");
    $tmplst = "    ".join ("\n    ", sort (keys (%rmtary)));
    die "Defined remote hosts are:\n$tmplst\nStopped";
}
#
# Parse remote host information into individual components
#
($vennam, $devcod, $termid) = split '\|', $rmtinf;
#
# Trim unwanted spaces
#
$vennam =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
$devcod =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
$termid =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
#
if ($arg_dbgflg)
{
    printf ("\nRemote host ($arg_rmthst) information\n");
    printf ("VenNam ($vennam)\n");
    printf ("DevCod ($devcod)\n");
    printf ("TermId ($termid)\n");
}
#
################################################################################
#
# If needed, prompt rf terminal for terminal id & read response
#
if (!$termid)
{
    #
    printf ("\nPrompting for rf terminal id\n") if ($arg_dbgflg);
    #
    # Retrieve rf terminal vendor information
    #
    $veninf = $venary {$vennam};
    if (!$veninf)
    {
	printf ("\nFATAL ERROR - Vendor ($vennam) information not found.\n");
	$tmplst = "    ".join ("\n    ", sort (keys (%venary)));
	die "Defined vendors are:\n$tmplst\nStopped"
    }
    #
    # Parse vendor information into individual components
    #
    ($veninq, $venrsp) = split '\|', $veninf;
    #
    $veninq =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
    $venrsp =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
    #
    if ($arg_inqflg)
    {
	printf ("\nVendor ($vennam) information\n");
	printf ("VenInq \"$veninq\"\n");
	printf ("VenRsp \"$venrsp\"\n");
	printf ("\n");
	printf ("Rf terminal query\n");
	printf ("Send ($veninq)\n");
    }
    #
    # Prompt rf terminal for terminal id
    #
    eval "printf (\"$veninq\")";
    #
    # Read & parse rf terminal response
    #
    $trmrsp = <STDIN>;
    #
    eval "\$termid = \$1 if (\$trmrsp =~ /$venrsp/)";
    #
    if ($arg_inqflg)
    {
	chomp $trmrsp;
	printf ("Recv ($trmrsp)\n");
	printf ("Rf terminal ($termid) identified\n");
	#
	if ($opt_i)
	{
	    open (INQFIL, ">> RfBootstrap.dbg");
	    printf (INQFIL "\nVendor ($vennam) information\n");
	    printf (INQFIL "VenInq \"$veninq\"\n");
	    printf (INQFIL "VenRsp \"$venrsp\"\n");
	    printf (INQFIL "\n");
	    printf (INQFIL "Rf terminal query\n");
	    printf (INQFIL "Send ($veninq)\n");
	    printf (INQFIL "Recv ($trmrsp)\n");
	    printf (INQFIL "Rf terminal ($termid) identified\n");
	    close (INQFIL);
	}
    }
}
#
################################################################################
#
# Retrieve rf terminal information for current rf terminal
#
$trminf = $trmary {$arg_rmthst.'\|'.$termid};
if (!$trminf)
{
    if (!$termid)
    {
	printf ("\nFATAL ERROR - Rf terminal ($vennam) information not found.\n");
	$tmplst = "    ".join ("\n    ", sort (keys (%trmary)));
	$tmplst =~ s/\\\|/\|/g;
	die "Defined rf terminals are:\n$tmplst\nStopped"
    }
    #	
    printf ("Rf terminal ($termid) information not found...\nUsing defaults\n");
    #
    # If needed, uncomment the following lines to list defined rf terminals
    #
    #$tmplst = "    ".join ("\n    ", sort (keys (%trmary)));
    #$tmplst =~ s/\\\|/\|/g;
    #printf ("Defined rf terminals are:\n$tmplst\n");
    #
    $trminf = "$defenv | $termid | v";
}
#
# Parse rf terminal information into individual components
#
($trmenv, $trmnum, $trmtyp) = split '\|', $trminf, 3;
#
# If only two components are present, then assume missing component is terminal
# environment.  So use default environment and adjust variables accordingly.
#
if ((!$trmtyp) && (!$envinf {$trmenv}))
{
    $trmtyp = $trmnum;
    $trmnum = $trmenv;
    $trmenv = $defenv;
}
#
# Trim unwanted spaces
#
$trmenv =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
$trmnum =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
$trmtyp =~ s/^\s*(\S.*\S|\S)\s*$/$1/;
#
if ($arg_dbgflg)
{
    printf ("\nRf terminal ($arg_rmthst|$termid) information\n");
    printf ("TrmEnv ($trmenv)\n");
    printf ("TrmNum ($trmnum)\n");
    printf ("TrmTyp ($trmtyp)\n");

}
#
################################################################################
#
# Retrieve environment information for current rf terminal
#
$envinf = $envary {$trmenv};
if (!$envinf)
{
    printf ("\nFATAL ERROR - Environment ($trmenv) information not found.\n");
    $tmplst = "    ".join ("\n    ", sort (keys (%envary)));
    die "Defined environments are:\n$tmplst\nStopped"
}
#
# Parse environment information into individual components
#
($srvadr, $srvprt, $binpth, $logpth) = split '\|', $envinf;
#
# Trim unwanted spaces
#
$srvadr =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
$srvprt =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
$binpth =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
$logpth =~ s/^\s*(\S(.*\S)?)\s*$/$1/;
#
if ($arg_dbgflg)
{
    printf ("\nEnvironment ($trmenv) information\n");
    printf ("SrvAdr ($srvadr)\n");
    printf ("SrvPrt ($srvprt)\n");
    printf ("BinPth ($binpth)\n");
    printf ("LogPth ($logpth)\n");
}
#
# If environment paths are not defined, then use default values
#
if (!$binpth && $pthary {"bin"})
{
    $binpth = $pthary {"bin"};
    printf ("Using default bin path ($binpth)\n") if ($arg_dbgflg);
}
#
if (!$logpth && $pthary {"log"})
{
    $logpth = $pthary {"log"};
    printf ("Using default log path ($logpth)\n") if ($arg_dbgflg);
}
#
# If needed, append path separator to environment paths
#
$tstchr = $1 if ($binpth =~ /^.*(.)$/);
if ($binpth && $tstchr ne $par_pthsep)
{
    $binpth = $binpth.$par_pthsep;
}
#
$tstchr = $1 if ($logpth =~ /^.*(.)$/);
if ($logpth && $tstchr ne $par_pthsep)
{
    $logpth = $logpth.$par_pthsep;
}
#
###############################################################################
#
# Initialize dependant variables according to rf terminal type
#
$trmtyp =~ tr/A-Z/a-z/;
#
if ($trmtyp eq "h" || $trmtyp eq "n")
{
    $trmtyp = "h";
    $trmfil = "handheld";
    $trmsfx = $par_trmsfx_h;
}
elsif ($trmtyp eq "v" || $trmtyp eq "w")
{
    $trmtyp = "v";
    $trmfil = "vehicle";
    $trmsfx = $par_trmsfx_v;
}
#LHCSTART - 01/09/02 (Steve Hanchar) - Add support for monarch rf terminals
elsif ($trmtyp eq "m")
{
    $trmtyp = "m";
    $trmfil = "monarch_handheld";
    $trmsfx = "M";
    #
    # Load custom monarch setup/definition files
    #
    $par_rfm_load = $ENV{LESDIR}."/labels/monarch";
    #
    opendir (DIRLST, "$par_rfm_load") || die "FATAL ERROR - Error getting directory ($par_rfm_load) listing.\nStopped";
    @fillst = grep (/^.*\.rfm$/, sort (readdir (DIRLST)));
    close (DIRLST);
    #
    foreach $filnam (@fillst)
    {
	$dspnam = $filnam;
	$dspnam =~ s/\..*//;
	printf ("Loading %s\n", $dspnam);
	system ("cat $par_rfm_load/$filnam");
	sleep (2);
    }
}
#LHCEND
else
{
    printf ("\nFATAL ERROR - Unrecognized rf terminal ($termid) type ($trmtyp) encountered.\n");
#LHCSTART - 01/09/02 (Steve Hanchar) - Add support for monarch rf terminals
#    die "Defined rf terminal types are: h, n, v, or w\nStopped";
    die "Defined rf terminal types are: m, h, n, v, or w\nStopped";
#LHCEND
}
#
$trmcod = "$devcod$trmnum$trmsfx";
$trmdir = lc ($trmcod);
#
###############################################################################
#
# If individual rf terminal sub-directory exists , then move to it
# (sub-directory is defined by the rf terminal's device code)
#
printf ("\nLooking for ($trmdir) subdirectory\n") if ($arg_dbgflg);
#
if (!-d $trmdir)
{
    printf ("Subdirectory ($trmdir) not found\n") if ($arg_dbgflg);
    $trmdir = "";
}
else
{
    printf ("Moving to ($trmdir) subdirectory\n") if ($arg_vbsflg);
    chdir ($trmdir);
}
#
###############################################################################
#
# Determine working file name and delete any old working files
#
$wrkfil = $logpth."Rf_$trmenv_$devcod$trmnum$trmtyp.cfg";
#
printf ("\nWrkFil ($wrkfil)\n") if ($arg_dbgflg);
#
if (-e $wrkfil)
{
    printf ("\nDeleting old working file ($wrkfil)\n") if ($arg_vbsflg);
    unlink ($wrkfil) || die "\nFATAL ERROR - Could not delete old working file ($wrkfil).\nStopped";
}
#
###############################################################################
#
# Write RF driver configuration file
#
open (WRKFIL, ">$wrkfil") || die "\nFATAL ERROR - Could not create working file ($wrkfil).\nStopped";
#
printf (WRKFIL "TERMID=$trmnum\n");
printf (WRKFIL "DEVCOD=$trmcod\n");
printf (WRKFIL "VOLUME=1\n");
printf (WRKFIL "FILELOC=L\n");
printf (WRKFIL "FILENAME=$arg_drvpfx$trmfil$arg_drvsfx.dat\n");
#
close (WRKFIL);
#
###############################################################################
#
# Update SAI_RF_TERMTYPE environment variable
#
if ($trmtyp eq "h")
{
    $ENV{SAI_RF_TERMTYPE} = "15";
}
#LHCSTART - 01/09/02 (Steve Hanchar) - Add support for monarch rf terminals
elsif ($trmtyp eq "m")
{
    $ENV{SAI_RF_LINES} = "4";
    $ENV{SAI_RF_COLUMNS} = "20";
    $ENV{SAI_RF_CAPTIONS} = "0";
}
#LHCEND
else
{
    $ENV{SAI_RF_TERMTYPE} = "";
}
#
###############################################################################
#
# Invoke Dcs RF Terminal Driver
#
$trmcmd = $binpth."dcsrdt -c $wrkfil -a $srvadr -p $srvprt -n -g";
#
if ($arg_dbgflg)
{
    printf ("\nInvoking rf terminal driver with following command:\n");
    printf ("TrmCmd ($trmcmd)\n\n");
}
#
system ($trmcmd);
#
###############################################################################
#
# Common exit point for command file.  Perform any required clean-up activities
#
unlink ($wrkfil) || die "\nFATAL ERROR - Could not delete working file ($wrkfil).\nStopped";
#
if ($trmdir)
{
    printf ("\nReturning to parent directory\n") if ($arg_dbgflg);
    chdir ("..");
}
#
exit (0);
#
###############################################################################
