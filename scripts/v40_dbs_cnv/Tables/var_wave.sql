#include <../../../include/varddl.h>
#include <../../../include/varcolwid.h>
#include <../../../include/vartbldef.h>
#include <sqlDataTypes.h>
#use $DCSDIR/include
#include <dcscolwid.h>

create table var_wave
(
        vc_wavnum           INTEGER_TY
                         PRIMARY_KEY_FIELD(var_wave_wavnum_pk),
        vc_wavdte            DATE_TY 
                         NOT_NULL_CONSTRAINT(var_wave_wavdte_nl),
        vc_wavsts           STRING_TY(WAVSTS_LEN) 
                         NOT_NULL_CONSTRAINT(var_wave_wavsts_nl),
        vc_wavqty           INTEGER_TY 
                         NOT_NULL_CONSTRAINT(var_wave_wavqty_nl),
        mod_usr_id       STRING_TY(USR_ID_LEN) null,
        moddte           DATE_TY  null
)        
TABLESPACE_INFO (VAR_WAVE_TBL_TBLSPC, VAR_WAVE_TBL_STORAGE)
RUN_DDL

#include <var_wave_pk.idx>
