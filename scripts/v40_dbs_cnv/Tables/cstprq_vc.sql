#use $MOCADIR/include
#include <sqlDataTypes.h>
alter table cstprq add (VC_LOTNUM   VARCHAR2(20))
RUN_SQL
alter table cstprq add ( VC_PRETCK  VARCHAR2(10))
RUN_SQL
alter table cstprq add ( VC_SPCINS  VARCHAR2(40))
RUN_SQL
alter table cstprq add ( VC_CSTPR1  VARCHAR2(20))
RUN_SQL
alter table cstprq add ( VC_RETPRC  NUMBER(8,3))
RUN_SQL
alter table cstprq add ( VC_PRENUM  NUMBER(10))
RUN_SQL
alter table cstprq add ( VC_LBLFD1  VARCHAR2(20))
RUN_SQL
alter table cstprq add ( VC_LBLFD2  VARCHAR2(20))
RUN_SQL
alter table cstprq add ( VC_LBLFD3  VARCHAR2(20))
RUN_SQL
alter table cstprq add ( VC_LBLFD4  VARCHAR2(20))
RUN_SQL
