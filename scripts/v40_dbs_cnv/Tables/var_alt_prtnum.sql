#include <../../../include/varddl.h>
#include <../../../include/varcolwid.h>
#include <../../../include/vartbldef.h>
#include <sqlDataTypes.h>
#use $DCSDIR/include
#include <dcscolwid.h>

create table var_alt_prtnum
(
        altnum	          STRING_TY(ALTPRT_LEN)
                            PRIMARY_KEY_FIELD(var_alt_prtnum_altnum_pk),
        prtnum		    STRING_TY(PRTNUM_LEN) 
                            NOT_NULL_CONSTRAINT(var_alt_prtnum_prtnum_nl)                             
)        
TABLESPACE_INFO (VAR_ALT_PRTNUM_TBL_TBLSPC, VAR_ALT_PRTNUM_TBL_STORAGE)
RUN_DDL

#include <var_alt_prtnum_pk.idx>
