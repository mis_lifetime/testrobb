/* the below script is used to update the usr_classdtl table with */
/* new product codes. Table is referenced in the Usr-Forecasting Report */

insert into usr_classdtl(vc_prdcod)
  select distinct b.vc_prdcod from prtmst b
  where not exists(select 1 from usr_classdtl c
  where c.vc_prdcod = b.vc_prdcod)
  and b.vc_prdcod is not null;
